<?php
// error_reporting(0);
if(!class_exists('simple_html_dom_node')){
	include_once 'includes/simple_html_dom.php';
}

// require('/var/www/html/wpthemes/wp_media_cleaner/wp-admin/includes/file.php');
class WPMC_API {

	function __construct( $core, $admin, $engine ) {
		$this->core = $core;
		$this->engine = $engine;
		$this->admin = $admin;
		add_action( 'wp_ajax_wpmc_prepare_do', array( $this, 'wp_ajax_wpmc_prepare_do' ) );
		add_action( 'wp_ajax_wpmc_get_cpt', array( $this, 'wpmc_get_cpt' ) );
		add_action( 'wp_ajax_wpmc_get_categories', array( $this, 'wpmc_get_categories' ) );
		add_action( 'wp_ajax_wpmc_get_image_extensions', array( $this, 'wpmc_get_image_extensions' ) );
		add_action( 'wp_ajax_support_form', array( $this, 'support_form' ) );
		add_action( 'wp_ajax_wpmc_media_default_action', array( $this, 'wpmc_media_default_action' ) );
		add_action( 'wp_ajax_wpmc_lisence_activation', array( $this, 'wpmc_lisence_activation' ) );
		add_action( 'wp_ajax_wpmc_lisence_deactivation', array( $this, 'wpmc_lisence_deactivation' ) );
	}

	/*******************************************************************************
	 * ASYNCHRONOUS AJAX FUNCTIONS
	 ******************************************************************************/
	
	// Get Multisite prefixes
	public function getAllSitePrefix(){
		global $wpdb;
		$prefixes = array();
		$is_multisite = get_sites();
		if($is_multisite){
			$abcd = count($is_multisite);
			$count = 0;
			for ($i=2; $i <= $abcd; $i++) { 
				$prefixes[$count]['prefix'] = $wpdb->get_blog_prefix($i);
				$prefixes[$count]['multisite_id'] = $i;
				$count++;
			}
		}else{
			$prefixes = 
			array(
				0 => array("prefix" => "wp_")
			);
		}
		return $prefixes;
	}
	// ALL DIRECTORY IMAGES
	public function walkDir($dir) {
		$dir = rtrim($dir,"/");
	    global $return;
		$dh = new DirectoryIterator($dir);   
		// Dirctary object
		foreach ($dh as $item) {
			if (!$item->isDot()) {
				if ($item->isDir()) {
					if($item !="wp_media_cleaner"){
						$this->walkDir("$dir/$item");
					}
				} else {
					if($item->isFile() && preg_match("/(\.gif|\.png|\.jpe?g)$/", $item->getFilename())){
						$fullpath = $dir . "/" . $item->getFilename();
						$type= $item->getExtension();
						$return[] = array('src'=>$fullpath);
					}
				}
			}
		}
		return $return;
	}
	// ALL WEBSITE IMAGES
	public function getinboundLinks($domain_name) {
	    $res = array();
	    $arr = array();
	    $url = $domain_name;
	    $url_without_www=str_replace('http://','',$url);
	    $url_without_www=str_replace('www.','',$url_without_www);
	    $url_without_www= str_replace(strstr($url_without_www,'/'),'',$url_without_www);
	    $url_without_www=trim($url_without_www);
	    $input = @file_get_contents($url);
	    if($input){
		    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
		    if(preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) {
		        // Site URL
		        $wp_site_urls = get_site_url();
		        foreach($matches as $match) {
		            if(strpos($match[2],site_url()) !== false){
		                if (filter_var($match[2], FILTER_VALIDATE_URL) && $match[2] !='mailto:' && strpos($match[2],$wp_site_urls) !== false) {
		                    $res[] = $match[2];
		                }
		            }
		        }
		    }
		    $data = array();
		    $orphan = array();
		    $i = 0;
		    foreach ($res as $value) {
		        @$html = file_get_html($value);
		        if($html){
					// For Images
			        foreach($html->find('img') as $element) {
			            if(strpos($element->src,site_url()) !== false){
			                $data[$i]['src'] = $element->src;

			                // Root directory path of WordPress website
			                $wp_root_pathh = get_home_path();
			                // Site URL
			                $wp_site_urll = get_site_url();

			                $dir_path =  str_replace($wp_site_urll.'/',$wp_root_pathh,$data[$i]['src']);
			                if(file_exists($dir_path)){
								$unixtime = filemtime($dir_path);
				                @$str = file_get_contents($value);
				                if(strlen($str)>0){
				                    $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
				                    preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
				                    $i++;
				                }
							}
			            }
			        }
			    }
		    }
		}
	    return $data;
	}
	// Get Media URL by id
	public function getImageUrlbyId($imgId){
		global $wpdb;
		$imgIdget = wp_get_attachment_url($imgId);
		if(!empty($imgIdget)){
			return $imgIdget;
		}else{
			return false;
		}
	}
	// Get Multisite Media URL by id
	public function getMultiSiteImageUrlbyId($imgId,$prefixValue){
		global $wpdb;
		$imgIdget = $wpdb->get_results("SELECT guid from ".$prefixValue."posts WHERE ID = $imgId" );
		$imgIdget = $imgIdget[0]->guid;
		if(!empty($imgIdget)){
			return $imgIdget;
		}else{
			return false;
		}
	}
	// Images from Woocommerce products
	public function getproductdetails($pid){
		global $wpdb;
		$pde = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE ID = '".$pid."'");
		return $pde[0];
	}
	public function getMultisiteProductDetails($pid,$sitePrefix){
		global $wpdb;
		$pde = $wpdb->get_results("SELECT * FROM ".$sitePrefix."posts WHERE ID = '".$pid."'");
		return $pde[0];
	}
	public function getattachmentproducts(){
		$mediaexistdata = array();
		global $wpdb;
		$attachmentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where post_type='attachment'");
		if(!empty($attachmentdata)){
			$attachmentarray = array();		
			foreach ($attachmentdata as $key => $attachvalue) {
				$media_id = $attachvalue->ID;
				$media_url = $attachvalue->guid;
				$mediauploaddate = $attachvalue->post_date;
				$media_name = substr($media_url, strrpos($media_url, '/') + 1);
				$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
							
				$attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'upload_date' =>$mediauploaddate,'media_url'=>$media_url,'source_from'=>'database');
			}
		}
		if(!empty($attachmentarray)){
			foreach ($attachmentarray as $value) {
				$media_id = $value['media_id']; 
				$medianame = $value['media_name']; 
				$mediadate = $value['upload_date'];
				$media_url = $value['media_url'];
				$media_type = substr($media_url, strrpos($media_url, '.') + 1);
				$source_from = $value['source_from'];

				$productids = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_thumbnail_id' AND  meta_value ='".$media_id."'");
				$postId = $productids[0]->post_id;
				$getAllMeta = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE post_id = '$postId' AND meta_key = '_variation_description'");
				if($getAllMeta){
					// get variation product details
					$variant_attribute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE post_id = '$postId' AND meta_key LIKE '%attribute_%'");
					$variant_attributes = '';
					foreach ($variant_attribute as $variant_value) {
						$variant_attributes .= $variant_value->meta_value.' | ';
					}
					$attribute = rtrim($variant_attributes,'| ');
					// $attribute_color = get_post_meta($postId, 'attribute_color', true);
					$sku_variant = get_post_meta($postId, '_sku', true);
					if(!empty($productids)){
						$productidsarray = array_column($productids, 'post_id');
						foreach ($productidsarray as $proid) {
							$productdetails = $this->getproductdetails($proid);
							$post_categories = wp_get_post_terms($proid,'product_cat');
							if($post_categories){
								$post_cat = $post_categories[0]->name;
							}
							if(!empty($productdetails)){
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $medianame,
									'src' => $media_url,
									'media_type' => $media_type,
									'title'=> $productdetails->post_title,
									'post_type' => $productdetails->post_type,
									'page_builder_name' => 'Variation Product',
									'post_category' => $post_cat,
									'variant_attribute' => $attribute,
									'variant_sku' => $sku_variant,
									'datetime' => $mediadate,
									'linked' => 'Yes',
									'source_from' => 'database',
									'website_prefix' => $wpdb->prefix
								);
							}
						}
					}
				}
				$pgde = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_product_image_gallery'  AND FIND_IN_SET(".$media_id.", meta_value)");
				if(!empty($pgde)){
					$productgids = array_column($pgde, 'post_id');
					foreach ($productgids as $valueg) {
						$productdetailsg = $this->getproductdetails($valueg);
						$post_categories = wp_get_post_terms($valueg,'product_cat');
						if($post_categories){
							$post_cat = $post_categories[0]->name;
						}
						if(!empty($productdetailsg)){
							$mediaexistdata[] = array(
								'media_id' => $media_id,
								'medianame' => $medianame,
								'src' => $media_url,
								'media_type' => $media_type,
								'title'=> $productdetailsg->post_title,
								'post_type' => $productdetailsg->post_type,
								'page_builder_name' => 'Gallery Image',
								'post_category' => $post_cat,
								'variant_attribute' => '',
								'variant_sku' => '',
								'datetime' => $mediadate,
								'linked' =>'Yes',
								'source_from' => 'database',
									'website_prefix' => $wpdb->prefix
							);
						}
					}
				}
			}
		}
		return $mediaexistdata;
	}
	// Get multisite product's Media
	public function getMultisiteAttachmentProducts(){
		$mediaexistdata = array();
		global $wpdb;
		$prefixes = $this->getAllSitePrefix();
		foreach ($prefixes as $mutliarray) {
			$prefixValue = $mutliarray['prefix'];
			$multisiteId = $mutliarray['multisite_id'];
			// Get multisite title
			$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
			$site_name = $current_blog_details->blogname;
			$attachmentdata = $wpdb->get_results("SELECT * from ".$prefixValue."posts where post_type='attachment'");
			if(!empty($attachmentdata)){
				$attachmentarray = array();		
				foreach ($attachmentdata as $key => $attachvalue) {
					$media_id = $attachvalue->ID;
					$media_url = $attachvalue->guid;
					$mediauploaddate = $attachvalue->post_date;
					$media_name = substr($media_url, strrpos($media_url, '/') + 1);
					$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
								
					$attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'upload_date' =>$mediauploaddate,'media_url'=>$media_url,'source_from'=>'database');
				}
			}
			if(!empty($attachmentarray)){
				foreach ($attachmentarray as $value) {
					$media_id = $value['media_id']; 
					$medianame = $value['media_name']; 
					$mediadate = $value['upload_date'];
					$media_url = $value['media_url'];
					$media_type = substr($media_url, strrpos($media_url, '.') + 1);
					$source_from = $value['source_from'];

					$productids = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key = '_thumbnail_id' AND  meta_value ='".$media_id."'");
					$postId = $productids[0]->post_id;
					$getAllMeta = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE post_id = '$postId' AND meta_key = '_variation_description'");				
					if($getAllMeta){
						// get variation product details
						$variant_attribute = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE post_id = '$postId' AND meta_key LIKE '%attribute_%'");
						$variant_attributes = '';
						foreach ($variant_attribute as $variant_value) {
							$variant_attributes .= $variant_value->meta_value.' | ';
						}
						$attribute = rtrim($variant_attributes,'| ');
						// $attribute_color = get_post_meta($postId, 'attribute_color', true);
						// $sku_variant = get_post_meta($postId, '_sku', true);
						$getting_sku_inmeta = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_sku' AND post_id = $postId" );
						$sku_variant = $getting_sku_inmeta[0]->meta_value;

						if(!empty($productids)){
							$productidsarray = array_column($productids, 'post_id');
							foreach ($productidsarray as $proid) {
								$productdetails = $this->getMultisiteProductDetails($proid,$prefixValue);
								// $post_categories = wp_get_post_terms($proid,'product_cat');
								$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$proid."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
								$post_categories = $wpdb->get_results($queryterms, OBJECT);
								$post_cat = array();
								$post_cats = "";
								if(!empty($post_categories)){
									foreach ($post_categories as $cat_value) {
										$post_cat[] = $cat_value->name;
									}
									$post_cats = implode(",", $post_cat);
								}
								if(!empty($productdetails)){
									$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
									$mediaexistdata[] = array(
										'media_id' => $media_id,
										'medianame' => $medianame,
										'src' => $media_url,
										'media_type' => $media_type,
										'title'=> $productdetails->post_title,
										'post_type' => $productdetails->post_type,
										'page_builder_name' => 'Variation Product',
										'post_category' => $post_cats,
										'variant_attribute' => $attribute,
										'variant_sku' => $sku_variant,
										'datetime' => $mediadate,
										'linked' =>$linkedd,
										'source_from' => 'database',
										'website_prefix' => $prefixValue
									);
								}
							}
						}
					}
					$pgde = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key = '_product_image_gallery'  AND FIND_IN_SET(".$media_id.", meta_value)");
					if(!empty($pgde)){
						$productgids = array_column($pgde, 'post_id');
						foreach ($productgids as $valueg) {
							$productdetailsg = $this->getMultisiteProductDetails($valueg,$prefixValue);
							$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$valueg."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
							$post_categories = $wpdb->get_results($queryterms, OBJECT);
							$post_cat = array();
							$post_cats = "";
							if(!empty($post_categories)){
								foreach ($post_categories as $cat_value) {
									$post_cat[] = $cat_value->name;
								}
								$post_cats = implode(",", $post_cat);
							}
							if(!empty($productdetailsg)){
								$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $medianame,
									'src' => $media_url,
									'media_type' => $media_type,
									'title'=> $productdetailsg['post_title'],
									'post_type' => $productdetailsg['post_type'],
									'page_builder_name' => 'Gallery Image',
									'post_category' => $post_cats,
									'variant_attribute' => '',
									'variant_sku' => '',
									'datetime' => $mediadate,
									'linked' =>$linkedd,
									'source_from' => 'database',
									'website_prefix' => $prefixValue
								);
							}
						}
					}
				}
			}
		}
		return $mediaexistdata;
	}
	// Get media from page builder Coded start by Hemant
	public function getPageBuilderContentMedia(){
		global $wpdb;
		$pbContent = $wpdb->get_results("SELECT * from ".$wpdb->prefix."posts WHERE post_type !='revision' && (post_status = 'publish' OR post_status = 'draft')");
		$uniqueArr = array();
		$inc = 0;
		$uniqueArrs = array();
		$uniqueArrs2 = array();
		$uniqueArrs3 = array();
		$uniqueArred = array();
		// Total types of extensions
		$totalExtensionsAvailable = array('gif','jpg','jpeg','png','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
		foreach ($pbContent as $getvalues) {
			$mediadate = $getvalues->post_date;
			$getFiles = array();
			if(!empty($getvalues)){
				$elem_builder = get_post_meta($getvalues->ID, '_elementor_edit_mode', true);
				$vc_builder = get_post_meta($getvalues->ID, '_wpb_vc_js_status', true);
				$beaver_builder = get_post_meta($getvalues->ID, '_fl_builder_enabled', true);			
				$brizy_builder = !empty(get_post_meta($getvalues->ID, 'brizy', true));
				$oxygen_builder = !empty(get_post_meta($getvalues->ID, 'ct_builder_shortcodes', true));
				$siteorigin_builder = get_post_meta($getvalues->ID, 'panels_data', true);
				$siteorigin_ck_builder = '';

				if(!empty($siteorigin_builder)){
					$siteorigin_ck_builder = "SiteOrigin";
				}
				$featuredImageArr = get_post_meta($getvalues->ID, '_thumbnail_id');
				@$featuredImagesId = $featuredImageArr[0];
				if($featuredImagesId){
					$featuredImageId = $this->getImageUrlbyId($featuredImagesId);
				}else{
					$featuredImageId = '';
				}
				$productgalleryArr = get_post_meta($getvalues->ID, '_product_image_gallery');
				@$productgalleryImg = $productgalleryArr[0];

				$the_title = $getvalues->post_title;
				$post_type = $getvalues->post_type;
				$the_content = $getvalues->post_content;

				if($elem_builder == 'builder'){
					$PageId = $getvalues->ID;
					$elementor_sql = get_post_meta($PageId, '_elementor_data');
					$elementor_content = $elementor_sql[0];
					
					// Get files url from URL & ID tag start
					$group4 = array();
					$group5 = array();
					$getsrcfiles = array();
					preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
					preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
					// Get files url from URL & ID tag end

					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}

					$imgArry = array();
					foreach ($group4[1] as $ElementorimageUrl) {
						$ElementorimageUrl = stripslashes($ElementorimageUrl);
						$if_file_exist =  str_replace(get_site_url().'/',get_home_path(),$ElementorimageUrl);
						if(file_exists($if_file_exist)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $ElementorimageUrl;
							$new_array['medianame'] = basename($ElementorimageUrl);
							$new_array['page_builder_name'] = "Elementor";
							// Getting filetime from dir URL
							$elementor_unixtime = filemtime($if_file_exist);
							$new_array['datetime'] = date("Y-m-d h:i:s",$elementor_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
					// Get files url from href tag start
					$gethreffiles = array();
					preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
					$hreffiles = array();
					if(!empty($gethreffiles[1])){
						foreach ($gethreffiles[1] as $hrefValues) {
							$hrefValues = stripslashes($hrefValues);
							$elementor2_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
							if(file_exists($elementor2_url_dir_path)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $hrefValues;
								$new_array['medianame'] = basename($hrefValues);
								$new_array['page_builder_name'] = "Elementor";
								// Getting filetime from dir URL
								$elementor2_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
								$elementor2_unixtime = filemtime($elementor2_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$elementor2_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// Get files url from href tag end

					// Get files src from href tag start
					$srcfiles = array();
					preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
					if(!empty($getsrcfiles[1])){
						foreach ($getsrcfiles[1] as $srcValues) {
							$srcValues = stripslashes($srcValues);
							$elementor3_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$srcValues);
							if(file_exists($elementor3_url_dir_path)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $srcValues;
								$new_array['medianame'] = basename($srcValues);
								$new_array['page_builder_name'] = "Elementor";
								// Getting filetime from dir URL
								$elementor3_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$srcValues);
								$elementor3_unixtime = filemtime($elementor3_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$elementor3_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// Get files src from href tag start

					$contentimgid = implode(',', $imgArry);
					if($group5[1][0] != ""){
						$group5arr = explode(',', $group5[1][0]);
						foreach ($group5arr as $imggelid) {
							$getidurlimge  = $this->getImageUrlbyId($imggelid);
							$getidurlimge = stripslashes($getidurlimge);
							$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$getidurlimge);
							if(file_exists($elementor4_url_dir_path)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $getidurlimge;
								$new_array['medianame'] = basename($getidurlimge);
								$new_array['page_builder_name'] = "Elementor";
								// Getting filetime from dir URL
								$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$getidurlimge);
								$elementor4_unixtime = filemtime($elementor4_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								array_push($uniqueArr,$new_array);
							}
						}
					}

					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image | Elementor";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				if($vc_builder == 'true'){
					$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;
					preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
					preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
					preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
					preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
					/* Raw HTML */
					preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
					$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
					if(!empty($RowHtmls)){
						foreach ($RowHtmls as $RowHtml) {
							$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
							$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
							
							preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
							$getFiles[] = str_replace(['?_=1'], [''], $group16[1]);
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
					preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFiles[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}

					/* Raw HTML */
					$PageId = $getvalues->ID;
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
					$groupsfile = $groupsfile[1];
					if(!empty($groupsfile)){
						foreach ($groupsfile as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@background-image: url([^"]+)@', $the_content, $group10);
					$bgimgUrlArr = str_replace(['(',')'], ['',''], $group10[1]);
					$bgimgUrl = array();
					foreach ($bgimgUrlArr as $BGvalue) {
						$bgimgUrl[] = substr($BGvalue, 0, strrpos($BGvalue, '?'));
					}
					
					// Merging Arrays Start
					$VCmediaContentPre = array();
					if(!empty($group9[1]) && !empty($bgimgUrl)){
						$VCmediaContentPre = array_merge($group9[1],$bgimgUrl);
					}elseif(!empty($group9[1]) && empty($bgimgUrl)){
						$VCmediaContentPre = $group9[1];
					}elseif(empty($group9[1]) && !empty($bgimgUrl)){
						$VCmediaContentPre = $bgimgUrl;
					}

					$VCmediaContent = array();
					if(!empty($VCmediaContentPre) && !empty($getFiles)){
						$VCmediaContent = array_merge($VCmediaContentPre,$getFiles);
					}elseif(!empty($VCmediaContentPre) && empty($getFiles)){
						$VCmediaContent = $VCmediaContentPre;
					}elseif(empty($VCmediaContentPre) && !empty($getFiles)){
						$VCmediaContent = $getFiles;
					}
					// Merging Arrays Ends
					
					$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
					$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
					$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
					$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}

					if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
						$VCimageids = '';
						$comma = '';
						if($VCsingleimageids != ""){
							$VCsingleimageidsArr = array();
							$VCsingleimageidsArr = explode(",", $VCsingleimageids);
							foreach ($VCsingleimageidsArr as $VCsingleimageidsArrValue) {
								$VCimages = $this->getImageUrlbyId($VCsingleimageidsArrValue);
								$vc_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimages);
								if(file_exists($vc_url_dir_path)){
									$new_array['media_id'] = $idvalue;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimages;
									$new_array['medianame'] = basename($VCimages);
									$new_array['page_builder_name'] = "Visual Composer";
									// Getting filetime from dir URL
									$vc_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimages);
									$vc_unixtime = filemtime($vc_url_dir_path);
									$new_array['datetime'] = date("Y-m-d h:i:s",$vc_unixtime);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['website_prefix'] = $wpdb->prefix;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						if(!empty($VCmediaContent)){
							$incs = 0;
							foreach ($VCmediaContent as $VCmediaContentUrl) {
								// Getting filetime from dir URL
								$vc2_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCmediaContentUrl);
								if(file_exists($vc2_url_dir_path)){
									$new_array['media_id'] = '';
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCmediaContentUrl;
									$new_array['medianame'] = basename($VCmediaContentUrl);
									$new_array['page_builder_name'] = "Visual Composer";
									$vc2_unixtime = filemtime($vc2_url_dir_path);
									$new_array['datetime'] = date("Y-m-d h:i:s",$vc2_unixtime);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['website_prefix'] = $wpdb->prefix;
									$incs++;
									array_push($uniqueArr,$new_array);
								}
							}
						}

						if($VCgalleryimageids != ""){
							if($VCimageids != ''){ $comma = ','; }						
							$theGalIds = explode(",", $VCgalleryimageids);
							$inc = 0;
							foreach ($theGalIds as $idvalue) {							
								$VCimage = $this->getImageUrlbyId($idvalue);
								// Getting filetime from dir URL
								$vc3_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage);
								if(file_exists($vc3_url_dir_path)){
									$new_array['media_id'] = $idvalue;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage;
									$new_array['medianame'] = basename($VCimage);
									$new_array['page_builder_name'] = "Visual Composer";
									$vc3_unixtime = filemtime($vc3_url_dir_path);
									$new_array['datetime'] = date("Y-m-d h:i:s",$vc3_unixtime);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['website_prefix'] = $wpdb->prefix;
									$inc++;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						
						if($VCcarouselimageids != ""){
							if($VCimageids != ''){ $comma = ','; }
							$theGalIds2 = explode(",", $VCcarouselimageids);
							$inc2 = 0;
							foreach ($theGalIds2 as $idvalue2) {
								$VCimage2 = $this->getImageUrlbyId($idvalue2);
								// Getting filetime from dir URL
								$vc4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage2);
								if(file_exists($vc4_url_dir_path)){
									$new_array['media_id'] = $idvalue2;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage2;
									$new_array['medianame'] = basename($VCimage2);
									$new_array['page_builder_name'] = "Visual Composer";
									$vc4_unixtime = filemtime($vc4_url_dir_path);
									$new_array['datetime'] = date("Y-m-d h:i:s",$vc4_unixtime);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['website_prefix'] = $wpdb->prefix;
									$inc2++;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						
						if($VChoverboximageids != ""){
							if($VCimageids != ''){ $comma = ','; }
							$VChoverboximagesIdArr2 = array();
							$VChoverboximagesIdArr2 = explode(",", $VChoverboximageids);
							foreach ($VChoverboximagesIdArr2 as $VChoverboximagesIdArrValue2) {
								$VCimage3 = $this->getImageUrlbyId($VChoverboximagesIdArrValue2);
								// Getting filetime from dir URL
								$vc5_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage3);
								if(file_exists($vc5_url_dir_path)){
									$new_array['media_id'] = $VChoverboximageids;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage3;
									$new_array['medianame'] = basename($VCimage3);
									$new_array['page_builder_name'] = "Visual Composer";
									$vc5_unixtime = filemtime($vc5_url_dir_path);
									$new_array['datetime'] = date("Y-m-d h:i:s",$vc5_unixtime);
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = 'Yes';
									$new_array['website_prefix'] = $wpdb->prefix;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						// If have featured image
						if(!empty($featuredImageId)){
							$new_array['media_id'] = $featuredImagesId;
							$new_array['title'] = $the_title;
							$new_array['src'] = $featuredImageId;
							$new_array['medianame'] = basename($featuredImageId);
							$new_array['page_builder_name'] = "Featured Image | Visual Composer";
							// Getting filetime from dir URL
							$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
							$elementor4_unixtime = filemtime($elementor4_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				if($oxygen_builder){
					$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
					$PageId = $getvalues->ID;
					$the_content = get_post_meta($PageId, 'ct_builder_shortcodes', true);

					preg_match_all('@"url":"([^"]+)"@', $the_content, $WordPressWidgetImages);
					preg_match_all('@"src":"([^"]+)"@', $the_content, $contentimgurl);
					preg_match_all('@"background-image":"([^"]+)"@', $the_content, $contentimgurl1);				
					preg_match_all('@"image_ids":"([^"]+)"@', $the_content, $gelleryimgids);
					preg_match_all('@"code-php":"([^"]+)"@', $the_content, $phpcodes_encode);

					$OxyCodeArr = array();
					foreach ($phpcodes_encode[1] as $phpcode_encode) {
						$phpcodes_decode = base64_decode($phpcode_encode);
						preg_match_all('@(?:src[^>]+>)(.*?)@', $phpcodes_decode, $phpcodesImgUrl);
						$phpcode_encode_url = str_replace(['src=',"'",'"',"/>",">"," type=application/pdf"], ['','','','','',''], $phpcodesImgUrl[0]);
						$OxyCodeArr[] = explode(" ", $phpcode_encode_url[0]);
					}

					$OXImageurls = array();
					foreach ($OxyCodeArr as $OxyCodevalue) {
						$OXImageurls[] = $OxyCodevalue[0];
					}

					$widget_images = array();
					foreach ($WordPressWidgetImages[1] as $WordPressWidgetValue) {
						$widget_images[] = base64_decode($WordPressWidgetValue);
					}

					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_gallery);
					// for gallery image
					$urls = '';
					if(!empty($oxy_gallery[1])){					
						foreach ($oxy_gallery[1] as $oxy_gallery_id) {
							$urls .= $oxy_gallery_id.',';
						}
					}
					$imageID = rtrim($urls,",");
					$imageIDs = explode(",", $imageID);
					foreach ($imageIDs as $EachId) {
						$getFiles[] = $this->getImageUrlbyId($EachId);
					}
					
					preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);

					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFiles[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}

					// Merging arrays
					$OXI1mageurlsArr = array();
					if(!empty($OXImageurls) && !empty($contentimgurl[1])){
						$OXI1mageurlsArr = array_merge($OXImageurls,$contentimgurl[1]);
					}elseif(!empty($OXImageurls) && empty($contentimgurl[1])){
						$OXI1mageurlsArr = $OXImageurls;
					}elseif(empty($OXImageurls) && !empty($contentimgurl[1])){
						$OXI1mageurlsArr = $contentimgurl[1];
					}
					$OXI2mageurlsArr = array();
					if(!empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
						$OXI2mageurlsArr = array_merge($OXI1mageurlsArr,$contentimgurl1[1]);
					}elseif(!empty($OXI1mageurlsArr) && empty($contentimgurl1[1])){
						$OXI2mageurlsArr = $OXI1mageurlsArr;
					}elseif(empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
						$OXI2mageurlsArr = $contentimgurl1[1];
					}

					$OXImageurlsArr = array();
					if(!empty($OXI2mageurlsArr) && !empty($getFiles)){
						$OXImageurlsArr = array_merge($OXI2mageurlsArr,$getFiles);
					}elseif(!empty($OXI2mageurlsArr) && empty($getFiles)){
						$OXImageurlsArr = $OXI2mageurlsArr;
					}elseif(empty($OXI2mageurlsArr) && !empty($getFiles)){
						$OXImageurlsArr = $getFiles;
					}

					$OXImageurlsArrFinal = array();
					if(!empty($OXImageurlsArr) && !empty($widget_images)){
						$OXImageurlsArrFinal = array_merge($OXImageurlsArr,$widget_images);
					}elseif(!empty($OXImageurlsArr) && empty($widget_images)){
						$OXImageurlsArrFinal = $OXImageurlsArr;
					}elseif(empty($OXImageurlsArr) && !empty($widget_images)){
						$OXImageurlsArrFinal = $widget_images;
					}

					$OXimgidsArr = array();
					if(!empty($OXImageurlsArrFinal)){
						foreach ($OXImageurlsArrFinal as $OXImageurl) {
							array_push($OXimgidsArr, $OXImageurl);
						}
					}

					if(!empty($gelleryimgids[1])){
						foreach ($gelleryimgids[1] as $gelleryimgid) {
							$gelleryimgURlbyid = array($this->getImageUrlbyId($gelleryimgid));
							array_push($OXimgidsArr,$gelleryimgURlbyid);
						}
					}
					$OXimgidsArrUni = array();
					$OXimgidsArrUni = array_unique($OXimgidsArr, SORT_REGULAR);

					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					foreach ($OXimgidsArrUni as $oxi_url) {
						// Get the created date of this media
				        $dir_path =  str_replace(get_site_url().'/',get_home_path(),$oxi_url);

				        if(file_exists($dir_path)){
				        	$oxy_unixtime = filemtime($dir_path);
				        	$new_array['media_id'] = "";
							$new_array['title'] = $the_title;
							$new_array['src'] = $oxi_url;
							$new_array['medianame'] = basename($oxi_url);
							$new_array['page_builder_name'] = "Oxygen Builder";
							$new_array['datetime'] = date("Y-m-d h:i:s",$oxy_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
				        }
					}
					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image | Oxygen Builder";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				if($beaver_builder == 1){
					$getFiles = array();
					$BBmergeArray = array();
					$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;
					$PageId = $getvalues->ID;
					preg_match_all('@src="([^"]+)"@', $the_content, $group6);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group7);

					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
					
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);
					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFiles[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}
					$beaver_src_array = $group6[1];
					$beaver_mp4_array = $group7[1];
					// Merging arrays
					$BBmergePreArray = array();
					if(!empty($getFiles) && !empty($beaver_src_array)){
						$BBmergePreArray = array_merge(array_unique($beaver_src_array),array_unique($getFiles));
					}elseif(!empty($getFiles) && empty($beaver_src_array)){
						$BBmergePreArray = array_unique($getFiles);
					}elseif(empty($getFiles) && !empty($beaver_src_array)){
						$BBmergePreArray = array_unique($beaver_src_array);
					}

					$BBmergeArray = array();
					if(!empty($BBmergePreArray) && !empty($beaver_mp4_array)){
						$BBmergeArray = array_merge(array_unique($beaver_mp4_array),array_unique($BBmergePreArray));
					}elseif(!empty($BBmergePreArray) && empty($beaver_mp4_array)){
						$BBmergeArray = array_unique($BBmergePreArray);
					}elseif(empty($BBmergePreArray) && !empty($beaver_mp4_array)){
						$BBmergeArray = array_unique($beaver_mp4_array);
					}
					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					// Beaver Builder Final Array
					foreach ($BBmergeArray as $beaver_urls) {
						$beaver_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$beaver_urls);
						if(file_exists($beaver_url_dir_path)){
							$new_array['media_id'] = "";
							$new_array['title'] = $the_title;
							$new_array['src'] = $beaver_urls;
							$new_array['medianame'] = basename($beaver_urls);
							$new_array['page_builder_name'] = "Beaver Builder";
							$beaver_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$beaver_urls);
							$beaver_unixtime = filemtime($beaver_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$beaver_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image | Beaver Builder";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				if($brizy_builder){
					$PageId = $getvalues->ID;
					$the_content = get_post_meta($PageId, 'brizy', true);
					$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
					$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
					preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
					preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);

					// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
					preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
					foreach ($case1_gethreffiles[1] as $case1_href_value){
						$case1_href_value = strtok($case1_href_value, " ");
			            $case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$case1_href_value);
						if(file_exists($case1_dir_path)){
							$hrefcase1[] = $case1_href_value;
						}
					}
					// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
					preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
					foreach ($case2_gethreffiles[1] as $case2_href_value) {
						$case2_href_value = strtok($case2_href_value, " ");
			            $case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$case2_href_value);
						if(file_exists($case2_dir_path)){
							$hrefcase2[] = $case2_href_value;
						}
					}				
					// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
					$srcfiles = array();
					preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
					foreach ($case3_gethreffiles[1] as $case3_href_value){
						$case3_href_value = strtok($case3_href_value, " ");
			            $case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$case3_href_value);
						if(file_exists($case3_dir_path)){
							$hrefcase3[] = $case3_href_value;
						}
					}				
					// CASE 4 (data-href) : Get url when pattern is <a data-href=
					preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
					foreach ($case4_gethreffiles[1] as $case4_href_value){					
						$case4_href_value = strtok($case4_href_value, " ");
						$case4_data_href = utf8_decode(urldecode($case4_href_value));
						preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
						$hrefcase4 = array();
						foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
							$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
							$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$theLinkVal);
							if(file_exists($case4_dir_path)){
								$hrefcase4[] = $theLinkVal;
							}
						}					
					}				
					// $the_content = htmlspecialchars($the_content);

					// CASE 5 (video) : Get url when pattern is "video":"http://some-url.mp4"
					preg_match_all('/"video"\h*:.*?\"(.*?)\",(?![^"\n]")/', $brizy_content_str, $videoUrl);
					foreach ($videoUrl[1] as $videoUrlValue) {
						$theVideoVal = str_replace("\",", "", $videoUrlValue);
						$theVideoVals = str_replace(get_site_url().'/',get_home_path(),$theVideoVal);
						if(file_exists($theVideoVals)){
							$hrefcase5[] = $theVideoVal;
						}
					}				

					// CASE 6 (image) : Get url when getting images
					// Merging Arrays Start
					$imagesNeme = array();
					if(!empty($group12[1]) && !empty($group14[1])){
						$imagesNeme = array_merge($group12[1],$group14[1]);
					}elseif(!empty($group12[1]) && empty($group14[1])){
						$imagesNeme = $group12[1];
					}elseif(empty($group12[1]) && !empty($group14[1])){
						$imagesNeme = $group14[1];
					}
					// Merging Arrays Ends
					$hrefcase6 = array();
					if(!empty($imagesNeme)){
						foreach ($imagesNeme as $imageNeme) {
							// Get Media id by Name only for brizy
							$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
							if(!empty($imgIdsquery)){
								$imagesIds = $this->getImageUrlbyId($imgIdsquery[0]->post_id);
								$hrefcase6[] = $imagesIds;
							}
						}
					}
					// Merging Arrays Start
					$href_arrayPre1 = array();
					if(!empty($hrefcase1) && !empty($hrefcase2)){
						$href_arrayPre1 = array_merge($hrefcase1,$hrefcase2);
					}elseif(!empty($hrefcase1) && empty($hrefcase2)){
						$href_arrayPre1 = $hrefcase1;
					}elseif(empty($hrefcase1) && !empty($hrefcase2)){
						$href_arrayPre1 = $hrefcase2;
					}

					$href_arrayPre2 = array();
					if(!empty($href_arrayPre1) && !empty($hrefcase3)){
						$href_arrayPre2 = array_merge($href_arrayPre1,$hrefcase3);
					}elseif(!empty($href_arrayPre1) && empty($hrefcase3)){
						$href_arrayPre2 = $href_arrayPre1;
					}elseif(empty($href_arrayPre1) && !empty($hrefcase3)){
						$href_arrayPre2 = $hrefcase3;
					}

					$href_arrayPre3 = array();
					if(!empty($href_arrayPre2) && !empty($hrefcase4)){
						$href_arrayPre3 = array_merge($href_arrayPre2,$hrefcase4);
					}elseif(!empty($href_arrayPre2) && empty($hrefcase4)){
						$href_arrayPre3 = $href_arrayPre2;
					}elseif(empty($href_arrayPre2) && !empty($hrefcase4)){
						$href_arrayPre3 = $hrefcase4;
					}

					$href_arrayPre4 = array();
					if(!empty($href_arrayPre3) && !empty($hrefcase5)){
						$href_arrayPre4 = array_merge($href_arrayPre3,$hrefcase5);
					}elseif(!empty($href_arrayPre3) && empty($hrefcase5)){
						$href_arrayPre4 = $href_arrayPre3;
					}elseif(empty($href_arrayPre3) && !empty($hrefcase5)){
						$href_arrayPre4 = $hrefcase5;
					}

					$href_array = array();
					if(!empty($href_arrayPre4) && !empty($hrefcase6)){
						$href_array = array_merge($href_arrayPre4,$hrefcase6);
					}elseif(!empty($href_arrayPre4) && empty($hrefcase6)){
						$href_array = $href_arrayPre4;
					}elseif(empty($href_arrayPre4) && !empty($hrefcase6)){
						$href_array = $hrefcase6;
					}
					// Merging Arrays Ends

					if(!empty($href_array)){
						// Get all categories
						$post_categories = wp_get_post_terms($PageId,'category');
						$post_cat = array();
						$post_cats = "";
						if(!empty($post_categories)){
							foreach ($post_categories as $cat_value) {
								$post_cat[] = $cat_value->name;
							}
							$post_cats = implode(",", $post_cat);
						}
						foreach ($href_array as $brizy_value) {
							$hrefValues = stripslashes($brizy_value);
							$brizy_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
							if(file_exists($brizy_url_dir_path)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $hrefValues;
								$new_array['medianame'] = basename($hrefValues);
								$new_array['page_builder_name'] = "Brizy";
								$brizy_unixtime = filemtime($brizy_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$brizy_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								array_push($uniqueArr,$new_array);
							}
						}
						// If have featured image
						if(!empty($featuredImageId)){
							$new_array['media_id'] = $featuredImagesId;
							$new_array['title'] = $the_title;
							$new_array['src'] = $featuredImageId;
							$new_array['medianame'] = basename($featuredImageId);
							$new_array['page_builder_name'] = "Featured Image | Brizy";
							// Getting filetime from dir URL
							$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
							$elementor4_unixtime = filemtime($elementor4_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				if($siteorigin_ck_builder == "SiteOrigin"){
					$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
					$PageId = $getvalues->ID;
					$the_content = get_post_meta($PageId, 'panels_data', true);		
					$siteorigenArray = array();		
					foreach ($the_content['widgets'] as $valueid) {
						// $siteorigenArray = array();
						// $getContentUrl = array();
						if(array_key_exists("attachment_id", $valueid)){
							$sitegetimgurlattachment_id = $this->getImageUrlbyId($valueid['attachment_id']);
							array_push($siteorigenArray, $sitegetimgurlattachment_id);
						}
						if(array_key_exists("ids", $valueid)){
							foreach ($valueid['ids'] as $galleryimg) {
								$sitegetimgurlids = $this->getImageUrlbyId($galleryimg);
								array_push($siteorigenArray, $sitegetimgurlids);
							}
						}
						if(array_key_exists("features", $valueid)){
							foreach ($valueid['features'] as $features_img) {
								$sitegetimgurlfeatures = $this->getImageUrlbyId($features_img['icon_image']);
								array_push($siteorigenArray, $sitegetimgurlfeatures);
							}
						}
						if(array_key_exists("image", $valueid)){
							$sitegetimgurlimage = $this->getImageUrlbyId($valueid['image']);
							array_push($siteorigenArray, $sitegetimgurlimage);
						}
						if(array_key_exists("frames", $valueid)){
							foreach ($valueid['frames'] as $frames_img) {
								$sitegetimgurlframes = $this->getImageUrlbyId($frames_img['background_image']);
								array_push($siteorigenArray, $sitegetimgurlframes);
							}
						}
						if(array_key_exists("content", $valueid)){
							$custom_html = $valueid['content'];
							preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
							foreach ($contentimg[1] as $imgurl){
								array_push($siteorigenArray, $imgurl);
							}
						}
						if(array_key_exists("content", $valueid)){
							preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
							foreach ($contentimgs[1] as $imgurls){
								array_push($siteorigenArray, $imgurls);
							}
						}			
						if(array_key_exists("text", $valueid)){
							$siteEditor = $valueid['text'];
							preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
							preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
							$groupsfilehref = $groupsfile[1];
							$groupsfilesrc = $srcgroupsfile[1];
							if(!empty($groupsfilehref)){
								foreach ($groupsfilehref as $filesvalue) {								
									$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvalue;
								    }
								}
							}
							if(!empty($groupsfilesrc)){
								foreach ($groupsfilesrc as $filesvaluesrc) {
									$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvaluesrc;
								    }
								}
							}
							preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
							preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
							preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
							preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
							preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
							preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
							preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
							preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
							preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
							preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
							preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
							preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
							preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
							preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
							preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
							preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
							preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
							preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
							preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
							preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
							preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
							// for vc video
							if(!empty($vc_video_url[1])){
								foreach ($vc_video_url[1] as $vc_video) {
									$getContentUrl[] = $vc_video;
								}
							}
							// for src
							if(!empty($group9[1])){
								foreach ($group9[1] as $group9s) {
									$getContentUrl[] = $group9s;
								}
							}
							// for mp3
							if(!empty($group11[1])){
								foreach ($group11[1] as $group11s) {
									$getContentUrl[] = $group11s;
								}
							}
							// for mp4
							if(!empty($group12[1])){
								foreach ($group12[1] as $group12s) {
									$getContentUrl[] = $group12s;
								}
							}
							// for pdf
							if(!empty($group13[1])){
								foreach ($group13[1] as $group13s) {
									$getContentUrl[] = $group13s;
								}
							}
							// for docx
							if(!empty($group14[1])){
								foreach ($group14[1] as $group14s) {
									$getContentUrl[] = $group14s;
								}
							}
							// for doc
							if(!empty($group15[1])){
								foreach ($group15[1] as $group15s) {
									$getContentUrl[] = $group15s;
								}
							}
							// for ppt
							if(!empty($group16[1])){
								foreach ($group16[1] as $group16s) {
									$getContentUrl[] = $group16s;
								}
							}
							// for xls
							if(!empty($group17[1])){
								foreach ($group17[1] as $group17s) {
									$getContentUrl[] = $group17s;
								}
							}
							// for pps
							if(!empty($group18[1])){
								foreach ($group18[1] as $group18s) {
									$getContentUrl[] = $group18s;
								}
							}
							// for ppsx
							if(!empty($group19[1])){
								foreach ($group19[1] as $group19s) {
									$getContentUrl[] = $group19s;
								}
							}
							// for xlsx
							if(!empty($group20[1])){
								foreach ($group20[1] as $group20s) {
									$getContentUrl[] = $group20s;
								}
							}
							// for odt
							if(!empty($group21[1])){
								foreach ($group21[1] as $group21s) {
									$getContentUrl[] = $group21s;
								}
							}
							// for ogg
							if(!empty($group22[1])){
								foreach ($group22[1] as $group22s) {
									$getContentUrl[] = $group22s;
								}
							}
							// for m4a
							if(!empty($group23[1])){
								foreach ($group23[1] as $group23s) {
									$getContentUrl[] = $group23s;
								}
							}
							// for wav
							if(!empty($group24[1])){
								foreach ($group24[1] as $group24s) {
									$getContentUrl[] = $group24s;
								}
							}
							// for mp4
							if(!empty($group25[1])){
								foreach ($group25[1] as $group25s) {
									$getContentUrl[] = $group25s;
								}
							}
							// for mov
							if(!empty($group26[1])){
								foreach ($group26[1] as $group26s) {
									$getContentUrl[] = $group26s;
								}
							}
							// for avi
							if(!empty($group27[1])){
								foreach ($group27[1] as $group27s) {
									$getContentUrl[] = $group27s;
								}
							}
							// for 3gp
							if(!empty($group28[1])){
								foreach ($group28[1] as $group28s) {
									$getContentUrl[] = $group28s;
								}
							}
							// for pptx
							if(!empty($group29[1])){
								foreach ($group29[1] as $group29s) {
									$getContentUrl[] = $group29s;
								}
							}
						}
						preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
						if(!empty($group10)){
							$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
							if(!empty($bgimgUrl1)){
								foreach ($bgimgUrl1 as $filesvaluebackground) {								
									$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvaluebackground;
								    }
								}
							}
						}
						$VCmediaContent = array_unique($getContentUrl);
						$siteorigenArray = array_unique($siteorigenArray);
						$siteOriginUrl = array();
						if(!empty($VCmediaContent) && !empty($siteorigenArray)){
							$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
						}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
							$siteOriginUrl = $VCmediaContent;
						}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){
							$siteOriginUrl = $siteorigenArray;
						}
						$final_array = array();
						foreach ($siteOriginUrl as $thevalues) {
							array_push($final_array, $thevalues);
						}
					}
					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					foreach ($final_array as $siteOriginUrlValue) {
						$SiteOriginimageUrl = stripslashes($siteOriginUrlValue);
						$siteorigin_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$SiteOriginimageUrl);
						if(file_exists($siteorigin_url_dir_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $SiteOriginimageUrl;
							$new_array['medianame'] = basename($SiteOriginimageUrl);
							$new_array['page_builder_name'] = "Site Origin";

							$unixtime = filemtime($siteorigin_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image | Site Origin";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				if($getvalues->post_type == 'product'){
					$PageId = $getvalues->ID;
					// Get thumbnail ID (For featured image)
					$productids = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_thumbnail_id' AND  post_id ='".$PageId."'");
					$media_id = $productids[0]->meta_value;				
					$post_categories = wp_get_post_terms($PageId,'product_cat');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}

					$getidurlimge  = $this->getImageUrlbyId($media_id);
					$product_feature_image = stripslashes($getidurlimge);
					$product_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$product_feature_image);
					if(file_exists($product_url_dir_path)){
						$new_array['media_id'] = $media_id;
						$new_array['title'] = $the_title;
						$new_array['src'] = $product_feature_image;
						$new_array['medianame'] = basename($product_feature_image);
						$new_array['page_builder_name'] = "Featured Image";
						$unixtime = filemtime($product_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $getvalues->post_type != 'product_variation' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder){
					$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');				
					$the_content = $getvalues->post_content;			
					$PageId = $getvalues->ID;

					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
					$groupsfile = $groupsfile[1];
					if(!empty($groupsfile)){
						foreach ($groupsfile as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}

					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for wmv
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}
					
					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

					if(!empty($gutenbergImgUrls) && !empty($getFiles)){
						$totalContentMedia = array_unique(array_merge($getFiles,$gutenbergImgUrls));
					}else{
						if(!empty($getFiles)){
							$totalContentMedia = array_unique($getFiles);
						}elseif(!empty($gutenbergImgUrls)){
							$totalContentMedia = array_unique($gutenbergImgUrls);
						}else{
							$totalContentMedia = array();
						}
					}

					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					foreach ($totalContentMedia as $gutenbergImgUrl) {
						$gutenberg_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$gutenbergImgUrl);
						if(file_exists($gutenberg_url_dir_path)){
							$guten_unixtime = filemtime($gutenberg_url_dir_path);
							$mediadatetime = date("Y-m-d h:i:s",$guten_unixtime);
							$uniqueArr[] = array(
								'media_id' => $PageId,
								'medianame' => basename($gutenbergImgUrl),
								'src' => $gutenbergImgUrl,
								'media_type' => "",
								'title'=> $the_title,
								'post_type' => $post_type,
								'page_builder_name' => 'Simple/Gutenberg Content Media',
								'post_category' => $post_cats,
								'variant_attribute' => '',
								'variant_sku' => '',
								'datetime' => $mediadatetime,
								'linked' =>'Yes',
								'source_from' => 'database',
								'website_prefix' => $wpdb->prefix
							);
						}
					}
					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
			}
		}
		return $uniqueArr;
	}
	// Get multsite media from page builder Coded start by Hemant
	public function getMultiSitePageBuilderContentMedia(){
		global $wpdb;
		$uniqueArr = array();
		$new_array = array();
		$prefixes = $this->getAllSitePrefix();
		// Total types of extensions
		$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
		foreach ($prefixes as $mutliarray) {
			$prefixValue = $mutliarray['prefix'];
			$multisiteId = $mutliarray['multisite_id'];
			foreach (get_sites() as $all_sites) {
				if($all_sites->blog_id == $multisiteId){
					$multisite_url = $all_sites->path;
				}
				if($all_sites->blog_id == 1){
					$mainsite_url = $all_sites->path;
				}
	        }
			// Get multisite title
			$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
			$site_name = $current_blog_details->blogname;
			$pbContent = $wpdb->get_results("SELECT * from ".$prefixValue."posts WHERE post_type !='revision' && (post_status = 'publish' OR post_status = 'draft')");
			$inc = 0;
			foreach ($pbContent as $getvalues) {
				$getFiles = array();
				$the_title = $getvalues->post_title;
				$post_type = $getvalues->post_type;
				$mediadate = $getvalues->post_date;
				$oxygen_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id=$getvalues->ID" );
				$oxygen_builder = $oxygen_builder[0]->meta_value;

				$elem_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_edit_mode' AND post_id=$getvalues->ID" );
				$elem_builder = $elem_builder[0]->meta_value;

				$vc_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_wpb_vc_js_status' AND post_id=$getvalues->ID" );
				$vc_builder = $vc_builder[0]->meta_value;

				$siteorigin_builders = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id=$getvalues->ID" );
				$siteorigin_builder = $siteorigin_builders[0]->meta_value;

				$siteorigin_ck_builder = '';
				if(!empty($siteorigin_builder)){
					$siteorigin_ck_builder = "SiteOrigin";
				}

				$beaver_builders = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_fl_builder_enabled' AND post_id=$getvalues->ID" );
				$beaver_builder = $beaver_builders[0]->meta_value;

				$featuredImageArrs = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_thumbnail_id' AND post_id=$getvalues->ID" );
				$featuredImageArr = $featuredImageArrs[0]->meta_value;

				@$featuredImagesId = $featuredImageArr[0];
				if($featuredImagesId){
					$featuredImageId = $this->getMultiSiteImageUrlbyId($featuredImagesId,$prefixValue);
				}else{
					$featuredImageId = '';
				}
				
				$productgalleryArrs = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_product_image_gallery' AND post_id=$getvalues->ID" );
				@$productgalleryImg = $productgalleryArrs[0];

				if($elem_builder == 'builder'){
					$PageId = $getvalues->ID;
					$elementor_sql = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_data' AND post_id=$PageId" );
					$elementor_content = $elementor_sql[0]->meta_value;
					
					// Get files url from URL & ID tag start
					$group4 = array();
					$group5 = array();
					$getsrcfiles = array();
					preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
					preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
					// Get files url from URL & ID tag end

					$imgArry = array();
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					// Get all category of the post
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					foreach ($group4[1] as $ElementorimageUrl) {
						$ElementorimageUrl = stripslashes($ElementorimageUrl);
						// Getting filetime from dir URL
						$elementor_url_dir_path4 =  str_replace(get_site_url().'/',get_home_path(),$ElementorimageUrl);
						$img_home_path4 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path4);
						if(file_exists($img_home_path4)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $ElementorimageUrl;
							$new_array['medianame'] = basename($ElementorimageUrl);
							$new_array['page_builder_name'] = "Elementor";
							$unixtime4 = filemtime($img_home_path4);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime4);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
					// Get files url from href tag start
					$gethreffiles = array();
					preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
					$hreffiles = array();
					if(!empty($gethreffiles[1])){
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($gethreffiles[1] as $hrefValues) {
							$hrefValues = stripslashes($hrefValues);
							// Getting filetime from dir URL
							$elementor_url_dir_path3 =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
							$img_home_path3 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path3);
							if(file_exists($img_home_path3)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $hrefValues;
								$new_array['medianame'] = basename($hrefValues);
								$new_array['page_builder_name'] = "Elementor href";
								$unixtime3 = filemtime($img_home_path3);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime3);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// Get files url from href tag end

					// Get files src from href tag start
					$srcfiles = array();
					preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
					if(!empty($getsrcfiles[1])){
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($getsrcfiles[1] as $srcValues) {
							$srcValues = stripslashes($srcValues);
							// Getting filetime from dir URL
							$elementor_url_dir_path2 =  str_replace(get_site_url().'/',get_home_path(),$srcValues);
							$img_home_path2 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path2);
							if(file_exists($img_home_path2)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $srcValues;
								$new_array['medianame'] = basename($srcValues);
								$new_array['page_builder_name'] = "Elementor href";
								$unixtime2 = filemtime($img_home_path2);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime2);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// Get files src from href tag start
					$contentimgid = implode(',', $imgArry);
					if($group5[1][0] != ""){
						$group5arr = explode(',', $group5[1][0]);
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($group5arr as $imggelid) {						
							$getidurlimge  = $this->getMultiSiteImageUrlbyId($imggelid,$prefixValue);
							$getidurlimge = stripslashes($getidurlimge);
							// Getting filetime from dir URL
							$elementor_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$getidurlimge);
							$img_home_path = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path);
							if(file_exists($img_home_path)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $getidurlimge;
								$new_array['medianame'] = basename($getidurlimge);
								$new_array['page_builder_name'] = "Elementor href";

								$unixtime = filemtime($img_home_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					return $uniqueArr;
				}
				if($vc_builder == 'true'){
					$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;
					preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
					preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
					preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
					preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
					/* Raw HTML */
					preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
					$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
					if(!empty($RowHtmls)){
						foreach ($RowHtmls as $RowHtml) {
							$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
							$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
							
							preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
							$getFiles[] = str_replace(['?_=1'], [''], $group16[1]);
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
					preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFiles[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}

					/* Raw HTML */
					$PageId = $getvalues->ID;

					// Get all category of the post
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}

					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
					$groupsfile = $groupsfile[1];
					if(!empty($groupsfile)){
						foreach ($groupsfile as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@background-image: url([^"]+)@', $the_content, $group10);
					$bgimgUrlArr = str_replace(['(',')'], ['',''], $group10[1]);
					$bgimgUrl = array();
					foreach ($bgimgUrlArr as $BGvalue) {
						$bgimgUrl[] = substr($BGvalue, 0, strrpos($BGvalue, '?'));
					}

					// Merging Arrays Start
					$VCmediaContentPre = array();
					if(!empty($group9[1]) && !empty($bgimgUrl)){
						$VCmediaContentPre = array_merge($group9[1],$bgimgUrl);
					}elseif(!empty($group9[1]) && empty($bgimgUrl)){
						$VCmediaContentPre = $group9[1];
					}elseif(empty($group9[1]) && !empty($bgimgUrl)){
						$VCmediaContentPre = $bgimgUrl;
					}

					$VCmediaContent = array();
					if(!empty($VCmediaContentPre) && !empty($getFiles)){
						$VCmediaContent = array_merge($VCmediaContentPre,$getFiles);
					}elseif(!empty($VCmediaContentPre) && empty($getFiles)){
						$VCmediaContent = $VCmediaContentPre;
					}elseif(empty($VCmediaContentPre) && !empty($getFiles)){
						$VCmediaContent = $getFiles;
					}
					// Merging Arrays Ends
					
					$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
					$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
					$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
					$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

					if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
						$VCimageids = '';
						$comma = '';
						if($VCsingleimageids != ""){
							$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							$VChoverboximagesIdArr = array();
							$VChoverboximagesIdArr = explode(",", $VCsingleimageids);
							foreach ($VChoverboximagesIdArr as $VChoverboximagesIdArrValue) {
								$VCimages = $this->getMultiSiteImageUrlbyId($VChoverboximagesIdArrValue,$prefixValue);
								// Get the created date of this media
								$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimages);
				                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
				                if(file_exists($multisite_url_dir)){
				                	$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
									$new_array['media_id'] = $idvalue;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimages;
									$new_array['medianame'] = basename($VCimages);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = $date_created;
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = $linkedd;
									$new_array['website_prefix'] = $prefixValue;
									array_push($uniqueArr,$new_array);
				                }
							}
						}
						if(!empty($VCmediaContent)){
							$incs = 0;
							$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							foreach ($VCmediaContent as $VCmediaContentUrl) {
								// Get the created date of this media
				                $dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCmediaContentUrl);
				                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
				                if(file_exists($multisite_url_dir)){
									$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
									$new_array['media_id'] = '';
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCmediaContentUrl;
									$new_array['medianame'] = basename($VCmediaContentUrl);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = $date_created;
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = $linkedd;
									$new_array['website_prefix'] = $prefixValue;
									$incs++;
									array_push($uniqueArr,$new_array);
								}
							}
						}

						if($VCgalleryimageids != ""){
							if($VCimageids != ''){ $comma = ','; }						
							$theGalIds = explode(",", $VCgalleryimageids);
							$inc = 0;
							$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							foreach ($theGalIds as $idvalue) {							
								$VCimage = $this->getMultiSiteImageUrlbyId($idvalue,$prefixValue);
								// Get the created date of this media
								$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage);
				                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
				                if(file_exists($multisite_url_dir)){
									$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
									$new_array['media_id'] = $idvalue;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage;
									$new_array['medianame'] = basename($VCimage);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = $date_created;
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = $linkedd;
									$new_array['website_prefix'] = $prefixValue;
									$inc++;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						
						if($VCcarouselimageids != ""){
							if($VCimageids != ''){ $comma = ','; }
							$theGalIds2 = explode(",", $VCcarouselimageids);
							$inc2 = 0;
							$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							foreach ($theGalIds2 as $idvalue2) {
								$VCimage2 = $this->getMultiSiteImageUrlbyId($idvalue2,$prefixValue);
								// Get the created date of this media
								$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage2);
				                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
				                if(file_exists($multisite_url_dir)){
									$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
									$new_array['media_id'] = $idvalue2;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage2;
									$new_array['medianame'] = basename($VCimage2);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = $date_created;
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = $linkedd;
									$new_array['website_prefix'] = $prefixValue;
									$inc2++;
									array_push($uniqueArr,$new_array);
								}
							}
						}
						
						if($VChoverboximageids != ""){
							$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							if($VCimageids != ''){ $comma = ','; }
							$VChoverboximagesIdArr2 = array();
							$VChoverboximagesIdArr2 = explode(",", $VChoverboximageids);
							foreach ($VChoverboximagesIdArr2 as $VChoverboximagesIdArrValue2) {
								$VCimage3 = $this->getMultiSiteImageUrlbyId($VChoverboximagesIdArrValue2,$prefixValue);
								// Get the created date of this media
								$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage3);
				                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
				                if(file_exists($multisite_url_dir)){
									$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
									$new_array['media_id'] = $VChoverboximageids;
									$new_array['title'] = $the_title;
									$new_array['src'] = $VCimage3;
									$new_array['medianame'] = basename($VCimage3);
									$new_array['page_builder_name'] = "Visual Composer";
									$new_array['datetime'] = $date_created;
									$new_array['post_type'] = $post_type;
									$new_array['post_category'] = $post_cats;
									$new_array['variant_attribute'] = '';
									$new_array['variant_sku'] = '';
									$new_array['source_from'] = 'database';
									$new_array['linked'] = $linkedd;
									$new_array['website_prefix'] = $prefixValue;
									array_push($uniqueArr,$new_array);
								}
							}
						}
					}
				}
				if($oxygen_builder){
					$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
					$PageId = $getvalues->ID;

					$the_content_meta = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id = $PageId" );
					$the_content = $the_content_meta[0]->meta_value;

					preg_match_all('@"url":"([^"]+)"@', $the_content, $WordPressWidgetImages);
					preg_match_all('@"src":"([^"]+)"@', $the_content, $contentimgurl);
					preg_match_all('@"background-image":"([^"]+)"@', $the_content, $contentimgurl1);
					preg_match_all('@"image_ids":"([^"]+)"@', $the_content, $gelleryimgids);
					preg_match_all('@"code-php":"([^"]+)"@', $the_content, $phpcodes_encode);
					
					$OxyCodeArr = array();
					foreach ($phpcodes_encode[1] as $phpcode_encode) {
						$phpcodes_decode = base64_decode($phpcode_encode);
						preg_match_all('@(?:src[^>]+>)(.*?)@', $phpcodes_decode, $phpcodesImgUrl);
						$phpcode_encode_url = str_replace(['src=',"'",'"',"/>",">"," type=application/pdf"], ['','','','','',''], $phpcodesImgUrl[0]);
						$OxyCodeArr[] = explode(" ", $phpcode_encode_url[0]);
					}

					$OXImageurls = array();
					foreach ($OxyCodeArr as $OxyCodevalue) {
						$OXImageurls[] = $OxyCodevalue[0];
					}

					$widget_images = array();
					foreach ($WordPressWidgetImages[1] as $WordPressWidgetValue) {
						$widget_images[] = base64_decode($WordPressWidgetValue);
					}

					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_gallery);
					// for gallery image
					$urls = '';
					if(!empty($oxy_gallery[1])){					
						foreach ($oxy_gallery[1] as $oxy_gallery_id) {
							$urls .= $oxy_gallery_id.',';
						}
					}
					$imageID = rtrim($urls,",");
					$imageIDs = explode(",", $imageID);
					foreach ($imageIDs as $EachId) {
						$getFiles[] = $this->getMultiSiteImageUrlbyId($EachId,$prefixValue);
					}
					
					preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);

					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFiles[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}

					// Merging arrays
					$OXI1mageurlsArr = array();
					if(!empty($OXImageurls) && !empty($contentimgurl[1])){
						$OXI1mageurlsArr = array_merge($OXImageurls,$contentimgurl[1]);
					}elseif(!empty($OXImageurls) && empty($contentimgurl[1])){
						$OXI1mageurlsArr = $OXImageurls;
					}elseif(empty($OXImageurls) && !empty($contentimgurl[1])){
						$OXI1mageurlsArr = $contentimgurl[1];
					}
					$OXI2mageurlsArr = array();
					if(!empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
						$OXI2mageurlsArr = array_merge($OXI1mageurlsArr,$contentimgurl1[1]);
					}elseif(!empty($OXI1mageurlsArr) && empty($contentimgurl1[1])){
						$OXI2mageurlsArr = $OXI1mageurlsArr;
					}elseif(empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
						$OXI2mageurlsArr = $contentimgurl1[1];
					}
					$OXImageurlsArr = array();
					if(!empty($OXI2mageurlsArr) && !empty($getFiles)){
						$OXImageurlsArr = array_merge($OXI2mageurlsArr,$getFiles);
					}elseif(!empty($OXI2mageurlsArr) && empty($getFiles)){
						$OXImageurlsArr = $OXI2mageurlsArr;
					}elseif(empty($OXI2mageurlsArr) && !empty($getFiles)){
						$OXImageurlsArr = $getFiles;
					}
					
					$OXImageurlsArrFinal = array();
					if(!empty($OXImageurlsArr) && !empty($widget_images)){
						$OXImageurlsArrFinal = array_merge($OXImageurlsArr,$widget_images);
					}elseif(!empty($OXImageurlsArr) && empty($widget_images)){
						$OXImageurlsArrFinal = $OXImageurlsArr;
					}elseif(empty($OXImageurlsArr) && !empty($widget_images)){
						$OXImageurlsArrFinal = $widget_images;
					}

					$OXimgidsArr = array();
					if(!empty($OXImageurlsArrFinal)){
						foreach ($OXImageurlsArrFinal as $OXImageurl) {
							array_push($OXimgidsArr, $OXImageurl);
						}
					}
					
					if(!empty($gelleryimgids[1])){
						foreach ($gelleryimgids[1] as $gelleryimgid) {
							$gelleryimgURlbyid = array($this->getMultiSiteImageUrlbyId($gelleryimgid,$prefixValue));
							array_push($OXimgidsArr,$gelleryimgURlbyid);
						}
					}
					$OXimgidsArrUni = array();
					$OXimgidsArrUni = array_unique($OXimgidsArr, SORT_REGULAR);
					// Get all categories
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					foreach ($OXimgidsArrUni as $oxi_url) {
						// Get the created date of this media
				        $dir_path =  str_replace(get_site_url().'/',get_home_path(),$oxi_url);
				        $img_home_path = str_replace($multisite_url, $mainsite_url, $dir_path);
				        if(file_exists($img_home_path)){
				        	$unixtime = filemtime($img_home_path);
				        	$new_array['media_id'] = "";
							$new_array['title'] = $the_title;
							$new_array['src'] = $oxi_url;
							$new_array['medianame'] = basename($oxi_url);
							$new_array['page_builder_name'] = "Oxygen Builder";
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
				        }
					}				
				}
				if($beaver_builder == 1){
					$getFiles = array();
					$BBmergeArray = array();
					$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;
					$PageId = $getvalues->ID;
					preg_match_all('@src="([^"]+)"@', $the_content, $group6);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group7);

					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
					
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);
					$groupsfiles = $groupsfiles[1];
					if(!empty($groupsfiles)){
						foreach ($groupsfiles as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getFiles[] = $vc_video;
						}
					}

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}
					$beaver_src_array = $group6[1];
					$beaver_mp4_array = $group7[1];
					// Merging arrays
					$BBmergePreArray = array();
					if(!empty($getFiles) && !empty($beaver_src_array)){
						$BBmergePreArray = array_merge(array_unique($beaver_src_array),array_unique($getFiles));
					}elseif(!empty($getFiles) && empty($beaver_src_array)){
						$BBmergePreArray = array_unique($getFiles);
					}elseif(empty($getFiles) && !empty($beaver_src_array)){
						$BBmergePreArray = array_unique($beaver_src_array);
					}

					$BBmergeArray = array();
					if(!empty($BBmergePreArray) && !empty($beaver_mp4_array)){
						$BBmergeArray = array_merge(array_unique($beaver_mp4_array),array_unique($BBmergePreArray));
					}elseif(!empty($BBmergePreArray) && empty($beaver_mp4_array)){
						$BBmergeArray = array_unique($BBmergePreArray);
					}elseif(empty($BBmergePreArray) && !empty($beaver_mp4_array)){
						$BBmergeArray = array_unique($beaver_mp4_array);
					}
					// Get all categories
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					// Beaver Builder Final Array
					foreach ($BBmergeArray as $beaver_urls) {
						$beaver_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$beaver_urls);
						if(file_exists($beaver_url_dir_path)){
							$new_array['media_id'] = "";
							$new_array['title'] = $the_title;
							$new_array['src'] = $beaver_urls;
							$new_array['medianame'] = basename($beaver_urls);
							$new_array['page_builder_name'] = "Beaver Builder";
							$beaver_unixtime = filemtime($beaver_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$beaver_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				if($brizy_builder){
					$PageId = $getvalues->ID;
					$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='brizy' AND post_id=$PageId" );
					$the_content = unserialize($the_content[0]->meta_value);
					$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
					$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
					preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
					preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);
					
					// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
					preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
					foreach ($case1_gethreffiles[1] as $case1_href_value){
						$case1_href_value = strtok($case1_href_value, " ");
			            $case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$case1_href_value);
						if(file_exists($case1_dir_path)){
							$hrefcase1[] = $case1_href_value;
						}
					}
					// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
					preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
					foreach ($case2_gethreffiles[1] as $case2_href_value) {
						$case2_href_value = strtok($case2_href_value, " ");
			            $case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$case2_href_value);
						if(file_exists($case2_dir_path)){
							$hrefcase2[] = $case2_href_value;
						}
					}				
					// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
					$srcfiles = array();
					preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
					foreach ($case3_gethreffiles[1] as $case3_href_value){
						$case3_href_value = strtok($case3_href_value, " ");
			            $case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$case3_href_value);
						if(file_exists($case3_dir_path)){
							$hrefcase3[] = $case3_href_value;
						}
					}				
					// CASE 4 (data-href) : Get url when pattern is <a data-href=
					preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
					foreach ($case4_gethreffiles[1] as $case4_href_value){					
						$case4_href_value = strtok($case4_href_value, " ");
						$case4_data_href = utf8_decode(urldecode($case4_href_value));
						preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
						$hrefcase4 = array();
						foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
							$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
							$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$theLinkVal);
							if(file_exists($case4_dir_path)){
								$hrefcase4[] = $theLinkVal;
							}
						}					
					}				
					// $the_content = htmlspecialchars($the_content);

					// CASE 5 (video) : Get url when pattern is "video":"http://some-url.mp4"
					preg_match_all('/"video"\h*:.*?\"(.*?)\",(?![^"\n]")/', $brizy_content_str, $videoUrl);
					foreach ($videoUrl[1] as $videoUrlValue) {
						$theVideoVal = str_replace("\",", "", $videoUrlValue);
						$theVideoVals = str_replace(get_site_url().'/',get_home_path(),$theVideoVal);
						if(file_exists($theVideoVals)){
							$hrefcase5[] = $theVideoVal;
						}
					}				

					// CASE 6 (image) : Get url when getting images
					// Merging Arrays Start
					$imagesNeme = array();
					if(!empty($group12[1]) && !empty($group14[1])){
						$imagesNeme = array_merge($group12[1],$group14[1]);
					}elseif(!empty($group12[1]) && empty($group14[1])){
						$imagesNeme = $group12[1];
					}elseif(empty($group12[1]) && !empty($group14[1])){
						$imagesNeme = $group14[1];
					}
					// Merging Arrays Ends
					$hrefcase6 = array();
					if(!empty($imagesNeme)){
						foreach ($imagesNeme as $imageNeme) {
							// Get Media id by Name only for brizy
							$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
							if(!empty($imgIdsquery)){
								$imagesIds = $this->getMultiSiteImageUrlbyId($imgIdsquery[0]->post_id,$prefixValue);
								$hrefcase6[] = $imagesIds;
							}
						}
					}
					
					// Mergining arrays start
					$brizyArr1 = array();
					if(!empty($hrefcase1) && !empty($hrefcase2)){
						$brizyArr1 = array_merge($hrefcase1,$hrefcase2);
					}elseif(!empty($hrefcase1) && empty($hrefcase2)){
						$brizyArr1 = $hrefcase1;
					}elseif(empty($hrefcase1) && !empty($hrefcase2)){
						$brizyArr1 = $hrefcase2;
					}

					$brizyArr2 = array();
					if(!empty($brizyArr1) && !empty($hrefcase3)){
						$brizyArr2 = array_merge($brizyArr1,$hrefcase3);
					}elseif(!empty($brizyArr1) && empty($hrefcase3)){
						$brizyArr2 = $brizyArr1;
					}elseif(empty($brizyArr1) && !empty($hrefcase3)){
						$brizyArr2 = $hrefcase3;
					}

					$brizyArr3 = array();
					if(!empty($brizyArr2) && !empty($hrefcase4)){
						$brizyArr3 = array_merge($brizyArr2,$hrefcase4);
					}elseif(!empty($brizyArr2) && empty($hrefcase4)){
						$brizyArr3 = $brizyArr2;
					}elseif(empty($brizyArr2) && !empty($hrefcase4)){
						$brizyArr3 = $hrefcase4;
					}

					$brizyArr4 = array();
					if(!empty($brizyArr3) && !empty($hrefcase5)){
						$brizyArr4 = array_merge($brizyArr3,$hrefcase5);
					}elseif(!empty($brizyArr3) && empty($hrefcase5)){
						$brizyArr4 = $brizyArr3;
					}elseif(empty($brizyArr3) && !empty($hrefcase5)){
						$brizyArr4 = $hrefcase5;
					}
					
					$href_array = array();
					if(!empty($brizyArr4) && !empty($hrefcase6)){
						$href_array = array_merge($brizyArr4,$hrefcase6);
					}elseif(!empty($brizyArr4) && empty($hrefcase6)){
						$href_array = $brizyArr4;
					}elseif(empty($brizyArr4) && !empty($hrefcase6)){
						$href_array = $hrefcase6;
					}
					// Mergining arrays ends
					if(!empty($href_array)){
						// Get all category of the post
						$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
						$post_categories = $wpdb->get_results($queryterms, OBJECT);
						$post_cat = array();
						$post_cats = "";
						if(!empty($post_categories)){
							foreach ($post_categories as $cat_value) {
								$post_cat[] = $cat_value->name;
							}
							$post_cats = implode(",", $post_cat);
						}
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($href_array as $brizy_value) {
							$hrefValues = stripslashes($brizy_value);
							// Getting filetime from dir URL
							$elementor_url_dir_path5 =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
							$img_home_path5 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path5);
							if(file_exists($img_home_path5)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $hrefValues;
								$new_array['medianame'] = basename($hrefValues);
								$new_array['page_builder_name'] = "Brizy";
								$unixtime5 = filemtime($img_home_path5);
								$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime5);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;						
								array_push($uniqueArr,$new_array);
							}
						}
					}
				}
				if($siteorigin_ck_builder == "SiteOrigin"){
					$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$PageId = $getvalues->ID;
					$siteorigin_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id = $PageId" );
					$the_content = unserialize($siteorigin_content[0]->meta_value);

					$siteorigenArray = array();
					
					foreach ($the_content['widgets'] as $valueid) {
						if(array_key_exists("attachment_id", $valueid)){
							$sitegetimgurlattachment_id = $this->getMultiSiteImageUrlbyId($valueid['attachment_id'],$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlattachment_id);
						}
						if(array_key_exists("ids", $valueid)){
							foreach ($valueid['ids'] as $galleryimg) {
								$sitegetimgurlids = $this->getMultiSiteImageUrlbyId($galleryimg,$prefixValue);
								array_push($siteorigenArray, $sitegetimgurlids);
							}
						}
						if(array_key_exists("features", $valueid)){
							foreach ($valueid['features'] as $features_img) {
								$sitegetimgurlfeatures = $this->getMultiSiteImageUrlbyId($features_img['icon_image'],$prefixValue);
								array_push($siteorigenArray, $sitegetimgurlfeatures);
							}
						}
						if(array_key_exists("image", $valueid)){
							$sitegetimgurlimage = $this->getMultiSiteImageUrlbyId($valueid['image'],$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlimage);
						}
						if(array_key_exists("frames", $valueid)){
							foreach ($valueid['frames'] as $frames_img) {
								$sitegetimgurlframes = $this->getMultiSiteImageUrlbyId($frames_img['background_image'],$prefixValue);
								array_push($siteorigenArray, $sitegetimgurlframes);
							}
						}
						if(array_key_exists("content", $valueid)){
							$custom_html = $valueid['content'];
							preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
							foreach ($contentimg[1] as $imgurl){
								array_push($siteorigenArray, $imgurl);
							}
						}
						if(array_key_exists("content", $valueid)){
							preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
							foreach ($contentimgs[1] as $imgurls){
								array_push($siteorigenArray, $imgurls);
							}
						}					
						if(array_key_exists("text", $valueid)){						
							$siteEditor = $valueid['text'];
							preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
							preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
							$groupsfilehref = $groupsfile[1];
							$groupsfilesrc = $srcgroupsfile[1];
							if(!empty($groupsfilehref)){
								foreach ($groupsfilehref as $filesvalue) {								
									$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvalue;
								    }
								}
							}
							if(!empty($groupsfilesrc)){
								foreach ($groupsfilesrc as $filesvaluesrc) {
									$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvaluesrc;
								    }
								}
							}
							preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
							preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
							preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
							preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
							preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
							preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
							preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
							preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
							preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
							preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
							preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
							preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
							preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
							preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
							preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
							preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
							preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
							preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
							preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
							preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
							preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
							// for vc video
							if(!empty($vc_video_url[1])){
								foreach ($vc_video_url[1] as $vc_video) {
									$getContentUrl[] = $vc_video;
								}
							}
							// for src
							if(!empty($group9[1])){
								foreach ($group9[1] as $group9s) {
									$getContentUrl[] = $group9s;
								}
							}
							// for mp3
							if(!empty($group11[1])){
								foreach ($group11[1] as $group11s) {
									$getContentUrl[] = $group11s;
								}
							}
							// for mp4
							if(!empty($group12[1])){
								foreach ($group12[1] as $group12s) {
									$getContentUrl[] = $group12s;
								}
							}
							// for pdf
							if(!empty($group13[1])){
								foreach ($group13[1] as $group13s) {
									$getContentUrl[] = $group13s;
								}
							}
							// for docx
							if(!empty($group14[1])){
								foreach ($group14[1] as $group14s) {
									$getContentUrl[] = $group14s;
								}
							}
							// for doc
							if(!empty($group15[1])){
								foreach ($group15[1] as $group15s) {
									$getContentUrl[] = $group15s;
								}
							}
							// for ppt
							if(!empty($group16[1])){
								foreach ($group16[1] as $group16s) {
									$getContentUrl[] = $group16s;
								}
							}
							// for xls
							if(!empty($group17[1])){
								foreach ($group17[1] as $group17s) {
									$getContentUrl[] = $group17s;
								}
							}
							// for pps
							if(!empty($group18[1])){
								foreach ($group18[1] as $group18s) {
									$getContentUrl[] = $group18s;
								}
							}
							// for ppsx
							if(!empty($group19[1])){
								foreach ($group19[1] as $group19s) {
									$getContentUrl[] = $group19s;
								}
							}
							// for xlsx
							if(!empty($group20[1])){
								foreach ($group20[1] as $group20s) {
									$getContentUrl[] = $group20s;
								}
							}
							// for odt
							if(!empty($group21[1])){
								foreach ($group21[1] as $group21s) {
									$getContentUrl[] = $group21s;
								}
							}
							// for ogg
							if(!empty($group22[1])){
								foreach ($group22[1] as $group22s) {
									$getContentUrl[] = $group22s;
								}
							}
							// for m4a
							if(!empty($group23[1])){
								foreach ($group23[1] as $group23s) {
									$getContentUrl[] = $group23s;
								}
							}
							// for wav
							if(!empty($group24[1])){
								foreach ($group24[1] as $group24s) {
									$getContentUrl[] = $group24s;
								}
							}
							// for mp4
							if(!empty($group25[1])){
								foreach ($group25[1] as $group25s) {
									$getContentUrl[] = $group25s;
								}
							}
							// for mov
							if(!empty($group26[1])){
								foreach ($group26[1] as $group26s) {
									$getContentUrl[] = $group26s;
								}
							}
							// for avi
							if(!empty($group27[1])){
								foreach ($group27[1] as $group27s) {
									$getContentUrl[] = $group27s;
								}
							}
							// for 3gp
							if(!empty($group28[1])){
								foreach ($group28[1] as $group28s) {
									$getContentUrl[] = $group28s;
								}
							}
							// for pptx
							if(!empty($group29[1])){
								foreach ($group29[1] as $group29s) {
									$getContentUrl[] = $group29s;
								}
							}
						}
						preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
						if(!empty($group10)){
							$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
							if(!empty($bgimgUrl1)){
								foreach ($bgimgUrl1 as $filesvaluebackground) {								
									$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getContentUrl[] = $filesvaluebackground;
								    }
								}
							}
						}
						$VCmediaContent = array_unique($getContentUrl);
						$siteorigenArray = array_unique($siteorigenArray);					
						$siteOriginUrl = array();
						if(!empty($VCmediaContent) && !empty($siteorigenArray)){
							$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
						}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
							$siteOriginUrl = $VCmediaContent;
						}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){						
							$siteOriginUrl = $siteorigenArray;
						}
						$final_array = array();
						foreach ($siteOriginUrl as $thevalues) {
							array_push($final_array, $thevalues);
						}
					}
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					// Get all category of the post
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					foreach ($final_array as $siteOriginUrlValue) {
						$SiteOriginimageUrl = stripslashes($siteOriginUrlValue);
						$siteorigin_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$SiteOriginimageUrl);
						$img_home_path = str_replace($multisite_url, $mainsite_url, $siteorigin_url_dir_path);
						if(file_exists($img_home_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $SiteOriginimageUrl;
							$new_array['medianame'] = basename($SiteOriginimageUrl);
							$new_array['page_builder_name'] = "Site Origin";
							$unixtime = filemtime($img_home_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);					
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				if($getvalues->post_type == 'product'){
					$PageId = $getvalues->ID;
					// Get thumbnail ID (For featured image)
					$productids = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key = '_thumbnail_id' AND  post_id ='".$PageId."'");
					$media_id = $productids[0]->meta_value;

					// Get all category of the post
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}

					$getidurlimge  = $this->getMultiSiteImageUrlbyId($media_id,$prefixValue);
					$product_feature_image = stripslashes($getidurlimge);
					$product_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$product_feature_image);
					$prod_img_home_path = str_replace($multisite_url, $mainsite_url, $product_url_dir_path);
					if(file_exists($prod_img_home_path)){
						$new_array['media_id'] = $media_id;
						$new_array['title'] = $the_title;
						$new_array['src'] = $product_feature_image;
						$new_array['medianame'] = basename($product_feature_image);
						$new_array['page_builder_name'] = "Featured Image";
						$unixtime = filemtime($prod_img_home_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $getvalues->post_type != 'product_variation' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder){
					$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

					$the_content = $getvalues->post_content;			
					$PageId = $getvalues->ID;
					preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
					$groupsfile = $groupsfile[1];
					if(!empty($groupsfile)){
						foreach ($groupsfile as $filesvalue) {
							$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getFiles[] = $filesvalue;
						    }
						}
					}

					preg_match_all('@src="([^"]+)"@', $the_content, $group9);
					preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
					preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
					preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
					preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
					preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
					preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
					preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
					preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
					preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
					preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
					preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
					preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
					preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
					preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
					preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
					preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
					preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getFiles[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getFiles[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getFiles[] = $group12s;
						}
					}

					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getFiles[] = $group13s;
						}
					}

					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getFiles[] = $group14s;
						}
					}

					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getFiles[] = $group15s;
						}
					}

					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getFiles[] = $group16s;
						}
					}

					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getFiles[] = $group17s;
						}
					}

					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getFiles[] = $group18s;
						}
					}

					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getFiles[] = $group19s;
						}
					}

					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getFiles[] = $group20s;
						}
					}

					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getFiles[] = $group21s;
						}
					}

					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getFiles[] = $group22s;
						}
					}

					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getFiles[] = $group23s;
						}
					}

					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getFiles[] = $group24s;
						}
					}

					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getFiles[] = $group25s;
						}
					}

					// for wmv
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getFiles[] = $group26s;
						}
					}

					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getFiles[] = $group27s;
						}
					}

					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getFiles[] = $group28s;
						}
					}

					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getFiles[] = $group29s;
						}
					}
					
					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

					if(!empty($gutenbergImgUrls) && !empty($getFiles)){
						$totalContentMedia = array_unique(array_merge($getFiles,$gutenbergImgUrls));
					}else{
						if(!empty($getFiles)){
							$totalContentMedia = array_unique($getFiles);
						}elseif(!empty($gutenbergImgUrls)){
							$totalContentMedia = array_unique($gutenbergImgUrls);
						}else{
							$totalContentMedia = array();
						}
					}

					// Get all category of the post
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					foreach ($totalContentMedia as $gutenbergImgUrl) {
						// Getting filetime from dir URL
						$elementor_url_dir_path6 =  str_replace(get_site_url().'/',get_home_path(),$gutenbergImgUrl);
						$img_home_path6 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path6);
						if(file_exists($img_home_path6)){
							$unixtime6 = filemtime($img_home_path6);
							$mediadatetime = date("Y-m-d h:i:s",$unixtime6);
							$uniqueArr[] = array(
								'media_id' => $PageId,
								'medianame' => basename($gutenbergImgUrl),
								'src' => $gutenbergImgUrl,
								'media_type' => "",
								'title'=> $the_title,
								'post_type' => $post_type,
								'page_builder_name' => 'Simple/Gutenberg Content Media',
								'post_category' => $post_cats,
								'variant_attribute' => '',
								'variant_sku' => '',
								'datetime' => $mediadatetime,
								'linked' => $linkedd,
								'source_from' => 'database',
								'website_prefix' => $prefixValue
							);
						}					
					}
				}
			}
		}
		$uniqueArr = array_unique($uniqueArr, SORT_REGULAR);
		return $uniqueArr;
	}
	//GET ATTACHMENT DETAILS FROM DATABASE
	function getattachmentdata(){
		global $wpdb;
		$mediaexistdata=  array();
		$pagebuildername = '';
		$mimetype = "image/jpeg";
		if(is_multisite()){
			$prefixes = $this->getAllSitePrefix();
			foreach ($prefixes as $mutliarray) {
				$prefixValue[] = $mutliarray['prefix'];
			}
		}
		$attachmentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where post_type='attachment'");

		if(!empty($attachmentdata)){
			$attachmentarray = array();
			foreach ($attachmentdata as $key => $value) {
				$media_id = $value->ID;
				$post_title = $value->post_title;
				$post_type = $value->post_type;
				$media_url = $value->guid;
				$mediauploaddate = $value->post_date;
				$post_mime_type = $value->post_mime_type;
				$media_name = substr($media_url, strrpos($media_url, '/') + 1);
				$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
				// Main URL of an image
				$mediaexistdata[] = array(
					'media_id' => $media_id,
					'medianame' => basename($media_url),
					'src' => $media_url,
					'media_type' => $post_mime_type,
					'title'=> $post_title,
					'post_type' => $value->post_type,
					'page_builder_name' => "",
					'post_category' => "",
					'variant_attribute' => '',
					'variant_sku' => '',
					'datetime' => $mediauploaddate,
					'linked' =>'No',
					'source_from' => 'From Media Library',
					'website_prefix' => $wpdb->prefix
				);
				$attachmentmetadata = $wpdb->get_results("select meta_value from ".$wpdb->prefix."postmeta where post_id='$media_id' AND meta_key='_wp_attachment_metadata'");
				$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
				if($unserializemetadata['sizes']){
					foreach ($unserializemetadata['sizes'] as $metavalue) {					
						$murl = $mediabaseurl.'/'.$metavalue['file'];
						$mediaexistdata[] = array(
							'media_id' => $media_id,
							'medianame' => $metavalue['file'],
							'src' => $murl,
							'media_type' => $metavalue['mime-type'],
							'title'=> $value->post_title,
							'post_type' => $value->post_type,
							'page_builder_name' => $pagebuildername,
							'post_category' => $post_cat,
							'variant_attribute' => '',
							'variant_sku' => '',
							'datetime' => $mediauploaddate,
							'linked' =>'No',
							'source_from' => 'From Media Library',
							'website_prefix' => $wpdb->prefix
						);					
					}				
				}else{
					$video_format = $unserializemetadata['mime_type'];
					if (strpos($video_format, 'video') !== false || strpos($video_format, 'audio') !== false) {
						$attachmentsdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where ID='$media_id'");
						$mediaexistdata[] = array(
							'media_id' => $attachmentsdata[0]->ID,
							'medianame' => basename($attachmentsdata[0]->guid),
							'src' => $attachmentsdata[0]->guid,
							'media_type' => $video_format,
							'title'=> $value->post_title,
							'post_type' => $value->post_type,
							'page_builder_name' => '',
							'post_category' => '',
							'variant_attribute' => '',
							'variant_sku' => '',
							'datetime' => $attachmentsdata[0]->post_date,
							'linked' =>'No',
							'source_from' => 'From Media Library',
							'website_prefix' => $wpdb->prefix
						);
					}
				}
			}
		}
		return $mediaexistdata;
	}
	//GET MULTISITE ATTACHMENT DETAILS FROM DATABASE
	public function getattAchmentDataMultiSite(){
		global $wpdb;
		$mediaexistdata=  array();
		$pagebuildername = '';
		$mimetype = "image/jpeg";
		$prefixes = $this->getAllSitePrefix();
		foreach ($prefixes as $mutliarray) {
			$prefixValue = $mutliarray['prefix'];
			$multisiteId = $mutliarray['multisite_id'];
			foreach (get_sites() as $all_sites) {
				if($all_sites->blog_id == $multisiteId){
					$multisite_url = $all_sites->path;
				}
				if($all_sites->blog_id == 1){
					$mainsite_url = $all_sites->path;
				}
	        }
			// Get multisite title
			$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
			$site_name = $current_blog_details->blogname;
			$attachmentdata = $wpdb->get_results("SELECT * from ".$prefixValue."posts where post_type='attachment'");		
			if(!empty($attachmentdata)){
				$attachmentarray = array();
				foreach ($attachmentdata as $key => $value) {
					$dir_path =  str_replace(get_site_url().'/',get_home_path(),$value->guid);
			        $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
					$fetch_only_images = mime_content_type($multisite_url_dir);				
					if(strpos($fetch_only_images, 'image') !== false){
						$media_id = $value->ID;
						$media_url = $value->guid;
						$mediauploaddate = $value->post_date;
						$media_name = substr($media_url, strrpos($media_url, '/') + 1);
						$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
						$attachmentmetadata = $wpdb->get_results("select meta_value from ".$prefixValue."postmeta where post_id='$media_id' AND meta_key='_wp_attachment_metadata'");
						$unserializemetadata = unserialize($attachmentmetadata[0]->meta_value);
						if($unserializemetadata['sizes']){
							foreach ($unserializemetadata['sizes'] as $metavalue) {					
								$murl = $mediabaseurl.'/'.$metavalue['file'];
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $metavalue['file'],
									'src' => $murl,
									'media_type' => $metavalue['mime-type'],
									'title'=> $value->post_title,
									'post_type' => $value->post_type,
									'page_builder_name' => $pagebuildername,
									'post_category' => $post_cat,
									'variant_attribute' => '',
									'variant_sku' => '',
									'datetime' => $mediauploaddate,
									'linked' =>'No',
									'source_from' => 'From Media Library'
								);					
							}				
						}else{
							$video_format = $unserializemetadata['mime_type'];
							if (strpos($video_format, 'video') !== false || strpos($video_format, 'audio') !== false) {
								$attachmentsdata = $wpdb->get_results("select * from ".$prefixValue."posts where ID='$media_id'");
								$mediaexistdata[] = array(
									'media_id' => $attachmentsdata[0]->ID,
									'medianame' => basename($attachmentsdata[0]->guid),
									'src' => $attachmentsdata[0]->guid,
									'media_type' => $video_format,
									'title'=> $value->post_title,
									'post_type' => $value->post_type,
									'page_builder_name' => '',
									'post_category' => '',
									'variant_attribute' => '',
									'variant_sku' => '',
									'datetime' => $attachmentsdata[0]->post_date,
									'linked' =>'No',
									'source_from' => 'From Media Library'
								);					
							}
						}
					}
				}			
			}
		}
		return $mediaexistdata;
	}
	public function wpmc_get_cpt() {
		global $wp_post_types;
		$cptnames = array();
		// Get All Post Types as List
	    foreach ( $wp_post_types as $postkey => $postsubvalue ) {
	    	if($postsubvalue->public == 1 ){
	    		if($postkey != "attachment"){
	    			$cptnames[] = $postkey;
	    		}
	      	}
	    }
	    $optionhtml = '';
	    foreach ($cptnames as $value) {
   			$optionhtml .= '<option value="'.$value.'">'.$value.'</option>';
   			}
   		echo $optionhtml;
   		exit;
	}
	public function wpmc_get_categories() {
		$cat_args=array(
		  'orderby' => 'name',
		  'order' => 'ASC'
		);
		$categories = get_categories($cat_args);
		$categoryoptionhtml = '';
		if(!empty($categories)){			
			foreach ( $categories as $category ) {
				$categoryoptionhtml .= '<option value="' . $category->name . '">'.$category->name.'</option>';
			}
		}else{
			$categoryoptionhtml .= '<option value="">No used category found</option>';
		}	
		
   		echo $categoryoptionhtml;
   		exit;
	}
	public function wpmc_get_image_extensions() {
		global $wpdb;
		$directory_path = $_POST['home_url'];
		$Domain = get_site_url();
		$type = $_POST['type'];
		if($type == "only_media_library"){
			$atdata1 = $this->getattachmentdata();
			if(is_multisite()){
				$atdata2 = $this->getattAchmentDataMultiSite();
			}else{
				$atdata2 = array();
			}
			if(!empty($atdata1) && !empty($atdata2)){
				$atdata = array_merge($atdata1,$atdata2);
			}elseif(!empty($atdata1) && empty($atdata2)){
				$atdata = $atdata1;
			}elseif(empty($atdata1) && !empty($atdata2)){
				$atdata = $atdata2;
			}else{
				$atdata = array();
			}

			if ( class_exists( 'WooCommerce' ) ) {
			  $newdata = $this->getattachmentproducts();
			} else {
			  $newdata = array();
			}
			if ( class_exists( 'WooCommerce' ) ) {
				if(is_multisite()){
					$newdata2 = $this->getMultisiteAttachmentProducts();
				}else{
					$newdata2 = array();
				}
			}
			if($newdata && $newdata2){
				$prodArrss = array_merge($newdata,$newdata2);
			}else{
				if($newdata){
					$prodArrss = $newdata;
				}elseif($newdata2){
					$prodArrss = $newdata2;
				}else{
					$prodArrss = array();
				}
			}

			if($prodArrss && $getImgs){
				$result2 = array_merge($prodArrss,$getImgs);
			}else{
				if($prodArrss){
					$result2 = $prodArrss;
				}elseif($getImgs){
					$result2 = $getImgs;
				}else{
					$result2 = array();
				}
			}

			if($atdata && $result2){
				$lresult = array_merge($result2,$atdata);
			}else{
				if($atdata){
					$lresult = $atdata;
				}elseif($result2){
					$lresult = $result2;
				}else{
					$lresult = array();
				}
			}
			$result = array_unique($lresult, SORT_REGULAR);
		}elseif($type == "all_media" || $type == "only_images"){
			$response = $this->walkDir($directory_path);
			$response = array_unique($response, SORT_REGULAR);

			$websiteMedia = $this->getinboundLinks($Domain);
			$webDirArray = array();
			// For website images
			foreach ($websiteMedia as $WebImages) {
				$webDirArray[]['src'] = $WebImages['src'];
			}
			// For directory images
			foreach ($response as $WebImages) {
				$webDirArray[]['src'] = $WebImages['src'];
			}

			$PageBuilderMedia = $this->getPageBuilderContentMedia();
			$getImgs = array_unique($getImgs, SORT_REGULAR);
			if(is_multisite()){
				$multisitePageBuilderMedia = $this->getMultiSitePageBuilderContentMedia();
			}else{
				$multisitePageBuilderMedia = array();
			}
			// Merge page builder content for single and multisite
			if(!empty($PageBuilderMedia) && !empty($multisitePageBuilderMedia)){
				$getImgs = array_merge($PageBuilderMedia,$multisitePageBuilderMedia);
			}elseif(empty($PageBuilderMedia) && !empty($multisitePageBuilderMedia)){
				$getImgs = $multisitePageBuilderMedia;
			}elseif(!empty($PageBuilderMedia) && empty($multisitePageBuilderMedia)){
				$getImgs = $PageBuilderMedia;
			}

			$atdata1 = $this->getattachmentdata();
			if(is_multisite()){
				$atdata2 = $this->getattAchmentDataMultiSite();
			}else{
				$atdata2 = array();
			}
			if(!empty($atdata2)){
				$atdata = array_merge($atdata1,$atdata2);
			}else{
				$atdata = $atdata1;
			}

			if ( class_exists( 'WooCommerce' ) ) {
			  $newdata = $this->getattachmentproducts();
			} else {
			  $newdata = array();
			}

			if(is_multisite()){
				$newdata2 = $this->getMultisiteAttachmentProducts();
			}else{
				$newdata2 = array();
			}
			if($atdata && $newdata){
				$lresult = array_merge($newdata,$atdata);
			}else{
				if($atdata){
					$lresult = $atdata;
				}elseif($newdata){
					$lresult = $newdata;
				}else{
					$lresult = array();
				}
			}
			if($lresult && $getImgs){
				$result2 = array_merge($lresult,$getImgs);
			}else{
				if($lresult){
					$result2 = $lresult;
				}elseif($getImgs){
					$result2 = $getImgs;
				}else{
					$result2 = array();
				}
			}

			if($result2 && $webDirArray){
				$result = array_merge($result2, $webDirArray);
			}else{
				if($result2){
					$result = $result2;
				}elseif($webDirArray){
					$result = $webDirArray;
				}else{
					$result = array();
				}
			}
		}
		
		foreach ($result as $AllSrcValue) {	
			$image_extension[] = pathinfo($AllSrcValue['src'], PATHINFO_EXTENSION);
		}
		$response = array_unique($image_extension);
		$array = array_values($response);
		$data = '';
		foreach ($array as $value) {
			if($type == "only_images"){
				$totalExtensionsAvailable = array('jpg','png','gif','svg');
				if(in_array($value, $totalExtensionsAvailable)){
					$data .= '<option value="'.$value.'">'.strtoupper($value).'</option>';
					$row_name = "wp_media_quality_".$value;		
					// Check if extension is already present with quality
					$if_available = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."options WHERE option_name = '$row_name'");
					$the_name = $if_available[0]->option_name;
					if(empty($the_name)){
						$is_update_quality = update_option( $row_name, 0 );
					}
				}
			}else{
				$data .= '<option value="'.$value.'">'.strtoupper($value).'</option>';
				$row_name = "wp_media_quality_".$value;		
				// Check if extension is already present with quality
				$if_available = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."options WHERE option_name = '$row_name'");
				$the_name = $if_available[0]->option_name;
				if(empty($the_name)){
					$is_update_quality = update_option( $row_name, 0 );
				}
			}			
		}
		echo $data;
		exit;
	}	
	public function support_form(){
		$name = $_POST['name'];
		$email = $_POST['email'];
		$subject = $_POST['subject'];
		$license = $_POST['license'];
		$message = $_POST['message'];

		$body = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
		<head>
			<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
			<meta content="width=device-width" name="viewport"/>
			<!--[if !mso]><!-->
			<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
			<!--<![endif]-->
			<title></title>
			<!--[if !mso]><!-->
			<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
			<!--<![endif]-->
			<style type="text/css">
				body {
					margin: 0;
					padding: 0;
				}

				table,
				td,
				tr {
					vertical-align: top;
					border-collapse: collapse;
				}

				* {
					line-height: inherit;
				}

				a[x-apple-data-detectors=true] {
					color: inherit !important;
					text-decoration: none !important;
				}
			</style>
			<style id="media-query" type="text/css">
				@media (max-width: 670px) {
					.block-grid,
					.col {
						min-width: 320px !important;
						max-width: 100% !important;
						display: block !important;
					}

					.block-grid {
						width: 100% !important;
					}

					.col {
						width: 100% !important;
					}

					.col>div {
						margin: 0 auto;
					}

					img.fullwidth,
					img.fullwidthOnMobile {
						max-width: 100% !important;
					}

					.no-stack .col {
						min-width: 0 !important;
						display: table-cell !important;
					}

					.no-stack.two-up .col {
						width: 50% !important;
					}

					.no-stack .col.num4 {
						width: 33% !important;
					}

					.no-stack .col.num8 {
						width: 66% !important;
					}

					.no-stack .col.num4 {
						width: 33% !important;
					}

					.no-stack .col.num3 {
						width: 25% !important;
					}

					.no-stack .col.num6 {
						width: 50% !important;
					}

					.no-stack .col.num9 {
						width: 75% !important;
					}

					.video-block {
						max-width: none !important;
					}

					.mobile_hide {
						min-height: 0px;
						max-height: 0px;
						max-width: 0px;
						display: none;
						overflow: hidden;
						font-size: 0px;
					}

					.desktop_hide {
						display: block !important;
						max-height: none !important;
					}
				}
			</style>
		</head>
		<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #F6F5FF;">
		<!--[if IE]><div class="ie-browser"><![endif]-->
		<table bgcolor="#F6F5FF" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F6F5FF; width: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td style="word-break: break-word; vertical-align: top;" valign="top">
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
		<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
		<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 0px; width: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid two-up no-stack" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="325" style="background-color:transparent;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:7px; padding-bottom:5px;"><![endif]-->
		<div class="col num6" style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top; width: 325px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:7px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<div align="left" class="img-container left fixedwidth" style="padding-right: 0px;padding-left: 10px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 10px;" align="left"><![endif]--><img alt="Image" border="0" class="left fixedwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/logo.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 243px; display: block;" title="Image" width="243"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td><td align="center" width="325" style="background-color:transparent;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
		<div class="col num6" style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top; width: 325px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 15px; padding-left: 0px;">
		<!--<![endif]-->
		<table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
		<table activate="activate" align="right" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: undefined; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" to="to" valign="top">
		<tbody>
		<tr align="right" style="vertical-align: top; display: inline-block; text-align: right;" valign="top">
		<td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 0px; padding-left: 5px;" valign="top"><a href="https://www.facebook.com/" target="_blank"><img alt="Facebook" height="32" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/facebook@2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: none; display: block;" title="Facebook" width="32"/></a></td>
		<td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 0px; padding-left: 5px;" valign="top"><a href="https://twitter.com/" target="_blank"><img alt="Twitter" height="32" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/twitter@2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: none; display: block;" title="Twitter" width="32"/></a></td>
		<td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 0px; padding-left: 5px;" valign="top"><a href="https://instagram.com/" target="_blank"><img alt="Instagram" height="32" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/instagram@2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: none; display: block;" title="Instagram" width="32"/></a></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
		<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 0px; width: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<div align="center" class="img-container center autowidth fullwidth">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center autowidth fullwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/Top.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 650px; display: block;" title="Image" width="650"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#FFFFFF"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#FFFFFF;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 30px; padding-left: 30px; padding-top:25px; padding-bottom:10px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:10px; padding-right: 30px; padding-left: 30px;">
		<!--<![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 5px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#B1AED1;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:5px;padding-left:10px;">
		<div style="font-size: 16px; line-height: 1.2; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; color: #B1AED1; mso-line-height-alt: 19px;">
		<p style="font-size: 20px; line-height: 1.2; text-align: center; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 20px;">Hello There</span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 5px; padding-left: 5px; padding-top: 0px; padding-bottom: 5px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
		<div style="color:#454562;font-family:\'Roboto\', Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:0px;padding-right:5px;padding-bottom:5px;padding-left:5px;">
		<div style="line-height: 1.2; font-size: 12px; font-family: \'Roboto\', Tahoma, Verdana, Segoe, sans-serif; color: #454562; mso-line-height-alt: 14px;">
		<p style="line-height: 1.2; font-size: 38px; text-align: left; mso-line-height-alt: 46px; margin: 0;"><span style="font-size: 38px;"><span style="font-size: 18px;">An user just sent a message from</span> <span style="font-size: 20px;"><span style="color: #eb2a77; font-size: 20px;">WP Media Cleaner</span> Support Forum</span></span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:30px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:30px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 40px; padding-left: 40px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#555555;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.5;padding-top:10px;padding-right:40px;padding-bottom:10px;padding-left:40px;">
		<div style="font-size: 14px; line-height: 1.5; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; color: #555555; mso-line-height-alt: 21px;">
		<p style="font-size: 14px; line-height: 1.5; text-align: center; mso-line-height-alt: 21px; margin: 0;">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:31.5pt; width:168pt; v-text-anchor:middle;" arcsize="120%" stroke="false" fillcolor="#E53373"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a href="#" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #E53373; border-radius: 50px; -webkit-border-radius: 50px; -moz-border-radius: 50px; width: auto; width: auto; border-top: 1px solid #E53373; border-right: 1px solid #E53373; border-bottom: 1px solid #E53373; border-left: 1px solid #E53373; padding-top: 5px; padding-bottom: 5px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:50px;padding-right:50px;font-size:16px;display:inline-block;">
		<span style="font-size: 16px; line-height: 2; mso-line-height-alt: 32px;"><strong>GO TO WEBSITE</strong></span>
		</span></a>
		<!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
		</div>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
		<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 0px; width: 100%;" valign="top" width="100%">
		<tbody>
		<tr style="vertical-align: top;" valign="top">
		<td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
		</tr>
		</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<div align="center" class="img-container center autowidth fullwidth" style="padding-right: 0px;padding-left: 0px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center autowidth fullwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/Top_blu.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 650px; display: block;" title="Image" width="650"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #4DB9AB;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:#4DB9AB;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#4DB9AB"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#4DB9AB;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 30px; padding-left: 30px; padding-top:20px; padding-bottom:20px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:20px; padding-right: 30px; padding-left: 30px;">
		<!--<![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 5px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#A8A3F2;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:5px;padding-left:10px;">
		<div style="line-height: 1.2; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; font-size: 12px; color: #A8A3F2; mso-line-height-alt: 14px;">
		<p style="line-height: 1.2; text-align: center; font-size: 20px; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 20px;background: #e53273;color: #fff;padding: 6px;">WP MEDIA CLEANER</span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 0px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
		<div style="color:#FFFFFF;font-family:\'Roboto\', Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;margin-top: 12px">
		<div style="font-size: 16px; line-height: 1.2; font-family: \'Roboto\', Tahoma, Verdana, Segoe, sans-serif; color: #FFFFFF; mso-line-height-alt: 19px;">
		<p style="font-size: 34px; line-height: 1.2; text-align: center; mso-line-height-alt: 41px; margin: 0;"><span style="font-size: 34px;">USER DETAILS</span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 30px; padding-left: 30px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#FFFFFF;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.5;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
		<div style="font-size: 14px; line-height: 1.5; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; color: #FFFFFF; mso-line-height-alt: 21px;">
		<p style="font-size: 14px; line-height: 1.5; text-align: center; mso-line-height-alt: 21px; margin: 0;">Here are some details of user</p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid three-up" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #4DB9AB;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:#4DB9AB;background-image:url(\'https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/bg_cta.jpg\');background-position:top center;background-repeat:no-repeat">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#4DB9AB"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="216" style="background-color:#4DB9AB;width:216px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 15px; padding-top:5px; padding-bottom:5px;"><![endif]-->
		<div class="col num4" style="max-width: 320px; min-width: 216px; display: table-cell; vertical-align: top; width: 216px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 10px; padding-left: 15px;">
		<!--<![endif]-->
		<div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center fixedwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/starter_pack.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 124px; display: block;" title="Image" width="124"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
		<div style="color:#FFFFFF;font-family:\'Roboto\', Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="line-height: 1.2; font-family: \'Roboto\', Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; color: #FFFFFF; mso-line-height-alt: 14px;">
		<p style="line-height: 1.2; text-align: center; font-size: 15px; mso-line-height-alt: 18px; margin: 0;"><span style="background-color: #E53373; font-size: 15px;"><strong> <span style="font-size: 15px;">NAME </span></strong></span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#F6F5FF;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="font-family: \'Open Sans\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.2; color: #F6F5FF; mso-line-height-alt: 14px;">
		<p style="font-size: 14px; line-height: 1.2; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$name.'</p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td><td align="center" width="216" style="background-color:#4DB9AB;width:216px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top:5px; padding-bottom:5px;"><![endif]-->
		<div class="col num4" style="max-width: 320px; min-width: 216px; display: table-cell; vertical-align: top; width: 216px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 10px; padding-left: 10px;">
		<!--<![endif]-->
		<div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center fixedwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/team_pack.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 128px; display: block;" title="Image" width="128"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
		<div style="color:#FFFFFF;font-family:\'Roboto\', Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="line-height: 1.2; font-family: \'Roboto\', Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; color: #FFFFFF; mso-line-height-alt: 14px;">
		<p style="line-height: 1.2; text-align: center; font-size: 15px; mso-line-height-alt: 18px; margin: 0;"><span style="font-size: 15px; background-color: #E53373;"><strong> LICENSE KEY</strong></span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#F6F5FF;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="font-family: \'Open Sans\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.2; color: #F6F5FF; mso-line-height-alt: 14px;">
		<p style="font-size: 14px; line-height: 1.2; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$license.'</p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td><td align="center" width="216" style="background-color:#4DB9AB;width:216px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 10px; padding-top:5px; padding-bottom:5px;"><![endif]-->
		<div class="col num4" style="max-width: 320px; min-width: 216px; display: table-cell; vertical-align: top; width: 216px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 15px; padding-left: 10px;">
		<!--<![endif]-->
		<div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center fixedwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/enterprise_pack.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 124px; display: block;" title="Image" width="124"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Tahoma, Verdana, sans-serif"><![endif]-->
		<div style="color:#FFFFFF;font-family:\'Roboto\', Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="line-height: 1.2; font-family: \'Roboto\', Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; color: #FFFFFF; mso-line-height-alt: 14px;">
		<p style="line-height: 1.2; text-align: center; font-size: 15px; mso-line-height-alt: 18px; margin: 0;"><span style="font-size: 15px; background-color: #E53373;"><strong>SUBJECT</strong></span></p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#F6F5FF;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="font-family: \'Open Sans\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.2; color: #F6F5FF; mso-line-height-alt: 14px;">
		<p style="font-size: 14px; line-height: 1.2; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$subject.'</p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>

		<div style="background-color:transparent;">
		<div class="block-grid" style="margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #4DB9AB;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:#4DB9AB;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#4DB9AB"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#4DB9AB;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:25px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:25px; padding-right: 0px; padding-left: 0px;">
		<div style="color:#FFFFFF;font-family:\'Roboto\', Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="line-height: 1.2; font-family: \'Roboto\', Tahoma, Verdana, Segoe, sans-serif; font-size: 12px; color: #FFFFFF; mso-line-height-alt: 14px;">
		<p style="line-height: 1.2; text-align: center; font-size: 15px; mso-line-height-alt: 18px; margin: 0;"><span style="background-color: #E53373; font-size: 15px;"><strong>&nbsp;<span style="font-size: 15px;">MESSAGE&nbsp;</span></strong></span></p>
		</div>
		</div>
		<div style="color:#F6F5FF;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
		<div style="font-family: \'Open Sans\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.2; color: #F6F5FF; mso-line-height-alt: 14px;">
		<p style="font-size: 14px; line-height: 1.2; text-align: center; mso-line-height-alt: 17px; margin: 0;">'.$message.'</p>
		</div>
		</div>
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>

		<div style="background-color:transparent;">
		<div class="block-grid" style="margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #4DB9AB;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:#4DB9AB;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#4DB9AB"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#4DB9AB;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:25px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:25px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:31.5pt; width:154.5pt; v-text-anchor:middle;" arcsize="120%" stroke="false" fillcolor="#E53373"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a href="#" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #E53373; border-radius: 50px; -webkit-border-radius: 50px; -moz-border-radius: 50px; width: auto; width: auto; border-top: 1px solid #E53373; border-right: 1px solid #E53373; border-bottom: 1px solid #E53373; border-left: 1px solid #E53373; padding-top: 5px; padding-bottom: 5px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:50px;padding-right:50px;font-size:16px;display:inline-block;">
		<span style="font-size: 16px; line-height: 2; mso-line-height-alt: 32px;"><strong>LEARN MORE</strong></span>
		</span></a>
		<!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
		</div>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:transparent;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
		<!--<![endif]-->
		<div align="center" class="img-container center autowidth fullwidth" style="padding-right: 0px;padding-left: 0px;">
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img align="center" alt="Image" border="0" class="center autowidth fullwidth" src="https://dev.marketingmindz.com/mediacleaner-2/wp-content/plugins/wp_media_cleaner/assets/images/email/Btm_blu.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 650px; display: block;" title="Image" width="650"/>
		<!--[if mso]></td></tr></table><![endif]-->
		</div>
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<div style="background-color:#FFFFFF;">
		<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#FFFFFF;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top:15px; padding-bottom:15px;"><![endif]-->
		<div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
		<div style="width:100% !important;">
		<!--[if (!mso)&(!IE)]><!-->
		<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 20px; padding-left: 20px;">
		<!--<![endif]-->
		<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
		<div style="color:#6B6B6B;font-family:\'Open Sans\', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
		<div style="font-family: \'Open Sans\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.2; color: #6B6B6B; mso-line-height-alt: 14px;">
		<p style="font-size: 14px; line-height: 1.2; text-align: center; mso-line-height-alt: 17px; margin: 0;">123 Creative street, San Francisco / All rights reserved<br/>Made with <span style="font-size: 14px; color: #E53373;">❤</span> </p>
		</div>
		</div>
		<!--[if mso]></td></tr></table><![endif]-->
		<!--[if (!mso)&(!IE)]><!-->
		</div>
		<!--<![endif]-->
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
		</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		</td>
		</tr>
		</tbody>
		</table>
		<!--[if (IE)]></div><![endif]-->
		</body>
		</html>';
		$wp_admin_email = get_option('wp_admin_email');
		$from = 'From: WP Media Cleaner <'. $email . " >\r\n";
		$headers = array('Content-Type: text/html; charset=UTF-8',$from);

		$if_sent = wp_mail( $wp_admin_email, $subject, $body, $headers );
		print_r($if_sent);
	}
	public function wpmc_media_default_action(){
		global $wpdb;
		$type = $_POST['type'];
		$value = $_POST['option'];
		if($type == "media_scan"){
			$is_updated = update_option( "media_scan_activate", $value, true );
		}elseif($type == "content_scan"){
			$is_updated = update_option( "content_scan_activate", $value, true );
		}elseif($type == "image_optimize"){
			$is_updated = update_option( "image_optimize_activate", $value, true );
		}elseif($type == "database_cleaner"){
			$is_updated = update_option( "database_cleaner_activate", $value, true );
		}elseif($type == "database_backup"){
			$is_updated = update_option( "database_backup_activate", $value, true );
		}
		$res = array("result"=>$is_updated);
		echo json_encode($res);
		exit();
	}
	public function wpmc_lisence_activation(){
		$licenses_key = $_POST['license_key'];
		$site_name = site_url();
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://localhost/wpthemes/test_plugin/wp-json/lmfwc/v2/licenses/activate/".$licenses_key."?consumer_key=ck_7967037f1775d8e5014a00cf22f2b735f65df3e3&consumer_secret=cs_b3b8ea1c7aabdfa752654fb5cba0e58b1a3acdb4&site_name=".$site_name,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			$message = __("Something went wrong, Please try after some time.","wp_media_cleaner");
			$data = array("date"=>$valid_till_date,"types"=>"expire_date","message"=>$message);
		} else {
			$result = json_decode($response);
			if (array_key_exists("success",$result)){
				$days = __(" Days","wp_media_cleaner");
				$valid_for_days = $result->data->validFor.$days;
				$valid_till_date = $result->data->expiresAt;
				if($valid_for_days){
					$message = __("Your license will be expired in ","wp_media_cleaner");
					$expire_message = $message.$valid_for_days;
					$data = array("date"=>$valid_for_days,"types"=>"remaining_day","message"=>$expire_message);
				}else{
					$message = __("Your license will be expired on ","wp_media_cleaner");
					$expire_message = $message.$valid_till_date;
					$data = array("date"=>$valid_till_date,"types"=>"expire_date","message"=>$expire_message);
				}
				update_option( "WPMC_lisence_activation", 1, true );
				update_option( "WPMC_lisence_key", $licenses_key, true );
			}else{
				$response_message = $result->message;
				$message = __($response_message,"wp_media_cleaner");
				$data = array("date"=>$valid_till_date,"types"=>"expire_date","message"=>$message);
				// update_option( "WPMC_lisence_activation", 0, true );
				// delete_option( "WPMC_lisence_key");
			}
			echo json_encode($data);
		}
		exit;
	}
	public function wpmc_lisence_deactivation(){
		$licenses_key = $_POST['license_key'];
		$site_name = site_url();
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://localhost/wpthemes/test_plugin/wp-json/lmfwc/v2/licenses/deactivate/".$licenses_key."?consumer_key=ck_7967037f1775d8e5014a00cf22f2b735f65df3e3&consumer_secret=cs_b3b8ea1c7aabdfa752654fb5cba0e58b1a3acdb4&site_name=".$site_name,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			$message = __("Something went wrong, Please try again.","wp_media_cleaner");
			$data = array("types"=>"deactivate","message"=>$message);
		} else {
			$result = json_decode($response);
			if (array_key_exists("success",$result)){
				$message = __("The License is successfully deactivated.","wp_media_cleaner");
				$data = array("types"=>"deactivate","message"=>$message);
				update_option( "WPMC_lisence_activation", 0, true );
				delete_option( "WPMC_lisence_key");
			}else{
				$response_message = $result->message;
				$message = __($response_message,"wp_media_cleaner");
				$data = array("types"=>"deactivate","message"=>$message);
			}
			echo json_encode($data);
		}
		exit;
	}
}