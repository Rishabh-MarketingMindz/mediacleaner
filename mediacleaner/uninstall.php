<?php
if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();
    global $wpdb;
    $table1 = $wpdb->prefix.'mediascan_data';
    $table2 = $wpdb->prefix.'contentscan_data';
    $table3 = $wpdb->prefix.'image_optimizer';
    $table4 = $wpdb->prefix.'optimizer_backup';
    $wpdb->query( "DROP TABLE IF EXISTS $table1" );
    $wpdb->query( "DROP TABLE IF EXISTS $table2" );
    $wpdb->query( "DROP TABLE IF EXISTS $table3" );
    $wpdb->query( "DROP TABLE IF EXISTS $table4" );
    // delete_option("my_plugin_db_version");