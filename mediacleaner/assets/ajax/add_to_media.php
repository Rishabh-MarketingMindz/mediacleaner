<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
global $wpdb;
$delArr = array();
$media_src = $_POST['media_src'];
$count = 1;
$file_exists = '';
foreach ($media_src as $source) {
  $image_src = wp_upload_dir()['url'].'/'.basename($source);
  $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_src ));
  if(empty($attachment)){
    $filename = basename($source);
    $upload_file = wp_upload_bits($filename, null, file_get_contents($source));
    if (!$upload_file['error']) {
      $wp_filetype = wp_check_filetype($filename, null );
      $attachments = array(
        'guid'           => $image_src,
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
        'post_content' => '',
        'post_status' => 'inherit'
      );
      $attachment_id = wp_insert_attachment( $attachments, $upload_file['file'] );
      if (!is_wp_error($attachment_id)) {
        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
        $result = wp_update_attachment_metadata( $attachment_id,  $attachment_data );
        if($result){
          $delArr[] = $file;
          $count++;
        }
      }
    }
  }else{
    $file_exists .= $source.'<br>'; 
  }
}
$result = json_encode($delArr);
echo $result;
// $total_img = $count;
// $resp = '';
// $resp .= '<div><p>'.$total_img.' Image(s) Added</p></div>';
// if($file_exists){
//   $resp .= '<div>These media are already exists: <br><strong>'.$file_exists.'</strong></div>';
// }
// echo $resp;