<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
global $wpdb;

$media_src = $_POST['media_src'];
$backup = $_POST['backup'];
$media_prefix = $_POST['media_prefix'];

$optimization_quality = $_POST['opt_quality'];
$request_type = $_POST['request_type'];
$counts = 0;
foreach ($optimization_quality as $extractQuality) {
  $newArr = explode(",", $extractQuality);
  $QualityArr[$counts]['quality'] = $newArr[0];
  $QualityArr[$counts]['type'] = $newArr[1];
  $counts++;
}
$count = 1;
$success = '';
$error = '';

// For Bulk Image Optimization
$image_src = $_POST['image_src'];
$prefixes = $_POST['prefixes'];
$imgArr = array();
$i = 0;
foreach ($image_src as $img_value) {
  $imgArr[$i]['image_url'] = $img_value;
  $i++;
}
$j = 0;
foreach ($prefixes as $img_prefix) {
  $imgArr[$j]['image_prefix'] = $img_prefix;
  $j++;
}


// Get directory path where to save backup images start
$directory_path = rtrim(get_home_path(), '/');
$wp_root_pathh = $directory_path.'/wp-content/plugins/wp_media_cleaner/backup/optimization/';
define('DIRECTORY', $wp_root_pathh);
// Get directory path where to save backup images ends

if($request_type == "single_image_opt"){
  $image_url = $media_src;
  // Get absolute path of URL
  $root_path = get_home_path();
  // Site URL
  $wp_site_url = get_site_url();
  $destination_media_name = basename($image_url);
  // URL of image where it will stored for backup
  $destination_media_url = $wp_root_pathh.$destination_media_name;

  preg_match('!\d+!', $media_prefix, $matches);
  $get_site_id = $matches[0];
  if(is_multisite()){
    // get main site path
    $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
    $mainsite_url = $current_blog_details->path;

    // get multi site path
    $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
    $multisite_url = $current_blog_details2->path;
    if($get_site_id){
      $img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$media_src);
      $dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
    }else{
      $dir_path = str_replace(get_site_url().'/',get_home_path(),$media_src);
    }
  }else{
    $dir_path = str_replace(get_site_url().'/',get_home_path(),$media_src);
  }

  $destination_dir_path = str_replace($wp_site_url.'/',$root_path,$destination_media_url);
  
  // $dir_path =  str_replace($wp_site_url.'/',$root_path,$image_url);
  chmod($dir_path, 0664);
  // get image quality as per image type
  $info = getimagesize($dir_path);
  foreach ($QualityArr as $ImgQuality) {
    $img_type = $ImgQuality['type'];
    if($img_type == 'jpg'){
      $img_type = 'jpeg';
    }
    $mime_type = $info['mime'];
    $mime_type = str_replace("image/", "", $mime_type);
    if($img_type == $mime_type){
      $img_quality = $ImgQuality['quality'];
    }
  }  
  if($backup == 'yes'){    
    $table_name = $wpdb->prefix."optimizer_backup";
    // Check if requested images are not in Excluded table start
    $exclude_array = $wpdb->get_results("SELECT * from $table_name where type = 'exclude' AND Image_url = '$image_url'");
    // Check if requested images are not in Excluded table ends
    if(empty($exclude_array)){
      // For saving image in backup folder
      if(file_exists($destination_dir_path)){
        $url_exist = $wpdb->get_results("SELECT * from $table_name WHERE Image_url ='$image_url' AND (type IS NULL OR type = 'restored_original')");
        $if_have = count($url_exist);
        if($if_have>0){
          $update_image = $wpdb->query( $wpdb->prepare("UPDATE $table_name SET optimization_amount = '$img_quality', type = NULL WHERE Image_url = '$image_url'"));
        }else{
          $update_image = $wpdb->insert( $table_name, array("Image_url" => $image_url,"optimization_amount" => $img_quality));
        }
      }else{
        $url_exist = $wpdb->get_results("SELECT * from $table_name where Image_url ='$image_url' AND (type IS NULL OR type = 'restored_original')");
        $if_have = count($url_exist);
        if($if_have>0){
          $update_image = $wpdb->query( $wpdb->prepare("UPDATE $table_name SET optimization_amount = '$img_quality', type = NULL WHERE Image_url = '$image_url'"));
        }else{
          $update_image = $wpdb->insert( $table_name, array("Image_url" => $image_url,"optimization_amount" => $img_quality));
        }
        if($update_image){
          $content = file_get_contents($dir_path);
          $is_backup = file_put_contents(DIRECTORY . basename($image_url), $content);
          chmod(DIRECTORY . basename($image_url), 0664);
        }
      }
       // echo "dir_path: ".$dir_path." >> img_quality: ".$img_quality;
      $optimization_result = compressImage($dir_path,$dir_path,$img_quality);
    }
  }
  // else{
  //   $optimization_result = compressImage($dir_path,$dir_path,$img_quality);
  //   if($optimization_result){
  //     $success .= '1';
  //   }else{
  //     $error .= '1';
  //   }
  // }
}elseif($request_type == "bulk_optimization"){
  foreach ($imgArr as $img_arr) {
    $image_url = $img_arr['image_url'];
    $media_prefix = $img_arr['image_prefix'];
    // Check if requested images are not in Excluded table start
    $exclude_table_name = $wpdb->prefix."optimizer_backup";
    $exclude_array = $wpdb->get_results("SELECT * from $exclude_table_name where type = 'exclude' AND Image_url = '$image_url'");
    // Check if requested images are not in Excluded table ends

    if(empty($exclude_array)){
      // Get absolute path of URL
      $root_path = get_home_path();
      // Site URL
      $wp_site_url = get_site_url();
      $destination_media_name = basename($image_url);
      // URL of image where it will stored for backup
      $destination_media_url = $wp_root_pathh.$destination_media_name;

      preg_match('!\d+!', $media_prefix, $matches);
      $get_site_id = $matches[0];
      if(is_multisite()){
        // get main site path
        $current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
        $mainsite_url = $current_blog_details->path;

        // get multi site path
        $current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
        $multisite_url = $current_blog_details2->path;
        if($get_site_id){
          $img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$image_url);
          $dir_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
        }else{
          $dir_path = str_replace(get_site_url().'/',get_home_path(),$image_url);
        }
      }else{
        $dir_path = str_replace(get_site_url().'/',get_home_path(),$image_url);
      }
      $destination_dir_path = str_replace(get_site_url().'/',get_home_path(),$destination_media_url);

      chmod($dir_path, 0664);
      // get image quality as per image type
      $info = getimagesize($dir_path);
      foreach ($QualityArr as $ImgQuality) {
        $img_type = $ImgQuality['type'];
        if($img_type == 'jpg'){
          $img_type = 'jpeg';
        }
        $mime_type = $info['mime'];
        $mime_type = str_replace("image/", "", $mime_type);
        if($img_type == $mime_type){
          $img_quality = $ImgQuality['quality'];
        }
      }
      if($backup == 'yes'){
        $table_name = $wpdb->prefix."optimizer_backup";
        // For saving image in backup folder
        if(file_exists($destination_dir_path)){
          $url_exist = $wpdb->get_results("SELECT * from $table_name where Image_url ='$image_url' AND (type IS NULL OR type = 'restored_original')");
          $if_have = count($url_exist);
          if($if_have>0){
            $update_image = $wpdb->query( $wpdb->prepare("UPDATE $table_name SET optimization_amount = '$img_quality', type = NULL WHERE Image_url = '$image_url'"));
          }else{
            $update_image = $wpdb->insert( $table_name, array("Image_url" => $image_url,"optimization_amount" => $img_quality));
          }
        }else{
          $url_exist = $wpdb->get_results("SELECT * from $table_name where Image_url ='$image_url' AND (type IS NULL OR type = 'restored_original')");
          $if_have = count($url_exist);
          if($if_have>0){
            $update_image = $wpdb->query( $wpdb->prepare("UPDATE $table_name SET optimization_amount = '$img_quality', type = NULL WHERE Image_url = '$image_url'"));
          }else{
            $update_image = $wpdb->insert( $table_name, array("Image_url" => $image_url,"optimization_amount" => $img_quality));
          }
          if($update_image){
            $content = file_get_contents($image_url);
            $is_backup = file_put_contents(DIRECTORY . basename($image_url), $content);
            chmod(DIRECTORY . basename($image_url), 0664);
          }
        }
        $optimization_result = compressImage($dir_path,$dir_path,$img_quality);
      }else{
        $optimization_result = compressImage($dir_path,$dir_path,$img_quality);
        if($optimization_result){
          $success .= '1';
        }else{
          $error .= '1';
        }
      }
    }
  }
}
function png_has_transparency( $filename ) {
  if ( strlen( $filename ) == 0 || !file_exists( $filename ) )
      return false;        
  if ( ord ( file_get_contents( $filename, false, null, 25, 1 ) ) & 4 )
      return true;        
  $contents = file_get_contents( $filename );
  if ( stripos( $contents, 'PLTE' ) !== false && stripos( $contents, 'tRNS' ) !== false )
      return true;        
  return false;
}
function compressImage($source_url, $destination_url, $quality) {
  $info = getimagesize($source_url);
  if ($info['mime'] == 'image/jpeg') {
    $image = imagecreatefromjpeg($source_url);
    $is_succeed = imagejpeg($image, $destination_url, $quality);
  }
  elseif ($info['mime'] == 'image/gif') {
    $image = imagecreatefromgif($source_url);
    $is_succeed = imagejpeg($image, $destination_url, $quality);
  }
  elseif ($info['mime'] == 'image/png') {    
    $is_transparent = png_has_transparency( $source_url );
    if($is_transparent){
      $image = imagecreatefrompng($source_url);
      imagealphablending($image, true); // setting alpha blending on
      imagesavealpha($image, true); // save alphablending setting (important)
      $is_succeed = imagepng($image, $destination_url, $quality);
    }else{
      $image = imagecreatefrompng($source_url);
      $is_succeed = imagepng($image, $destination_url, $quality);
    }
  }
  //save file  
  chmod($destination_url, 0664);
  //return destination file
  return $is_succeed;
}

if($error){
  $optArr = 'Some images are not optimized, Please try again.';
}else{
  $optArr = 'All images are optimized successfully.';
}
echo $optArr;