<?php
error_reporting(0);
require 'simple_html_dom.php';

function getinboundLinks($domain_name) {
	$res = array();
	$arr = array();
	$url = $domain_name;
	$url_without_www=str_replace('http://','',$url);
	$url_without_www=str_replace('www.','',$url_without_www);
	$url_without_www= str_replace(strstr($url_without_www,'/'),'',$url_without_www);
	$url_without_www=trim($url_without_www);
	$input = @file_get_contents($url) or die('Could not access file: $url');
	$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
	if(preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) {
		
		foreach($matches as $match) {
			if (filter_var($match[2], FILTER_VALIDATE_URL) && $match[2] !='mailto:' && strpos($match[2], "http://localhost/wpthemes/wp_media_cleaner")!==false) {
				$res[] = $match[2];
			}
		}
	}
	$data = array();
	$i = 0;
	foreach ($res as $value) {
		$html = file_get_html($value);
		// For Images
		foreach($html->find('img') as $element) {
			$data[$i]['src'] = $element->src;
			//echo "<br>";
			$str = file_get_contents($value);
			if(strlen($str)>0){
				$str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
				preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
				$data[$i]['title'] = $title[1];
				//echo "<br>";
			}
		    $i++;
		}
	}
	echo "<pre>";
	print_r($data);
} 
// ************************Usage********************************
$Domain='http://localhost/wpthemes/wp_media_cleaner/';
getinboundLinks($Domain);