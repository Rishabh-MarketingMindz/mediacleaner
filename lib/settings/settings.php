<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';
@$lisence_key = get_option("WPMC_lisence_key");
?>

<style type="text/css">
  .alert-box {
    display: none;
    position: fixed;
    width: 25%;
    height: 60px;
    z-index: 99999999999;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
  }
  .form_error{
    border: 1px solid red !important;
  }
</style>
  <!-- Rotating logo -->
  <div class="loading" style="display: none">
    <div class="loading_box"><?php _e("Scanning for media files","wp_media_cleaner"); ?><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span> 
      <div class="progress_center">
        <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
        <div id="myProgress">
          <span id="progress_counter"></span>
          <div id="myBar"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Rotating logo for actions-->
  <div class="action_loader">
  <div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span><?php _e("LOADING...","wp_media_cleaner"); ?></span> </div>
  </div>
  <!-- Only Rotating logo -->
  <div class="loading_rotating" style="display: none;">
    <div class="loading_box"><div class="action_message"></div><span style="color: #fff"><?php _e("Do not close this window","wp_media_cleaner"); ?></span><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
    </div>
  </div>
<!-- Alert box -->
<div class="alert-box"></div>

<div class="WPMC_settings">
  <div class="left_block">
    <ul class="setting_menu">
      <li><a href="javascript:void(0)" class="license_section"><?php echo __('License'); ?></a></li>
      <!-- <li><a href="javascript:void(0)" class="bulk_job_option_section"><?php echo __('Bulk job options'); ?></a></li> -->
      <!-- <li><a href="javascript:void(0)" class="translate_section"><?php echo __('Translate'); ?></a></li> -->
      <li><a href="javascript:void(0)" class="deactivate_section"><?php echo __('Deactivate'); ?></a></li>
      <li><a href="javascript:void(0)" class="support_section"><?php echo __('Support'); ?></a></li>
      <li><a href="javascript:void(0)" class="system_requirements"><?php echo __('System requirements'); ?></a></li>
    </ul>
  </div>
  <div class="right_block">
    <div class="setting_section">
      <div class="setting_section_block license_section_block">
        <?php require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'license.php'; ?>
      </div>
      <!--
      <div class="setting_section_block bulk_job_option_section_block">
        <?php require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'bulk-job-option.php'; ?>
      </div>

      <div class="setting_section_block translate_section_block">
        <?php require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'translate.php'; ?>
      </div>
      -->
      <div class="setting_section_block deactivate_section_block">
        <?php require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'deactivate.php'; ?>
      </div>
      <div class="setting_section_block support_section_block">
        <?php require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'support.php'; ?>
      </div>
      <div class="setting_section_block system_requirements_block">
        <?php require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'system-requirement.php'; ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  jQuery(window).load(function(){
    setTimeout(function() {
        jQuery('.action_loader').fadeOut('fast');
    }, 3000);
  });
  jQuery("#WPSupportForm").submit(function(event){
    jQuery('.action_loader').show();
    event.preventDefault();
    var user_name = jQuery("#user_name").val();
    var user_email = jQuery("#user_email").val();
    var user_subject = jQuery("#user_subject").val();
    var user_license = jQuery("#user_license").val();
    var user_message = jQuery("#user_message").val();

    jQuery.ajax({
      url: ajaxurl,
      type: "POST",
      data: {
        action: 'support_form',
        name: user_name,
        email: user_email,
        subject: user_subject,
        license: user_license,
        message: user_message
      },
      success: function(response) {
        if(response == 1){
          jQuery('.action_loader').hide();
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Message sent successfully. We will get back to you ASAP</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        }else{
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something Went Wrong. Please Try Again.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        }
      },
      error: function (ErrorResponse) {
        jQuery('.action_loader').hide();
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something Went Wrong. Please Try Again.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
      }
    });
  });
  jQuery(document).ready(function(){
    jQuery("#media_scan").change(function(){
      if(jQuery(this).prop("checked") == true){
      var is_enable = "yes";
      }else{
        var is_enable = "no";
      }
      jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        data: {
          action: 'wpmc_media_default_action',
          type: "media_scan",
          option: is_enable
        },
        success: function(response) {
          var resp = jQuery.parseJSON(response);
          if(resp.result){
            jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Option updated.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
          }
        },
        error: function (ErrorResponse) {
        }
      });
    });
    jQuery("#content_scan").change(function(){
      if(jQuery(this).prop("checked") == true){
      var is_enable = "yes";
      }else{
        var is_enable = "no";
      }
      jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        data: {
          action: 'wpmc_media_default_action',
          type: "content_scan",
          option: is_enable
        },
        success: function(response) {
          var resp = jQuery.parseJSON(response);
          if(resp.result){
            jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Option updated.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
          }
        },
        error: function (ErrorResponse) {
        }
      });
    });
    jQuery("#image_optimize").change(function(){
      if(jQuery(this).prop("checked") == true){
      var is_enable = "yes";
      }else{
        var is_enable = "no";
      }
      jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        data: {
          action: 'wpmc_media_default_action',
          type: "image_optimize",
          option: is_enable
        },
        success: function(response) {
          var resp = jQuery.parseJSON(response);
          if(resp.result){
            jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Option updated.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
          }
        },
        error: function (ErrorResponse) {
        }
      });
    });
    jQuery("#database_cleaner").change(function(){
      if(jQuery(this).prop("checked") == true){
      var is_enable = "yes";
      }else{
        var is_enable = "no";
      }
      jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        data: {
          action: 'wpmc_media_default_action',
          type: "database_cleaner",
          option: is_enable
        },
        success: function(response) {
          var resp = jQuery.parseJSON(response);
          if(resp.result){
            jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Option updated.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
          }
        },
        error: function (ErrorResponse) {
        }
      });
    });
    jQuery("#database_backup").change(function(){
      if(jQuery(this).prop("checked") == true){
      var is_enable = "yes";
      }else{
        var is_enable = "no";
      }
      jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        data: {
          action: 'wpmc_media_default_action',
          type: "database_backup",
          option: is_enable
        },
        success: function(response) {
          var resp = jQuery.parseJSON(response);
          if(resp.result){
            jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Option updated.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
          }
        },
        error: function (ErrorResponse) {
        }
      });
    });

    // Validate Lisence key
    jQuery("#validate_lisence").click(function(event){
      event.preventDefault();
      
      var license = jQuery("#license_key").val();
      if(license.length>0){
        jQuery(".loading_rotating").show();
        jQuery("#license_key").removeClass("form_error");
        jQuery.ajax({
          url: ajaxurl,
          type: "POST",
          data: {
            action: 'wpmc_lisence_activation',
            license_key: license
          },
          success: function(response) {
            jQuery(".loading_rotating").fadeOut(300);
            var resp = jQuery.parseJSON(response);
            if(resp.types == "remaining_day"){
              jQuery(".if_license_activated").empty().html(resp.message);
              jQuery("#license_key").prop("disabled", true);
              jQuery("#validate_lisence").prop("disabled", true);
              jQuery("#deactivate_license").prop("disabled", false);
              
              jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+resp.message+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
            }else{
              jQuery(".if_license_activated").empty().html(resp.message);
              jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+resp.message+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
            }
          },
          error: function (ErrorResponse) {
            jQuery(".loading_rotating").fadeOut(300);
          }
        });
      }else{
        jQuery("#license_key").addClass("form_error");
      }
    });
    // Deactivate license key
    jQuery("#deactivate_license").click(function(event){
      event.preventDefault();
      jQuery(".loading_rotating").show();
      var license = jQuery("#license_key").val();
      if(license.length>0){
        jQuery("#license_key").removeClass("form_error");
        jQuery.ajax({
          url: ajaxurl,
          type: "POST",
          data: {
            action: 'wpmc_lisence_deactivation',
            license_key: license
          },
          success: function(response) {
            jQuery(".loading_rotating").fadeOut(300);
            var resp = jQuery.parseJSON(response);
            if(resp.types == "deactivate"){
              jQuery(".if_license_activated").empty().html(resp.message);
              jQuery("#license_key").val("").prop("disabled", false);
              jQuery("#validate_lisence").prop("disabled", false);
              jQuery("#deactivate_license").prop("disabled", true);
              jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+resp.message+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
            }else{
              jQuery(".if_license_activated").empty().html(resp.message);
              jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+resp.message+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
            }
          },
          error: function (ErrorResponse) {
            jQuery(".loading_rotating").fadeOut(300);
          }
        });
      }else{
        jQuery("#license_key").addClass("form_error");
      }
    });
  });
</script>