<?php
/**
 * WP media Cleaner Plugin is the simplest WordPress plugin for Complete package of Media Cleaner, Image Optimization, WordPress backup and Database Cleaner.
 * Take this as a base plugin and modify as per your need.
 *
 * @package WP Media Cleaner
 * @author Marketingmindz
 * @license GPL-2.0+
 * @link https://wp-media-cleaner.com
 * @copyright 2017 WP media Cleaner, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: WP Media Cleaner
 *            Plugin URI: https://wp-media-cleaner.com
 *            Description: WP media Cleaner Plugin is the simplest WordPress plugin for Complete package of Media Cleaner, Image Optimization, WordPress backup and Database Cleaner.
 *            Version: 1.0
 *            Author: Marketingmindz
 *            Author URI: https://www.marketingmindz.com/
 *            Text Domain: wp_media_cleaner
 *            Contributors: WP media Cleaner
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// TODO: Change the class name to something unique
// Plugin Basename
define( 'WPMC_PLUGIN_BASENAME', basename( dirname( __FILE__ ) ) . '/' . basename( __FILE__ ) );

// Plugin Path
define( 'WPMC_PATH', dirname( __FILE__ ) );

// Plugin URL
define( 'WPMC_URL', plugins_url( '', WPMC_PLUGIN_BASENAME ) );

// Main Directory Path

define( 'WPMC_MAIN', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/main');

// Setting Directory Path

define( 'WPMC_SETTINGS', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/settings');

// Setting Directory URL

define( 'WPMC_SETTINGS_URL', WPMC_URL  . DIRECTORY_SEPARATOR . 'lib/settings');

// Media Scan Directory Path

define( 'WPMC_MEDIA_SCAN', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/media-scan');

// Content Scan Directory Path

define( 'WPMC_CONTENT_SCAN', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/content-scan');

// Image Optimize Directory Path

define( 'WPMC_IMAGE_OPTIMIZE', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/image-optimize');

// Database Cleaner Directory Path

define( 'WPMC_DATABASE_CLEANER', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/database-cleaner');

// Backup Directory Path

define( 'WPMC_BACKUP', dirname( __FILE__ )  . DIRECTORY_SEPARATOR . 'lib/backup');

// Backup Directory URL

define( 'WPMC_BACKUPFILE',  WPMC_PATH . DIRECTORY_SEPARATOR . 'backup');

// Ajax Directory URL

define( 'WPMC_AJAX',  WPMC_URL . DIRECTORY_SEPARATOR . 'assets/ajax');

// Image Directory URL

define( 'WPMC_IMAGE',  WPMC_URL . DIRECTORY_SEPARATOR . 'assets/images');

// CSS Directory URL

define( 'WPMC_CSS',  WPMC_URL . DIRECTORY_SEPARATOR . 'assets/css');

// JS Directory URL

define( 'WPMC_JS',  WPMC_URL . DIRECTORY_SEPARATOR . 'assets/js');

// Wordpress load.php file

define( 'WPMC_LOAD',  WPMC_URL . DIRECTORY_SEPARATOR . '../../../../load.php');

// Initialize class
class WPMC {

  // Put all your add_action, add_shortcode, add_filter functions in __construct()
  // For the callback name, use this: array($this,'<function name>')
  // <function name> is the name of the function within this class, so need not be globally unique
  // Some sample commonly used functions are included below
    public function __construct() {
        
        // TODO: Edit the calls here to only include the ones you want, or add more
        // $head = WPMC_MAIN.'/head.php';
        $menu = WPMC_MAIN.'/menu.php';

        // Add Javascript and CSS for admin screens
        add_action('admin_enqueue_scripts', array($this,'enqueueAdmin'));
        //Creates tables in database for plugin uses.
        register_activation_hook(__FILE__, array( $this, 'create_wpmc_database_table'));
        //register_deactivation_hook( __FILE__, array( $this, 'my_plugin_remove_database' ) );

        add_action( 'dbdelta_queries', array( $this, 'EventDBDeltaQuery' ) );
        add_action( 'query', array( $this, 'EventDropQuery' ) );

        // Action for translation
        add_action( 'init', array($this, 'load_plugin_textdomain'));

        // Include loader
        // require_once($head);
        require_once($menu);
        require __DIR__ . '/core.php';
    }

    /* ENQUEUE SCRIPTS AND STYLES */
    // This is an example of enqueuing a Javascript file and a CSS file for use on the editor 
    public function enqueueAdmin() {

        if(isset($_GET['page']) && $_GET['page'] == "media-cleaner-setting" || isset($_GET['page']) && $_GET['page'] == "media-cleaner-media-scan" || isset($_GET['page']) && $_GET['page'] == "media-cleaner-content-scan" || isset($_GET['page']) && $_GET['page'] == "media-cleaner-image-optimize" || isset($_GET['page']) && $_GET['page'] == "media-cleaner-database-cleaner" || isset($_GET['page']) && $_GET['page'] == "media-cleaner-backup"){

            // ALL SCRIPT
            wp_enqueue_script('datatable', plugins_url('assets/js/jquery.dataTables.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('datatableselect', plugins_url('assets/js/dataTables.select.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('bootstrapjs', plugins_url('assets/js/bootstrap.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('dashboardjs', plugins_url('scripts/dashboard.js', __FILE__), array('jquery'), '1.0', true);   
            wp_enqueue_script('momentmin', plugins_url('assets/js/moment.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('daterangepickerjs', plugins_url('assets/js/daterangepicker.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('jquery-multi-select-min-js', plugins_url('assets/js/bootstrap-multiselect.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('wpmc-timepicker', plugins_url('assets/js/jquery.timepicker.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_enqueue_script('wpmc-script', plugins_url('assets/js/WPMC-script.js', __FILE__), array('jquery'), '1.0', true);

            // ALL STYLES
            wp_enqueue_style('bootstrap', plugins_url('assets/css/bootstrap.min.css', __FILE__), null, '1.0');
            wp_enqueue_style('datatables', plugins_url('assets/css/jquery.dataTables.min.css', __FILE__), null, '1.0');
            wp_enqueue_style('daterangepickercss', plugins_url('assets/css/daterangepicker.css', __FILE__), null, '1.0');
            wp_enqueue_style('multi-select-min-css', plugins_url('assets/css/bootstrap-multiselect.css', __FILE__), null, '1.0');
            wp_enqueue_style('wpmc-css-timepicker', plugins_url('assets/css/jquery.timepicker.min.css', __FILE__), null, '1.0');
            wp_enqueue_style('very-exciting-name', plugins_url('assets/css/WPMC-style.css', __FILE__), null, '1.0');
       }
       wp_enqueue_style('change-icons', plugins_url('assets/css/menu-icon.css', __FILE__), null, '1.0');
    }

    // Function for translation localization
    public function load_plugin_textdomain() {
        $domain = 'wp_media_cleaner';
        $mo_file = WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . get_locale() . '.mo';
        // print_r($mo_file);die;
        load_textdomain( $domain, $mo_file ); 
        load_plugin_textdomain( $domain, false,  dirname(plugin_basename( __FILE__ )) . '/languages/' ); 
    }

    public function create_wpmc_database_table(){
        global $wpdb;
        // Update in option table
		update_option( "wp_media_keep_original", "" );
		update_option( "media_scan_activate", "yes" );
		update_option( "content_scan_activate", "yes" );
		update_option( "image_optimize_activate", "yes" );
		update_option( "database_cleaner_activate", "yes" );
		update_option( "database_backup_activate", "yes" );
        update_option( "WPMC_lisence_activation", "0" );

        $plugindatatable = $wpdb->prefix.'plugin_data';
        if( $wpdb->get_var("SHOW TABLES LIKE '$plugindatatable'") != $plugindatatable ) {
            $charset_collate = $wpdb->get_charset_collate();
            $pluginsql = "CREATE TABLE $plugindatatable (
                id int(11) NOT NULL AUTO_INCREMENT,
                plugin_name varchar(255) DEFAULT NULL,
                plugin_tables varchar(255) DEFAULT NULL,
                plugin_file varchar(255) DEFAULT NULL,
                created_at timestamp DEFAULT CURRENT_TIMESTAMP,
                modified_at timestamp DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (id)
            ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $pluginsql );
        }

            
        $mediascantable = $wpdb->prefix.'mediascan_data';
        if( $wpdb->get_var("SHOW TABLES LIKE '$mediascantable'") != $mediascantable ) {
            $charset_collate = $wpdb->get_charset_collate();
            $mediascansql = "CREATE TABLE $mediascantable (
                id int(11) NOT NULL AUTO_INCREMENT,
                media_id varchar(255) DEFAULT NULL,
                ptype varchar(255) DEFAULT NULL,
                medianame varchar(255) DEFAULT NULL,
                media_url varchar(255) DEFAULT NULL,
                media_type varchar(255) DEFAULT NULL,
                post_title varchar(255) DEFAULT NULL,
                post_type varchar(255) DEFAULT NULL,
                post_category varchar(255) DEFAULT NULL,
                variant_attribute varchar(255) DEFAULT NULL,
                variant_sku varchar(255) DEFAULT NULL,
                page_builder_name varchar(255) DEFAULT NULL,
                upload_date varchar(255) DEFAULT NULL,
                linked_status varchar(255) DEFAULT NULL,
                source_from varchar(255) DEFAULT NULL,
                created_at timestamp DEFAULT CURRENT_TIMESTAMP,
                modified_at timestamp DEFAULT CURRENT_TIMESTAMP,
                website_prefix varchar(255) DEFAULT NULL,
                PRIMARY KEY  (id)
            ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $mediascansql );
        }

        $contentscantable = $wpdb->prefix.'contentscan_data';
        if( $wpdb->get_var("SHOW TABLES LIKE '$contentscantable'") != $contentscantable ) {
            $charset_collate = $wpdb->get_charset_collate();
            $contentscansql = "CREATE TABLE $contentscantable (
                id int(11) NOT NULL AUTO_INCREMENT,
                post_id varchar(255) DEFAULT NULL,
                post_url varchar(255) DEFAULT NULL,
                post_date varchar(255) DEFAULT NULL,
                post_title varchar(255) DEFAULT NULL,
                post_status varchar(255) DEFAULT NULL,
                post_type varchar(255) DEFAULT NULL,
                content_site varchar(255) DEFAULT NULL,
                multisite_name varchar(255) DEFAULT NULL,
                media_found longtext DEFAULT NULL,
                media_source text DEFAULT NULL,
                scheduled_date varchar(255) DEFAULT NULL,
                created_at timestamp DEFAULT CURRENT_TIMESTAMP,
                modified_at timestamp DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (id)
            ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $contentscansql );
        }

        // Image Optimization table
        $image_optimization_table = $wpdb->prefix.'image_optimizer';
        if( $wpdb->get_var("SHOW TABLES LIKE '$image_optimization_table'") != $image_optimization_table ) {
            $charset_collate = $wpdb->get_charset_collate();
            $img_optimizer_sql = "CREATE TABLE $image_optimization_table (
                id int(11) NOT NULL AUTO_INCREMENT,
                medianame varchar(255) DEFAULT NULL,
                media_url varchar(255) DEFAULT NULL,
                media_type varchar(255) DEFAULT NULL,
                post_title varchar(255) DEFAULT NULL,
                post_category varchar(255) DEFAULT NULL,
                post_type varchar(255) DEFAULT NULL,
                upload_date varchar(255) DEFAULT NULL,
                source_from varchar(255) DEFAULT NULL,
                linked_status varchar(255) DEFAULT NULL,
                optimized_amount int(11) DEFAULT NULL,
                website_prefix varchar(255) DEFAULT NULL,
                created_at timestamp DEFAULT CURRENT_TIMESTAMP,
                modified_at timestamp DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (id)
            ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $img_optimizer_sql );
        }

        // Create Optimization/Deletion table (optimizer_backup)
        $optimizer_backup = $wpdb->prefix.'optimizer_backup';
        if( $wpdb->get_var("SHOW TABLES LIKE '$optimizer_backup'") != $optimizer_backup ) {
            $charset_collate = $wpdb->get_charset_collate();
            $optimizerbackupsql = "CREATE TABLE $optimizer_backup (
                backup_id int(11) NOT NULL AUTO_INCREMENT,
                type varchar(255) DEFAULT NULL,
                blog_id int(11) DEFAULT NULL,
                optimization_amount int(11) DEFAULT NULL,
                media_id int(11) DEFAULT NULL,
                Image_url varchar(255) DEFAULT NULL,
                backup_date timestamp DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (backup_id)
            ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $optimizerbackupsql );
        }
    }

    public function EventDBDeltaQuery( $queries ) {
        $type_queries = array(
            'create' => array(),
            'update' => array(),
            'delete' => array(),
        );
        global $wpdb;

        foreach ( $queries as $qry ) {
            $qry = str_replace( '`', '', $qry );
            $str = explode( ' ', $qry );
            if ( preg_match( '|CREATE TABLE ([^ ]*)|', $qry ) ) {
                if ( $str[2] !== $wpdb->get_var( "SHOW TABLES LIKE '" . $str[2] . "'" ) ) {
                    /**
                     * Some plugins keep trying to create tables even
                     * when they already exist- would result in too
                     * many alerts.
                     */
                    array_push( $type_queries['create'], $str[2] );
                }
            } elseif ( preg_match( '|ALTER TABLE ([^ ]*)|', $qry ) ) {
                array_push( $type_queries['update'], $str[2] );
            } elseif ( preg_match( '|DROP TABLE ([^ ]*)|', $qry ) ) {
                if ( ! empty( $str[4] ) ) {
                    array_push( $type_queries['delete'], $str[4] );
                } else {
                    array_push( $type_queries['delete'], $str[2] );
                }
            }
        }

        if ( ! empty( $type_queries['create'] ) || ! empty( $type_queries['update'] ) || ! empty( $type_queries['delete'] ) ) {
            $actype        = isset( $_SERVER['SCRIPT_NAME'] ) ? basename( sanitize_text_field( wp_unslash( $_SERVER['SCRIPT_NAME'] ) ), '.php' ) : false;
            $alert_options = $this->GetActionType( $actype );

            foreach ( $type_queries as $query_type => $table_names ) {
                if ( ! empty( $table_names ) ) {
                    
                    $alert_options['TableNames'] = implode( ',', $table_names );
                    $this->StorePluginTableInfo($alert_options);

                }
            }
        }

        return $queries;
    }

    public function EventDropQuery( $query ) {
        global $wpdb;
        $table_names = array();
        $str         = explode( ' ', $query );

       if ( preg_match( '|CREATE TABLE IF NOT EXISTS ([^ ]*)|', $query ) ) {
            $table_name = str_replace( '`', '', $str[5] );
            if ( $table_name !== $wpdb->get_var( "SHOW TABLES LIKE '" . $table_name . "'" ) ) {
                /**
                 * Some plugins keep trying to create tables even
                 * when they already exist- would result in too
                 * many alerts.
                 */
                array_push( $table_names, $table_name );
                $actype        = isset( $_SERVER['SCRIPT_NAME'] ) ? basename( sanitize_text_field( wp_unslash( $_SERVER['SCRIPT_NAME'] ) ), '.php' ) : false;
                $alert_options = $this->GetActionType( $actype );
                $type_query    = 'create';
            }
        }

        if ( ! empty( $table_names ) ) {
            $alert_options['TableNames'] = implode( ',', $table_names );
            $this->StorePluginTableInfo($alert_options);
           
        }
        return $query;
       
    }

    protected function GetActionType( $actype ) {
        // Check the component type (theme or plugin).

       
        $is_themes  = 'themes' === $actype;
        $is_plugins = 'plugins' === $actype;

        // Action Plugin Component.
        $alert_options = array();
        if ( $is_plugins ) {
            $plugin_file = '';
            // @codingStandardsIgnoreStart
            if ( isset( $_GET['plugin'] ) ) {
                $plugin_file = sanitize_text_field( wp_unslash( $_GET['plugin'] ) );
            } elseif ( isset( $_GET['checked'] ) ) {
                $plugin_file = sanitize_text_field( wp_unslash( $_GET['checked'][0] ) );
            }
            // @codingStandardsIgnoreEnd

            // Get plugin data.
            $plugins = get_plugins();
            if ( isset( $plugins[ $plugin_file ] ) ) {
                $plugin = $plugins[ $plugin_file ];
                // Set alert options.
                $alert_options['Plugin'] = (object) array(
                    'Name'      => $plugin['Name'],
                    'PluginURI' => $plugin['PluginURI'],
                    'Version'   => $plugin['Version'],
                    'PluginFile'=> $plugin_file 
                );
            } else {
                $plugin_name             = basename( $plugin_file, '.php' );
                $plugin_name             = str_replace( array( '_', '-', '  ' ), ' ', $plugin_name );
                $plugin_name             = ucwords( $plugin_name );
                $alert_options['Plugin'] = (object) array( 'Name' => $plugin_name );
            }
        } elseif ( $is_themes ) {
            // Action Theme Component.
            $theme_name = '';

            // @codingStandardsIgnoreStart
            if ( isset( $_GET['theme'] ) ) {
                $theme_name = sanitize_text_field( wp_unslash( $_GET['theme'] ) );
            } elseif ( isset( $_GET['checked'] ) ) {
                $theme_name = sanitize_text_field( wp_unslash( $_GET['checked'][0] ) );
            }
            // @codingStandardsIgnoreEnd

            $theme_name             = str_replace( array( '_', '-', '  ' ), ' ', $theme_name );
            $theme_name             = ucwords( $theme_name );
            $alert_options['Theme'] = (object) array( 'Name' => $theme_name );
        } else {
            // Action Unknown Component.
            $alert_options['Component'] = 'Unknown';
        }
        return $alert_options;
    }

    protected function StorePluginTableInfo($ptableinfo){
        global $wpdb;
       
        if($ptableinfo){
            $pluginname = trim($ptableinfo['Plugin']->Name); 
            $pluginfile = trim($ptableinfo['Plugin']->PluginFile); 
            $plugintablename = trim($ptableinfo['TableNames']); 
            $plugintableexist = $wpdb->get_results("select * from ".$wpdb->prefix."plugin_data where plugin_name='".$pluginname."' AND plugin_tables='".$plugintablename."'");
            if(!$plugintableexist){
                $wpdb->insert( $wpdb->prefix."plugin_data", array("plugin_name" => $pluginname,  "plugin_tables" => $plugintablename, "plugin_file" => $pluginfile));
            }
        }
    }
}

// TODO: Replace these with a variable named appropriately and the class name above
// If you need this available beyond our initial creation, you can create it as a global
global $skeleton;

// Create an instance of our class to kick off the whole thing
$skeleton = new WPMC();
$admin = '';
$wpmc = new WPMC_Core($admin);