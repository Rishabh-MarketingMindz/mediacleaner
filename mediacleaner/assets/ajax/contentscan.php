<?php
// error_reporting(0);
// @ini_set('display_errors', 0);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
// require '../../includes/simple_html_dom.php';
global $wpdb;
function totalAvailableExtension($the_content){
	// Total types of extensions
	$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
	$groups = array();
	$totalGroup = array();
	$theURL = '';
	if($the_content){
		foreach ($totalExtensionsAvailable as $Extvalue) {
			preg_match_all('@'.$Extvalue.'="([^"]+)"@', $the_content, $groups);
			if(!empty($groups)){
				$totalGroup[] = $groups;
			}else{
				unset($groups);
			}
		}
		foreach ($totalGroup as $key => $finalExtUrl) {
			if(!empty($finalExtUrl[1][0])){
				$theURL .= $finalExtUrl[1][0].',';
			}
		}
	}
	$theURL = rtrim($theURL,",");
	$allExtensionURL = explode(",", $theURL);

	return $allExtensionURL;
}

function getAllSitePrefix(){
	global $wpdb;
	$prefixes = array();
	$abcd = count(get_sites());
	$count = 0;
	for ($i=2; $i <= $abcd; $i++) { 
		$prefixes[$count]['prefix'] = $wpdb->get_blog_prefix($i);
		$prefixes[$count]['multisite_id'] = $i;
		$count++;
	}
	return $prefixes;
}

// Get Media id by url
function getImgidbyURL($imgUrl){
	global $wpdb;
	$imgUrlquery = $wpdb->get_results("SELECT ID FROM ".$wpdb->prefix."posts WHERE guid='$imgUrl'");
	if(!empty($imgUrlquery)){
		return $imgUrlquery[0]->ID;
	}else{
		return false;
	}
}

// Get Multisite Media id by url
function getMultiSiteImgidbyURL($imgUrl,$prefixValue){
	global $wpdb;
	$imgUrlquery = $wpdb->get_results("SELECT ID FROM ".$prefixValue."posts WHERE guid='$imgUrl'");
	if(!empty($imgUrlquery)){
		return $imgUrlquery[0]->ID;
	}else{
		return false;
	}
}

// Get Media URL by id
function getImageUrlbyId($imgId){
	global $wpdb;
	$imgIdget = wp_get_attachment_url($imgId);
	if(!empty($imgIdget)){
		return $imgIdget;
	}else{
		return false;
	}
}

// Get Multisite Media URL by id
function getMultiSiteImageUrlbyId($imgId,$prefixValue){
	global $wpdb;
	$imgIdget = $wpdb->get_results("SELECT guid from ".$prefixValue."posts WHERE ID = $imgId" );
	$imgIdget = $imgIdget[0]->guid;
	if(!empty($imgIdget)){
		return $imgIdget;
	}else{
		return false;
	}
}

// Get media from page builder Coded start by Sohel
function getPageBuilderContentMedia(){
	global $wpdb;
	$prefixValue = $wpdb->prefix;
	$pbContent = $wpdb->get_results("SELECT * from ".$wpdb->prefix."posts WHERE post_status != 'trash' && post_type NOT IN('revision','scheduled-action','attachment')");
	$uniqueArr = array();
	$getFiles = array();
	$inc = 0;
	// Total types of extensions
	$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','PDF','DOCX','doc','DOC','XLS','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
	foreach ($pbContent as $getvalues) {		
		$getFiles = array();
		$elem_builder = get_post_meta($getvalues->ID, '_elementor_edit_mode', true);
		$vc_builder = get_post_meta($getvalues->ID, '_wpb_vc_js_status', true);
		$beaver_builder = get_post_meta($getvalues->ID, '_fl_builder_enabled', true);
		$siteorigin_builder = get_post_meta($getvalues->ID, 'panels_data', true);
		$brizy_builder = !empty(get_post_meta($getvalues->ID, 'brizy', true));
		$oxygen_builder = !empty(get_post_meta($getvalues->ID, 'ct_builder_shortcodes', true));
		$siteorigin_ck_builder = '';
		if(!empty($siteorigin_builder)){
			$siteorigin_ck_builder = "SiteOrigin";
		}
		$featuredImageArr = get_post_meta($getvalues->ID, '_thumbnail_id');
		@$featuredImagesId = $featuredImageArr[0];
		if($featuredImagesId){
			$featuredImageId = getImageUrlbyId($featuredImagesId);
		}else{
			$featuredImageId = '';
		}
		$productgalleryArr = get_post_meta($getvalues->ID, '_product_image_gallery');
		@$productgalleryImg = $productgalleryArr[0];
		
		if($elem_builder == 'builder'){
			$PageId = $getvalues->ID;
			$elementor_sql = get_post_meta($PageId, '_elementor_data');
			$elementor_content = $elementor_sql[0];
			
			// Get files url from URL & ID tag start
			$group4 = array();
			$group5 = array();
			$getsrcfiles = array();
			preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
			preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
			// Get files url from URL & ID tag end

			$imgArry = array();
			foreach ($group4[1] as $getId) {
				$getImgidbyURL = stripslashes($getId);
				if($getImgidbyURL){
					array_push($imgArry, $getImgidbyURL);
				}
			}

			// Get files url from href tag start
			$gethreffiles = array();
			preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
			$hreffiles = array();
			if(!empty($gethreffiles[1])){
				foreach ($gethreffiles[1] as $hrefValues) {
					$hreffiles = stripslashes($hrefValues); // href URL data
					if($hreffiles){
						array_push($imgArry, $hreffiles);
					}
				}
			}
			// Get files url from href tag end

			// Get files src from href tag start
			$srcfiles = array();
			preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
			if(!empty($getsrcfiles[1])){
				foreach ($getsrcfiles[1] as $srcValues) {
					$srcfiles = stripslashes($srcValues); // href URL data
					if($srcfiles){
						array_push($imgArry, $srcfiles);
					}
				}
			}
			// Get files src from href tag start

			$contentimgid = implode(',', $imgArry);
			if($group5[1][0] != ""){
				$group5arr = explode(',', $group5[1][0]);
				foreach ($group5arr as $imggelid) {
					if($imggelid){
						$getidurlimge  = getImageUrlbyId($imggelid);
						$contentimgid .= ','.$getidurlimge;
					}
				}
			}

			$ERimagesids = implode(',', array_keys(array_flip(explode(',', $contentimgid))));
			if($ERimagesids != ""){
				$media_count = count(explode(",", $ERimagesids));
				$VCI = '';
				for ($x = 1; $x <= $media_count; $x++) {
					$VCI .= 'Elementor Content Media';
					if($x < $media_count){
						$VCI .= ',';
					}
				}
			}
			if($featuredImageId != ''){
				$ERimagesids .= ','.$featuredImageId;
				$VCI .= ',Featured Image';
			}
			if(!empty($ERimagesids)){
				$ERimagesidSc = array($ERimagesids,$VCI,$prefixValue);
				$uniqueArr += array($PageId=>$ERimagesidSc);
			}
			$ERimagesids = null;
			$VCI = null;
			
		}
		if($oxygen_builder){
			$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

			$PageId = $getvalues->ID;
			$the_content = get_post_meta($PageId, 'ct_builder_shortcodes', true);

			preg_match_all('@"url":"([^"]+)"@', $the_content, $WordPressWidgetImages);
			preg_match_all('@"src":"([^"]+)"@', $the_content, $contentimgurl);
			preg_match_all('@"background-image":"([^"]+)"@', $the_content, $contentimgurl1);
			preg_match_all('@"image_ids":"([^"]+)"@', $the_content, $gelleryimgids);
			preg_match_all('@"code-php":"([^"]+)"@', $the_content, $phpcodes_encode);
			$OxyCodeArr = array();
			foreach ($phpcodes_encode[1] as $phpcode_encode) {
				$phpcodes_decode = base64_decode($phpcode_encode);
				preg_match_all('@(?:src[^>]+>)(.*?)@', $phpcodes_decode, $phpcodesImgUrl);
				$phpcode_encode_url = str_replace(['src=',"'",'"',"/>",">"," type=application/pdf"], ['','','','','',''], $phpcodesImgUrl[0]);
				$OxyCodeArr[] = explode(" ", $phpcode_encode_url[0]);
			}

			$OXImageurls = array();
			foreach ($OxyCodeArr as $OxyCodevalue) {
				$OXImageurls[] = $OxyCodevalue[0];
			}

			$widget_images = array();
			foreach ($WordPressWidgetImages[1] as $WordPressWidgetValue) {
				$widget_images[] = base64_decode($WordPressWidgetValue);
			}
			preg_match_all('@src="([^"]+)"@', $the_content, $group9);
			preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
			preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
			preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
			preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
			preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
			preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
			preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
			preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
			preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
			preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
			preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
			preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
			preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
			preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
			preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
			preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
			preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
			preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

			preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_gallery);
			// for gallery image
			$urls = '';
			if(!empty($oxy_gallery[1])){				
				foreach ($oxy_gallery[1] as $oxy_gallery_id) {
					$urls .= $oxy_gallery_id.',';
				}
			}
			$imageID = rtrim($urls,",");
			$imageIDs = explode(",", $imageID);
			foreach ($imageIDs as $EachId) {
				$getFiles[] = getImageUrlbyId($EachId);
			}
			
			preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
			preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);

			$groupsfiles = $groupsfiles[1];
			if(!empty($groupsfiles)){
				foreach ($groupsfiles as $filesvalue) {
					$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
					if(in_array($fileExtension, $totalExtensionsAvailable)){
				        $getFiles[] = $filesvalue;
				    }
				}
			}
			// for vc video
			if(!empty($vc_video_url[1])){
				foreach ($vc_video_url[1] as $vc_video) {
					$getFiles[] = $vc_video;
				}
			}

			// for src
			if(!empty($group9[1])){
				foreach ($group9[1] as $group9s) {
					$getFiles[] = $group9s;
				}
			}
			// for mp3
			if(!empty($group11[1])){
				foreach ($group11[1] as $group11s) {
					$getFiles[] = $group11s;
				}
			}
			// for mp4
			if(!empty($group12[1])){
				foreach ($group12[1] as $group12s) {
					$getFiles[] = $group12s;
				}
			}

			// for pdf
			if(!empty($group13[1])){
				foreach ($group13[1] as $group13s) {
					$getFiles[] = $group13s;
				}
			}

			// for docx
			if(!empty($group14[1])){
				foreach ($group14[1] as $group14s) {
					$getFiles[] = $group14s;
				}
			}

			// for doc
			if(!empty($group15[1])){
				foreach ($group15[1] as $group15s) {
					$getFiles[] = $group15s;
				}
			}

			// for ppt
			if(!empty($group16[1])){
				foreach ($group16[1] as $group16s) {
					$getFiles[] = $group16s;
				}
			}

			// for xls
			if(!empty($group17[1])){
				foreach ($group17[1] as $group17s) {
					$getFiles[] = $group17s;
				}
			}

			// for pps
			if(!empty($group18[1])){
				foreach ($group18[1] as $group18s) {
					$getFiles[] = $group18s;
				}
			}

			// for ppsx
			if(!empty($group19[1])){
				foreach ($group19[1] as $group19s) {
					$getFiles[] = $group19s;
				}
			}

			// for xlsx
			if(!empty($group20[1])){
				foreach ($group20[1] as $group20s) {
					$getFiles[] = $group20s;
				}
			}

			// for odt
			if(!empty($group21[1])){
				foreach ($group21[1] as $group21s) {
					$getFiles[] = $group21s;
				}
			}

			// for ogg
			if(!empty($group22[1])){
				foreach ($group22[1] as $group22s) {
					$getFiles[] = $group22s;
				}
			}

			// for m4a
			if(!empty($group23[1])){
				foreach ($group23[1] as $group23s) {
					$getFiles[] = $group23s;
				}
			}

			// for wav
			if(!empty($group24[1])){
				foreach ($group24[1] as $group24s) {
					$getFiles[] = $group24s;
				}
			}

			// for mp4
			if(!empty($group25[1])){
				foreach ($group25[1] as $group25s) {
					$getFiles[] = $group25s;
				}
			}

			// for mov
			if(!empty($group26[1])){
				foreach ($group26[1] as $group26s) {
					$getFiles[] = $group26s;
				}
			}

			// for avi
			if(!empty($group27[1])){
				foreach ($group27[1] as $group27s) {
					$getFiles[] = $group27s;
				}
			}

			// for 3gp
			if(!empty($group28[1])){
				foreach ($group28[1] as $group28s) {
					$getFiles[] = $group28s;
				}
			}

			// for pptx
			if(!empty($group29[1])){
				foreach ($group29[1] as $group29s) {
					$getFiles[] = $group29s;
				}
			}

			// Merging arrays
			$OXI1mageurlsArr = array();
			if(!empty($OXImageurls) && !empty($contentimgurl[1])){
				$OXI1mageurlsArr = array_merge($OXImageurls,$contentimgurl[1]);
			}elseif(!empty($OXImageurls) && empty($contentimgurl[1])){
				$OXI1mageurlsArr = $OXImageurls;
			}elseif(empty($OXImageurls) && !empty($contentimgurl[1])){
				$OXI1mageurlsArr = $contentimgurl[1];
			}

			$OXI2mageurlsArr = array();
			if(!empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
				$OXI2mageurlsArr = array_merge($OXI1mageurlsArr,$contentimgurl1[1]);
			}elseif(!empty($OXI1mageurlsArr) && empty($contentimgurl1[1])){
				$OXI2mageurlsArr = $OXI1mageurlsArr;
			}elseif(empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
				$OXI2mageurlsArr = $contentimgurl1[1];
			}

			$OXImageurlsArr = array();
			if(!empty($OXI2mageurlsArr) && !empty($getFiles)){
				$OXImageurlsArr = array_merge($OXI2mageurlsArr,$getFiles);
			}elseif(!empty($OXI2mageurlsArr) && empty($getFiles)){
				$OXImageurlsArr = $OXI2mageurlsArr;
			}elseif(empty($OXI2mageurlsArr) && !empty($getFiles)){
				$OXImageurlsArr = $getFiles;
			}			

			$OXImageurlsArrFinal = array();
			if(!empty($OXImageurlsArr) && !empty($widget_images)){
				$OXImageurlsArrFinal = array_merge($OXImageurlsArr,$widget_images);
			}elseif(!empty($OXImageurlsArr) && empty($widget_images)){
				$OXImageurlsArrFinal = $OXImageurlsArr;
			}elseif(empty($OXImageurlsArr) && !empty($widget_images)){
				$OXImageurlsArrFinal = $widget_images;
			}

			$OXimgidsArr = array();
			if(!empty($OXImageurlsArrFinal)){
				foreach ($OXImageurlsArrFinal as $OXImageurl) {
					array_push($OXimgidsArr, $OXImageurl);
				}
			}

			if(!empty($gelleryimgids[1])){
				foreach ($gelleryimgids[1] as $gelleryimgid) {
					$gelleryimgURlbyid = getImageUrlbyId($gelleryimgid);					
					array_push($OXimgidsArr,$gelleryimgURlbyid);
				}
			}

			$OXimgidsArrUnis = array_unique($OXimgidsArr, SORT_REGULAR);
			$OXimgidsArrUni = array_filter($OXimgidsArrUnis);
			
			if(!empty($OXimgidsArrUni)){
				$media_count = count($OXimgidsArrUni);
				$OXimagesids = '';
				$VCI = '';
				$x = 1;
				foreach ($OXimgidsArrUni as $bImgid) {
					$oxy_file_exist =  str_replace(get_site_url().'/',get_home_path(),$bImgid);
					if(file_exists($oxy_file_exist)){
						$OXimagesids .= $bImgid;
						$VCI .= 'Oxygen Content Media';
						if($x < $media_count){
							$OXimagesids .= ',';
							$VCI .= ',';
						}
						$x++;
					}
				}
			}
			if($productgalleryImg != ''){
				$productgalleryImgArr = explode(",", $productgalleryImg);
				foreach ($productgalleryImgArr as $productgalleryImgURL) {
					$oxy_gall_file_exist =  str_replace(get_site_url().'/',get_home_path(),$productgalleryImgURL);
					if(file_exists($oxy_gall_file_exist)){
						if($OXimagesids != ''){
							$OXimagesids .= ',';
							$VCI .= ',';
						}
						$OXimagesids .= getImageUrlbyId($productgalleryImgURL);
						$VCI .= 'Product Gallery Image';
					}
				}
			}
			if($featuredImageId != ''){
				$oxy_featured_file_exist =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
				if(file_exists($oxy_featured_file_exist)){
					if($OXimagesids != ''){
						$OXimagesids .= ',';
						$VCI .= ',';
					}
					$OXimagesids .= $featuredImageId;
					$VCI .= 'Featured Image';
				}
			}
			if(!empty($OXimagesids)){
				$OXimagesidSc = array($OXimagesids,$VCI,$prefixValue);
				$uniqueArr += array($PageId=>$OXimagesidSc);
			}			
			$OXimagesids = null;
			$VCI = null;
			$getFiles = null;
			$group9 = null;
			$imageIDs = null;
			$urls = null;
		}
		if($brizy_builder){
			$PageId = $getvalues->ID;
			$the_content = get_post_meta($PageId, 'brizy', true);
			$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
			$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
			preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
			preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);

			// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
			preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
			foreach ($case1_gethreffiles[1] as $case1_href_value){
				$case1_href_value = strtok($case1_href_value, " ");
	            $case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$case1_href_value);
				if(file_exists($case1_dir_path)){
					$hrefcase1[] = $case1_href_value;
				}
			}
			// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
			preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
			foreach ($case2_gethreffiles[1] as $case2_href_value) {
				$case2_href_value = strtok($case2_href_value, " ");
	            $case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$case2_href_value);
				if(file_exists($case2_dir_path)){
					$hrefcase2[] = $case2_href_value;
				}
			}				
			// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
			$srcfiles = array();
			preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
			foreach ($case3_gethreffiles[1] as $case3_href_value){
				$case3_href_value = strtok($case3_href_value, " ");
	            $case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$case3_href_value);
				if(file_exists($case3_dir_path)){
					$hrefcase3[] = $case3_href_value;
				}
			}				
			// CASE 4 (data-href) : Get url when pattern is <a data-href=
			preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
			foreach ($case4_gethreffiles[1] as $case4_href_value){					
				$case4_href_value = strtok($case4_href_value, " ");
				$case4_data_href = utf8_decode(urldecode($case4_href_value));
				preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
				$hrefcase4 = array();
				foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
					$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
					$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$theLinkVal);
					if(file_exists($case4_dir_path)){
						$hrefcase4[] = $theLinkVal;
					}
				}					
			}				
			// $the_content = htmlspecialchars($the_content);

			// CASE 5 (video) : Get url when pattern is "video":"http://some-url.mp4"
			preg_match_all('/"video"\h*:.*?\"(.*?)\",(?![^"\n]")/', $brizy_content_str, $videoUrl);
			foreach ($videoUrl[1] as $videoUrlValue) {
				$theVideoVal = str_replace("\",", "", $videoUrlValue);
				$theVideoVals = str_replace(get_site_url().'/',get_home_path(),$theVideoVal);
				if(file_exists($theVideoVals)){
					$hrefcase5[] = $theVideoVal;
				}
			}				

			// CASE 6 (image) : Get url when getting images
			// Merging Arrays Start
			$imagesNeme = array();
			if(!empty($group12[1]) && !empty($group14[1])){
				$imagesNeme = array_merge($group12[1],$group14[1]);
			}elseif(!empty($group12[1]) && empty($group14[1])){
				$imagesNeme = $group12[1];
			}elseif(empty($group12[1]) && !empty($group14[1])){
				$imagesNeme = $group14[1];
			}
			// Merging Arrays Ends
			$hrefcase6 = array();
			if(!empty($imagesNeme)){
				foreach ($imagesNeme as $imageNeme) {
					// Get Media id by Name only for brizy
					$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
					if(!empty($imgIdsquery)){
						$imagesIds = getImageUrlbyId($imgIdsquery[0]->post_id);
						$hrefcase6[] = $imagesIds;
					}
				}
			}
			// Merging Arrays Start
			$href_arrayPre1 = array();
			if(!empty($hrefcase1) && !empty($hrefcase2)){
				$href_arrayPre1 = array_merge($hrefcase1,$hrefcase2);
			}elseif(!empty($hrefcase1) && empty($hrefcase2)){
				$href_arrayPre1 = $hrefcase1;
			}elseif(empty($hrefcase1) && !empty($hrefcase2)){
				$href_arrayPre1 = $hrefcase2;
			}

			$href_arrayPre2 = array();
			if(!empty($href_arrayPre1) && !empty($hrefcase3)){
				$href_arrayPre2 = array_merge($href_arrayPre1,$hrefcase3);
			}elseif(!empty($href_arrayPre1) && empty($hrefcase3)){
				$href_arrayPre2 = $href_arrayPre1;
			}elseif(empty($href_arrayPre1) && !empty($hrefcase3)){
				$href_arrayPre2 = $hrefcase3;
			}

			$href_arrayPre3 = array();
			if(!empty($href_arrayPre2) && !empty($hrefcase4)){
				$href_arrayPre3 = array_merge($href_arrayPre2,$hrefcase4);
			}elseif(!empty($href_arrayPre2) && empty($hrefcase4)){
				$href_arrayPre3 = $href_arrayPre2;
			}elseif(empty($href_arrayPre2) && !empty($hrefcase4)){
				$href_arrayPre3 = $hrefcase4;
			}

			$href_arrayPre4 = array();
			if(!empty($href_arrayPre3) && !empty($hrefcase5)){
				$href_arrayPre4 = array_merge($href_arrayPre3,$hrefcase5);
			}elseif(!empty($href_arrayPre3) && empty($hrefcase5)){
				$href_arrayPre4 = $href_arrayPre3;
			}elseif(empty($href_arrayPre3) && !empty($hrefcase5)){
				$href_arrayPre4 = $hrefcase5;
			}

			$href_array = array();
			if(!empty($href_arrayPre4) && !empty($hrefcase6)){
				$href_array = array_merge($href_arrayPre4,$hrefcase6);
			}elseif(!empty($href_arrayPre4) && empty($hrefcase6)){
				$href_array = $href_arrayPre4;
			}elseif(empty($href_arrayPre4) && !empty($hrefcase6)){
				$href_array = $hrefcase6;
			}
			// Merging Arrays Ends

			if(!empty($href_array)){
				$media_count = count($href_array);
				$Bimagesids = '';
				$VCI = '';
				$x = 1;
				foreach ($href_array as $bImgid) {
					if($bImgid != ''){
						$Bimagesids .= $bImgid;
						$VCI .= 'Brizy Content Media';
						if($x < $media_count){
							$Bimagesids .= ',';
							$VCI .= ',';
						}
					}
					$x++;
				}
			}
			if($featuredImageId != ''){
				if($Bimagesids != ''){
					$Bimagesids .= ',';
					$VCI .= ',';
				}
				$Bimagesids .= $featuredImageId;
				$VCI .= 'Featured Image';
			}
			$BimagesidSc = array($Bimagesids,$VCI,$prefixValue);
			$uniqueArr += array($PageId=>$BimagesidSc);
			$Bimagesids = null;
			$VCI = null;

		}
		if($siteorigin_ck_builder == "SiteOrigin"){
			$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
			$PageId = $getvalues->ID;
			$the_content = get_post_meta($PageId, 'panels_data', true);		
			$siteorigenArray = array();

			// Get row background images
			$media_urls = html_entity_decode($getvalues->post_content);
			preg_match_all('@"background_image_attachment":"([^"]+)"@', $media_urls, $backgroundId);
			$all_back_media_id = $backgroundId[1];
			foreach ($all_back_media_id as $back_value) {
				$background_design_image = getImageUrlbyId($back_value);
				array_push($siteorigenArray, $background_design_image);
			}
			foreach ($the_content['widgets'] as $valueid) {
				// $siteorigenArray = array();
				// $getContentUrl = array();
				if(array_key_exists("attachment_id", $valueid)){
					$sitegetimgurlattachment_id = getImageUrlbyId($valueid['attachment_id']);
					array_push($siteorigenArray, $sitegetimgurlattachment_id);
				}
				if(array_key_exists("ids", $valueid)){
					foreach ($valueid['ids'] as $galleryimg) {
						$sitegetimgurlids = getImageUrlbyId($galleryimg);
						array_push($siteorigenArray, $sitegetimgurlids);
					}
				}
				if(array_key_exists("features", $valueid)){
					foreach ($valueid['features'] as $features_img) {
						$sitegetimgurlfeatures = getImageUrlbyId($features_img['icon_image']);
						array_push($siteorigenArray, $sitegetimgurlfeatures);
					}
				}
				if(array_key_exists("image", $valueid)){
					$sitegetimgurlimage = getImageUrlbyId($valueid['image']);
					array_push($siteorigenArray, $sitegetimgurlimage);
				}
				if(array_key_exists("frames", $valueid)){
					foreach ($valueid['frames'] as $frames_img) {
						$sitegetimgurlframes = getImageUrlbyId($frames_img['background_image']);
						array_push($siteorigenArray, $sitegetimgurlframes);
					}
				}
				if(array_key_exists("content", $valueid)){
					$custom_html = $valueid['content'];
					preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
					foreach ($contentimg[1] as $imgurl){
						array_push($siteorigenArray, $imgurl);
					}
				}
				if(array_key_exists("content", $valueid)){
					preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
					foreach ($contentimgs[1] as $imgurls){
						array_push($siteorigenArray, $imgurls);
					}
				}			
				if(array_key_exists("text", $valueid)){
					$siteEditor = $valueid['text'];
					preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
					preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
					$groupsfilehref = $groupsfile[1];
					$groupsfilesrc = $srcgroupsfile[1];
					if(!empty($groupsfilehref)){
						foreach ($groupsfilehref as $filesvalue) {								
							$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getContentUrl[] = $filesvalue;
						    }
						}
					}
					if(!empty($groupsfilesrc)){
						foreach ($groupsfilesrc as $filesvaluesrc) {
							$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getContentUrl[] = $filesvaluesrc;
						    }
						}
					}
					preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
					preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
					preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
					preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
					preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
					preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
					preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
					preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
					preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
					preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
					preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
					preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
					preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
					preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
					preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
					preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
					preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
					preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
					preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
					preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
					preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
					// for vc video
					if(!empty($vc_video_url[1])){
						foreach ($vc_video_url[1] as $vc_video) {
							$getContentUrl[] = $vc_video;
						}
					}
					// for src
					if(!empty($group9[1])){
						foreach ($group9[1] as $group9s) {
							$getContentUrl[] = $group9s;
						}
					}
					// for mp3
					if(!empty($group11[1])){
						foreach ($group11[1] as $group11s) {
							$getContentUrl[] = $group11s;
						}
					}
					// for mp4
					if(!empty($group12[1])){
						foreach ($group12[1] as $group12s) {
							$getContentUrl[] = $group12s;
						}
					}
					// for pdf
					if(!empty($group13[1])){
						foreach ($group13[1] as $group13s) {
							$getContentUrl[] = $group13s;
						}
					}
					// for docx
					if(!empty($group14[1])){
						foreach ($group14[1] as $group14s) {
							$getContentUrl[] = $group14s;
						}
					}
					// for doc
					if(!empty($group15[1])){
						foreach ($group15[1] as $group15s) {
							$getContentUrl[] = $group15s;
						}
					}
					// for ppt
					if(!empty($group16[1])){
						foreach ($group16[1] as $group16s) {
							$getContentUrl[] = $group16s;
						}
					}
					// for xls
					if(!empty($group17[1])){
						foreach ($group17[1] as $group17s) {
							$getContentUrl[] = $group17s;
						}
					}
					// for pps
					if(!empty($group18[1])){
						foreach ($group18[1] as $group18s) {
							$getContentUrl[] = $group18s;
						}
					}
					// for ppsx
					if(!empty($group19[1])){
						foreach ($group19[1] as $group19s) {
							$getContentUrl[] = $group19s;
						}
					}
					// for xlsx
					if(!empty($group20[1])){
						foreach ($group20[1] as $group20s) {
							$getContentUrl[] = $group20s;
						}
					}
					// for odt
					if(!empty($group21[1])){
						foreach ($group21[1] as $group21s) {
							$getContentUrl[] = $group21s;
						}
					}
					// for ogg
					if(!empty($group22[1])){
						foreach ($group22[1] as $group22s) {
							$getContentUrl[] = $group22s;
						}
					}
					// for m4a
					if(!empty($group23[1])){
						foreach ($group23[1] as $group23s) {
							$getContentUrl[] = $group23s;
						}
					}
					// for wav
					if(!empty($group24[1])){
						foreach ($group24[1] as $group24s) {
							$getContentUrl[] = $group24s;
						}
					}
					// for mp4
					if(!empty($group25[1])){
						foreach ($group25[1] as $group25s) {
							$getContentUrl[] = $group25s;
						}
					}
					// for mov
					if(!empty($group26[1])){
						foreach ($group26[1] as $group26s) {
							$getContentUrl[] = $group26s;
						}
					}
					// for avi
					if(!empty($group27[1])){
						foreach ($group27[1] as $group27s) {
							$getContentUrl[] = $group27s;
						}
					}
					// for 3gp
					if(!empty($group28[1])){
						foreach ($group28[1] as $group28s) {
							$getContentUrl[] = $group28s;
						}
					}
					// for pptx
					if(!empty($group29[1])){
						foreach ($group29[1] as $group29s) {
							$getContentUrl[] = $group29s;
						}
					}
				}
				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				if(!empty($group10)){
					$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
					if(!empty($bgimgUrl1)){
						foreach ($bgimgUrl1 as $filesvaluebackground) {								
							$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
							if(in_array($fileExtension, $totalExtensionsAvailable)){
						        $getContentUrl[] = $filesvaluebackground;
						    }
						}
					}
				}
				$VCmediaContent = array_unique($getContentUrl);
				$siteorigenArray = array_unique($siteorigenArray);
				$siteOriginUrl = array();
				if(!empty($VCmediaContent) && !empty($siteorigenArray)){
					$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
				}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
					$siteOriginUrl = $VCmediaContent;
				}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){
					$siteOriginUrl = $siteorigenArray;
				}
				$final_array = array();
				foreach ($siteOriginUrl as $thevalues) {
					array_push($final_array, $thevalues);
				}
			}
			
			$SOidsarray = array_unique($final_array);
			$SOimagesids = implode(',', $SOidsarray);
			if(!empty($SOidsarray)){
				$media_count = count($SOidsarray);
				$VCI = '';
				for ($x = 1; $x <= $media_count; $x++) {
					$VCI .= 'SiteOrigin Content Media';
					if($x < $media_count){
						$VCI .= ',';
					}
				}
			}
			if($featuredImageId != ''){
				$SOimagesids .= ','.$featuredImageId;
				$VCI .= ',Featured Image';
			}
			if(!empty($SOimagesids)){
				$SOimagesidSc = array($SOimagesids,$VCI,$prefixValue);
				$uniqueArr += array($PageId=>$SOimagesidSc);
			}
			$SOimagesids = null;
			$VCI = null;
			$getFiles = null;
		}
		if($beaver_builder == 1){
			$getFiles = array();
			$BBmergeArray = array();
			$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

			$the_content = $getvalues->post_content;
			// echo "<pre>"; print_r(htmlspecialchars($the_content));
			$PageId = $getvalues->ID;
			preg_match_all('@src="([^"]+)"@', $the_content, $group6);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group7);

			preg_match_all('@src="([^"]+)"@', $the_content, $group9);
			preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
			preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
			preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
			preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
			preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
			preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
			preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
			preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
			preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
			preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
			preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
			preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
			preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
			preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
			preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
			preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
			preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
			preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
			
			preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);
			$groupsfiles = $groupsfiles[1];
			if(!empty($groupsfiles)){
				foreach ($groupsfiles as $filesvalue) {
					$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
					if(in_array($fileExtension, $totalExtensionsAvailable)){
				        $getFiles[] = $filesvalue;
				    }
				}
			}
			// for vc video
			if(!empty($vc_video_url[1])){
				foreach ($vc_video_url[1] as $vc_video) {
					$getFiles[] = $vc_video;
				}
			}

			// for src
			if(!empty($group9[1])){
				foreach ($group9[1] as $group9s) {
					$getFiles[] = $group9s;
				}
			}
			// for mp3
			if(!empty($group11[1])){
				foreach ($group11[1] as $group11s) {
					$getFiles[] = $group11s;
				}
			}
			// for mp4
			if(!empty($group12[1])){
				foreach ($group12[1] as $group12s) {
					$getFiles[] = $group12s;
				}
			}

			// for pdf
			if(!empty($group13[1])){
				foreach ($group13[1] as $group13s) {
					$getFiles[] = $group13s;
				}
			}

			// for docx
			if(!empty($group14[1])){
				foreach ($group14[1] as $group14s) {
					$getFiles[] = $group14s;
				}
			}

			// for doc
			if(!empty($group15[1])){
				foreach ($group15[1] as $group15s) {
					$getFiles[] = $group15s;
				}
			}

			// for ppt
			if(!empty($group16[1])){
				foreach ($group16[1] as $group16s) {
					$getFiles[] = $group16s;
				}
			}

			// for xls
			if(!empty($group17[1])){
				foreach ($group17[1] as $group17s) {
					$getFiles[] = $group17s;
				}
			}

			// for pps
			if(!empty($group18[1])){
				foreach ($group18[1] as $group18s) {
					$getFiles[] = $group18s;
				}
			}

			// for ppsx
			if(!empty($group19[1])){
				foreach ($group19[1] as $group19s) {
					$getFiles[] = $group19s;
				}
			}

			// for xlsx
			if(!empty($group20[1])){
				foreach ($group20[1] as $group20s) {
					$getFiles[] = $group20s;
				}
			}

			// for odt
			if(!empty($group21[1])){
				foreach ($group21[1] as $group21s) {
					$getFiles[] = $group21s;
				}
			}

			// for ogg
			if(!empty($group22[1])){
				foreach ($group22[1] as $group22s) {
					$getFiles[] = $group22s;
				}
			}

			// for m4a
			if(!empty($group23[1])){
				foreach ($group23[1] as $group23s) {
					$getFiles[] = $group23s;
				}
			}

			// for wav
			if(!empty($group24[1])){
				foreach ($group24[1] as $group24s) {
					$getFiles[] = $group24s;
				}
			}

			// for mp4
			if(!empty($group25[1])){
				foreach ($group25[1] as $group25s) {
					$getFiles[] = $group25s;
				}
			}

			// for mov
			if(!empty($group26[1])){
				foreach ($group26[1] as $group26s) {
					$getFiles[] = $group26s;
				}
			}

			// for avi
			if(!empty($group27[1])){
				foreach ($group27[1] as $group27s) {
					$getFiles[] = $group27s;
				}
			}

			// for 3gp
			if(!empty($group28[1])){
				foreach ($group28[1] as $group28s) {
					$getFiles[] = $group28s;
				}
			}

			// for pptx
			if(!empty($group29[1])){
				foreach ($group29[1] as $group29s) {
					$getFiles[] = $group29s;
				}
			}

			$beaver_src_array = $group6[1];
			$beaver_mp4_array = $group7[1];
			// Merging arrays
			$BBmergePreArray = array();
			if(!empty($getFiles) && !empty($beaver_src_array)){
				$BBmergePreArray = array_merge(array_unique($beaver_src_array),array_unique($getFiles));
			}elseif(!empty($getFiles) && empty($beaver_src_array)){
				$BBmergePreArray = array_unique($getFiles);
			}elseif(empty($getFiles) && !empty($beaver_src_array)){
				$BBmergePreArray = array_unique($beaver_src_array);
			}

			$BBmergeArray = array();
			if(!empty($BBmergePreArray) && !empty($beaver_mp4_array)){
				$BBmergeArray = array_merge(array_unique($beaver_mp4_array),array_unique($BBmergePreArray));
			}elseif(!empty($BBmergePreArray) && empty($beaver_mp4_array)){
				$BBmergeArray = array_unique($BBmergePreArray);
			}elseif(empty($BBmergePreArray) && !empty($beaver_mp4_array)){
				$BBmergeArray = array_unique($beaver_mp4_array);
			}

			$BBidsarray = array();
			$BBmergeArrays = array();
			$BBmergeArrays = array_unique($BBmergeArray);
			foreach ($BBmergeArrays as $bbimgurl) {
				if($bbimgurl){
					array_push($BBidsarray, $bbimgurl);
				}
			}
			$BBimagesids = array();
			$BBimagesids = implode(',', $BBidsarray);
			if(!empty($BBidsarray)){
				$media_count = count($BBidsarray);
				$VCI = '';
				for ($x = 1; $x <= $media_count; $x++) {
					$VCI .= 'Beaver Builder Content Media';
					if($x < $media_count){
						$VCI .= ',';
					}
				}
			}
			if($featuredImageId != ''){
				$BBimagesids .= ','.$featuredImageId;
				$VCI .= ',Featured Image';
			}
			if(!empty($SOimagesids)){
				$BBimagesidSc = array($BBimagesids,$VCI,$prefixValue);			
				$uniqueArr += array($PageId=>$BBimagesidSc);
			}
			$BBimagesids = null;
			$VCI = null;
		}
		if($vc_builder == 'true'){
			$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

			$the_content = $getvalues->post_content;
			preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
			preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);
			preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
			preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
			/* Raw HTML */
			preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
			$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
			$RowContent = '';
			$RawimgUrl = array();
			if(!empty($RowHtmls)){
				foreach ($RowHtmls as $RowHtml) {
					$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
					$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
					
					preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
					$RawimgUrl[] = str_replace(['?_=1'], [''], $group16[1]);
				}
			}
			preg_match_all('@src="([^"]+)"@', $the_content, $group9);
			preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
			preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
			preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
			preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
			preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
			preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
			preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
			preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
			preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
			preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
			preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
			preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
			preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
			preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
			preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
			preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
			preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
			preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

			preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
			preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
			$groupsfiles = $groupsfiles[1];
			if(!empty($groupsfiles)){
				foreach ($groupsfiles as $filesvalue) {
					$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
					if(in_array($fileExtension, $totalExtensionsAvailable)){
				        $getFiles[] = $filesvalue;
				    }
				}
			}
			// for vc video
			if(!empty($vc_video_url[1])){
				foreach ($vc_video_url[1] as $vc_video) {
					$getFiles[] = $vc_video;
				}
			}

			// for src
			if(!empty($group9[1])){
				foreach ($group9[1] as $group9s) {
					$getFiles[] = $group9s;
				}
			}
			// for mp3
			if(!empty($group11[1])){
				foreach ($group11[1] as $group11s) {
					$getFiles[] = $group11s;
				}
			}
			// for mp4
			if(!empty($group12[1])){
				foreach ($group12[1] as $group12s) {
					$getFiles[] = $group12s;
				}
			}

			// for pdf
			if(!empty($group13[1])){
				foreach ($group13[1] as $group13s) {
					$getFiles[] = $group13s;
				}
			}

			// for docx
			if(!empty($group14[1])){
				foreach ($group14[1] as $group14s) {
					$getFiles[] = $group14s;
				}
			}

			// for doc
			if(!empty($group15[1])){
				foreach ($group15[1] as $group15s) {
					$getFiles[] = $group15s;
				}
			}

			// for ppt
			if(!empty($group16[1])){
				foreach ($group16[1] as $group16s) {
					$getFiles[] = $group16s;
				}
			}

			// for xls
			if(!empty($group17[1])){
				foreach ($group17[1] as $group17s) {
					$getFiles[] = $group17s;
				}
			}

			// for pps
			if(!empty($group18[1])){
				foreach ($group18[1] as $group18s) {
					$getFiles[] = $group18s;
				}
			}

			// for ppsx
			if(!empty($group19[1])){
				foreach ($group19[1] as $group19s) {
					$getFiles[] = $group19s;
				}
			}

			// for xlsx
			if(!empty($group20[1])){
				foreach ($group20[1] as $group20s) {
					$getFiles[] = $group20s;
				}
			}

			// for odt
			if(!empty($group21[1])){
				foreach ($group21[1] as $group21s) {
					$getFiles[] = $group21s;
				}
			}

			// for ogg
			if(!empty($group22[1])){
				foreach ($group22[1] as $group22s) {
					$getFiles[] = $group22s;
				}
			}

			// for m4a
			if(!empty($group23[1])){
				foreach ($group23[1] as $group23s) {
					$getFiles[] = $group23s;
				}
			}

			// for wav
			if(!empty($group24[1])){
				foreach ($group24[1] as $group24s) {
					$getFiles[] = $group24s;
				}
			}

			// for mp4
			if(!empty($group25[1])){
				foreach ($group25[1] as $group25s) {
					$getFiles[] = $group25s;
				}
			}

			// for mov
			if(!empty($group26[1])){
				foreach ($group26[1] as $group26s) {
					$getFiles[] = $group26s;
				}
			}

			// for avi
			if(!empty($group27[1])){
				foreach ($group27[1] as $group27s) {
					$getFiles[] = $group27s;
				}
			}

			// for 3gp
			if(!empty($group28[1])){
				foreach ($group28[1] as $group28s) {
					$getFiles[] = $group28s;
				}
			}

			// for pptx
			if(!empty($group29[1])){
				foreach ($group29[1] as $group29s) {
					$getFiles[] = $group29s;
				}
			}
			/* Raw HTML */
			$PageId = $getvalues->ID;
			preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
			$groupsfile = $groupsfile[1];
			if(!empty($groupsfile)){
				foreach ($groupsfile as $filesvalue) {
					$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
					if(in_array($fileExtension, $totalExtensionsAvailable)){
				        $getFiles[] = $filesvalue;
				    }
				}
			}
			preg_match_all('@src="([^"]+)"@', $the_content, $group9);
			// preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
			// preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);

			preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
			$bgimgUrl = str_replace(['(',')'], ['',''], $group10[1]);

			$RawimgUrlArrys = array();
			if(!empty($group9[1]) && !empty($bgimgUrl)){
				$RawimgUrlArrys = array_merge($group9[1],$bgimgUrl);
			}elseif(!empty($group9[1]) && empty($bgimgUrl)){
				$RawimgUrlArrys = $group9[1];
			}elseif(empty($group9[1]) && !empty($bgimgUrl)){
				$RawimgUrlArrys = $bgimgUrl;
			}

			$VCmediaContents = array();
			if(!empty($RawimgUrlArrys) && !empty($getFiles)){
				$VCmediaContents = array_merge($RawimgUrlArrys,$getFiles);
			}elseif(!empty($RawimgUrlArrys) && empty($getFiles)){
				$VCmediaContents = $RawimgUrlArrys;
			}elseif(empty($RawimgUrlArrys) && !empty($getFiles)){
				$VCmediaContents = $getFiles;
			}

			$VCmediaContent = array();
			if(!empty($VCmediaContents) && !empty($RawimgUrl)){
				$VCmediaContent = array_merge($VCmediaContents,$RawimgUrl);
			}elseif(!empty($VCmediaContents) && empty($RawimgUrl)){
				$VCmediaContent = $VCmediaContents;
			}elseif(empty($VCmediaContents) && !empty($RawimgUrl)){
				$VCmediaContent = $RawimgUrl;
			}

			$VCsingleimageids = array_unique(explode(',', implode(',', $group1[1])));
			$VCgalleryimageids = array_unique(explode(',', implode(',', $group2[2])));
			$VCcarouselimageids = array_unique(explode(',', implode(',', $group3[1])));
			$VChoverboximageids = array_unique(explode(',', implode(',', $group4[1])));

			if(!empty($VCsingleimageids) || !empty($VCgalleryimageids) || !empty($VCcarouselimageids) || !empty($VChoverboximageids) || !empty($VCmediaContent)){
				$VCimageids = '';
				$comma = '';
				if($VCsingleimageids != ""){
					foreach ($VCsingleimageids as $VCsingleimageid) {
						if($VCimageids != ''){ $comma = ','; }
						$VCimageids .= $comma.getImageUrlbyId($VCsingleimageid);
						$VCI .= $comma.'Visual Composer Image';
					}
				}
				if(!empty($VCmediaContent)){
					foreach ($VCmediaContent as $VCmediaContentUrl) {
						if($VCimageids != ''){ $comma = ','; }
						$VCimageids .= $comma.$VCmediaContentUrl;
						$VCI .= $comma.'Visual Composer Media';
					}
				}
				if($VCgalleryimageids != ""){
					foreach ($VCgalleryimageids as $VCgalleryimageid) {
						if($VCimageids != ''){ $comma = ','; }
						$VCimageids .= $comma.getImageUrlbyId($VCgalleryimageid);
						$VCI .= $comma.'Visual Composer Gallery Image';
					}
				}
				if($VCcarouselimageids != ""){
					foreach ($VCcarouselimageids as $VCcarouselimageid) {
						if($VCimageids != ''){ $comma = ','; }
						$VCimageids .= $comma.getImageUrlbyId($VCcarouselimageid);
						$VCI .= $comma.'Visual Composer Carousel Image';
					}
				}
				if($VChoverboximageids != ""){
					foreach ($VChoverboximageids as $VChoverboximageid) {
						if($VCimageids != ''){ $comma = ','; }
						$VCimageids .= $comma.getImageUrlbyId($VChoverboximageid);
						$VCI .= $comma.'Visual Composer Hover Box Image';
					}
				}
			}
			if($productgalleryImg != ''){
				$productgalleryImgArr = explode(",", $productgalleryImg);
				foreach ($productgalleryImgArr as $productgalleryImgURL) {
					if($VCimageids != ''){
						$VCimageids .= ',';
						$VCI .= ',';
					}
					$VCimageids .= getImageUrlbyId($productgalleryImgURL);
					$VCI .= 'Product Gallery Image';
				}
			}
			if($featuredImageId != ''){
				if($VCimageids != ''){ $comma = ','; }
				$VCimageids .= $comma.$featuredImageId;
				$VCI .= $comma.'Featured Image';
			}
			$VCimagesidSc = array($VCimageids,$VCI,$prefixValue);
			$uniqueArr += array($PageId=>$VCimagesidSc);
			$VCimageids = null;
			$VCI = null;
			$getFiles = null;
			$VCsingleimageids = '';
			$VCmediaContent = null;
			$VCgalleryimageids = '';
			$VCcarouselimageids = '';
			$VChoverboximageids = '';
			$productgalleryImg = '';

		}
		if($getvalues->post_type == 'product'){
			$the_content = $getvalues->post_content;
			$PageId = $getvalues->ID;
			preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
			$groupsfile = $groupsfile[1];
			if(!empty($groupsfile)){
				foreach ($groupsfile as $filesvalue) {
					$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
					if(in_array($fileExtension, $totalExtensionsAvailable)){
				        $getFiles[] = $filesvalue;
				    }
				}
			}
			preg_match_all('@src="([^"]+)"@', $the_content, $group9);
			preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
			preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
			preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
			preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
			preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
			preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
			preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
			preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
			preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
			preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
			preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
			preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
			preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
			preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
			preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
			preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
			preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
			preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
			// for vc video
			if(!empty($vc_video_url[1])){
				foreach ($vc_video_url[1] as $vc_video) {
					$getFiles[] = $vc_video;
				}
			}
			// for src
			if(!empty($group9[1])){
				foreach ($group9[1] as $group9s) {
					$getFiles[] = $group9s;
				}
			}
			// for mp3
			if(!empty($group11[1])){
				foreach ($group11[1] as $group11s) {
					$getFiles[] = $group11s;
				}
			}
			// for mp4
			if(!empty($group12[1])){
				foreach ($group12[1] as $group12s) {
					$getFiles[] = $group12s;
				}
			}
			// for pdf
			if(!empty($group13[1])){
				foreach ($group13[1] as $group13s) {
					$getFiles[] = $group13s;
				}
			}
			// for docx
			if(!empty($group14[1])){
				foreach ($group14[1] as $group14s) {
					$getFiles[] = $group14s;
				}
			}
			// for doc
			if(!empty($group15[1])){
				foreach ($group15[1] as $group15s) {
					$getFiles[] = $group15s;
				}
			}
			// for ppt
			if(!empty($group16[1])){
				foreach ($group16[1] as $group16s) {
					$getFiles[] = $group16s;
				}
			}
			// for xls
			if(!empty($group17[1])){
				foreach ($group17[1] as $group17s) {
					$getFiles[] = $group17s;
				}
			}
			// for pps
			if(!empty($group18[1])){
				foreach ($group18[1] as $group18s) {
					$getFiles[] = $group18s;
				}
			}
			// for ppsx
			if(!empty($group19[1])){
				foreach ($group19[1] as $group19s) {
					$getFiles[] = $group19s;
				}
			}
			// for xlsx
			if(!empty($group20[1])){
				foreach ($group20[1] as $group20s) {
					$getFiles[] = $group20s;
				}
			}
			// for odt
			if(!empty($group21[1])){
				foreach ($group21[1] as $group21s) {
					$getFiles[] = $group21s;
				}
			}
			// for ogg
			if(!empty($group22[1])){
				foreach ($group22[1] as $group22s) {
					$getFiles[] = $group22s;
				}
			}
			// for m4a
			if(!empty($group23[1])){
				foreach ($group23[1] as $group23s) {
					$getFiles[] = $group23s;
				}
			}
			// for wav
			if(!empty($group24[1])){
				foreach ($group24[1] as $group24s) {
					$getFiles[] = $group24s;
				}
			}
			// for mp4
			if(!empty($group25[1])){
				foreach ($group25[1] as $group25s) {
					$getFiles[] = $group25s;
				}
			}
			// for mov
			if(!empty($group26[1])){
				foreach ($group26[1] as $group26s) {
					$getFiles[] = $group26s;
				}
			}
			// for avi
			if(!empty($group27[1])){
				foreach ($group27[1] as $group27s) {
					$getFiles[] = $group27s;
				}
			}
			// for 3gp
			if(!empty($group28[1])){
				foreach ($group28[1] as $group28s) {
					$getFiles[] = $group28s;
				}
			}
			// for pptx
			if(!empty($group29[1])){
				foreach ($group29[1] as $group29s) {
					$getFiles[] = $group29s;
				}
			}

			preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
			$bgimgUrl = str_replace(['(',')'], ['',''], $group10[1]);

			$guten_media = array();
			if(!empty($group9[1]) && !empty($bgimgUrl)){
				$guten_media = array_merge($group9[1],$bgimgUrl);
			}elseif(!empty($group9[1]) && empty($bgimgUrl)){
				$guten_media = $group9[1];
			}elseif(empty($group9[1]) && !empty($bgimgUrl)){
				$guten_media = $bgimgUrl;
			}

			$gutenbergImgUrls = array();
			if(!empty($guten_media) && !empty($totalExt)){
				$gutenbergImgUrls = array_merge($guten_media,$totalExt);
			}elseif(!empty($guten_media) && empty($totalExt)){
				$gutenbergImgUrls = $guten_media;
			}elseif(empty($guten_media) && !empty($totalExt)){
				$gutenbergImgUrls = $totalExt;
			}


			if(!empty($gutenbergImgUrls) && !empty($getFiles)){
				$hrefMedias = array_unique(array_merge($getFiles,$gutenbergImgUrls));
			}else{
				if(!empty($getFiles)){
					$hrefMedias = array_unique($getFiles);
				}elseif(!empty($gutenbergImgUrls)){
					$hrefMedias = array_unique($gutenbergImgUrls);
				}else{
					$hrefMedias = array();
				}
			}
			// Merging arrays
			$totalContentMedia = array();
			if(!empty($gutenbergImgUrls) && !empty($hrefMedias)){
				$totalContentMedia = array_merge($gutenbergImgUrls,$hrefMedias);
			}elseif(!empty($gutenbergImgUrls) && empty($hrefMedias)){
				$totalContentMedia = $gutenbergImgUrls;
			}elseif(empty($gutenbergImgUrls) && !empty($hrefMedias)){
				$totalContentMedia = $hrefMedias;
			}

			$ProImgids = array();
			foreach ($totalContentMedia as $gutenbergImgUrl) {
				if($gutenbergImgUrl){
					array_push($ProImgids, $gutenbergImgUrl);
				}
			}
			$Proimagesids = '';
			$PCI = '';
			if(!empty($ProImgids)){
				$media_count = count($ProImgids);
				$x = 1;
				foreach ($ProImgids as $gutenbergImgid) {
					if($gutenbergImgid != ''){
						$Proimagesids .= $gutenbergImgid;
						$PCI .= 'Simple/Gutenberg Content Media';
						if($x < $media_count){
							$Proimagesids .= ',';
							$PCI .= ',';
						}
					}
					$x++;
				}
			}
			if($productgalleryImg != ''){
				$productgalleryImgArr = explode(",", $productgalleryImg);
				foreach ($productgalleryImgArr as $productgalleryImgURL) {
					if($Proimagesids != ''){
						$Proimagesids .= ',';
						$PCI .= ',';
					}
					$Proimagesids .= getImageUrlbyId($productgalleryImgURL);
					$PCI .= 'Product Gallery Image';
				}
			}
			if($featuredImageId != ''){
				if($Proimagesids != ''){
					$Proimagesids .= ',';
					$PCI .= ',';
				}
				$Proimagesids .= $featuredImageId;
				$PCI .= 'Featured Image';
			}
			$ProimagesidSc = array($Proimagesids,$PCI,$prefixValue);
			$uniqueArr += array($PageId=>$ProimagesidSc);
			$Proimagesids = null;
			$the_content = null;
			$PageId = null;
			$PCI = null;
			$totalContentMedia = null;
			$gutenbergImgUrls = null;
			$hrefMedias = null;
			$getFiles = null;
			$groupsfile = null;
		}
		if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder){
			$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

			$the_content = $getvalues->post_content;			
			$PageId = $getvalues->ID;

			preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
			$groupsfile = $groupsfile[1];
			if(!empty($groupsfile)){
				foreach ($groupsfile as $filesvalue) {
					$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
					if(in_array($fileExtension, $totalExtensionsAvailable)){
				        $getFiles[] = $filesvalue;
				    }
				}
			}

			preg_match_all('@src="([^"]+)"@', $the_content, $group9);
			preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
			preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
			preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
			preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
			preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
			preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
			preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
			preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
			preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
			preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
			preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
			preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
			preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
			preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
			preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
			preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
			preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
			preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
			preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

			// for src
			if(!empty($group9[1])){
				foreach ($group9[1] as $group9s) {
					$getFiles[] = $group9s;
				}
			}
			// for mp3
			if(!empty($group11[1])){
				foreach ($group11[1] as $group11s) {
					$getFiles[] = $group11s;
				}
			}
			// for mp4
			if(!empty($group12[1])){
				foreach ($group12[1] as $group12s) {
					$getFiles[] = $group12s;
				}
			}

			// for pdf
			if(!empty($group13[1])){
				foreach ($group13[1] as $group13s) {
					$getFiles[] = $group13s;
				}
			}

			// for docx
			if(!empty($group14[1])){
				foreach ($group14[1] as $group14s) {
					$getFiles[] = $group14s;
				}
			}

			// for doc
			if(!empty($group15[1])){
				foreach ($group15[1] as $group15s) {
					$getFiles[] = $group15s;
				}
			}

			// for ppt
			if(!empty($group16[1])){
				foreach ($group16[1] as $group16s) {
					$getFiles[] = $group16s;
				}
			}

			// for xls
			if(!empty($group17[1])){
				foreach ($group17[1] as $group17s) {
					$getFiles[] = $group17s;
				}
			}

			// for pps
			if(!empty($group18[1])){
				foreach ($group18[1] as $group18s) {
					$getFiles[] = $group18s;
				}
			}

			// for ppsx
			if(!empty($group19[1])){
				foreach ($group19[1] as $group19s) {
					$getFiles[] = $group19s;
				}
			}

			// for xlsx
			if(!empty($group20[1])){
				foreach ($group20[1] as $group20s) {
					$getFiles[] = $group20s;
				}
			}

			// for odt
			if(!empty($group21[1])){
				foreach ($group21[1] as $group21s) {
					$getFiles[] = $group21s;
				}
			}

			// for ogg
			if(!empty($group22[1])){
				foreach ($group22[1] as $group22s) {
					$getFiles[] = $group22s;
				}
			}

			// for m4a
			if(!empty($group23[1])){
				foreach ($group23[1] as $group23s) {
					$getFiles[] = $group23s;
				}
			}

			// for wav
			if(!empty($group24[1])){
				foreach ($group24[1] as $group24s) {
					$getFiles[] = $group24s;
				}
			}

			// for mp4
			if(!empty($group25[1])){
				foreach ($group25[1] as $group25s) {
					$getFiles[] = $group25s;
				}
			}

			// for wmv
			if(!empty($group26[1])){
				foreach ($group26[1] as $group26s) {
					$getFiles[] = $group26s;
				}
			}

			// for avi
			if(!empty($group27[1])){
				foreach ($group27[1] as $group27s) {
					$getFiles[] = $group27s;
				}
			}

			// for 3gp
			if(!empty($group28[1])){
				foreach ($group28[1] as $group28s) {
					$getFiles[] = $group28s;
				}
			}

			// for pptx
			if(!empty($group29[1])){
				foreach ($group29[1] as $group29s) {
					$getFiles[] = $group29s;
				}
			}

			preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
			$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

			if(!empty($gutenbergImgUrls) && !empty($getFiles)){
				$hrefMedias = array_unique(array_merge($getFiles,$gutenbergImgUrls));
			}else{
				if(!empty($getFiles)){
					$hrefMedias = array_unique($getFiles);
				}elseif(!empty($gutenbergImgUrls)){
					$hrefMedias = array_unique($gutenbergImgUrls);
				}else{
					$hrefMedias = array();
				}
			}

			// Merging arrays
			$totalContentMedia = array();
			if(!empty($gutenbergImgUrls) && !empty($hrefMedias)){
				$totalContentMedia = array_merge($gutenbergImgUrls,$hrefMedias);
			}elseif(!empty($gutenbergImgUrls) && empty($hrefMedias)){
				$totalContentMedia = $gutenbergImgUrls;
			}elseif(empty($gutenbergImgUrls) && !empty($hrefMedias)){
				$totalContentMedia = $hrefMedias;
			}

			$totalContentMedia = array_filter($totalContentMedia);
			
			$gutenbergImgids = array();
			foreach ($totalContentMedia as $gutenbergImgUrl) {
				if($gutenbergImgUrl){
					array_push($gutenbergImgids, $gutenbergImgUrl);
				}
			}
			$imagesids = '';
			$VCI = '';
			if(!empty($gutenbergImgids)){
				$media_count = count($gutenbergImgids);
				$x = 1;
				foreach ($gutenbergImgids as $gutenbergImgid) {
					if($gutenbergImgid != ''){
						$imagesids .= $gutenbergImgid;
						$VCI .= 'Simple/Gutenberg Content Media';
						if($x < $media_count){
							$imagesids .= ',';
							$VCI .= ',';
						}
					}
					$x++;
				}
			}
			if($featuredImageId != ''){
				if($imagesids != ''){
					$imagesids .= ',';
					$VCI .= ',';
				}
				$imagesids .= $featuredImageId;
				$VCI .= 'Featured Image';
			}
			if(!empty($imagesids)){
				$SimagesidSc = array($imagesids,$VCI,$prefixValue);
				$uniqueArr += array($PageId=>$SimagesidSc);
			}
			$imagesids = null;
			$the_content = null;
			$PageId = null;
			$gutenbergImgids = null;
			$getFiles = null;
			$VCI = null;
		}
	}
	return $uniqueArr;
}
// Get media from page builder Coded start by Hemant
function getMultiSitePageBuilderContentMedia(){
	global $wpdb;
	$prefixes = getAllSitePrefix();
	$uniqueArr = array();
	foreach ($prefixes as $mutliarray) {
		$getFiles = array();
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		foreach (get_sites() as $all_sites) {
			if($all_sites->blog_id == $multisiteId){
				$multisite_url = $all_sites->path;
			}
			if($all_sites->blog_id == 1){
				$mainsite_url = $all_sites->path;
			}
        }
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;

		$pbContent = $wpdb->get_results("SELECT * from ".$prefixValue."posts WHERE post_status !='trash' AND post_type NOT IN('revision','scheduled-action','attachment')");
		
		$getFiles = array();
		$inc = 0;
		// Total types of extensions
		$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','PDF','DOCX','doc','DOC','XLS','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');		
		foreach ($pbContent as $getvalues) {
			$elem_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_edit_mode' AND post_id=$getvalues->ID" );
			$elem_builder = $elem_builder[0]->meta_value;

			$vc_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_wpb_vc_js_status' AND post_id=$getvalues->ID" );
			$vc_builder = $vc_builder[0]->meta_value;

			$beaver_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_fl_builder_enabled' AND post_id=$getvalues->ID" );
			$beaver_builder = $beaver_builder[0]->meta_value;

			$siteorigin_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id=$getvalues->ID" );
			$siteorigin_builder = $siteorigin_builder[0]->meta_value;

			$brizy_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='brizy' AND post_id=$getvalues->ID" );
			$brizy_builder = $brizy_builder[0]->meta_value;

			$oxygen_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id=$getvalues->ID" );
			$oxygen_builder = $oxygen_builder[0]->meta_value;

			$siteorigin_ck_builder = '';
			if(!empty($siteorigin_builder)){
				$siteorigin_ck_builder = "SiteOrigin";
			}
			$featuredImageArr = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_thumbnail_id' AND post_id=$getvalues->ID" );
			@$featuredImagesId = $featuredImageArr[0]->meta_value;

			if($featuredImagesId){
				$featuredImageId = getMultiSiteImageUrlbyId($featuredImagesId,$prefixValue);
			}else{
				$featuredImageId = '';
			}
			$productgalleryArr = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_product_image_gallery' AND post_id=$getvalues->ID" );
			@$productgalleryImg = $productgalleryArr[0]->meta_value;
			
			if($elem_builder == 'builder'){
				$PageId = $getvalues->ID;
				$elementor_sql = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_data' AND post_id=$PageId" );
				$elementor_content = $elementor_sql[0]->meta_value;

				// Get files url from URL & ID tag start
				$group4 = array();
				$group5 = array();
				$getsrcfiles = array();
				preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
				preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);

				// Get files url from URL & ID tag end
				// $myVar = htmlspecialchars($elementor_content, ENT_QUOTES);

				$imgArry = array();
				foreach ($group4[1] as $getId) {
					//$getImgidbyURL = getImgidbyURL(stripslashes($getId));
					$getImgidbyURL = stripslashes($getId);
					$case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$getImgidbyURL);
					$case1_dir_path = str_replace($multisite_url, $mainsite_url, $case1_dir_path);
					if(file_exists($case1_dir_path)){
						array_push($imgArry, $getImgidbyURL);
					}
				}

				// Get files url from href tag start
				$gethreffiles = array();
				preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $gethreffiles);
				$hreffiles = array();
				if(!empty($gethreffiles[1])){
					foreach ($gethreffiles[1] as $hrefValues) {
						$hreffiles = stripslashes($hrefValues); // href URL data
						$case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$hreffiles);
						$case2_dir_path = str_replace($multisite_url, $mainsite_url, $case2_dir_path);
						if(file_exists($case2_dir_path)){
							array_push($imgArry, $hreffiles);							
						}
					}
				}
				// Get files url from href tag end

				// Get files src from href tag start
				$srcfiles = array();
				preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
				if(!empty($getsrcfiles[1])){
					foreach ($getsrcfiles[1] as $srcValues) {
						$srcfiles = stripslashes($srcValues); // href URL data
						$case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$srcfiles);
						$case3_dir_path = str_replace($multisite_url, $mainsite_url, $case3_dir_path);
						if(file_exists($case3_dir_path)){
							array_push($imgArry, $srcfiles);
						}
					}
				}
				// Get files src from href tag start

				$contentimgid = implode(',', $imgArry);
				if(!empty($group5[1])){
					$group5arr1 = array();
					foreach ($group5[1] as $GalleryImgValue) {
						$group5arr1[] = explode(',', $GalleryImgValue);
					}
					foreach ($group5arr1 as $imggelid) {
						$getidurlimge  = getMultiSiteImageUrlbyId($imggelid,$prefixValue);						
						$contentimgid .= ','.$getidurlimge;
					}
				}

				$ERimagesids = implode(',', array_keys(array_flip(explode(',', $contentimgid))));

				if($ERimagesids != ""){
					$media_count = count(explode(",", $ERimagesids));
					$VCI = '';
					for ($x = 1; $x <= $media_count; $x++) {
						$VCI .= 'Elementor Content Media';
						if($x < $media_count){
							$VCI .= ',';
						}
					}
				}				
				if($featuredImageId != ''){
					$ERimagesids .= ','.$featuredImageId;
					$VCI .= ',Featured Image';
				}
				if(!empty($ERimagesids)){
					$ERimagesidSc = array($ERimagesids,$VCI,$prefixValue);				
					$uniqueArr += array($PageId=>$ERimagesidSc);
				}
				$ERimagesids = null;
			}
			if($oxygen_builder){
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
				$PageId = $getvalues->ID;
				$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id=$PageId" );
				$the_content = $the_content[0]->meta_value;
				
				preg_match_all('@"url":"([^"]+)"@', $the_content, $WordPressWidgetImages);
				preg_match_all('@"src":"([^"]+)"@', $the_content, $contentimgurl);
				preg_match_all('@"background-image":"([^"]+)"@', $the_content, $contentimgurl1);
				preg_match_all('@"image_ids":"([^"]+)"@', $the_content, $gelleryimgids);
				preg_match_all('@"code-php":"([^"]+)"@', $the_content, $phpcodes_encode);
				
				$OxyCodeArr = array();
				foreach ($phpcodes_encode[1] as $phpcode_encode) {
					$phpcodes_decode = base64_decode($phpcode_encode);
					preg_match_all('@(?:src[^>]+>)(.*?)@', $phpcodes_decode, $phpcodesImgUrl);
					$phpcode_encode_url = str_replace(['src=',"'",'"',"/>",">"," type=application/pdf"], ['','','','','',''], $phpcodesImgUrl[0]);
					$OxyCodeArr[] = explode(" ", $phpcode_encode_url[0]);
				}

				$OXImageurls = array();
				foreach ($OxyCodeArr as $OxyCodevalue) {
					$OXImageurls[] = $OxyCodevalue[0];
				}

				$widget_images = array();
				foreach ($WordPressWidgetImages[1] as $WordPressWidgetValue) {
					$widget_images[] = base64_decode($WordPressWidgetValue);
				}

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_gallery);
				// for gallery image
				if(!empty($oxy_gallery[1])){
					$urls = '';
					foreach ($oxy_gallery[1] as $oxy_gallery_id) {
						$urls .= $oxy_gallery_id.',';
					}
				}
				preg_match_all('/"url"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_absolute_url);
				foreach ($oxy_absolute_url[1] as $oxy_absolute_urls) {
					$getFiles[] = $oxy_absolute_urls;
				}

				$imageID = rtrim($urls,",");
				$imageIDs = explode(",", $imageID);
				foreach ($imageIDs as $EachId) {
					$getFiles[] = getMultiSiteImageUrlbyId($EachId,$prefixValue);
				}

				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				// Merging arrays
				$OXI1mageurlsArr = array();
				if(!empty($OXImageurls) && !empty($contentimgurl[1])){
					$OXI1mageurlsArr = array_merge($OXImageurls,$contentimgurl[1]);
				}elseif(!empty($OXImageurls) && empty($contentimgurl[1])){
					$OXI1mageurlsArr = $OXImageurls;
				}elseif(empty($OXImageurls) && !empty($contentimgurl[1])){
					$OXI1mageurlsArr = $contentimgurl[1];
				}
				$OXI2mageurlsArr = array();
				if(!empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
					$OXI2mageurlsArr = array_merge($OXI1mageurlsArr,$contentimgurl1[1]);
				}elseif(!empty($OXI1mageurlsArr) && empty($contentimgurl1[1])){
					$OXI2mageurlsArr = $OXI1mageurlsArr;
				}elseif(empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
					$OXI2mageurlsArr = $contentimgurl1[1];
				}
				$OXImageurlsArr = array();
				if(!empty($OXI2mageurlsArr) && !empty($getFiles)){
					$OXImageurlsArr = array_merge($OXI2mageurlsArr,$getFiles);
				}elseif(!empty($OXI2mageurlsArr) && empty($getFiles)){
					$OXImageurlsArr = $OXI2mageurlsArr;
				}elseif(empty($OXI2mageurlsArr) && !empty($getFiles)){
					$OXImageurlsArr = $getFiles;
				}

				$OXImageurlsArrFinal = array();
				if(!empty($OXImageurlsArr) && !empty($widget_images)){
					$OXImageurlsArrFinal = array_merge($OXImageurlsArr,$widget_images);
				}elseif(!empty($OXImageurlsArr) && empty($widget_images)){
					$OXImageurlsArrFinal = $OXImageurlsArr;
				}elseif(empty($OXImageurlsArr) && !empty($widget_images)){
					$OXImageurlsArrFinal = $widget_images;
				}

				$OXimgidsArr = array();
				if(!empty($OXImageurlsArrFinal)){
					foreach ($OXImageurlsArrFinal as $OXImageurl) {
						array_push($OXimgidsArr, $OXImageurl);
					}
				}

				if(!empty($gelleryimgids[1])){
					foreach ($gelleryimgids[1] as $gelleryimgid) {
						$gelleryimgURlbyid = array(getMultiSiteImageUrlbyId($gelleryimgid,$prefixValue));
						array_push($OXimgidsArr,$gelleryimgURlbyid);
					}
				}
				$OXimgidsArrUni = array_unique($OXimgidsArr);
				if(!empty($OXimgidsArrUni)){
					$media_count = count($OXimgidsArrUni);
					$OXimagesids = '';
					$VCI = '';
					$x = 1;
					foreach ($OXimgidsArrUni as $bImgid) {						
						$case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$bImgid);
						$case1_dir_path = str_replace($multisite_url, $mainsite_url, $case1_dir_path);
						if(file_exists($case1_dir_path)){
							if($bImgid != ''){
								$OXimagesids .= $bImgid;
								$VCI .= 'Oxygen Content Media';
								if($x < $media_count){
									$OXimagesids .= ',';
									$VCI .= ',';
								}
							}
							$x++;
						}
					}
				}

				if($productgalleryImg != ''){
					$productgalleryImgArr = explode(",", $productgalleryImg);
					foreach ($productgalleryImgArr as $productgalleryImgURL) {
						if($OXimagesids != ''){
							$OXimagesids .= ',';
							$VCI .= ',';
						}
						$OXimagesids .= getMultiSiteImageUrlbyId($productgalleryImgURL,$prefixValue);
						$VCI .= 'Product Gallery Image';
					}
				}
				if($featuredImageId != ''){
					if($OXimagesids != ''){
						$OXimagesids .= ',';
						$VCI .= ',';
					}
					$OXimagesids .= $featuredImageId;
					$VCI .= 'Featured Image';
				}
				if(!empty($OXimagesids)){
					$OXimagesidSc = array($OXimagesids,$VCI,$prefixValue);
					$uniqueArr += array($PageId=>$OXimagesidSc);
				}
				$OXimagesids = null;
				$VCI = null;
			}
			if($brizy_builder){
				$PageId = $getvalues->ID;
				$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='brizy' AND post_id=$PageId" );
				$the_content = unserialize($the_content[0]->meta_value);
				$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
				$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
				preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
				preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);
				
				// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
				preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
				foreach ($case1_gethreffiles[1] as $case1_href_value){
					$case1_href_value = strtok($case1_href_value, " ");
		            $case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$case1_href_value);
		            $case1_dir_path = str_replace($multisite_url, $mainsite_url, $case1_dir_path);
					if(file_exists($case1_dir_path)){
						$hrefcase1[] = $case1_href_value;
					}
				}
				// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
				preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
				foreach ($case2_gethreffiles[1] as $case2_href_value) {
					$case2_href_value = strtok($case2_href_value, " ");
		            $case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$case2_href_value);
		            $case2_dir_path = str_replace($multisite_url, $mainsite_url, $case2_dir_path);
					if(file_exists($case2_dir_path)){
						$hrefcase2[] = $case2_href_value;
					}
				}				
				// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
				$srcfiles = array();
				preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
				foreach ($case3_gethreffiles[1] as $case3_href_value){
					$case3_href_value = strtok($case3_href_value, " ");
		            $case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$case3_href_value);
		            $case3_dir_path = str_replace($multisite_url, $mainsite_url, $case3_dir_path);
					if(file_exists($case3_dir_path)){
						$hrefcase3[] = $case3_href_value;
					}
				}				
				// CASE 4 (data-href) : Get url when pattern is <a data-href=
				preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
				foreach ($case4_gethreffiles[1] as $case4_href_value){					
					$case4_href_value = strtok($case4_href_value, " ");
					$case4_data_href = utf8_decode(urldecode($case4_href_value));
					preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
					$hrefcase4 = array();
					foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
						$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
						$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$theLinkVal);
						$case4_dir_path = str_replace($multisite_url, $mainsite_url, $case4_dir_path);
						if(file_exists($case4_dir_path)){
							$hrefcase4[] = $theLinkVal;
						}
					}					
				}				
				// $the_content = htmlspecialchars($the_content);

				// CASE 5 (video) : Get url when pattern is "video":"http://some-url.mp4"
				preg_match_all('/"video"\h*:.*?\"(.*?)\",(?![^"\n]")/', $brizy_content_str, $videoUrl);
				foreach ($videoUrl[1] as $videoUrlValue) {					
					$theVideoVal = str_replace("\",", "", $videoUrlValue);
					$theVideoVals = str_replace(get_site_url().'/',get_home_path(),$theVideoVal);
					$theVideoVals = str_replace($multisite_url, $mainsite_url, $theVideoVals);
					if(file_exists($theVideoVals)){
						$hrefcase5[] = $theVideoVal;
					}
				}				

				// CASE 6 (image) : Get url when getting images
				// Merging Arrays Start
				$imagesNeme = array();
				if(!empty($group12[1]) && !empty($group14[1])){
					$imagesNeme = array_merge($group12[1],$group14[1]);
				}elseif(!empty($group12[1]) && empty($group14[1])){
					$imagesNeme = $group12[1];
				}elseif(empty($group12[1]) && !empty($group14[1])){
					$imagesNeme = $group14[1];
				}
				// Merging Arrays Ends

				$hrefcase6 = array();
				if(!empty($imagesNeme)){
					foreach ($imagesNeme as $imageNeme) {
						// Get Media id by Name only for brizy
						$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
						if(!empty($imgIdsquery)){
							$imagesIds = getMultiSiteImageUrlbyId($imgIdsquery[0]->post_id,$prefixValue);
							$hrefcase6[] = $imagesIds;
						}
					}
				}
				
				// Mergining arrays start
				$brizyArr1 = array();
				if(!empty($hrefcase1) && !empty($hrefcase2)){
					$brizyArr1 = array_merge($hrefcase1,$hrefcase2);
				}elseif(!empty($hrefcase1) && empty($hrefcase2)){
					$brizyArr1 = $hrefcase1;
				}elseif(empty($hrefcase1) && !empty($hrefcase2)){
					$brizyArr1 = $hrefcase2;
				}

				$brizyArr2 = array();
				if(!empty($brizyArr1) && !empty($hrefcase3)){
					$brizyArr2 = array_merge($brizyArr1,$hrefcase3);
				}elseif(!empty($brizyArr1) && empty($hrefcase3)){
					$brizyArr2 = $brizyArr1;
				}elseif(empty($brizyArr1) && !empty($hrefcase3)){
					$brizyArr2 = $hrefcase3;
				}

				$brizyArr3 = array();
				if(!empty($brizyArr2) && !empty($hrefcase4)){
					$brizyArr3 = array_merge($brizyArr2,$hrefcase4);
				}elseif(!empty($brizyArr2) && empty($hrefcase4)){
					$brizyArr3 = $brizyArr2;
				}elseif(empty($brizyArr2) && !empty($hrefcase4)){
					$brizyArr3 = $hrefcase4;
				}

				$brizyArr4 = array();
				if(!empty($brizyArr3) && !empty($hrefcase5)){
					$brizyArr4 = array_merge($brizyArr3,$hrefcase5);
				}elseif(!empty($brizyArr3) && empty($hrefcase5)){
					$brizyArr4 = $brizyArr3;
				}elseif(empty($brizyArr3) && !empty($hrefcase5)){
					$brizyArr4 = $hrefcase5;
				}
				
				$href_array = array();
				if(!empty($brizyArr4) && !empty($hrefcase6)){
					$href_array = array_merge($brizyArr4,$hrefcase6);
				}elseif(!empty($brizyArr4) && empty($hrefcase6)){
					$href_array = $brizyArr4;
				}elseif(empty($brizyArr4) && !empty($hrefcase6)){
					$href_array = $hrefcase6;
				}
				// Mergining arrays ends
				
				if(!empty($href_array)){
					$media_count = count($href_array);
					$Bimagesids = '';
					$VCI = '';
					$x = 1;
					foreach ($href_array as $bImgid) {
						if($bImgid != ''){
							$Bimagesids .= $bImgid;
							$VCI .= 'Brizy Content Media';
							if($x < $media_count){
								$Bimagesids .= ',';
								$VCI .= ',';
							}
						}
						$x++;
					}
				}
				if($featuredImageId != ''){
					if($Bimagesids != ''){
						$Bimagesids .= ',';
						$VCI .= ',';
					}
					$Bimagesids .= $featuredImageId;
					$VCI .= 'Featured Image';
				}
				if(!empty($Bimagesids)){
					$BimagesidSc = array($Bimagesids,$VCI,$prefixValue);
					$uniqueArr += array($PageId=>$BimagesidSc);
				}
				$Bimagesids = null;
				$VCI = null;
			}
			if($siteorigin_ck_builder == "SiteOrigin"){
				$PageId = $getvalues->ID;
				$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id=$PageId" );
				$the_content = unserialize($the_content[0]->meta_value);
				$siteorigenArray = array();

				// Get row background images
				$media_urls = html_entity_decode($getvalues->post_content);
				preg_match_all('@"background_image_attachment":"([^"]+)"@', $media_urls, $backgroundId);
				$all_back_media_id = $backgroundId[1];
				foreach ($all_back_media_id as $back_value) {
					$background_design_image = getImageUrlbyId($back_value);
					array_push($siteorigenArray, $background_design_image);
				}

				foreach ($the_content['widgets'] as $valueid) {
					if(array_key_exists("attachment_id", $valueid)){
						$sitegetimgurl = getMultiSiteImageUrlbyId($valueid['attachment_id'],$prefixValue);
						$case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$sitegetimgurl);
						$case1_dir_path = str_replace($multisite_url, $mainsite_url, $case1_dir_path);
						if(file_exists($case1_dir_path)){
							array_push($siteorigenArray, $sitegetimgurl);
						}
					}
					if(array_key_exists("ids", $valueid)){
						foreach ($valueid['ids'] as $galleryimg) {
							$sitegetimgurl = getMultiSiteImageUrlbyId($galleryimg,$prefixValue);
							$case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$sitegetimgurl);
							$case2_dir_path = str_replace($multisite_url, $mainsite_url, $case2_dir_path);
							if(file_exists($case2_dir_path)){
								array_push($siteorigenArray, $sitegetimgurl);
							}
						}
					}
					if(array_key_exists("features", $valueid)){
						foreach ($valueid['features'] as $features_img) {
							$sitegetimgurl = getMultiSiteImageUrlbyId($features_img['icon_image'],$prefixValue);
							$case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$sitegetimgurl);
							$case3_dir_path = str_replace($multisite_url, $mainsite_url, $case3_dir_path);
							if(file_exists($case3_dir_path)){
								array_push($siteorigenArray, $sitegetimgurl);
							}
						}
					}					
					if(array_key_exists("image", $valueid)){
						$sitegetimgurl = getMultiSiteImageUrlbyId($valueid['image'],$prefixValue);						
						$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$sitegetimgurl);
						$case4_dir_path = str_replace($multisite_url, $mainsite_url, $case4_dir_path);
						if(file_exists($case4_dir_path)){
							array_push($siteorigenArray, $sitegetimgurl);
						}
					}
					if(array_key_exists("frames", $valueid)){
						foreach ($valueid['frames'] as $frames_img) {
							$sitegetimgurl = getMultiSiteImageUrlbyId($frames_img['background_image'],$prefixValue);
							$case5_dir_path = str_replace(get_site_url().'/',get_home_path(),$sitegetimgurl);
							$case5_dir_path = str_replace($multisite_url, $mainsite_url, $case5_dir_path);
							if(file_exists($case5_dir_path)){
								array_push($siteorigenArray, $sitegetimgurl);
							}
						}
					}
					if(array_key_exists("content", $valueid)){
						$custom_html = $valueid['content'];
						preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
						foreach ($contentimg[1] as $imgurl){
							$case6_dir_path = str_replace(get_site_url().'/',get_home_path(),$imgurl);
							$case6_dir_path = str_replace($multisite_url, $mainsite_url, $case6_dir_path);
							if(file_exists($case6_dir_path)){
								array_push($siteorigenArray, $imgurl);
							}
						}
					}
					if(array_key_exists("content", $valueid)){
						preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
						foreach ($contentimgs[1] as $imgurls){
							$case7_dir_path = str_replace(get_site_url().'/',get_home_path(),$imgurls);
							$case7_dir_path = str_replace($multisite_url, $mainsite_url, $case7_dir_path);
							if(file_exists($case7_dir_path)){
								array_push($siteorigenArray, $imgurls);
							}
						}
					}
					if(array_key_exists("text", $valueid)){
						$siteEditor = $valueid['text'];
						preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
						$groupsfile = $groupsfile[1];
						if(!empty($groupsfile)){
							foreach ($groupsfile as $filesvalue) {
								$case8_dir_path = str_replace(get_site_url().'/',get_home_path(),$filesvalue);
								$case8_dir_path = str_replace($multisite_url, $mainsite_url, $case8_dir_path);
								if(file_exists($case8_dir_path)){
									$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
									if(in_array($fileExtension, $totalExtensionsAvailable)){
								        $getFiles[] = $filesvalue;
								    }
								}
							}
						}
						preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
						preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
						preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
						preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
						preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
						preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
						preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
						preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
						preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
						preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
						preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
						preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
						preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
						preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
						preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
						preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
						preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
						preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
						preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);

						// for vc video
						if(!empty($vc_video_url[1])){
							foreach ($vc_video_url[1] as $vc_video) {
								$getFiles[] = $vc_video;
							}
						}
						// for src
						if(!empty($group9[1])){
							foreach ($group9[1] as $group9s) {
								$getFiles[] = $group9s;
							}
						}
						// for mp3
						if(!empty($group11[1])){
							foreach ($group11[1] as $group11s) {
								$getFiles[] = $group11s;
							}
						}
						// for mp4
						if(!empty($group12[1])){
							foreach ($group12[1] as $group12s) {
								$getFiles[] = $group12s;
							}
						}
						// for pdf
						if(!empty($group13[1])){
							foreach ($group13[1] as $group13s) {
								$getFiles[] = $group13s;
							}
						}
						// for docx
						if(!empty($group14[1])){
							foreach ($group14[1] as $group14s) {
								$getFiles[] = $group14s;
							}
						}
						// for doc
						if(!empty($group15[1])){
							foreach ($group15[1] as $group15s) {
								$getFiles[] = $group15s;
							}
						}
						// for ppt
						if(!empty($group16[1])){
							foreach ($group16[1] as $group16s) {
								$getFiles[] = $group16s;
							}
						}
						// for xls
						if(!empty($group17[1])){
							foreach ($group17[1] as $group17s) {
								$getFiles[] = $group17s;
							}
						}
						// for pps
						if(!empty($group18[1])){
							foreach ($group18[1] as $group18s) {
								$getFiles[] = $group18s;
							}
						}
						// for ppsx
						if(!empty($group19[1])){
							foreach ($group19[1] as $group19s) {
								$getFiles[] = $group19s;
							}
						}
						// for xlsx
						if(!empty($group20[1])){
							foreach ($group20[1] as $group20s) {
								$getFiles[] = $group20s;
							}
						}
						// for odt
						if(!empty($group21[1])){
							foreach ($group21[1] as $group21s) {
								$getFiles[] = $group21s;
							}
						}
						// for ogg
						if(!empty($group22[1])){
							foreach ($group22[1] as $group22s) {
								$getFiles[] = $group22s;
							}
						}
						// for m4a
						if(!empty($group23[1])){
							foreach ($group23[1] as $group23s) {
								$getFiles[] = $group23s;
							}
						}
						// for wav
						if(!empty($group24[1])){
							foreach ($group24[1] as $group24s) {
								$getFiles[] = $group24s;
							}
						}
						// for mp4
						if(!empty($group25[1])){
							foreach ($group25[1] as $group25s) {
								$getFiles[] = $group25s;
							}
						}
						// for mov
						if(!empty($group26[1])){
							foreach ($group26[1] as $group26s) {
								$getFiles[] = $group26s;
							}
						}
						// for avi
						if(!empty($group27[1])){
							foreach ($group27[1] as $group27s) {
								$getFiles[] = $group27s;
							}
						}
						// for 3gp
						if(!empty($group28[1])){
							foreach ($group28[1] as $group28s) {
								$getFiles[] = $group28s;
							}
						}
						// for pptx
						if(!empty($group29[1])){
							foreach ($group29[1] as $group29s) {
								$getFiles[] = $group29s;
							}
						}
						preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
						$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
						
						if(!empty($group9[1]) && !empty($bgimgUrl1)){
							$VCmediaContentPre = array_merge($group9[1],$bgimgUrl1);
						}elseif(!empty($group9[1]) && empty($bgimgUrl1)){
							$VCmediaContentPre = $group9[1];
						}elseif(empty($group9[1]) && !empty($bgimgUrl1)){
							$VCmediaContentPre = $bgimgUrl1;
						}

						if(!empty($VCmediaContentPre) && !empty($getFiles)){
							$VCmediaContent = array_merge($VCmediaContentPre,$getFiles);
						}elseif(!empty($VCmediaContentPre) && empty($getFiles)){
							$VCmediaContent = $VCmediaContentPre;
						}elseif(empty($VCmediaContentPre) && !empty($getFiles)){
							$VCmediaContent = $getFiles;
						}

						$VCmediaContent = array_unique($VCmediaContent, SORT_REGULAR);

						foreach ($VCmediaContent as $imgurl){
							array_push($siteorigenArray, $imgurl);
						}
					}
				}
				$SOidsarray = array_unique($siteorigenArray);
				$SOimagesids = implode(',', $SOidsarray);
				if(!empty($SOidsarray)){
					$media_count = count($SOidsarray);
					$VCI = '';
					for ($x = 1; $x <= $media_count; $x++) {
						$VCI .= 'SiteOrigin Content Media';
						if($x < $media_count){
							$VCI .= ',';
						}
					}
				}
				if($featuredImageId != ''){
					$SOimagesids .= ','.$featuredImageId;
					$VCI .= ',Featured Image';
				}
				if(!empty($SOimagesids)){
					$SOimagesidSc = array($SOimagesids,$VCI,$prefixValue);			
					$uniqueArr += array($PageId=>$SOimagesidSc);
				}
				$SOimagesids = null;
				$VCI = null;
				$getFiles = null;
			}
			if($beaver_builder == 1){
				$getFiles = array();
				$BBmergeArray = array();
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				$PageId = $getvalues->ID;
				preg_match_all('@src="([^"]+)"@', $the_content, $group6);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group7);

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
				
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				$beaver_src_array = $group6[1];
				$beaver_mp4_array = $group7[1];
				// Merging arrays
				$BBmergePreArray = array();
				if(!empty($getFiles) && !empty($beaver_src_array)){
					$BBmergePreArray = array_merge(array_unique($beaver_src_array),array_unique($getFiles));
				}elseif(!empty($getFiles) && empty($beaver_src_array)){
					$BBmergePreArray = array_unique($getFiles);
				}elseif(empty($getFiles) && !empty($beaver_src_array)){
					$BBmergePreArray = array_unique($beaver_src_array);
				}

				$BBmergeArray = array();
				if(!empty($BBmergePreArray) && !empty($beaver_mp4_array)){
					$BBmergeArray = array_merge(array_unique($beaver_mp4_array),array_unique($BBmergePreArray));
				}elseif(!empty($BBmergePreArray) && empty($beaver_mp4_array)){
					$BBmergeArray = array_unique($BBmergePreArray);
				}elseif(empty($BBmergePreArray) && !empty($beaver_mp4_array)){
					$BBmergeArray = array_unique($beaver_mp4_array);
				}

				$BBidsarray = array();
				$BBmergeArray = array_unique($BBmergeArray);
				foreach ($BBmergeArray as $bbimgurl) {
					array_push($BBidsarray, $bbimgurl);
				}
				$BBimagesids = array();
				$BBimagesids = implode(',', $BBidsarray);
				if(!empty($BBidsarray)){
					$media_count = count($BBidsarray);
					$VCI = '';
					for ($x = 1; $x <= $media_count; $x++) {
						$VCI .= 'Beaver Builder Content Media';
						if($x < $media_count){
							$VCI .= ',';
						}
					}
				}
				if($featuredImageId != ''){
					$BBimagesids .= ','.$featuredImageId;
					$VCI .= ',Featured Image';
				}
				if(!empty($BBimagesids)){
					$BBimagesidSc = array($BBimagesids,$VCI,$prefixValue);			
					$uniqueArr += array($PageId=>$BBimagesidSc);
				}
				$BBimagesids = null;
				$VCI = null;
			}
			if($vc_builder == 'true'){
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
				preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
				preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
				preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
				/* Raw HTML */
				preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
				$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
				$RawimgUrl = array();
				if(!empty($RowHtmls)){
					foreach ($RowHtmls as $RowHtml) {
						$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
						$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
						
						preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
						$RawimgUrl[] = str_replace(['?_=1'], [''], $group16[1]);
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				/* Raw HTML */
				$PageId = $getvalues->ID;
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				// preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				// preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				// $totalExt = totalAvailableExtension($the_content);

				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$bgimgUrl = str_replace(['(',')'], ['',''], $group10[1]);

				$RawimgUrlArrys = array();
				if(!empty($group9[1]) && !empty($bgimgUrl)){
					$RawimgUrlArrys = array_merge($group9[1],$bgimgUrl);
				}elseif(!empty($group9[1]) && empty($bgimgUrl)){
					$RawimgUrlArrys = $group9[1];
				}elseif(empty($group9[1]) && !empty($bgimgUrl)){
					$RawimgUrlArrys = $bgimgUrl;
				}

				$VCmediaContents = array();
				if(!empty($RawimgUrlArrys) && !empty($getFiles)){
					$VCmediaContents = array_merge($RawimgUrlArrys,$getFiles);
				}elseif(!empty($RawimgUrlArrys) && empty($getFiles)){
					$VCmediaContents = $RawimgUrlArrys;
				}elseif(empty($RawimgUrlArrys) && !empty($getFiles)){
					$VCmediaContents = $getFiles;
				}

				$VCmediaContent = array();
				if(!empty($VCmediaContents) && !empty($RawimgUrl)){
					$VCmediaContent = array_merge($VCmediaContents,$RawimgUrl);
				}elseif(!empty($VCmediaContents) && empty($RawimgUrl)){
					$VCmediaContent = $VCmediaContents;
				}elseif(empty($VCmediaContents) && !empty($RawimgUrl)){
					$VCmediaContent = $RawimgUrl;
				}
				
				$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
				$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
				$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
				$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

				if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
					$VCimageids = '';
					$comma = '';
					if($VCsingleimageids != ""){
						$VCimageids .= getMultiSiteImageUrlbyId($VCsingleimageids,$prefixValue);
						$media_count = count(explode(",", $VCsingleimageids));
						$VCI = '';
						for ($x = 1; $x <= $media_count; $x++) {
							$VCI .= 'Visual Composer Image';
							if($x < $media_count){
								$VCI .= ',';
							}
						}
					}
					if(!empty($VCmediaContent)){
						foreach ($VCmediaContent as $VCmediaContentUrl) {
							if($VCimageids != ''){ $comma = ','; }
							$VCimageids .= $comma.$VCmediaContentUrl;
							$VCI .= $comma.'Visual Composer Media';
						}
					}

					if($VCgalleryimageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$theGalIds = explode(",", $VCgalleryimageids);
						foreach ($theGalIds as $idvalue) {
							$VCimageids .= $comma.getMultiSiteImageUrlbyId($idvalue,$prefixValue);
						}
						$gallerymedia_count = count(explode(",", $VCgalleryimageids));
						for ($y = 1; $y <= $gallerymedia_count; $y++) {
							if($VCI != ''){ $comma1 = ','; }
							$VCI .= $comma1.'Visual Composer Gallery Image';
						}
					}
					
					if($VCcarouselimageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$theGalIds2 = explode(",", $VCcarouselimageids);
						foreach ($theGalIds2 as $idvalue2) {
							$VCimageids .= $comma.getMultiSiteImageUrlbyId($idvalue2,$prefixValue);
						}
						// $VCimageids .= $comma.getMultiSiteImageUrlbyId($VCcarouselimageids,$prefixValue);
						$carouselmedia_count = count(explode(",", $VCcarouselimageids));
						for ($z = 1; $z <= $carouselmedia_count; $z++) {
							if($VCI != ''){ $comma1 = ','; }
							$VCI .= $comma1.'Visual Composer Carousel Image';
						}
					}
					
					if($VChoverboximageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$VCimageids .= $comma.getMultiSiteImageUrlbyId($VChoverboximageids,$prefixValue);
						$hoverboxmedia_count = count(explode(",", $VChoverboximageids));
						for ($z = 1; $z <= $hoverboxmedia_count; $z++) {
							if($VCI != ''){ $comma1 = ','; }
							$VCI .= $comma1.'Visual Composer Hover Box Image';
						}
					}
				}
				if($featuredImageId != ''){
					if($VCimageids != ''){ $comma = ','; }
					$VCimageids .= $comma.$featuredImageId;
					$VCI .= $comma.'Featured Image';
				}
				if(!empty($VCimageids)){
					$VCimagesidSc = array($VCimageids,$VCI,$prefixValue);
					$uniqueArr += array($PageId=>$VCimagesidSc);
				}
				$VCimageids = null;

			}
			if($getvalues->post_type == 'product' || $getvalues->post_type == 'product_variation'){
				$the_content = $getvalues->post_content;
				$PageId = $getvalues->ID;
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}
				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}
				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}
				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}
				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}
				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}
				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}
				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}
				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}
				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}
				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}
				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}
				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}
				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}
				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}
				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}
				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}
				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}
				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}
				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$bgimgUrl = str_replace(['(',')'], ['',''], $group10[1]);

				if(!empty($group9[1]) || !empty($bgimgUrl) || !empty($totalExt)){
					$gutenbergImgUrls = array_unique(array_merge($group9[1],$bgimgUrl));
				}else{
					$gutenbergImgUrls = array();
				}


				if(!empty($gutenbergImgUrls) && !empty($getFiles)){
					$hrefMedias = array_unique(array_merge($getFiles,$gutenbergImgUrls));
				}else{
					if(!empty($getFiles)){
						$hrefMedias = array_unique($getFiles);
					}elseif(!empty($gutenbergImgUrls)){
						$hrefMedias = array_unique($gutenbergImgUrls);
					}else{
						$hrefMedias = array();
					}
				}
				$totalContentMedia = array_unique(array_merge($gutenbergImgUrls,$hrefMedias));
				$ProImgids = array();
				foreach ($totalContentMedia as $gutenbergImgUrl) {
					array_push($ProImgids, $gutenbergImgUrl);
				}
				if(!empty($ProImgids)){
					$media_count = count($ProImgids);
					$Proimagesids = '';
					$PCI = '';
					$x = 1;
					foreach ($ProImgids as $gutenbergImgid) {
						if($gutenbergImgid != ''){
							$Proimagesids .= $gutenbergImgid;
							$PCI .= 'Simple/Gutenberg Content Media';
							if($x < $media_count){
								$Proimagesids .= ',';
								$PCI .= ',';
							}
						}
						$x++;
					}
				}
				if($productgalleryImg != ''){
					$productgalleryImgArr = explode(",", $productgalleryImg);
					foreach ($productgalleryImgArr as $productgalleryImgURL) {
						if($Proimagesids != ''){
							$Proimagesids .= ',';
							$PCI .= ',';
						}
						$Proimagesids .= getMultiSiteImageUrlbyId($productgalleryImgURL,$prefixValue);
						$PCI .= 'Product Gallery Image';
					}
				}
				if($featuredImageId != ''){
					if($Proimagesids != ''){
						$Proimagesids .= ',';
						$PCI .= ',';
					}
					if($getvalues->post_type == 'product_variation'){
						$Proimagesids .= $featuredImageId;
						$PCI .= 'Product Variant Image';
					}else{
						$Proimagesids .= $featuredImageId;
						$PCI .= 'Featured Image';
					}
				}

				$ProimagesidSc = array($Proimagesids,$PCI,$prefixValue);
				$uniqueArr += array($PageId=>$ProimagesidSc);
				$Proimagesids = null;
				$the_content = null;
				$PageId = null;
				$PCI = null;
				$totalContentMedia = null;
			}
			if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder){
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;			
				$PageId = $getvalues->ID;

				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for wmv
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

				if(!empty($gutenbergImgUrls) && !empty($getFiles)){
					$hrefMedias = array_unique(array_merge($getFiles,$gutenbergImgUrls));
				}else{
					if(!empty($getFiles)){
						$hrefMedias = array_unique($getFiles);
					}elseif(!empty($gutenbergImgUrls)){
						$hrefMedias = array_unique($gutenbergImgUrls);
					}else{
						$hrefMedias = array();
					}
				}
				$totalContentMedia = array_unique(array_merge($gutenbergImgUrls,$hrefMedias));
				
				$gutenbergImgids = array();
				foreach ($totalContentMedia as $gutenbergImgUrl) {
					array_push($gutenbergImgids, $gutenbergImgUrl);
				}
				if(!empty($gutenbergImgids)){
					$media_count = count($gutenbergImgids);
					$imagesids = '';
					$VCI = '';
					$x = 1;
					foreach ($gutenbergImgids as $gutenbergImgid) {
						if($gutenbergImgid != ''){
							$imagesids .= $gutenbergImgid;
							$VCI .= 'Simple/Gutenberg Content Media';
							if($x < $media_count){
								$imagesids .= ',';
								$VCI .= ',';
							}
						}
						$x++;
					}
				}
				if($featuredImageId != ''){
					if($imagesids != ''){
						$imagesids .= ',';
						$VCI .= ',';
					}
					$imagesids .= $featuredImageId;
					$VCI .= 'Featured Image';
				}
				if(!empty($imagesids)){
					$SimagesidSc = array($imagesids,$VCI,$prefixValue);
					$uniqueArr += array($PageId=>$SimagesidSc);
				}
				$imagesids = null;
				$the_content = null;
				$PageId = null;
				$gutenbergImgids = null;
				$getFiles = null;
				$VCI = null;
			}
		}		
	}
	return $uniqueArr;
}
function getcontent(){
	global $wpdb;
	$contentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts WHERE post_status != 'trash' && post_type NOT IN('revision','scheduled-action','attachment')");
	$getImgs = getPageBuilderContentMedia();
	if(!empty($contentdata)){
		$contentarray = array();
		foreach ($contentdata as $value) {
			$post_id = $value->ID;
			$post_url = $value->guid;
			$post_date = $value->post_date;
			$post_title = $value->post_title;
			$post_status = $value->post_status;
			$post_type = $value->post_type;
			$media_found = $getImgs[$post_id][0];
			$media_source = $getImgs[$post_id][1];
			$content_site = $wpdb->prefix;
			$multisite_name = '';
			
			$contentarray[] = array('post_id'=>$post_id, 'post_url' => $post_url, 'post_date' =>$post_date,'post_title'=>$post_title,'post_status'=>$post_status,'post_type'=>$post_type,'media_found'=>$media_found,'media_source'=>$media_source,'content_site'=>$content_site,'multisite_name'=>$multisite_name);
		}
	}
	return $contentarray;
}
$contentarrays = getcontent();

function getMultiSiteContent(){
	global $wpdb;
	$prefixes = getAllSitePrefix();
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;

		$contentdata = $wpdb->get_results("SELECT * FROM ".$prefixValue."posts WHERE post_status != 'trash' && post_type NOT IN('revision','scheduled-action','attachment')");
		$getImg = getMultiSitePageBuilderContentMedia();
		
		if(!empty($contentdata)){
			$contentarray = array();
			foreach ($contentdata as $value) {
				$post_id = $value->ID;
				$post_url = $value->guid;
				$post_date = $value->post_date;
				$post_title = $value->post_title;
				$post_status = $value->post_status;
				$post_type = $value->post_type;
				$media_found = $getImg[$post_id][0];
				$media_source = $getImg[$post_id][1];
				$content_site = $prefixValue;
				$multisite_name = $site_name;
				$contentarray[] = array('post_id'=>$post_id, 'post_url' => $post_url, 'post_date' =>$post_date,'post_title'=>$post_title,'post_status'=>$post_status,'post_type'=>$post_type,'media_found'=>$media_found,'media_source'=>$media_source,'content_site'=>$content_site,'multisite_name'=>$multisite_name);
			}
		}
	}
	return $contentarray;
}
if(is_multisite()){
	$multicontentarray = getMultiSiteContent();
}else{
	$multicontentarray = array();
}

$contentarray = array();
if(!empty($contentarrays) && !empty($multicontentarray)){
	$contentarray = array_merge($contentarrays,$multicontentarray);
}elseif(!empty($contentarrays) && empty($multicontentarray)){
	$contentarray = $contentarrays;
}elseif(empty($contentarrays) && !empty($multicontentarray)){
	$contentarray = $multicontentarray;
}

$table_name = $wpdb->prefix."contentscan_data";
$wpdb->query("TRUNCATE TABLE ".$table_name);
$successdata = array();
foreach ($contentarray as $finalResponse) {
	$post_id = $finalResponse['post_id'];
    $post_url = $finalResponse['post_url'];
    $post_date = $finalResponse['post_date'];
    $post_title = $finalResponse['post_title'];
    $post_status = $finalResponse['post_status'];
    $post_type = $finalResponse['post_type'];
    $media_found = $finalResponse['media_found'];
    $media_source = $finalResponse['media_source'];
    $content_site = $finalResponse['content_site'];
    $multisite_name = $finalResponse['multisite_name'];

    $successdata[] = $wpdb->insert( $table_name, array("post_id" => $post_id,  "post_url" => $post_url, "post_date" => $post_date, "post_title" => $post_title,  "post_status"=> $post_status, "post_type"=> $post_type, 'media_found'=>$media_found,'media_source'=>$media_source,'content_site'=>$content_site,'multisite_name'=>$multisite_name));
}

if(in_array(0, $successdata)){
    echo "Some files not scanned due to server issue. Please Scan again !";
}else{
	$querystr = "SELECT * FROM $table_name ORDER BY post_date DESC";
	$pageposts = $wpdb->get_results($querystr, ARRAY_A);
	$count = 0;
	$bytes = '';
	$seprator = "";
	foreach ($pageposts as $finalResult) {
		$post_id = $finalResult['post_id'];
		$post_type = $finalResult['post_type'];
		if(empty($post_type)){
			$post_type = __("No content found","wp_media_cleaner");
		}

		// Get all categories of a post
		$post_categories = wp_get_post_terms($post_id,'category');
		if(!empty($post_categories)){
			foreach ($post_categories as $cat_value) {
				$all_cat[] = $cat_value->name;
			}
			$all_categories = implode(",",$all_cat).",";
		}else{
			$all_categories = "";
		}

		// Get all product categories of a post
		$product_categories = wp_get_post_terms($post_id,'product_cat');
		if(!empty($product_categories)){
			foreach ($product_categories as $prod_cat_value) {
				$all_prod_cat[] = $prod_cat_value->name;
			}
			$all_prod_categories = implode(",",$all_prod_cat).",";
		}else{
			$all_prod_categories = "";
		}
		
		// Check if media file exist on site server start
		$wp_root_path = get_home_path();
        $wp_site_url = get_site_url();
        $wp_site_url2 = get_site_url(2);
        $wp_site_url3 = get_site_url(3);

        $got_media = array();
        if(strpos($finalResult['media_found'],",") !== false){
        	$got_media = explode(",", $finalResult['media_found']);
        }else{
        	$got_media[] = $finalResult['media_found'];
        }
        $totalMedia = array();
        if(!empty($got_media)){
        	foreach ($got_media as $Mediavalue) {
        		if(strpos($Mediavalue,$wp_site_url2) !== false){
        			$wp_site_url = $wp_site_url2;
        		}elseif(strpos($Mediavalue,$wp_site_url3) !== false){
        			$wp_site_url = $wp_site_url3;
        		}
        		$got_media_path =  str_replace($wp_site_url.'/',$wp_root_path,$Mediavalue);
        		if(file_exists($got_media_path)){
        			$totalMedia[] = $Mediavalue;
        		}
        	}
        }
        if(strpos($finalResult['media_found'],",") !== false){
        	$finalResult['media_found'] = implode(",", $totalMedia);
        }      
        $prefixxes = $finalResult['content_site'];
        // get main site prefix
		$main_prefix = $wpdb->get_blog_prefix(1);
		$csite = substr($prefixxes, -2);
		$csite = rtrim($csite,"_");
        // Check if media file exist on site server end        
		if($post_type == 'product_variation'){
			$get_parent_post = get_post( $post_id );
			$the_parent_post = $get_parent_post->post_parent;
			if($prefixxes == $main_prefix){
				$edit_link = site_url()."/wp-admin/post.php?post=".$the_parent_post."&action=edit";
			}else{
				$theUrl = get_site_url($csite);
				$edit_link = $theUrl."/wp-admin/post.php?post=".$the_parent_post."&action=edit";
			}
		}else{
			if($prefixxes == $main_prefix){
				$edit_link = site_url()."/wp-admin/post.php?post=".$post_id."&action=edit";
			}else{
				$theUrl = get_site_url($csite);
				$edit_link = $theUrl."/wp-admin/post.php?post=".$post_id."&action=edit";
			}
		}
		if($finalResult['media_found'] != null){
			if(!empty($totalMedia)){
				$media_count = '<span>'.count(explode(',', $finalResult['media_found'])).' Media(s) files found</span>';
				$media_ids = $finalResult['media_found'];
				$media_event = 'Edit Media';
			}else{
				$media_count = '<span>No Media Files Found</span>';
				$media_ids = 0;
				$media_event = 'Add Media';
			}
		}else{
			$media_count = '<span>No Media Files Found</span>';
			$media_ids = 0;
			$media_event = 'Add Media';
		}
		$multisite_name = $finalResult['multisite_name'];
		if($multisite_name){
			$multisite_name = "Multisite: ".$multisite_name;
		}
		$checkbox_content = $finalResult['post_id'].','.$post_type.','.$finalResult['content_site'];
		$select_row = '<input type="checkbox" name="check_list" value="'.$checkbox_content.'" class="chkbox" id="id_chk'.$count.'">';

		if(!empty($finalResult['post_title'])){ $seprator = " | "; }

		if($post_type == "product_variation"){
			$vdetail = substr($finalResult['post_title'], strrpos($finalResult['post_title'], '-') + 1);
			$fposttitle = $finalResult['post_title'];
			$posttypename = "Variation Product";
		}else{
			$posttypename = ucwords($post_type);
			$fposttitle = $finalResult['post_title'];
		}
		// If no title found
		if(!$fposttitle){
			$fposttitle = __("No title found","wp_media_cleaner");
		}
		$PostId = $finalResult['post_id'];
		$prefixx = $finalResult['content_site'];
		if($finalResult['post_status'] == 'future'){
			global $wpdb;
			$futureCreateDate = $wpdb->get_results("SELECT post_modified FROM ".$prefixx."posts WHERE ID='$PostId'");
			$postdatetime = $futureCreateDate[0]->post_modified;
			$futureDate = '</br>'.str_replace(" "," | ",$finalResult['post_date']);
		}else{
			$postdatetime = $finalResult['post_date'];
			$futureDate = '';
		}
		// Details for tooltip
		$allpostdetails = $wpdb->get_results("SELECT * FROM ".$prefixx."posts WHERE ID='$PostId'");
		$thepostid = $allpostdetails[0]->ID;
		$tooltipPostTitle = $allpostdetails[0]->post_title;
		$tooltipPostContent = $allpostdetails[0]->post_content;
		if(empty($tooltipPostContent)){
			$oxygen_builders = $wpdb->get_results("SELECT meta_value from ".$prefixx."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id=$thepostid" );
			$tooltipPostContent = $oxygen_builders[0]->meta_value;
		}
		if(empty($tooltipPostContent)){
			$tooltipPostContent = "No Description Available";
		}
		$tooltipPostContent = htmlspecialchars($tooltipPostContent, ENT_QUOTES);
		$tooltipPostGuid = $allpostdetails[0]->guid;
		$tooltipPostDate = $allpostdetails[0]->post_date;
		$tooltipPostDate = strtotime($tooltipPostDate);
		$tooltipPostDate = date('Y-m-d',$tooltipPostDate);
		$tooltip_icon = site_url() . '/wp-content/plugins/wp_media_cleaner/assets/images/tooltip.png';
		$tooltipContent = '
		<div class=mytooltip><img class="hover_me" width="25" src="'.$tooltip_icon.'">
		  <div class="tooltipstext" tabindex="-1">
			<div class="tooltip-main">
			  <div class="close"><img src="'.site_url().'/wp-content/plugins/wp_media_cleaner/assets/images/wp_media_close.png"></div>
			  <ul class="media_list">';
			  	if(!empty($totalMedia)){
					foreach ($totalMedia as $AllImageUrl) {
						$get_main_site_prefix = $wpdb->get_blog_prefix(1);
						preg_match('!\d+!', $prefixx, $matches);
						$get_site_id = $matches[0];

						if(is_multisite()){
							// get main site path
							$current_blog_details = get_blog_details( array( 'blog_id' => 1 ) );
							$mainsite_url = $current_blog_details->path;

							// get multi site path
							$current_blog_details2 = get_blog_details( array( 'blog_id' => $get_site_id ) );
							$multisite_url = $current_blog_details2->path;
							if($get_site_id){
								$img_home_path_pre = str_replace(get_site_url().'/',get_home_path(),$AllImageUrl);
								$img_home_path = str_replace($multisite_url, $mainsite_url, $img_home_path_pre);
							}else{
								$img_home_path = str_replace(get_site_url().'/',get_home_path(),$AllImageUrl);
							}
						}else{
							$img_home_path = str_replace(get_site_url().'/',get_home_path(),$AllImageUrl);
						}
						if(file_exists($img_home_path)){
							$tooltipContent .= '
							<li>
								<div class="left_part">';
									$imgUrlquery = $wpdb->get_results("SELECT * FROM ".$prefixx."posts WHERE guid='$AllImageUrl'");		  	
								  	if($imgUrlquery){
										$tooltip_post_id = $imgUrlquery[0]->ID;
										$tooltip_post_image = $imgUrlquery[0]->guid;
										$tooltip_post_title = $imgUrlquery[0]->post_title;
										$tooltip_post_caption = $imgUrlquery[0]->post_excerpt;
										$tooltip_post_description = $imgUrlquery[0]->post_content;

										$alternative_text = $wpdb->get_results("SELECT meta_value from ".$prefixx."postmeta WHERE meta_key='_wp_attachment_image_alt' AND post_id=$tooltip_post_id" );
										if($alternative_text){
						    				$alternative_text = $alternative_text[0]->meta_value;
						    			}else{
						    				$alternative_text = '';
						    			}

								  		// Get media type start
									    $media_type = get_headers($tooltip_post_image);							    
									    foreach ($media_type as $valued) {
									      if (strpos($valued, 'Content-Type') !== false) {
									        $m_type = $valued;
									      }
									    }
									    $variable = substr($m_type, 0, strpos($m_type, "/"));
									    $mediatype = substr(strstr($variable, 'Content-Type: '), strlen('Content-Type: '));
									    if($mediatype == 'image'){
								          $tooltipContent .='
								          <div class="media_show">
								          	<img src="'.$AllImageUrl.'" alt="">
								          </div>';
								        }elseif($mediatype == 'video'){
									    	$tooltipContent .= '
									    	<div class="media_show">							    	
												<video style="width: 32%;" controls>
									              <source src="'.$tooltip_post_image.'" type="video/mp4">
									              Sorry, your browser doesnt support the video element.
									            </video>
								            </div>';
									    }elseif($mediatype == "application"){
								          $ext = pathinfo($tooltip_post_image, PATHINFO_EXTENSION);
								          if($ext == 'pdf'){
								            $tooltipContent .= '
								            <div class="media_show">
								            	<embed src="'.$tooltip_post_image.'" type="application/pdf">
								            </div>';

								          }elseif($ext == 'docx' || $ext == 'doc'){
								            $tooltipContent .= '
								            <div class="media_show" style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$tooltip_post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
								          }else{
								            $tooltipContent .= '<div class="media_show" style="text-align:center"><p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$tooltip_post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a></div>';
								          }
								        }elseif($mediatype == "audio"){
								          $tooltipContent .='
								          <div class="media_show">
									          <audio controls="">
									            <source src="'.$tooltip_post_image.'" type="audio/ogg">
									            <source src="'.$tooltip_post_image.'" type="audio/mpeg">
									            Your browser does not support the audio element.
									          </audio>
								          </div>';
								        }else{
								          $tooltipContent .= '
								          <div class="media_show" style="text-align:center">
								          	<p style="font-size: 15px;font-weight: bold;color: #4db9ab;">No preview available for this format</p><br><a href="'.$tooltip_post_image.'" download><button class="btn btn-blue" type="button">Download fie</button></a>
								          </div>';
								        }
								        $tooltipContent .= '
											<div class="files_details">
											  <p><strong>Filename:</strong> '.($tooltip_post_title ? $tooltip_post_title : 'No title').'</p>
											  <p><strong>Alt text:</strong> '.($alternative_text ? $alternative_text : 'No Alt text').'</p>
											  <p><strong>Caption:</strong> '.($tooltip_post_caption ? $tooltip_post_caption : 'No Caption').'</p>
											  <p><strong>Image Type:</strong> '.($posttypename ? $posttypename : 'No Type').'</p>
											</div>
										</div>
										<div class="description_part">
											<p><strong>Description:</strong></p>
											<br>
											<p>'.($tooltip_post_description ? $tooltip_post_description : 'No Type').'</p>
										</div>';
									}else{
										if(file_exists($img_home_path)){
										}else{
											$AllImageUrl = "No image found";
										}
										$tooltipContent .= '
										<div class="media_show">
											<img src="'.$AllImageUrl.'" alt="">
										</div>
										<div class="files_details">
											<p><strong class="red">No information found for Resized images.</strong></p>
										</div>
									</div>
									<div class="description_part">
									<p>&nbsp;</p>
									</div>';
									}
									$tooltipContent .= '
							</li>';
						}else{
							// $tooltipContent .= '
							// <li>
							// 	<div class="no_media_div">
							// 		<strong class="red">This media is not found on server</strong>
							// 	</div>
							// </li>';
						}
					}
				}else{
					$tooltipContent .= '
					<li>
						<div class="no_media_div">
							<strong class="red">No Media Found</strong>
						</div>
					</li>';
				}
				$tooltipContent .= '
			  </ul>
			  <div class="tooltip_inline">
				<div class="tooltip_inline_child">
				  <p class="tooltip_guid">
					<button class="btn btn-blue tooltip_button" type="button">
					<a target="_blank" href="'.$tooltipPostGuid.'">Visit post</a>
					</button>
				  </p>
				</div>
				<div class="tooltip_inline_child">
				  <p class="tooltip_date">Post Date:<span>'.$tooltipPostDate.'</span></p>
				</div>
			  </div>
			</div>
		  </div>
		</div>';

		$media_source_data = $finalResult['media_source'];
		$edit_url = '../wp-content/plugins/wp_media_cleaner/lib/content-scan/edit-media-result.php';
		if($finalResult['media_found'] != null){
			$linked_filename = $media_count.'<br/><a href=\'javascript:openPopupPage("'.$edit_url.'","'.$media_source_data.'","'.$media_ids.'","'.$fposttitle.'","'.$posttypename.'","'.$prefixx.'")\'>'.$media_event.'</a> | <span class="p_detail">'.$multisite_name.'</span>';
		}else{
			$linked_filename = $media_count.'<br/><a href="'.$edit_link.'" target="_blank">'.$media_event.'</a> | <span class="p_detail">'.$multisite_name.'</span>';
		}
		$relatedpageinfo = $fposttitle."<span class='post_type' data-posttype='".$post_type."'></span>";
		$type_content = "<span class='post_type'>".$posttypename."</span>";
		$postDate = explode(' ', $postdatetime);

		$select_row .= "<input type='hidden' id='att-filter' value='$posttypename,$postDate[0],$all_categories$all_prod_categories".$finalResult['post_status']."' />";
		$getFinalResults[$count][] = $select_row;
		$getFinalResults[$count][] = $relatedpageinfo;
		$getFinalResults[$count][] = $type_content;
		$getFinalResults[$count][] = $linked_filename;
		$getFinalResults[$count][] = "<span class='date_picker' data-datepicker='".$postdatetime."'>".str_replace(" "," | ",$postdatetime)."</span>";
		$getFinalResults[$count][] = "<span class='post_status' data-poststatus='".$finalResult['post_status']."'>".$finalResult['post_status']."</span>".$futureDate;
		$getFinalResults[$count][] = $tooltipContent;
		$count++;
	}
}
$getFinalResult['data'] = $getFinalResults;
$getFinalResult['total_files'] = count($getFinalResults);
$ars = json_encode($getFinalResult);
echo $ars;
exit;