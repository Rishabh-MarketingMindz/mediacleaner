<?php
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';
require_once("../wp-load.php");
require_once("../wp-admin/includes/plugin.php");
//require("../../../../../wp-load.php");
//require("../../../../../wp-admin/includes/plugin.php");
?>
<?php
// check if database cleaner page is active
@$data_cleaner = get_option('database_cleaner_activate');
@$lisence_activation = get_option('WPMC_lisence_activation');
if($data_cleaner == "yes" && $lisence_activation == 1){
// Bytes to formatSizeUnits
function formatSizeUnits($bytes)
{
  $label = array( 'B', 'KB', 'MB', 'GB');
  for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
  return( round( $bytes, 2 ) . " " . $label[$i] );
}

function checkplugindbtables($pluginname){
  global $wpdb;
  $pluginname = trim($pluginname);
  $pdresult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."plugin_data WHERE plugin_name = '$pluginname'");
  return $pdresult;
}


global $wpdb;
$prefixTable = $wpdb->get_blog_prefix();
$PostTable = $prefixTable.'posts';
$PostmetaTable = $prefixTable.'postmeta';
$CommentsTable = $prefixTable.'comments';
$CommentmetaTable = $prefixTable.'commentmeta';
$TermsTable = $prefixTable.'terms';
$TermmetaTable = $prefixTable.'termmeta';
$UsersTable = $prefixTable.'users';
$UsermetaTable = $prefixTable.'usermeta';
$OptionsTable = $prefixTable.'options';


?>

<div class="alert-box"></div>

<div id="ConfirmationModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span id="ConfirmationText"></span></label>
          <input type="hidden" name="actionValue" id="actionValue" value="" />
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" class="btn btn-blue deleteAllConfirm">
        <?= __('Ok') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>

<div id="deletePluginCnfmBox" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span id="PConfirmationText"></span></label>
          <input type="hidden" name="actionValue" id="PactionValue" value="" />
          <input type="hidden" name="PpluginPathName" id="PpluginPathName" value="" />
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" class="btn btn-blue deletePlugin">
        <?= __('Ok') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>

<div id="clearDatabaseCnfmBox" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span id="CConfirmationText"></span></label>
          <input type="hidden" name="actionValue" id="CactionValue" value="" />
          <input type="hidden" name="CpluginPathName" id="CpluginPathName" value="" />
          <input type="hidden" name="Cpluginname" id="Cpluginname" value="" />
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" class="btn btn-blue clearDatabase">
        <?= __('Ok') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>

<div id="ResetWithPackageModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span id="ResetConfirmationText"></span></label>
          <input type="hidden" name="actionValue" id="resetactionValue" value="" />
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" class="btn btn-blue ResetWithPackageConfirm">
        <?= __('Ok') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>

<div id="ActivePluginModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span id="activeInactiveText"></span></label>
          <input type="hidden" name="pluginPathName" id="pluginPathName" value="" />
          <input type="hidden" name="ActivatingDeactivatingplugintxt" id="ActivatingDeactivatingpluginCtxt" value="" />
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" class="btn btn-blue avtiveInactive">
        <?= __('Ok') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>

<!-- Edit media modal -->
<div id="ScheduleSet" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
      <form action="" method="POST">
        <div class="row">
          <label class="col-sm-12">
            <?= __('Please Select Schedule for ') ?>
            <span class="ScheduleName"></span>:</label>
          <input type="hidden" name="whichSchedule" id="whichSchedule" value="" />
          <div class="col-sm-6">
            <select class="form-control" name="ScheduleInterval" id="ScheduleInterval">
              <option value="_oneoff">
              <?= __('-- Please Select Recurrence --') ?>
              </option>
              <option value="hourly">
              <?= __('Hourly') ?>
              </option>
              <option value="daily">
              <?= __('Daily') ?>
              </option>
              <option value="monthly">
              <?= __('Monthly') ?>
              </option>
              <option value="yearly">
              <?= __('Yearly') ?>
              </option>
            </select>
          </div>
          <div class="col-sm-6">
            <select name="ScheduleIntervalTime" id="ScheduleIntervalTime">
              <option value="_oneoff">
              <?= __('-- Please Select Time --') ?>
              </option>
              <?php for($i = 1; $i <= 24; $i++): ?>
              <option value="<?= $i; ?>">
              <?= date("h.i A", strtotime("$i:00")); ?>
              </option>
              <?php endfor; ?>
            </select>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" id="save_exit" class="btn btn-blue">
        <?= __('Save and Exit') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
    </form>
  </div>
</div>
</div>
<div class="WPMC_database_cleaner"> 
  <!-- Rotating logo -->
  <div style="display: none" class="loading">
    <div class="loading_box">Please wait... <span>Do not close this window</span> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"> </div>
  </div>
  <div style="display: none" class="loading" id="ActivatingDeactivatingplugin">
    <div class="loading_box"><span id="ActivatingDeactivatingplugintxt"></span> <span>Do not close this window</span> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"> </div>
  </div>
  <!-- Rotating logo for actions-->
  <div class="action_loader">
    <div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span>LOADING</span> </div>
  </div>
  <div class="col-md-12">
    <ul class="data_cleaner">
      <li>
        <h5>
          <?= __('Overview') ?>
        </h5>
        <div class="form-group">
          <?php
            $totalTable = $wpdb->get_results("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA()");
            $totalTableCount = count($totalTable);
          ?>
          <label><span class="ActionName">
            <?= __('Database') ?>
            </span>: <span class="ActionnValue">
            <?= $totalTableCount ?>
            </span>
            <?= __('Tabels') ?>
          </label>
          <button class="btn btn-blue deleteAll" value="TabelsReindex" type="button">
          <?= __('Reindex') ?>
          </button>
          <button class="btn btn-blue deleteAll" value="TabelsOptimize" type="button">
          <?= __('Optimize') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $ExpiredTransients = $wpdb->get_var("SELECT COUNT(*) FROM $OptionsTable WHERE option_name LIKE '\_transient\_timeout\__%%' AND option_value < NOW()")
          ?>
          <label><span class="ActionName">
            <?= __('Expired Transients') ?>
            </span>: <span class="ActionnValue">
            <?= $ExpiredTransients ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="ExprTransientClear" type="button">
          <?= __('Clear') ?>
          </button>
          <button class="btn btn-blue Schedule" value="ExprTransientSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $AutoloadSizeQry = $wpdb->get_results("SELECT SUM(LENGTH(option_value)) as autoload_size FROM $OptionsTable WHERE autoload='yes'");
            $AutoloadSize = formatSizeUnits($AutoloadSizeQry[0]->autoload_size);
          ?>
          <label>
            <?= __('Autoload size') ?>
            :
            <?= $AutoloadSize ?>
          </label>
          <?php if($AutoloadSizeQry[0]->autoload_size < 1048576){ ?>
          <button class="btn btn-blue" id="" value="AutoloadOk" type="button">
          <?= __('OK') ?>
          </button>
          <?php }else{?>
          <button class="btn btn-red" id="" value="AutoloadOk" type="button">
          <?= __('Clean Database') ?>
          </button>
          <?php } ?>
        </div>
        <div class="form-group"> <span class="ActionName" style="display: none;">
          <?= __('All') ?>
          </span>
          <button class="btn btn-blue w2 Schedule" value="ScheduleAll" type="button">
          <?= __('Schedule for all') ?>
          </button>
        </div>
      </li>
      <li>
        <h5>
          <?= __('Content') ?>
        </h5>
        <div class="form-group">
          <?php
            $RevisionsQry = $wpdb->get_results("select count(post_type) as revisions from $PostTable where post_type = 'revision'");
            $RevisionsCount = $RevisionsQry[0]->revisions;
          ?>
          <label><span class="ActionName">
            <?= __('Revisions') ?>
            </span>: <span class="ActionnValue">
            <?= $RevisionsCount ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="RevisionsDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="RevisionsSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $TrashedQry = $wpdb->get_results("select count(post_type) as trashed from $PostTable where post_status = 'trash'");
            $TrashedCount = $TrashedQry[0]->trashed;
          ?>
          <label><span class="ActionName">
            <?= __('Trashed posts') ?>
            </span>: <span class="ActionnValue">
            <?= $TrashedCount ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="TrashedPostsDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="TrashedPostsSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $OrphanPostmetaQry = $wpdb->get_results("SELECT meta_id FROM $PostmetaTable LEFT JOIN $PostTable ON $PostTable.ID = $PostmetaTable.post_id WHERE $PostTable.ID IS NULL");
            $OrphanPostmeta = count($OrphanPostmetaQry);
          ?>
          <label><span class="ActionName">
            <?= __('Orphan postmeta') ?>
            </span>: <span class="ActionnValue">
            <?= $OrphanPostmeta ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="OrphanPostmetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="OrphanPostmetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            global $wpdb;
            $orphanattachment = array();
            $attachmentdata = $wpdb->get_results("select * from ".$prefixTable."posts where post_type='attachment'");
            if(!empty($attachmentdata)){
                foreach ($attachmentdata as $key => $value) {
                    $media_id = $value->ID;
                    $media_url = $value->guid;
                    $media_name = substr($media_url, strrpos($media_url, '/') + 1);
                    $attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'media_url'=>$media_url);
                }
            }
            function walkDir($dir) {
                global $return;
                $dh = new DirectoryIterator($dir);   
                // Dirctary object
                foreach ($dh as $item) {
                    if (!$item->isDot()) {
                        if ($item->isDir()) {
                            if($item !="wp_media_cleaner"){
                                walkDir("$dir/$item");
                            }
                        } else {
                            if($item->isFile() && preg_match("/(\.gif|\.png|\.jpe?g)$/", $item->getFilename())){
                                $fullpath = $dir . "/" . $item->getFilename();
                                $filename = $item->getFilename();
                                $type= $item->getExtension();
                                $return[] = array('path'=>$fullpath, 'filename'=>$filename,'Type'=>$type);
                            }
                        }
                    }
                }
                return $return;
            }
            $upload_directory_path =  ABSPATH.'wp-content/uploads/';
            $response = walkDir($upload_directory_path);
            @$filesindir =array_column($response, 'filename');
            if(!empty($attachmentarray)){
                foreach ($attachmentarray as $value){
                  if($filesindir != null){
                    if(!in_array($value['media_name'], $filesindir) ){
                        $orphanattachment[] = $value;
                    }
                  }
                }
            }
            $attachments = $wpdb->get_col("SELECT ID FROM $PostTable WHERE post_parent NOT IN (SELECT ID FROM $PostTable) AND post_parent != 0 AND post_type = 'attachment'");
            $orphanatch = array_column($orphanattachment, 'media_id');
            $finalorphanatch = array_merge($attachments, $orphanatch);
            $finalorphanatch = array_unique($finalorphanatch);
            $OrphanAttachments = count($finalorphanatch);
          ?>
          <label><span class="ActionName">
            <?= __('Orphan attachments') ?>
            </span>: <span class="ActionnValue">
            <?= $OrphanAttachments ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="OrphanAttachmentsDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="OrphanAttachmentsSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $duplicatedPostmetaQry = $wpdb->get_results("SELECT pm.meta_id AS meta_id, pm.post_id AS post_id FROM $PostmetaTable pm INNER JOIN (SELECT post_id, meta_key, meta_value, COUNT(*) FROM $PostmetaTable GROUP BY post_id, meta_key, meta_value HAVING COUNT(*) > 1) pm2 ON pm.post_id = pm2.post_id AND pm.meta_key = pm2.meta_key AND pm.meta_value = pm2.meta_value WHERE pm.meta_key NOT IN ('_price', '_used_by')");
            $duplicatedPostmeta = count($duplicatedPostmetaQry);
          ?>
          <label><span class="ActionName">
            <?= __('Duplicated postmeta') ?>
            </span>: <span class="ActionnValue">
            <?= $duplicatedPostmeta ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="DuplicatedPostmetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="DuplicatedPostmetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
      </li>
      <li>
        <h5>
          <?= __('Comments'); ?>
        </h5>
        <div class="form-group">
          <?php
            $SpamCommentsQry = $wpdb->get_results("SELECT comment_approved FROM $CommentsTable WHERE comment_approved='spam'");
            $SpamComments = count($SpamCommentsQry);
          ?>
          <label><span class="ActionName">
            <?= __('Spam comments') ?>
            </span>: <span class="ActionnValue">
            <?= $SpamComments ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="SpamCommentsDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="SpamCommentsSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $TrashedCommentsQry = $wpdb->get_results("SELECT comment_approved FROM $CommentsTable WHERE comment_approved='trash'");
            $TrashedComments = count($TrashedCommentsQry);
          ?>
          <label><span class="ActionName">
            <?= __('Trashed comments') ?>
            </span>: <span class="ActionnValue">
            <?= $TrashedComments ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="TrashedCommentsDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="TrashedCommentsSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $OrphanComments = $wpdb->get_var("SELECT COUNT(*) FROM $CommentmetaTable WHERE comment_id NOT IN (SELECT comment_ID FROM $CommentsTable)");
          ?>
          <label><span class="ActionName">
            <?= __('Orphan commentmeta') ?>
            </span>: <span class="ActionnValue">
            <?= $OrphanComments ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="OrphanCommentmetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="OrphanCommentmetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $DuplicatedCommentmetaQry = $wpdb->get_results("SELECT cm.meta_id AS meta_id, cm.comment_id AS comment_id FROM $CommentmetaTable cm INNER JOIN (SELECT comment_id, meta_key, meta_value, COUNT(*) FROM $CommentmetaTable GROUP BY comment_id, meta_key, meta_value HAVING COUNT(*) > 1) cm2 ON cm.comment_id = cm2.comment_id AND cm.meta_key = cm2.meta_key AND cm.meta_value = cm2.meta_value");
            $DuplicatedCommentmeta = count($DuplicatedCommentmetaQry);
          ?>
          <label><span class="ActionName">
            <?= __('Duplicated commentmeta') ?>
            </span>: <span class="ActionnValue">
            <?= $DuplicatedCommentmeta ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="DuplicatedCommentmetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="DuplicatedCommentmetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
      </li>
      <li>
        <h5>
          <?= __('Terms and Users'); ?>
        </h5>
        <div class="form-group">
          <?php
            $OrphanTermmetaQry = $wpdb->get_results("SELECT meta_id FROM $TermmetaTable LEFT JOIN $TermsTable ON $TermsTable.term_id = $TermmetaTable.term_id WHERE $TermsTable.term_id IS NULL");
            $OrphanTermmeta = count($OrphanTermmetaQry);
          ?>
          <label><span class="ActionName">
            <?= __('Orphan termmeta') ?>
            </span>: <span class="ActionnValue">
            <?= $OrphanTermmeta ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="OrphanTermmetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="OrphanTermmetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $OrphanUsermetaQry = $wpdb->get_results("SELECT umeta_id FROM $UsermetaTable LEFT JOIN $UsersTable ON $UsersTable.ID = $UsermetaTable.user_id WHERE $UsersTable.ID IS NULL");
            $OrphanUsermeta = count($OrphanUsermetaQry);
          ?>
          <label><span class="ActionName">
            <?= __('Orphan usermeta') ?>
            </span>: <span class="ActionnValue">
            <?= $OrphanUsermeta ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="OrphanUsermetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="OrphanUsermetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
        <div class="form-group">
          <?php
            $DuplicatedUsermetaQry = $wpdb->get_results("SELECT um.umeta_id AS umeta_id, um.user_id AS user_id FROM $UsermetaTable um INNER JOIN (SELECT user_id, meta_key, meta_value, COUNT(*) FROM $UsermetaTable GROUP BY user_id, meta_key, meta_value HAVING COUNT(*) > 1) um2 ON um.user_id = um2.user_id AND um.meta_key = um2.meta_key AND um.meta_value = um2.meta_value");
            $DuplicatedUsermeta = count($DuplicatedUsermetaQry);
          ?>
          <label><span class="ActionName">
            <?= __('Duplicated usermeta') ?>
            </span>: <span class="ActionnValue">
            <?= $DuplicatedUsermeta ?>
            </span></label>
          <button class="btn btn-blue deleteAll" value="DuplicatedUsermetaDelete" type="button">
          <?= __('Delete all') ?>
          </button>
          <button class="btn btn-blue Schedule" value="DuplicatedUsermetaSchedule" type="button">
          <?= __('Schedule') ?>
          </button>
        </div>
      </li>
      <li class="Plugins_box">
        <h5>
          <?= __('Plugins') ?>
        </h5>
        <?php
          $pluginsAvailabel = get_plugins();
          foreach ($pluginsAvailabel as $pluginkey => $pluginAvailabel) {
            if($pluginAvailabel['Name'] == 'WP Media Cleaner'){
              continue;
            }
            $plugintableexist = checkplugindbtables($pluginAvailabel['Name']);
            ?>
        <div class="form-group">
          <label>
            <?= $pluginAvailabel['Name'] ?>
          </label>
          <span class="text-right">
          <?php
                if(is_plugin_active( $pluginkey )){
                  ?>
          <a href="javascript:void(0)" class="btn btn-red deactivePlugin " data-plugin-path="<?= $pluginkey ?>">Deactivate</a>
          <?php
                }else{
                  ?>
          <a href="javascript:void(0)" class="btn btn-blue activePlugin" data-plugin-path="<?= $pluginkey ?>">Active</a>
          <?php
                }
              ?>
          <button class="btn btn-blue deletePluginCnfm" value="DeletePlugin" data-plugin-name="<?= $pluginAvailabel['Name'] ?>" data-plugin-key="<?= $pluginkey ?>"  type="button">
          <?= __('Delete plugin') ?>
          </button>
          <?php if($plugintableexist){ ?>
          <button class="btn btn-blue clearDatabaseCnfm"  value="ClearDatabase" data-plugin-name="<?= $pluginAvailabel['Name'] ?>" data-plugin-key="<?= $pluginkey ?>" type="button">
          <?= __('Clear Database') ?>
          </button>
          <?php }else{ ?>
          <button class="btn btn-blue clearDatabaseCnfm"  value="ClearDatabase" data-plugin-name="<?= $pluginAvailabel['Name'] ?>" data-plugin-key="<?= $pluginkey ?>" type="button" disabled="true">
          <?= __('Clear Database') ?>
          </button>
          <?php
              } ?>
          </span> </div>
        <?php } 
           $pluginstableAvailable = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."plugin_data GROUP BY plugin_name");
           foreach ($pluginstableAvailable as $value) { 
              if(!empty(trim($value->plugin_name))){
              if(!array_key_exists($value->plugin_file,$pluginsAvailabel)){
               ?>
        <div class="form-group">
          <label>
            <?= $value->plugin_name ?>
          </label>
          <span class="text-right">
          <button class="btn btn-blue activePlugin" disabled="true">
          <?= __('Active'); ?>
          </button>
          <button class="btn btn-blue" value="DeletePlugin" data-plugin-name="<?= $value->plugin_name ?>" data-plugin-key="<?= $value->plugin_file ?>" disabled="true" type="button">
          <?= __('Delete plugin') ?>
          </button>
          <button class="btn btn-blue clearDatabaseCnfm"  value="ClearDatabase" data-plugin-name="<?= $value->plugin_name ?>" data-plugin-key="<?= $value->plugin_file ?>" type="button">
          <?= __('Clear Database') ?>
          </button>
          </span> </div>
        <?php
              }
            }
          }
          ?>
      </li>
      <li class="reset_wordpress">
        <h5>
          <?= __('Reset Wordpress') ?>
        </h5>
        <div class="form-group">
          <div class="left">
            <button class="btn btn-red" id="" value="HardReset" data-toggle="modal" data-target="#confirmresetModal" type="button">
            <?= __('Reset') ?>
            </button>
          </div>
          <div class="red right">
            <?= __('Warning this a hard reset') ?>
          </div>
        </div>
        <hr />
        <div class="form-group">
          <div class="left">
            <button class="btn btn-red" id="ResetWithPackage" value="ResetWithPackage" type="button">
            <?= __('Reset with package') ?>
            </button>
          </div>
          <div class="right text-center">
            <?= __('Package comment') ?>
          </div>
        </div>
        <div class="form-group">
          <div class="left"> <span class="btn btn-blue fileinput-button"> <span>
            <?= __('Upload plugin') ?>
            </span>
            <input type="file" id="plugins" name="plugins[]" accept=".zip" multiple />
            </span> <span class="btn btn-blue fileinput-button"> <span>
            <?= __('Upload theme') ?>
            </span>
            <input type="file" id="themes" name="themes[]" accept=".zip" multiple />
            </span> <span class="btn btn-blue fileinput-button"> <span>
            <?= __('Upload image') ?>
            </span>
            <input type="file" id="medias" name="medias[]" accept="audio/*, video/*, image/*" multiple />
            </span> </div>
          <div class="right border_box"> <span id="plugindetails"  > <span id="plugincounts">0 </span> Plugin(s) <i  class="glyphicon glyphicon-info-sign pull-right"></i><span class="hover_tooltip" id="pluginnames"></span></span> <span id="themedetails"  > <span id="themecounts">0 </span> Theme(s) <i  class="glyphicon glyphicon-info-sign pull-right"></i><span class="hover_tooltip" id="themenames"></span></span> <span id="mediadetails"   > <span id="mediacounts">0 </span> Media file(s) <i  class="glyphicon glyphicon-info-sign pull-right"></i><span class="hover_tooltip" id="medianames"></span></span> </div>
        </div>
      </li>
    </ul>
  </div>
</div>
<div class="modal" id="confirmModal" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-sm vertical-align-center">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btn_close_modal_info"><span aria-hidden="true">&times;</span></button>
          <div id="confirmContent" style="font-weight: normal;"></div>
        </div>
        <div class="modal-footer bg-warning text-center" id="footer_modal">
          <button type="button" class="btn btn-primary btn_yes_confirm">
          <?= __('Yes') ?>
          </button>
          <button type="button" class="btn btn-primary btn_no_confirm" data-dismiss="modal" aria-label="Close">
          <?= __('No') ?>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="confirmresetModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span><?= __('Do you want to take backup before reset ? ') ?></span></label>
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" value="HardReset" class="btn btn-blue hardresetcnfrm" data-dismiss="modal">
       <!--  <button type="button" class="btn btn-blue takebackupandreset" value="HardReset"> -->
        <?= __('Yes') ?>
        </button>
        <button type="button" value="HardReset" class="btn btn-red hardresetcnfrm" data-dismiss="modal">
        <?= __('No') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>

<div id="HRConfirmationModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form type="form" method="post" id="updateMediaData">
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><span><?= __('Are you sure you want to reset Wordpress, This will be a hard reset ?') ?></span></label>
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="button" class="btn btn-blue hardreset">
        <?= __('Ok') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?= __('Cancel') ?>
        </button>
      </div>
    </div>
  </div>
  </div>
</div>
<?php $site_Url = get_site_url().'/wp-admin'; ?>
<script>
jQuery(window).load(function(){
		setTimeout(function() {
		    jQuery('.action_loader').fadeOut('fast');
		}, 3000);
	});

if(sessionStorage.getItem("SessionMessage")){
  setTimeout(
  function() 
  {
    jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+sessionStorage.getItem("SessionMessage")+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
    sessionStorage.removeItem("SessionMessage");
  }, 3000);
}

jQuery(".deleteAll").click(function(){
  jQuery('#ConfirmationModal').modal('show');
  var actionValue = jQuery(this).val();
  var actiontext = jQuery(this).parent().find(".ActionName").text();
  var actionDelete = jQuery(this).text();
  jQuery("#ConfirmationText").text('Are you sure to '+actionDelete+' '+actiontext+'?');
  jQuery("#actionValue").val(actionValue);
  
});

jQuery(".deleteAllConfirm").click(function(){
  jQuery('#ConfirmationModal').modal('hide');
  var actionValue = jQuery("#actionValue").val();
    jQuery(".loading").show();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php",
      data: {
        action: actionValue
      },
      cache: false,
      success: function(response){
        jQuery(".loading").hide();
        if(actionValue != 'TabelsReindex' && actionValue != 'TabelsOptimize'){
          jQuery("[value="+actionValue+"]").parent().find(".ActionnValue").text('0');
        }
        jQuery( ".alert-box" ).addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
      },
      error: function (jqXHR, exception) {
        jQuery(".loading").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
});

jQuery(document).on("click",".activePlugin",function() {
  var pluginPath = jQuery(this).attr('data-plugin-path');
  jQuery("#activeInactiveText").text('Are you sure you want  to activate this pulgin?');
  jQuery("#pluginPathName").val(pluginPath);
  jQuery("#ActivatingDeactivatingpluginCtxt").text('Activating plugin');
  jQuery('#ActivePluginModal').modal('show');
});

jQuery(document).on("click",".deactivePlugin",function() {
  var pluginPath = jQuery(this).attr('data-plugin-path');
  jQuery("#activeInactiveText").text('Are you sure you want  to deactivate this pulgin?');
  jQuery("#pluginPathName").val(pluginPath);
  jQuery("#ActivatingDeactivatingpluginCtxt").text('Deactivating plugin');
  jQuery('#ActivePluginModal').modal('show');
});

jQuery(".avtiveInactive").click(function(){
  jQuery('#ActivePluginModal').modal('hide');
  var actionName = "avtiveInactive";
  var pluginPath = jQuery("#pluginPathName").val();
    jQuery('#ActivatingDeactivatingplugintxt').text(jQuery("#ActivatingDeactivatingpluginCtxt").text());
    jQuery("#ActivatingDeactivatingplugin").show();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php?plugin="+pluginPath,
      data: {
        action: actionName,
        pluginPath: pluginPath
      },
      cache: false,
      success: function(response){
        jQuery("#ActivatingDeactivatingplugin").hide();
        jQuery( ".alert-box" ).addClass('success');
        sessionStorage.setItem("SessionMessage", response);
        if(response == 'Plugin Activated Successfully'){
          jQuery('*[data-plugin-path="'+pluginPath+'"]').toggleClass('activePlugin deactivePlugin');
          jQuery('*[data-plugin-path="'+pluginPath+'"]').toggleClass('btn-blue btn-red');
          jQuery('*[data-plugin-path="'+pluginPath+'"]').text('Deactivate');
        }
        if(response == 'Plugin Deactivated Successfully'){
          jQuery('*[data-plugin-path="'+pluginPath+'"]').toggleClass('activePlugin deactivePlugin');
          jQuery('*[data-plugin-path="'+pluginPath+'"]').toggleClass('btn-blue btn-red');
          jQuery('*[data-plugin-path="'+pluginPath+'"]').text('Active');
        }
        //jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        location.reload();
      },
      error: function (jqXHR, exception) {
        jQuery("#ActivatingDeactivatingplugin").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
});

jQuery(document).on("click",".Schedule",function() {
  var Scheduletext = jQuery(this).parent().find(".ActionName").text();
  var ScheduleValue = jQuery(this).val();
  jQuery("#whichSchedule").val(ScheduleValue);
  jQuery(".ScheduleName").text(Scheduletext);
  jQuery('#ScheduleSet').modal('show');
});

jQuery("#save_exit").click(function(){
  var whichSchedule = jQuery("#whichSchedule").val();
  var ScheduleInterval = jQuery("#ScheduleInterval").val();
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  var ScheduleIntervalTime = today + ' ' + jQuery("#ScheduleIntervalTime").val() + ':00:00';
  jQuery(".loading").show();
  jQuery.ajax({
    type: "POST",
    url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php",
    data: {
      action: whichSchedule,
      ScheduleInterval: ScheduleInterval,
      ScheduleIntervalTime: ScheduleIntervalTime
    },
    cache: false,
    success: function(response){
      jQuery(".loading").hide();
      jQuery('#ScheduleSet').modal('hide');
      jQuery( ".alert-box" ).addClass('success');
      jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
    },
    error: function (jqXHR, exception) {
      jQuery(".loading").hide();
      jQuery( ".alert-box" ).addClass('unsuccess');
      jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
    }
  });
});

jQuery(".deletePluginCnfm").click(function(){
  jQuery('#deletePluginCnfmBox').modal('show');
  var actionValue = jQuery(this).val();
  var actiontext = jQuery(this).attr('data-plugin-name');
  var pluginkey =  jQuery(this).attr('data-plugin-key');
  jQuery("#PConfirmationText").text('Are you sure you want to delete this '+actiontext+' plugin?');
  jQuery("#PactionValue").val(actionValue);
  jQuery("#PpluginPathName").val(pluginkey);
});

jQuery(".deletePlugin").click(function(){
  jQuery('#deletePluginCnfmBox').modal('hide');
  var pluginkey = jQuery("#PpluginPathName").val();
  var actionValue = jQuery('#PactionValue').val();
  //var element = this;
    jQuery(".loading").show();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php",
      data: {
        action: actionValue,
        pluginkey: pluginkey
      },

      cache: false,
      success: function(response){
        jQuery(".loading").hide();
        if(response == "true"){
          jQuery( ".alert-box" ).addClass('success');
          var attrr = jQuery('*[data-plugin-key="'+pluginkey+'"]').siblings("button").attr('disabled');
          if (typeof attrr !== typeof undefined && attrr !== false) {
            jQuery('*[data-plugin-key="'+pluginkey+'"]').parent().parent().remove();
          }else{
            jQuery('*[data-plugin-key="'+pluginkey+'"]').attr('disabled',true);
          }
          var deletestatus = "Plugin successfully deleted.";
          sessionStorage.setItem("SessionMessage", deletestatus);
        }else{
          jQuery( ".alert-box" ).addClass('danger');
          var deletestatus = "Please try again to delete plugin.";
          sessionStorage.setItem("SessionMessage", deletestatus);
        }
        //jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+deletestatus+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        //setTimeout(location.reload.bind(location), 1600);
        location.reload();
      },
      error: function (jqXHR, exception) {
        jQuery(".loading").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
});

jQuery(".clearDatabaseCnfm").click(function(){
  jQuery('#clearDatabaseCnfmBox').modal('show');
  var actionValue = jQuery(this).val();
  var pluginname = jQuery(this).attr('data-plugin-name');
  var pluginkey = jQuery(this).attr('data-plugin-key');
  jQuery("#CConfirmationText").text('Are you sure you want to delete tables for this '+pluginname+' plugin?');
  jQuery("#CactionValue").val(actionValue);
  jQuery("#CpluginPathName").val(pluginkey);
  jQuery("#Cpluginname").val(pluginname);
});

jQuery(".clearDatabase").click(function(){
  jQuery('#clearDatabaseCnfmBox').modal('hide');
  var actionValue = jQuery("#CactionValue").val();
  var pluginname = jQuery("#Cpluginname").val();
  var pluginkey = jQuery("#CpluginPathName").val();
    jQuery(".loading").show();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php",
      data: {
        action: actionValue,
        pluginname: pluginname,
        pluginkey: pluginkey
      },
      cache: false,
      success: function(response){
        jQuery(".loading").hide();
        console.log(response);
        if(response == "true"){
          jQuery( ".alert-box" ).addClass('success');
          var attrr = jQuery('*[data-plugin-key="'+pluginkey+'"]').siblings("button").attr('disabled');
         if (typeof attrr !== typeof undefined && attrr !== false) {
            jQuery('*[data-plugin-key="'+pluginkey+'"]').parent().remove();
          }else{
            jQuery('*[data-plugin-key="'+pluginkey+'"]').attr('disabled',true);
          }
          var deletestatus = "Database Tables for plugin successfully deleted.";
        }else{
          jQuery( ".alert-box" ).addClass('danger');
          var deletestatus = "Please try again to clear database.";
        }
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+deletestatus+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        setTimeout(location.reload.bind(location), 1600);
      },
      error: function (jqXHR, exception) {
        jQuery(".loading").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
});

jQuery(".hardresetcnfrm").click(function(){
  jQuery('#HRConfirmationModal').modal('show');
});

jQuery(".hardreset").click(function(){
  var actionValue = 'HardReset';
  jQuery('#HRConfirmationModal').modal('hide');
    jQuery(".loading").show();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php",
      data: {
        action: actionValue,
      },
      cache: false,
      success: function(response){
        jQuery(".loading").hide();
        console.log(response);
        jQuery( ".alert-box" ).addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Wordpress reset Done!</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        window.location.replace("<?= $site_Url; ?>");
      },
      error: function (jqXHR, exception) {
        jQuery(".loading").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
});

jQuery("#ResetWithPackage").click(function(e){
  e.preventDefault();
  jQuery("#ResetConfirmationText").text('Are you sure you want to reset WordPress with this package ?');
  var actionValue = jQuery(this).val();
  jQuery("#resetactionValue").val(actionValue);
  jQuery('#ResetWithPackageModal').modal('show');
});

jQuery(".ResetWithPackageConfirm").click(function(e){
  e.preventDefault();
  jQuery('#ResetWithPackageModal').modal('hide');
  var actionValue = jQuery("#resetactionValue").val();
  var pluginsData = new FormData();
  var pluginsDataLength = document.getElementById('plugins').files.length;
  for (var x = 0; x < pluginsDataLength; x++) {
    pluginsData.append("plugins[]", document.getElementById('plugins').files[x]);
  }
  var themesDataLength = document.getElementById('themes').files.length;
  for (var x = 0; x < themesDataLength; x++) {
    pluginsData.append("themes[]", document.getElementById('themes').files[x]);
  }
  var mediasDataLength = document.getElementById('medias').files.length;
  for (var x = 0; x < mediasDataLength; x++) {
    pluginsData.append("medias[]", document.getElementById('medias').files[x]);
  }

    jQuery(".loading").show();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/database_cleaner.php?action="+actionValue,
      data: pluginsData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        jQuery(".loading").hide();
        jQuery( ".alert-box" ).addClass('success');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Wordpress reset with package Done!</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        window.location.replace("<?= $site_Url; ?>");
      },
      error: function (jqXHR, exception) {
        jQuery(".loading").hide();
        jQuery(".loading").hide();
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
});

jQuery('#plugins').on('change',function(){
    var fi = document.getElementById('plugins');
    // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
    if (fi.files.length > 0) {
     
        // RUN A LOOP TO CHECK EACH SELECTED FILE.
        for (var i = 0; i <= fi.files.length - 1; i++) {
            var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
            //var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
            // SHOW THE EXTRACTED DETAILS OF THE FILE.
            document.getElementById('plugincounts').innerHTML = fi.files.length;
            document.getElementById('pluginnames').innerHTML =
                document.getElementById('pluginnames').innerHTML + '<br /> ' +
                    fname + '';

                   var titl =  jQuery('#plugindetails').attr('title');
                   var newtitl = titl + fname + ' | ';

                   jQuery('#plugindetails').attr('title',newtitl);
        }
    }
    else { 
        alert('Please select a file.') 
    }
  });

jQuery('#themes').on('change',function(){
    var fi = document.getElementById('themes');
    // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
    if (fi.files.length > 0) {
    
        // RUN A LOOP TO CHECK EACH SELECTED FILE.
        for (var i = 0; i <= fi.files.length - 1; i++) {
            var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
            //var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
            // SHOW THE EXTRACTED DETAILS OF THE FILE.
            document.getElementById('themecounts').innerHTML = fi.files.length;
            document.getElementById('themenames').innerHTML =
                document.getElementById('themenames').innerHTML + '<br /> ' +
                    fname + '';

                   var titl =  jQuery('#themedetails').attr('title');
                   var newtitl = titl + fname + ' | ';

                  jQuery('#themedetails').attr('title',newtitl);
        }
    }
    else { 
        alert('Please select a file.') 
    }
  });

jQuery('#medias').on('change',function(){
    var fi = document.getElementById('medias');
    // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
    if (fi.files.length > 0) {
        
       
        // RUN A LOOP TO CHECK EACH SELECTED FILE.
        for (var i = 0; i <= fi.files.length - 1; i++) {
            var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
            //var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
            // SHOW THE EXTRACTED DETAILS OF THE FILE.
            document.getElementById('mediacounts').innerHTML = fi.files.length;
            document.getElementById('medianames').innerHTML =
                document.getElementById('medianames').innerHTML + ' | ' +
                    fname + '';

                   var titl =  jQuery('#mediadetails').attr('title');
                   var newtitl = titl + fname + ' <br> ';

                   jQuery('#mediadetails').attr('title',newtitl);
        }
    }
    else { 
        alert('Please select a file.') 
    }
  });

</script>
<?php
}else{
  if($media_scan != "yes" && $lisence_activation == 1){
    ?>
    <div class="WPMC_media_scan">
      <div class="disable_section">
        <h3><?php _e("Database cleaner page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_cleaner"); ?></h3>
        <a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Go to settings","wp_media_cleaner"); ?></a>
      </div>
    </div>
    <?php
  }elseif($media_scan == "yes" && $lisence_activation == 0){
    $admin_url = get_admin_url();
    ?>
    <div class="WPMC_media_scan">
      <div class="disable_section">
        <h3><?php _e("Please activate your license to access database cleaner page","wp_media_cleaner"); ?></h3>
        <a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Activate Now","wp_media_cleaner"); ?></a>
      </div>
    </div>
    <?php
  }
  
}
?>