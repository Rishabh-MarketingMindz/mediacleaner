<?php
/*error_reporting(1);
@ini_set('display_errors', 1);*/
@ini_set('memory_limit', '-1');
error_reporting(E_ERROR | E_PARSE);
set_time_limit(0);

require("../../../../../wp-load.php");
require_once("../../../../../wp-admin/includes/plugin.php");

if(!@$action = $_POST['action']){
  $action = $_GET['action'];
}else{
  $action = $_POST['action'];
}

function exportMultisiteDatabase($multi_array, $old_URL, $new_URL, $one_multisite, $destination_prefix, $oldNewMultiSitePrefix){
    global $wpdb;
    $output = '';
    $output .= 'SET FOREIGN_KEY_CHECKS = 0;';
    if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
        mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
    }
    $finalbackuppath = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp';
    $blogArrs = array();
    $incs = 2;
    foreach ($multi_array as $old_value) {
        $onlyPrefix[] = $old_value['prefix']; // multisite which user want to export
        preg_match_all('!\d+!', $old_value['prefix'], $matches);
        $multisite_blog_ids[] = $matches[0][0];
        if($old_value['prefix'] != $wpdb->prefix){
            $blogArrs[$wpdb->prefix.$incs.'_'] = $old_value['prefix'];
            $incs++;
        }        
    }

    // get prefixes of multisites
    $total_site_count = get_blog_count();
    for ($x = $total_site_count; $x >= 2; $x--) {
        $prefixes[] = $wpdb->get_blog_prefix($x);
    }  
    $tables = $wpdb->get_col('SHOW TABLES');
    $table_main = $wpdb->get_col('SHOW TABLES');
    $all_comm_tables = $wpdb->get_col('SHOW TABLES'); // for common tables

    // Get only main site table when selected eg: wp_
    foreach ($table_main as $maintable_key => $mainvalue) {
        foreach ($prefixes as $allprefixes) {
            if (strpos($mainvalue, $allprefixes) !== false && $allprefixes != $wpdb->prefix){
                unset($table_main[$maintable_key]);
            }
        }
    }

    // Get multisite table(s) eg: wp_2_ or wp_3_ it will not include main site tables
    $newtable = array();
    $heyArr = array();
    if(count($onlyPrefix) == '1' && $onlyPrefix[0] == $wpdb->prefix){
    }else{
        foreach ($tables as $table_key => $value) {
            foreach ($onlyPrefix as $key => $prevalue) {
                if (strpos($value, $prevalue) !== false && $prevalue != $wpdb->prefix){
                    // if($one_multisite == "single"){
                    //     if (strpos($value, "_options") == false){
                    //         $newtable[] = $value;
                    //     }
                    // }else{
                    //     $newtable[] = $value;
                    // }
                    $newtable[] = $value;
                    $heyArr[] = str_replace($prevalue, $wpdb->prefix, $value);
                }
            }
        }
    }    
    
    // Remove those tables from main prefix tables which are present in multisite tables
    if($one_multisite == "single"){
        foreach ($table_main as $main_key => $filter_value) {            
            if(in_array($filter_value, $heyArr)){
                unset($table_main[$main_key]);
            }
            // Remove multisite's table that created in the main table prefix
            if (strpos($filter_value, "_blogs") !== false || strpos($filter_value, "_blogmeta") !== false || strpos($filter_value, "_site") !== false || strpos($filter_value, "_sitemeta") !== false || strpos($filter_value, "_blog_versions") !== false){
                unset($table_main[$main_key]);
            }
        }
        // array_push($table_main, $wpdb->prefix."options");
    }

    if($one_multisite == "multi"){
        // Skip the default wp tables if main website is not selected
        if(!in_array($wpdb->prefix, $onlyPrefix)){
            $toSkip = array($wpdb->prefix."blogs", $wpdb->prefix."blogmeta", $wpdb->prefix."blog_versions", $wpdb->prefix."site", $wpdb->prefix."sitemeta", $wpdb->prefix."options",$wpdb->prefix."users",$wpdb->prefix."usermeta",$wpdb->prefix."posts",$wpdb->prefix."postmeta",$wpdb->prefix."terms",$wpdb->prefix."termmeta",$wpdb->prefix."term_relationships",$wpdb->prefix."term_taxonomy",$wpdb->prefix."comments",$wpdb->prefix."commentmeta",$wpdb->prefix."links");
            $table_main = array_diff($table_main, $toSkip);
        }
    }

    $new_arr = array();
    if(!empty($table_main) && !empty($newtable)){
        $new_arr = array_merge($table_main, $newtable);
    }elseif(!empty($table_main) && empty($newtable)){
        $new_arr = $table_main;
    }elseif(empty($table_main) && !empty($newtable)){
        $new_arr = $newtable;
    }

    $wpmc_plugins_table = array($wpdb->prefix."mediascan_data",$wpdb->prefix."optimizer_backup",$wpdb->prefix."image_optimizer",$wpdb->prefix."contentscan_data");

    $common_tables = array();
    foreach ($new_arr as $com_value) {
        if(!in_array($com_value, $wpmc_plugins_table)){
            $common_tables[] = $com_value;
        }
    }
    foreach ($common_tables as $table){
        $result = $wpdb->get_results("SELECT * FROM {$table}", ARRAY_N);
        foreach ($blogArrs as $key => $value) {
            if(strpos($table, $value) !== false){
                $table = str_replace($value, $key, $table);
            }
        }
        if(!strpos($table, '_usermeta') !== false){
            $exists = 'DROP TABLE IF EXISTS ' . $table . ';';
            $row2 = $wpdb->get_row('SHOW CREATE TABLE ' . $table, ARRAY_N);
            $output .= "\n\n" . $exists . "\n\n" . $row2[1] . ";\n\n";
        }
        for ($i = 0; $i < count($result); $i++) {
           $row = $result[$i];
           if(!in_array("session_tokens", $row)){
                $output .= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < count($result[0]); $j++) {
                    $row[$j] = $wpdb->_real_escape($row[$j]);
                    $output .= (isset($row[$j])) ? '"' . $row[$j] . '"' : '""';
                    if ($j < (count($result[0]) - 1)) {
                        $output .= ',';
                    }
                }
                $output .= ");\n";
            }
        }
        $output .= "\n";
    }
    if($one_multisite == "multi"){
        $inc = 1;
        array_unshift($multisite_blog_ids, '1');
        $blogIds = implode(", ",array_filter($multisite_blog_ids));
        // echo "<pre>"; print_r($blogIds); die();
        $blog_table = $wpdb->prefix.'blogs';
        $get_blogs = $wpdb->get_results("SELECT * FROM {$blog_table} WHERE blog_id IN ($blogIds)", ARRAY_N);

        $output .= "DROP TABLE {$blog_table};"."\n\n";
        $output .= "CREATE TABLE {$blog_table} (
          `blog_id` bigint(20) NOT NULL AUTO_INCREMENT,
          `site_id` bigint(20) COLLATE utf8mb4_unicode_520_ci DEFAULT '0' NOT NULL,
          `domain` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `path` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `registered` datetime COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `last_updated` datetime COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `public` tinyint(2) COLLATE utf8mb4_unicode_520_ci DEFAULT '1' NOT NULL,
          `archived` tinyint(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `mature` tinyint(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `spam` tinyint(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `deleted` tinyint(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          `lang_id` int(11) COLLATE utf8mb4_unicode_520_ci NOT NULL,
          PRIMARY KEY (`blog_id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;\n\n";
        $output .= "INSERT INTO {$blog_table} VALUES";

        $do_blog = '';
        $blodidCount = 1;
        foreach ($get_blogs as $blogsvalue) {
            $do_blog .= '("'.$blodidCount.'","'.$blogsvalue[1].'","'.$blogsvalue[2].'","'.$blogsvalue[3].'","'.$blogsvalue[4].'","'.$blogsvalue[5].'","'.$blogsvalue[6].'","'.$blogsvalue[7].'","'.$blogsvalue[8].'","'.$blogsvalue[9].'","'.$blogsvalue[10].'","'.$blogsvalue[11].'"),';
            $blodidCount++;
        }
        $output .= rtrim($do_blog,",");
        $output .= ";\n\n";
    }

    $output .= 'SET FOREIGN_KEY_CHECKS = 1;';    
    // $outputq = str_replace($old_URL, $new_URL, $output);
    if($one_multisite == "single"){
        // get multisite (child) URL for replace
        foreach ($multisite_blog_ids as $blog_value) {
            $the_site = get_site($blog_value);
            $the_domain = $the_site->domain;
            $the_path = $the_site->path;
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $siteurl = $protocol.$the_domain.$the_path;
            $output = str_replace(preg_replace('{/$}', '', $siteurl), preg_replace('{/$}', '', $new_URL), $output);
        }
        $output = str_replace(preg_replace('{/$}', '', $old_URL), preg_replace('{/$}', '', $new_URL), $output);
        $output = str_replace(preg_replace('{/$}', '', $onlyPrefix[0]), preg_replace('{/$}', '', $wpdb->prefix), $output);
    }else{
        foreach ($oldNewMultiSitePrefix as $blog_value) {
            $from_blogID = $blog_value['chosen_site'];
            $to_blogID = $blog_value['updated_site'];
            $from_site_preff = $wpdb->get_blog_prefix($from_blogID);
            $to_site_preff = $wpdb->get_blog_prefix($to_blogID);
            $output = str_replace(preg_replace('{/$}', '', $from_site_preff), preg_replace('{/$}', '', $to_site_preff), $output);
        }
    }
    // change path from old to new sites
    $existingPath = parse_url($old_URL);
    $newPath = parse_url($new_URL);

    $output = str_replace(preg_replace('{/$}', '', $existingPath), preg_replace('{/$}', '', $newPath), $output);
    $output = str_replace(preg_replace('{/$}', '', $wpdb->prefix), preg_replace('{/$}', '', $destination_prefix), $output);

    $databaseName = 'WPMC_Backup_'. time() .'.sql';
    $backup_file_name_with_path_to_save = $finalbackuppath.'/'.$databaseName;
    file_put_contents($backup_file_name_with_path_to_save, $output);
}

/* Zip provide directory files recursively */
function zipme($source, $destination,$excludestatus){
    if (!file_exists($source)){
        echo "source doesn't exist";
        return false;
    } 
     
    if (!extension_loaded('zip')){
        echo "zip extension not loaded in php";
        return false;
    }
  
    $zip = new ZipArchive();
    if (!$zip->open($destination, (ZipArchive::CREATE | ZipArchive::OVERWRITE))) {
        echo "failed to create zip file on destination";
        return false;
    }
  
    $source = str_replace('\\', '/', realpath($source));
  
    if (is_dir($source) === true){
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
  
        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);
  
            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;
  
            $file = realpath($file);
            if (is_dir($file) === true) {

                if (strpos($file, 'wp_media_cleaner') != true && $excludestatus == true){
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                if($excludestatus == false){
                     $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }

            } else if (is_file($file) === true) {
                if (strpos($file, 'wp_media_cleaner') != true && $excludestatus == true){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }

                if ($excludestatus == false){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
    } else if (is_file($source) === true) {
        $zip->addFromString(basename($source), file_get_contents($source));
    }
    return $zip->close();
}

function zipmeNoSites($source, $destination,$excludestatus){
    if (!file_exists($source)){
        echo "source doesn't exist";
        return false;
    } 
     
    if (!extension_loaded('zip')){
        echo "zip extension not loaded in php";
        return false;
    }
  
    $zip = new ZipArchive();
    if (!$zip->open($destination, (ZipArchive::CREATE | ZipArchive::OVERWRITE))) {
        echo "failed to create zip file on destination";
        return false;
    }
  
    $source = str_replace('\\', '/', realpath($source));
  
    if (is_dir($source) === true){
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
  
        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);
  
            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;
  
            $file = realpath($file);
            if (is_dir($file) === true) {

                if (strpos($file, 'sites') != true && $excludestatus == true){
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                if($excludestatus == false){
                     $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }

            } else if (is_file($file) === true) {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                if ($excludestatus == false){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
    } else if (is_file($source) === true) {
        $zip->addFromString(basename($source), file_get_contents($source));
    }
    return $zip->close();
}

function zipmeMultisite($source, $destination,$excludestatus){
    if (!file_exists($source)){
        echo "source doesn't exist";
        return false;
    } 
     
    if (!extension_loaded('zip')){
        echo "zip extension not loaded in php";
        return false;
    }
  
    $zip = new ZipArchive();
    if (!$zip->open($destination, (ZipArchive::CREATE | ZipArchive::OVERWRITE))) {
        echo "failed to create zip file on destination";
        return false;
    }
  
    $source = str_replace('\\', '/', realpath($source));
  
    if (is_dir($source) === true){
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
  
        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);
  
            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;
  
            $file = realpath($file);
            if (is_dir($file) === true) {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                if($excludestatus == false){
                     $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }

            } else if (is_file($file) === true) {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                if ($excludestatus == false){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
    } else if (is_file($source) === true) {
        $zip->addFromString(basename($source), file_get_contents($source));
    }
    return $zip->close();
}

/* delete function that deals with directories recursively */
function delete_files($target) {
    if(is_dir($target)){
        /*GLOB_MARK adds a slash to directories returned*/
        $files = glob( $target . '*', GLOB_MARK ); 
        if(!empty($files)){
            foreach( $files as $file ){
                delete_files( $file );      
            }
        }
        rmdir( $target );
    } elseif(is_file($target)) {
        unlink( $target );  
    }
}

/* Export database for backup */
function exportDatabase($old_URL, $new_URL, $destination_prefix, $one_multisite, $content_filter){
    global $wpdb;
    $tables = $wpdb->get_col('SHOW TABLES');
    $output = '';
    $output .= 'SET FOREIGN_KEY_CHECKS = 0;';
    if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
        mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
    }
    $finalbackuppath = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp';
    if(!empty($tables)){
        foreach ($tables as $table) {
            // if content filters
            if($content_filter && strpos($table, '_posts') !== false){
                $result = $wpdb->get_results('SELECT * FROM '.$table.' WHERE post_type IN ("' . implode('", "', $content_filter) . '")', ARRAY_N);
                foreach ($result as $value) {
                    $getpostids[] = $value[0];
                }
                $currentSitePrefix = str_replace("_posts", "", $table);
                $tablemeta = $currentSitePrefix.'_postmeta';
                $result_meta = $wpdb->get_results('SELECT * FROM '.$tablemeta.' WHERE post_id IN ("' . implode('", "', $getpostids) . '")', ARRAY_N);

                $exists = 'DROP TABLE IF EXISTS ' . $table . ';';
                $row2 = $wpdb->get_row('SHOW CREATE TABLE ' . $table, ARRAY_N);
                $output .= "\n\n" . $exists . "\n\n" . $row2[1] . ";\n\n";
                for ($i = 0; $i < count($result); $i++) {
                   $row = $result[$i];
                    $output .= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < count($result[0]); $j++) {
                        $row[$j] = $wpdb->_real_escape($row[$j]);
                        $output .= (isset($row[$j])) ? '"' . $row[$j] . '"' : '""';
                        if ($j < (count($result[0]) - 1)) {
                            $output .= ',';
                        }
                    }
                    $output .= ");\n";
                }
                $output .= "\n";

                if($result_meta){
                    $exists = 'DROP TABLE IF EXISTS ' . $tablemeta . ';';
                    $row2 = $wpdb->get_row('SHOW CREATE TABLE ' . $tablemeta, ARRAY_N);
                    $output .= "\n\n" . $exists . "\n\n" . $row2[1] . ";\n\n";
                    for ($i = 0; $i < count($result_meta); $i++) {
                       $row = $result_meta[$i];
                        $output .= 'INSERT INTO ' . $tablemeta . ' VALUES(';
                        for ($j = 0; $j < count($result_meta[0]); $j++) {
                            $row[$j] = $wpdb->_real_escape($row[$j]);
                            $output .= (isset($row[$j])) ? '"' . $row[$j] . '"' : '""';
                            if ($j < (count($result_meta[0]) - 1)) {
                                $output .= ',';
                            }
                        }
                        $output .= ");\n";
                    }
                    $output .= "\n";
                }
            }else{
                if($content_filter){
                    if(strpos($table, '_postmeta') == false){
                        $result = $wpdb->get_results("SELECT * FROM {$table}", ARRAY_N);
                        $exists = 'DROP TABLE IF EXISTS ' . $table . ';';
                        $row2 = $wpdb->get_row('SHOW CREATE TABLE ' . $table, ARRAY_N);
                        $output .= "\n\n" . $exists . "\n\n" . $row2[1] . ";\n\n";
                        for ($i = 0; $i < count($result); $i++) {
                           $row = $result[$i];
                            $output .= 'INSERT INTO ' . $table . ' VALUES(';
                            for ($j = 0; $j < count($result[0]); $j++) {
                                $row[$j] = $wpdb->_real_escape($row[$j]);
                                $output .= (isset($row[$j])) ? '"' . $row[$j] . '"' : '""';
                                if ($j < (count($result[0]) - 1)) {
                                    $output .= ',';
                                }
                            }
                            $output .= ");\n";
                        }
                        $output .= "\n";
                    }
                }else{
                    // Condition when content filter is not selected
                    $result = $wpdb->get_results("SELECT * FROM {$table}", ARRAY_N);
                    $exists = 'DROP TABLE IF EXISTS ' . $table . ';';
                    $row2 = $wpdb->get_row('SHOW CREATE TABLE ' . $table, ARRAY_N);
                    $output .= "\n\n" . $exists . "\n\n" . $row2[1] . ";\n\n";
                    for ($i = 0; $i < count($result); $i++) {
                       $row = $result[$i];
                        $output .= 'INSERT INTO ' . $table . ' VALUES(';
                        for ($j = 0; $j < count($result[0]); $j++) {
                            $row[$j] = $wpdb->_real_escape($row[$j]);
                            $output .= (isset($row[$j])) ? '"' . $row[$j] . '"' : '""';
                            if ($j < (count($result[0]) - 1)) {
                                $output .= ',';
                            }
                        }
                        $output .= ");\n";
                    }
                    $output .= "\n";
                }
            }
        }
    }
    $output .= 'SET FOREIGN_KEY_CHECKS = 1;';
    $output = str_replace(preg_replace('{/$}', '', $old_URL), preg_replace('{/$}', '', $new_URL), $output);
    // replacing old prefix to new website prefix
    $output = str_replace(preg_replace('{/$}', '', $wpdb->prefix), preg_replace('{/$}', '', $destination_prefix), $output);
    if($one_multisite == "multi" && is_multisite()){
        $get_url_details = parse_url($new_URL);
        $get_path = rtrim($get_url_details['path'],"/");

        $get_sitedetails = get_site(1);
        $site_paths = rtrim($get_sitedetails->path,"/");
        // Replacing path and domain name
        $output = str_replace(preg_replace('{/$}', '', $site_paths.'/'), preg_replace('{/$}', '', $get_path.'/'), $output);
    }
    $databaseName = 'WPMC_Backup_'. time() .'.sql';
    $backup_file_name_with_path_to_save = $finalbackuppath.'/'.$databaseName;
    file_put_contents($backup_file_name_with_path_to_save, $output);
}

/*Extract zip file recursiverly */
function Unzip($dir, $bzipfilename, $destiny=""){
    $path_file = $bzipfilename;
    $zip = zip_open($path_file);
    $_tmp = array();
    $count=0;
    if ($zip)
    {
        $unzipping = '';
        while ($zip_entry = zip_read($zip))
        {
            $_tmp[$count]["filename"] = zip_entry_name($zip_entry);
            $_tmp[$count]["stored_filename"] = zip_entry_name($zip_entry);
            $_tmp[$count]["size"] = zip_entry_filesize($zip_entry);
            $_tmp[$count]["compressed_size"] = zip_entry_compressedsize($zip_entry);
            $_tmp[$count]["mtime"] = "";
            $_tmp[$count]["comment"] = "";
            $_tmp[$count]["folder"] = dirname(zip_entry_name($zip_entry));
            $_tmp[$count]["index"] = $count;
            $_tmp[$count]["status"] = "ok";
            $_tmp[$count]["method"] = zip_entry_compressionmethod($zip_entry);

            if (zip_entry_open($zip, $zip_entry, "r"))
            {
                $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                if($destiny)
                {
                    $path_file = str_replace("/",DIRECTORY_SEPARATOR, $destiny . zip_entry_name($zip_entry));
                }
                else
                {
                    $path_file = str_replace("/",DIRECTORY_SEPARATOR, $dir . zip_entry_name($zip_entry));
                }
                $new_dir = dirname($path_file);

                // Create Recursive Directory (if not exist)  
                if (!file_exists($new_dir)) {
                  mkdir($new_dir, 0777);
                }
                $fp = fopen($dir . zip_entry_name($zip_entry), "w");
                fwrite($fp, $buf);
                fclose($fp);

                zip_entry_close($zip_entry);
                $unzipping = $count;
            }
           
            $count++;
        }

        zip_close($zip);
        return $unzipping;
    }
}

function do_delete_themes(){
    global $wp_version;

    if (!function_exists('delete_theme')) {
      require_once ABSPATH . 'wp-admin/includes/theme.php';
    }

    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }

    $all_themes = wp_get_themes(array('errors' => null));

    foreach ($all_themes as $theme_slug => $theme_details) {
      $res = delete_theme($theme_slug);
    }
    delete_folder(ABSPATH . 'wp-content/themes/', array('.', '..'));
    return sizeof($all_themes);
}

function do_delete_plugins(){
    $keep_wp_reset = true;
    $silent_deactivate = false;
    if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }

    $wp_reset_basename = 'wp_media_cleaner/wp_media_cleaner.php';

    $all_plugins = get_plugins();
    $active_plugins = (array) get_option('active_plugins', array());
    if (true == $keep_wp_reset) {
      if (($key = array_search($wp_reset_basename, $active_plugins)) !== false) {
        unset($active_plugins[$key]);
      }
      unset($all_plugins[$wp_reset_basename]);
    }

    if (!empty($active_plugins)) {
      deactivate_plugins($active_plugins, $silent_deactivate, false);
    }

    if (!empty($all_plugins)) {
      delete_plugins(array_keys($all_plugins));
    }
    delete_folder(ABSPATH . 'wp-content/plugins/', array('.', '..','wp_media_cleaner'));

    return sizeof($all_plugins);
}

function do_delete_uploads(){
    $upload_dir = wp_get_upload_dir();
    $delete_count = 0;
    delete_folder($upload_dir['basedir'], array('.', '..'));
    return $delete_count;
} 

// Recursively deletes a folder
function delete_folder($folderPath, $exceptFolder){
  if (is_dir($folderPath)) {
    $removableFiles = array_diff(scandir($folderPath), $exceptFolder);
    foreach ($removableFiles as $removableFile) {
      if ($removableFile != "." && $removableFile != ".." && $removableFile != "wp_media_cleaner") {
        if (filetype($folderPath."/".$removableFile) == "dir"){
           delete_folder($folderPath."/".$removableFile,$exceptFolder);
           rmdir($folderPath."/".$removableFile); 
         }
        else{ unlink   ($folderPath."/".$removableFile); }
      }
    }
    reset($removableFiles);
  }
}

function custom_copy($src, $dst) {
    // open the source directory 
    $dir = opendir($src);
  
    // Make the destination directory if not exist
    @mkdir($dst);  
    // Loop through the files in source directory
    while( $file = readdir($dir) ) {
        if (strpos($file, '.zip') != true && strpos($file, 'wp_media_cleaner') != true && $file != "wp_media_cleaner"){
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ){
                    // Recursively calling custom copy function
                    // for sub directory
                    custom_copy($src . '/' . $file, $dst . '/' . $file);
                }
                else {
                    // echo $file."<br>";
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
    }
    closedir($dir);
    return 1;
}

function rrmdir($dir,$except_zip) {
    $dir = str_replace("\\", "/", realpath($dir));
    if($except_zip == "except_zip"){
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if(strpos($object, '.zip') != true && strpos($object, '.sql') != true){
                        if (filetype($dir."/".$object) == "dir") 
                            rrmdir($dir."/".$object,"except_zip"); 
                        else unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
            return 1;
        }
    }else{
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if($object != "sites"){
                        if (filetype($dir."/".$object) == "dir") 
                            rrmdir($dir."/".$object,"no_zip"); 
                        else unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
            return 1;
        }
    }
}

global $wpdb;
$prefixTable = $wpdb->get_blog_prefix();

if($action == 'importBackup'){
    if(isset($_FILES['importBackupFile']) && !empty($_FILES['importBackupFile'])){
        $no_files = count($_FILES["importBackupFile"]['name']);
        $mediaPath = WPMC_BACKUPFILE;
        for ($k = 0; $k < $no_files; $k++) {
            if ($_FILES["importBackupFile"]["error"][$k] > 0) {
                echo "Error: " . $_FILES["importBackupFile"]["error"][$k] . "<br>";
            } else {
                if (file_exists($mediaPath . $_FILES["importBackupFile"]["name"][$k])) {
                } else {
                    $fileNamewithEx = $_FILES["importBackupFile"]["name"][$k];
                    $fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNamewithEx);
                    $file = $mediaPath . '/' . $fileNamewithEx;
                    $fileSize = $_FILES["importBackupFile"]["size"][$k];
                    move_uploaded_file($_FILES["importBackupFile"]["tmp_name"][$k], $file);
                    $insertBackupData = array(
                        'title' => $fileName,
                        'content_title' => 'Imported',
                        'content_type' => 'Imported',
                        'size' => $fileSize,
                        'multisite_prefix' => $prefixTable
                    );
                    //$insertBackup = $wpdb->insert($BackupTable, $insertBackupData);
                }
            }
        }
        wp_redirect( get_site_url().'/wp-admin/admin.php?page=media-cleaner-backup' );
    }
}

if($action == 'RemoveBackupFile'){
    $filesNames = json_decode(stripslashes($_POST['files']));
    $FilePathDir = WPMC_BACKUPFILE;
    $response = array();
    foreach ($filesNames as $filesName) {
        $FilePath = $FilePathDir.'/'.$filesName;
        if(file_exists($FilePath)){
            unlink($FilePath);
            $response[] = $filesName;
        }
    }
    echo json_encode($response);
}

if($action == 'fullBackup'){
    if(!empty($_POST['old_URL']) || !empty($_POST['new_URL'])){
        $old_URL = $_POST['old_URL'];
        $new_URL = $_POST['new_URL'];
    }else{
        $old_URL = '';
        $new_URL = '';
    }
    $content_filter = $_POST['content_filter'];
    $one_multisite = $_POST['one_multisite'];
    $destination_prefix = $_POST['destination_prefix'];
    exportDatabase($old_URL, $new_URL, $destination_prefix, $one_multisite, $content_filter);
    $fbackup_file_name = "";
    $the_folder = ABSPATH.'wp-content/';
    if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
        mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
    }
    $zip_file_name = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/wpmcbackuparchive.zip';
    $tempbackup = zipme($the_folder, $zip_file_name,true);

    if($tempbackup == true){
        $fileswithdb = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/';
        $finalbackuppath = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup';
        $finalzipfilename = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/wpmcbackupfdbarchive.zip';
        $resf = zipme($fileswithdb, $finalzipfilename,false);

        if($resf === true){
            $fzipcontent = file_get_contents($finalzipfilename); 
               
            $fdata = base64_encode($fzipcontent); 
            $fstrdata = chunk_split($fdata, 4, ' ');
            $date = date('Ymd');
            if($one_multisite == "multi"){
                $fbackup_file_name = 'multisite-full_backup'.$date.'_'.time().'.wpmc';
            }else{
                $fbackup_file_name = 'full_backup'.$date.'_'.time().'.wpmc';
            }
            $fbackup_file_name_with_path_to_save = $finalbackuppath.'/'.$fbackup_file_name;

            $ffp = fopen($fbackup_file_name_with_path_to_save, 'w');
            fwrite($ffp,$fstrdata);
            fclose($ffp);
            delete_files(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/');
            unlink($finalzipfilename);
        }
    }
    echo $fbackup_file_name;
}

if($action == 'restoreBackupFile'){
    $backupFileName = json_decode(stripslashes($_POST['backupfile']));
    $backupFileName = $backupFileName[0];
    // Validate the file by multisite or single site
    if (strpos($backupFileName, "multisite") !== false){
        $multi_back = 1;
    }else{
        $multi_back = 0;
    }
    if(is_multisite()){
        $is_multi = 1;
    }else{
        $is_multi = 0;
    }
    if($multi_back == 1 && $is_multi == 0){
        $response = array(
            "status" => "error",
            "message" => __("The backup you have is for multisite and this website is not multisite.","wp_media_cleaner")
        );
        echo json_encode($response);
        exit;
    }elseif($multi_back == 0 && $is_multi == 1){
        $response = array(
            "status" => "error",
            "message" => __("The backup you have is for single site and this website is multisite.","wp_media_cleaner")
        );
        echo json_encode($response);
        exit;
    }
    $backupFilePath = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/'.$backupFileName;
    if (file_exists($backupFilePath)){ 
        $backupfilecontent = file_get_contents($backupFilePath); 
        $stripped = preg_replace('/\s/', '', $backupfilecontent);
        $resp = file_put_contents(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/wpmcbackup.zip', base64_decode($stripped));
        $zipfilename = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/wpmcbackup.zip';

        $zipn = new ZipArchive;
        $zipex = $zipn->open($zipfilename);
        if ($zipex === TRUE) {
            $zipn->extractTo(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/');
            $zipn->close();
            unlink($zipfilename);
            do_delete_plugins();
            do_delete_uploads();
            do_delete_themes();
            
            $bzipfiles = array();
            foreach (glob(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/*.zip') as $zfile) {
              $bzipfiles[] = $zfile;
            }
            $bzipfilename = $bzipfiles[0];
            $dir = ABSPATH.'wp-content/';
            $unzipping = Unzip($dir, $bzipfilename);

            if($unzipping){
                $response = array(
                    "status" => "success",
                    "message" => __("Files unzipped successfully. Now database will be replaced.","wp_media_cleaner")
                );
                
            }else{
                $response = array(
                    "status" => "error",
                    "message" => __("Error !! Problem unzipping file on the server. Please provide file permission to plugin directory","wp_media_cleaner")
                );
            }            
            
        } else {
            $response = array(
                "status" => "error",
                "message" => __("Error !! Cannot restore backup file corrupted","wp_media_cleaner")
            );
        }
    }else{
        $response = array(
            "status" => "error",
            "message" => __("Backup file does not exists in directory","wp_media_cleaner")
        );
    }
    echo json_encode($response);
    exit;
}

if($action == 'restoreDatabase'){
    // Firstly delete all the tables in the database for restoring new database tables
    $table_main = $wpdb->get_col('SHOW TABLES');
    foreach ($table_main as $key => $value) {
        if(!strpos($value, 'usermeta') !== false){
            $wpdb->query("DROP TABLE IF EXISTS {$value}");
        }else{
            $wpdb->query("DELETE FROM {$value} WHERE meta_key NOT IN ('session_tokens')");
        }
    }
    // Restore the database to the new
    $files = array();
    foreach (glob(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/*.sql') as $file) {
      $files[] = $file;
    }
    $filename = $files[0];
    $templine = '';
    $lines = file($filename);
    foreach ($lines as $line){
        if (substr($line, 0, 2) == '--' || $line == '')
        continue;
        $templine .= $line;
        if (substr(trim($line), -1, 1) == ';'){
            $wpdb->get_results($templine);
            $templine = '';
        }
    }
    // Remove temporary backup folder
    $delete_temp_folder = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp';
    $del = rrmdir($delete_temp_folder,"no_zip");

    $response = array(
        "status" => "success",
        "message" => __("Successfully Restored","wp_media_cleaner")
    );
    echo json_encode($response);
    exit;
}

if($action == 'multisiteBackup'){
    global $wpdb;

    $content_filter = $_POST['content_filter'];
    $one_multisite = $_POST['one_multisite'];
    $destination_prefix = $_POST['destination_prefix'];    
    // Main site prefix
    $main_prefix = $wpdb->prefix;    
    if(!empty($_POST['old_URL']) || !empty($_POST['new_URL'])){
        $old_URL = rtrim($_POST['old_URL'],"/");
        $new_URL = rtrim($_POST['new_URL'],"/");
    }else{
        $old_URL = '';
        $new_URL = '';
    }
    $siteids = $_POST['siteids'];
    $counts = 0;
    $inccs = 2;
    $newSiteArr = array();
    foreach ($siteids as $sitevalue) {
        $newSiteArr[$counts]['chosen_site'] = $sitevalue;
        $newSiteArr[$counts]['updated_site'] = $inccs;
        $inccs++;
        $counts++;        
    }

    $number_of_site = count($siteids);
    if(is_multisite()){
        $is_multisite = get_sites();
        $total_sites = count($is_multisite);
    }

    // echo "<pre>"; print_r($newSiteArr);
    

    $counter = 1;
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $multi_site_prefixes = array();

    // Check if user choose multisite and haven't choosen the main site
    if($one_multisite == "multi" && !in_array("1", $siteids)){
        $multi_site_prefixes = array(
            array("prefix" => $main_prefix, "site_url" => site_url())
        );
    }
    foreach ($siteids as $site_value) {
        $get_site = get_site($site_value);
        $domain = $get_site->domain;
        $path = $get_site->path;
        $multisite_url = $protocol.$domain.$path;
        $multi_site_prefixes[$counter]['prefix'] = $wpdb->get_blog_prefix($site_value);
        $multi_site_prefixes[$counter]['site_url'] = $multisite_url;
        $counter++;
    }
    exportMultisiteDatabase($multi_site_prefixes, $old_URL, $new_URL, $one_multisite, $destination_prefix, $newSiteArr);
    // die();
    if($number_of_site == 1 && $one_multisite == "single"){
        $qwerty = $multi_site_prefixes[1]['prefix'];
        preg_match_all('!\d+!', $qwerty, $matches);
        $site_number = $matches[0][0];

        if(!empty($site_number)){
            $fbackup_file_name = "";
            $the_folder = ABSPATH.'wp-content/';
            if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
                mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
            }
            if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single')) {
                mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single', 0777, true);
            }
            $forPaste = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/';
            $zip_file_name = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/wpmcbackuparchive.zip';
            $is_done = custom_copy($the_folder, $forPaste);
            if($is_done == 1){
                $get_multisite_upload = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/uploads/sites/'.$site_number.'/';
                $to_delete = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/uploads/';
                $delete_uploads = rrmdir($to_delete, "no_zip");
                if($delete_uploads == 1){
                    $multi_uploads_restore = custom_copy($get_multisite_upload, $to_delete);
                    if($multi_uploads_restore == 1){
                        $delete_sites = rrmdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/uploads/sites/',"no_zip");
                        if($delete_sites == 1){
                            $tempbackup = zipmeMultisite($forPaste, $zip_file_name,true);
                            if($tempbackup){
                                $delete_content = rrmdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/',"except_zip");
                            }
                        }
                    }
                }
            }
        }else{
            // When select single site from multisite and the single site is 1st (wp_)
            $fbackup_file_name = "";
            $the_folder = ABSPATH.'wp-content/';
            if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
                mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
            }
            $zip_file_name = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/wpmcbackuparchive.zip';
            $tempbackup = zipmeNoSites($the_folder, $zip_file_name,true);
        }
    }else{
        if($number_of_site != $total_sites){
            $fbackup_file_name = "";
            $the_folder = ABSPATH.'wp-content/';
            if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
                mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
            }
            if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single')) {
                mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single', 0777, true);
            }
            $forPaste = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/';
            $zip_file_name = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/wpmcbackuparchive.zip';
            $is_done = custom_copy($the_folder, $forPaste);
            if($is_done == 1){
                foreach ($newSiteArr as $siteValues) {
                    if($siteValues['chosen_site'] != $siteValues['updated_site']){
                        $siteFrom = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/uploads/sites/'.$siteValues['chosen_site'].'/';
                        $siteTo = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multi_single/uploads/sites/'.$siteValues['updated_site'].'/';
                        $delete_uploads = rrmdir($siteTo, "no_zip");
                        if($delete_uploads){
                            $multi_uploads_restore = custom_copy($siteFrom, $siteTo);                       
                        }
                    }else{
                        $multi_uploads_restore = '1';
                    }
                    
                }
                if($multi_uploads_restore){
                    $tempbackup = zipmeMultisite($forPaste, $zip_file_name,true);
                    if($tempbackup){
                        $delete_content = rrmdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/',"except_zip");
                    }
                }
            }
        }else{
            $fbackup_file_name = "";
            $the_folder = ABSPATH.'wp-content/';
            if (!file_exists(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp')) {
                mkdir(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp', 0777, true);
            }
            $zip_file_name = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/wpmcbackuparchive.zip';
            $tempbackup = zipme($the_folder, $zip_file_name,true);
        }
    }
    // Create multisite file if select multisite
    if($one_multisite == "multi"){
        $file_source = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/multisite.txt';
        $multi_file = fopen($file_source, "w") or die("Unable to open file!");
        $txt = '<a site_id='.max($siteids).'>';
        fwrite($multi_file, $txt);
        fclose($multi_file);
    }

    if($tempbackup == true){
        $fileswithdb = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/';
        $finalbackuppath = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup';
        $finalzipfilename = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/wpmcbackupfdbarchive.zip';
        $resf = zipme($fileswithdb, $finalzipfilename,false);

        if($resf === true){
            $fzipcontent = file_get_contents($finalzipfilename); 
               
            $fdata = base64_encode($fzipcontent); 
            $fstrdata = chunk_split($fdata, 4, ' ');
            $date = date('Ymd');
            if($number_of_site == 1 && $one_multisite == "single"){
                $fbackup_file_name = 'full_backup'.$date.'_'.time().'.wpmc';
            }else{
                $fbackup_file_name = 'multisite-full_backup'.$date.'_'.time().'.wpmc';
            }
            $fbackup_file_name_with_path_to_save = $finalbackuppath.'/'.$fbackup_file_name;

            $ffp = fopen($fbackup_file_name_with_path_to_save, 'w');
            fwrite($ffp,$fstrdata);
            fclose($ffp);
            delete_files(ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/backuptemp/');
            unlink($finalzipfilename);
        }
    }    
    echo $fbackup_file_name;
}