<div class="support_form_block">
  <div class="support_form_detail">
    <p>
      <?php echo __('Did you found a bug or do you have a request we can implement?','wp_media_cleaner'); ?>
    <br />
      <?php echo __('Please let us know and we will be happy to help.','wp_media_cleaner'); ?>
    </p>
  </div>
  <p>
    <?php echo __('You can let us know by filling in the form or email us at support@wp-media-cleaner.com' ,'wp_media_cleaner'); ?>
  </p>
  <form id="WPSupportForm" method="post" class="support_form">
    <div class="form-group">
      <label for="name"><?php echo __('Name','wp_media_cleaner')?></label>
      <input class="form-control border_none" type="text" id="user_name" name="user_name">
    </div>
    <div class="form-group">
      <label for="email"><?php echo __('Email','wp_media_cleaner')?></label>
      <input class="form-control border_none"  type="email" id="user_email" name="user_email">
    </div>
    <div class="form-group">
      <label for="subject"><?php echo __('Subject','wp_media_cleaner')?></label>
      <input class="form-control border_none"  type="text" id="user_subject" name="user_subject">
    </div>
    <div class="form-group">
      <label for="license"><?php echo __('License','wp_media_cleaner')?></label>
      <input class="form-control border_none"  type="text" id="user_license" name="user_license">
    </div>
    <div class="form-group">
      <label for="message"><?php echo __('Message','wp_media_cleaner')?></label>
      <textarea class="form-control border_none"  name="user_message" id="user_message"></textarea>
    </div>
    <div class="form-group">
    <label>&nbsp;</label>
      <input type="submit" name="support" value="Submit">
    </div>
  </form>
</div>
