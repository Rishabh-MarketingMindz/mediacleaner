<?php
/*error_reporting(1);
@ini_set('display_errors', 1);*/
require("../../../../../wp-load.php");
require_once("../../../../../wp-admin/includes/plugin.php");

if(!@$action = $_POST['action']){
  $action = $_GET['action'];
}else{
  $action = $_POST['action'];
}



global $wpdb;
$prefixTable = $wpdb->get_blog_prefix();
$PostTable = $prefixTable.'posts';
$PostmetaTable = $prefixTable.'postmeta';
$CommentsTable = $prefixTable.'comments';
$CommentmetaTable = $prefixTable.'commentmeta';
$TermsTable = $prefixTable.'terms';
$TermmetaTable = $prefixTable.'termmeta';
$UsersTable = $prefixTable.'users';
$UsermetaTable = $prefixTable.'usermeta';
$OptionsTable = $prefixTable.'options';

//$action = 'ResetWithPackage';

if($action == 'TabelsReindex'){
    $table = '';
    $like = $wpdb->prefix . $table . '%';
    $tables = $wpdb->get_results($wpdb->prepare("SELECT TABLE_NAME, ENGINE FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE %s", $like));
        foreach ($tables as $table){
            $table_name = esc_sql($table->TABLE_NAME);
            switch ($table->ENGINE){
                case 'MyISAM':
                $wpdb->query("REPAIR TABLE {$table_name}");
                break;
                case 'InnoDB':
                $wpdb->query("ALTER TABLE {$table_name} ENGINE = InnoDB");
                break;
            }
        }
    $dbtablesdata = $wpdb->get_results("show tables from ".$wpdb->dbname);
    if(!empty($dbtablesdata)){
        $columnstable  = array();
        $alloweddbtype =  array('varchar','bigint','int','tinyint','float');
        foreach ($dbtablesdata as $value) {
            $new = 'Tables_in_'.$wpdb->dbname;
            $tablename = $value->{$new};
            $tablecolumnames = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$tablename."' AND table_schema = '".$wpdb->dbname."' ORDER BY ORDINAL_POSITION");
            foreach ($tablecolumnames as $key => $colvalue) {
                $shores = $wpdb->get_results("SHOW INDEX FROM ".$tablename." WHERE column_name = '".$colvalue->COLUMN_NAME."'");
                if(empty($shores)){
                    $columntype = $wpdb->get_results("SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$tablename."' AND COLUMN_NAME = '".$colvalue->COLUMN_NAME."' AND table_schema = '".$wpdb->dbname."'");
                    if(in_array($columntype[0]->DATA_TYPE, $alloweddbtype)){
                        $wpdb->get_results("CREATE INDEX ".$colvalue->COLUMN_NAME." ON ".$tablename." (".$colvalue->COLUMN_NAME.")");
                    }
                }
            }
        }
    }
    echo __('All Tables Reindex Successfully');
}

if($action == 'TabelsOptimize'){
  $tables = $wpdb->get_col("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE '$prefixTable%'");
  $tables = implode( ',', $tables );
  $wpdb->query("OPTIMIZE TABLE {$tables}");
  echo __('All Talbes Optimize Successfully');
}

if($action == 'ExprTransientClear'){
  $options = $wpdb->get_col("SELECT option_name FROM $OptionsTable WHERE option_name LIKE '_transient_timeout_%' AND option_value < NOW()");
    foreach ($options as $option) {
    if ( strpos( $option, '_site_transient_' ) !== false ) {
      delete_site_transient( str_replace( '_site_transient_timeout_', '', $option ) );
    } else {
      delete_transient( str_replace( '_transient_timeout_', '', $option ) );
    }
    }
  echo __('All Expired Transients Deleted Successfully');
}

if($action == 'RevisionsDelete'){
  $posts = $wpdb->get_col("SELECT ID FROM $PostTable WHERE `post_type` LIKE 'revision'");
    foreach ($posts as $id) {
          wp_delete_post_revision($id);
    }
  echo __('All Revision Data Deleted Successfully');
}

if($action == 'TrashedPostsDelete'){
  $posts = $wpdb->get_col("SELECT ID FROM $PostTable WHERE `post_status` LIKE 'trash'");
    foreach ($posts as $id) {
          wp_delete_post($id, true);
    }
  echo __('All Trashed Post Data Deleted Successfully');
}

if($action == 'OrphanPostmetaDelete'){
  $postmeta = $wpdb->get_results("SELECT meta_id, post_id, meta_key FROM $PostmetaTable WHERE post_id NOT IN (SELECT ID FROM $PostTable)");
    foreach ($postmeta as $meta) {
    if (empty($meta->post_id)) {
      $wpdb->query("DELETE FROM $PostmetaTable WHERE meta_id = {$meta->meta_id}");
    } else {
      delete_post_meta( $meta->post_id, $meta->meta_key );
    }
    }
  echo __('All Orphan Postmeta Data Deleted Successfully');
}

if($action == 'OrphanAttachmentsDelete'){
    global $wpdb;
    $orphanattachment = array();
    $attachmentdata = $wpdb->get_results("select * from ".$prefixTable."posts where post_type='attachment'");
    if(!empty($attachmentdata)){
        foreach ($attachmentdata as $key => $value) {
            $media_id = $value->ID;
            $media_url = $value->guid;
            $media_name = substr($media_url, strrpos($media_url, '/') + 1);
            $attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'media_url'=>$media_url);
        }
    }
    function walkDir($dir) {
        global $return;
        $dh = new DirectoryIterator($dir);   
        // Dirctary object
        foreach ($dh as $item) {
            if (!$item->isDot()) {
                if ($item->isDir()) {
                    if($item !="wp_media_cleaner"){
                        walkDir("$dir/$item");
                    }
                } else {
                    if($item->isFile() && preg_match("/(\.gif|\.png|\.jpe?g)$/", $item->getFilename())){
                        $fullpath = $dir . "/" . $item->getFilename();
                        $filename = $item->getFilename();
                        $type= $item->getExtension();
                        $return[] = array('path'=>$fullpath, 'filename'=>$filename,'Type'=>$type);
                    }
                }
            }
        }
        return $return;
    }
    $upload_directory_path =  ABSPATH.'wp-content/uploads/';
    $response = walkDir($upload_directory_path);
    @$filesindir =array_column($response, 'filename');
    if(!empty($attachmentarray)){
        foreach ($attachmentarray as $value){
          if($filesindir != null){
            if(!in_array($value['media_name'], $filesindir) ){
                $orphanattachment[] = $value;
            }
          }
        }
    }
    $attachments = $wpdb->get_col("SELECT ID FROM $PostTable WHERE post_parent NOT IN (SELECT ID FROM $PostTable) AND post_parent != 0 AND post_type = 'attachment'");
    $orphanatch = array_column($orphanattachment, 'media_id');
    $finalorphanatch = array_merge($attachments, $orphanatch);
    $finalorphanatch = array_unique($finalorphanatch);
    
    foreach ($finalorphanatch as $attachment) {
        wp_delete_attachment($attachment, true);
    }
    echo __('All Orphan Attachments Deleted Successfully');
}

if($action == 'DuplicatedPostmetaDelete'){
  $_postmeta = array();
    $postmeta = $wpdb->get_results("SELECT pm.meta_id AS meta_id, pm.post_id AS post_id FROM $PostmetaTable pm INNER JOIN (SELECT post_id, meta_key, meta_value, COUNT(*) FROM $PostmetaTable GROUP BY post_id, meta_key, meta_value HAVING COUNT(*) > 1) pm2 ON pm.post_id = pm2.post_id AND pm.meta_key = pm2.meta_key AND pm.meta_value = pm2.meta_value WHERE pm.meta_key NOT IN ('_price', '_used_by')");
    foreach($postmeta as $meta){
    $_postmeta[$meta->post_id][] = $meta->meta_id;
    }
    foreach ($_postmeta as $post_id => $meta_ids) {
    array_pop($meta_ids);
    $wpdb->query("DELETE FROM $PostmetaTable WHERE meta_id IN (" . implode( ',', $meta_ids ) . ")");
    }
  echo __('All Duplicate Postmeta Deleted Successfully');
}

if($action == 'SpamCommentsDelete'){
  $comments = $wpdb->get_col("SELECT comment_ID FROM $CommentsTable WHERE comment_approved = 'spam'");
  foreach ( $comments as $comment ) {
    wp_delete_comment($comment, true );
  }
  echo __('All Spam Comments Deleted Successfully');
}

if($action == 'TrashedCommentsDelete'){
  $comments = $wpdb->get_col("SELECT comment_ID FROM $CommentsTable WHERE comment_approved = 'trash'");
    foreach ( $comments as $comment ) {
    wp_delete_comment($comment, true );
  }
  echo __('All Trash Comments Deleted Successfully');
}

if($action == 'OrphanCommentmetaDelete'){
  $commentmeta = $wpdb->get_results("SELECT meta_id, comment_id, meta_key FROM $CommentmetaTable WHERE comment_id NOT IN (SELECT comment_ID FROM $CommentsTable)");
    foreach ($commentmeta as $meta) {
    if (empty($meta->comment_id)) {
      $wpdb->query("DELETE FROM $CommentmetaTable WHERE meta_id = {$meta->meta_id}");
    } else {
      delete_comment_meta( $meta->comment_id, $meta->meta_key );
    }
    }
  echo __('All Orphan Comments Deleted Successfully');
}

if($action == 'DuplicatedCommentmetaDelete'){
  $_commentmeta = array();
    $commentmeta = $wpdb->get_results("SELECT cm.meta_id AS meta_id, cm.comment_id AS comment_id FROM $CommentmetaTable cm INNER JOIN (SELECT comment_id, meta_key, meta_value, COUNT(*) FROM $CommentmetaTable GROUP BY comment_id, meta_key, meta_value HAVING COUNT(*) > 1) cm2 ON cm.comment_id = cm2.comment_id AND cm.meta_key = cm2.meta_key AND cm.meta_value = cm2.meta_value");
    foreach($commentmeta as $meta){
    $_commentmeta[$meta->comment_id][] = $meta->meta_id;
    }
    foreach ($_commentmeta as $comment_id => $meta_ids) {
    array_pop($meta_ids);
    $wpdb->query("DELETE FROM $CommentmetaTable WHERE meta_id IN (" . implode( ',', $meta_ids ) . ")");
    }
  echo __('All Duplicate Commentmeta Deleted Successfully');
}

if($action == 'OrphanTermmetaDelete'){
  $termmeta = $wpdb->get_results("SELECT meta_id, term_id, meta_key FROM $TermmetaTable WHERE term_id NOT IN (SELECT term_id FROM $TermsTable)");
    foreach ($termmeta as $meta) {
    if (empty($meta->term_id)) {
            $wpdb->query("DELETE FROM $TermmetaTable WHERE meta_id = {$meta->meta_id}");
    } else {
            delete_term_meta( $meta->term_id, $meta->meta_key );
    }
    }
  echo __('All Orphan Termmeta Deleted Successfully');
}

if($action == 'OrphanUsermetaDelete'){
  $usermeta = $wpdb->get_results("SELECT umeta_id, user_id, meta_key FROM $UsermetaTable WHERE user_id NOT IN (SELECT ID FROM $UsersTable)");
    foreach ($usermeta as $meta) {
    if (empty($meta->user_id)) {
        $wpdb->query("DELETE FROM $UsermetaTable WHERE meta_id = {$meta->umeta_id}");
    } else {
        delete_user_meta( $meta->user_id, $meta->meta_key );
    }
    }
  echo __('All Orphan Usermeta Deleted Successfully');
}

if($action == 'DuplicatedUsermetaDelete'){
  $_usermeta = array();
    $usermeta = $wpdb->get_results("SELECT um.umeta_id AS umeta_id, um.user_id AS user_id FROM $UsermetaTable um INNER JOIN (SELECT user_id, meta_key, meta_value, COUNT(*) FROM $UsermetaTable GROUP BY user_id, meta_key, meta_value HAVING COUNT(*) > 1) um2 ON um.user_id = um2.user_id AND um.meta_key = um2.meta_key AND um.meta_value = um2.meta_value");
    foreach($usermeta as $meta){
        $_usermeta[$meta->user_id][] = $meta->umeta_id;
    }

    foreach ($_usermeta as $user_id => $meta_ids) {
        array_pop($meta_ids);
    $wpdb->query("DELETE FROM $UsermetaTable WHERE umeta_id IN (" . implode( ',', $meta_ids ) . ")");
    }
  echo __('All Duplicated Usermeta Deleted Successfully');
}

/* Cron Job Scheduling */

function add_cron($schedule, $scheduletime, $hookname){
    if ($scheduletime) {
      $next_run = strtotime($scheduletime);
    } else {
      $next_run = time();
    }
    if (! wp_next_scheduled ( $hookname )) {
        // start new schedule
        wp_schedule_event($next_run, $schedule, $hookname);
    }
    else {
        // delete previous running schedule
        wp_clear_scheduled_hook( $hookname );
        // start new schedule
        wp_schedule_event($next_run, $schedule, $hookname);
    }
}


if($action == 'ExprTransientSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  //add_cron('every_minute', $action);
  echo __('Expired Transients Scheduled Successfully');
}
if($action == 'ScheduleAll'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Autoload Scheduled All Successfully');
}
if($action == 'RevisionsSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Revision Scheduled Successfully');
}
if($action == 'TrashedPostsSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Trash Posts Scheduled Successfully');
}
if($action == 'OrphanPostmetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Orphan Postmeta Scheduled Successfully');
}
if($action == 'OrphanAttachmentsSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Orphan Attachments Scheduled Successfully');
}
if($action == 'DuplicatedPostmetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Duplicate Postmeta Scheduled Successfully');
}
if($action == 'SpamCommentsSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Spam Comments Scheduled Successfully');
}
if($action == 'TrashedCommentsSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Trashed Comments Scheduled Successfully');
}
if($action == 'OrphanCommentmetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Orphan Commentmeta Scheduled Successfully');
}
if($action == 'DuplicatedCommentmetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Duplicated Commentmeta Scheduled Successfully');
}
if($action == 'OrphanTermmetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Orphan Termmeta Scheduled Successfully');
}
if($action == 'OrphanUsermetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Orphan Usermeta Scheduled Successfully');
}
if($action == 'DuplicatedUsermetaSchedule'){
  add_cron($_POST['ScheduleInterval'], $_POST['ScheduleIntervalTime'], $action);
  echo __('Duplicated Usermeta Scheduled Successfully');
}


if($action == 'avtiveInactive'){
  $pluginPath = $_POST['pluginPath'];
  if(is_plugin_active( $pluginPath )){
    deactivate_plugins($pluginPath);
    echo 'Plugin Deactivated Successfully';
  }else{
    $is_active = activate_plugin($pluginPath);
    if(!$is_active){
      echo 'Plugin Activated Successfully';
    }else{
      echo 'Error occour while activation!';
    }
  }
}

//Delete plugin files
if($action == 'DeletePlugin'){
  if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }
  $pluginkey = array($_POST['pluginkey']);
    if(!empty($pluginkey)) {
        deactivate_plugins($pluginkey, true, false);
      $deletestatus = delete_plugins($pluginkey);
    }
    if($deletestatus){
      echo "true";
    }else{
      echo "false";
    }
    exit;
}

//DROP plugin tables from database
if($action == 'ClearDatabase'){
  if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }
  $pluginname = trim($_POST['pluginname']);
  $pluginkey = array($_POST['pluginkey']);
  $pdresult = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."plugin_data WHERE plugin_name = '$pluginname'");
  $statuscheckarr = array();
  $resultf  = "";
  if(!empty($pdresult)) {
    deactivate_plugins($pluginkey, true, false);
        foreach ($pdresult as $value) {
          $wpdb->get_results("DROP TABLE $value->plugin_tables");
        }
        $tablena = $wpdb->prefix."plugin_data";
        $resultf = $wpdb->delete( $tablena, array( 'plugin_name' => $pluginname ) );
  }
    if($resultf){
      echo "true";
    }else{
      echo "false";
    }
    exit;
}

if($action == 'HardReset'){
  if(is_multisite()){
    multisiteTruncateTbl();
    do_delete_plugins();
    do_delete_themes();
    do_delete_uploads();
  }else{
    hardReset();
  }
}

if($action == 'ResetWithPackage'){
  if(is_multisite()){
    multisiteTruncateTbl();
    do_delete_plugins();
    do_delete_themes();
    do_delete_uploads();
    packageRestore();
  }else{
    hardReset();
    packageRestore();
  }
}

function hardReset(){
  global $current_user, $wpdb;
      
  if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }
    // Check if request is from admin Privileges
    if (!current_user_can('administrator')) {
      return false;
    }
    // Check function is included
    if (!function_exists('wp_install')) {
      require ABSPATH . '/wp-admin/includes/upgrade.php';
    }
    // save values that need to be restored after reset
    // todo: use params to determine what gets restored after reset
    $blogname = get_option('blogname');
    $blog_public = get_option('blog_public');
    $wplang = get_option('wplang');
    $siteurl = get_option('siteurl');
    $home = get_option('home');
   
    $active_plugins = get_option('active_plugins');
    $active_theme = wp_get_theme();
    do_delete_plugins();
      
    // Delete tables with prefix will not delete any custom table and plugin table.
    $prefix = str_replace('_', '\_', $wpdb->base_prefix);
    $tables = $wpdb->get_col("SHOW TABLES");
    if(!empty($tables)){
      foreach ($tables as $table) {
        /*if($table == $plugindatatable || $table == $contentscantable || $table == $mediascantable){
          continue;
        }*/
        $table = trim($table);
          $query = $wpdb->query("DROP TABLE $table");
        }
    }
    
    do_delete_uploads();
    do_delete_themes();
    
    // supress errors for WP_CLI
    // todo: find a better way to supress errors and send/not send email on reset
    $result = @wp_install($blogname, $current_user->user_login, $current_user->user_email, $blog_public, '', md5(rand()), $wplang);
    $user_id = $result['user_id'];

    // restore user pass
    $query = $wpdb->prepare("UPDATE {".$wpdb->base_prefix."users} SET user_pass = %s, user_activation_key = '' WHERE ID = %d LIMIT 1", array($current_user->user_pass, $user_id));
    $wpdb->query($query);

    // restore rest of the settings including WP Reset's
    update_option('siteurl', $siteurl);
    update_option('home', $home);
    // remove password nag
    if (get_user_meta($user_id, 'default_password_nag')) {
      update_user_meta($user_id, 'default_password_nag', false);
    }
    if (get_user_meta($user_id, $wpdb->prefix . 'default_password_nag')) {
      update_user_meta($user_id, $wpdb->prefix . 'default_password_nag', false);
    }

    
    // switch_theme($active_theme->get_stylesheet());
    activate_plugin('wp_media_cleaner/wp_media_cleaner.php');
    
    // Reactivate all plugins
   /* if (!empty($active_plugins)) {
      foreach ($active_plugins as $plugin_file) {
        activate_plugin($plugin_file);
      }
    }*/

    //Logout and Login the old user
    //Since the password doesn't change this is unnecessary
  //wp_clear_auth_cookie();

  wp_set_auth_cookie($user_id);
  return "success";
}

function do_delete_themes(){
    global $wp_version;

    if (!function_exists('delete_theme')) {
      require_once ABSPATH . 'wp-admin/includes/theme.php';
    }

    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }

    $all_themes = wp_get_themes(array('errors' => null));

    foreach ($all_themes as $theme_slug => $theme_details) {
      $res = delete_theme($theme_slug);
    }
    delete_folder(ABSPATH . 'wp-content/themes/', array('.', '..'));
    return sizeof($all_themes);
}

function do_delete_plugins(){
  $keep_wp_reset = true;
  $silent_deactivate = false;
    if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if (!function_exists('request_filesystem_credentials')) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
    }

    $wp_reset_basename = 'wp_media_cleaner/wp_media_cleaner.php';

    $all_plugins = get_plugins();
    $active_plugins = (array) get_option('active_plugins', array());
    if (true == $keep_wp_reset) {
      if (($key = array_search($wp_reset_basename, $active_plugins)) !== false) {
        unset($active_plugins[$key]);
      }
      unset($all_plugins[$wp_reset_basename]);
    }

    if (!empty($active_plugins)) {
      deactivate_plugins($active_plugins, $silent_deactivate, false);
    }

    if (!empty($all_plugins)) {
      delete_plugins(array_keys($all_plugins));
    }
    delete_folder(ABSPATH . 'wp-content/plugins/', array('.', '..','wp_media_cleaner'));

    return sizeof($all_plugins);
}


function do_delete_uploads(){
    $upload_dir = wp_get_upload_dir();
    $delete_count = 0;
    delete_folder($upload_dir['basedir'], array('.', '..'));
    return $delete_count;
} 


// Recursively deletes a folder
function delete_folder($folderPath, $exceptFolder){
  if (is_dir($folderPath)) {
    $removableFiles = array_diff(scandir($folderPath), $exceptFolder);
    foreach ($removableFiles as $removableFile) {
      if ($removableFile != "." && $removableFile != ".." && $removableFile != "wp_media_cleaner") {
        if (filetype($folderPath."/".$removableFile) == "dir"){
           delete_folder($folderPath."/".$removableFile,$exceptFolder);
           rmdir($folderPath."/".$removableFile); 
         }
        else{ unlink   ($folderPath."/".$removableFile); }
      }
    }
    reset($removableFiles);
  }
}

/* Multisite Truncate Table */
function multisiteTruncateTbl(){
  global $wpdb;
  $exceptTableRm = array($wpdb->base_prefix.'blogmeta',$wpdb->base_prefix.'blogs',$wpdb->base_prefix.'blog_versions',$wpdb->base_prefix.'registration_log',$wpdb->base_prefix.'signups',$wpdb->base_prefix.'site',$wpdb->base_prefix.'sitemeta',$wpdb->base_prefix.'users',$wpdb->base_prefix.'usermeta');
  $allOptionsTbls = $wpdb->get_results("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE '%options'");
  $defaultwpTbls = array('commentmeta','comments','links','options','postmeta','posts','termmeta','terms','term_relationships','term_taxonomy','plugin_data','mediascan_data','contentscan_data');
  $defaultTbls = array_merge($exceptTableRm,$defaultwpTbls);
  foreach ($allOptionsTbls as $allOptionsTbl) {
    array_push($exceptTableRm, $allOptionsTbl->TABLE_NAME);
    $wpdb->get_results("DELETE FROM ".$allOptionsTbl->TABLE_NAME." WHERE `option_name` LIKE '%transient%'");
  }
  $totalTables = $wpdb->get_results("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME LIKE '%'");
  foreach ($totalTables as $totalTable) {
    if(!in_array($totalTable->TABLE_NAME, $exceptTableRm)){
      $wpdb->query("TRUNCATE TABLE ".$totalTable->TABLE_NAME);
    }
    foreach ($defaultTbls as $defaultTbl) {
      if(!strpos($totalTable->TABLE_NAME, $defaultTbl !== FALSE)){
        $wpdb->query("DROP TABLE ".$totalTable->TABLE_NAME);
        return true;
      }
    }
  }
  $getUsers = $wpdb->get_results("SELECT user_id FROM ".$wpdb->base_prefix."usermeta WHERE meta_key like '%_capabilities' and meta_value NOT like '%administrator%'");
  foreach ($getUsers as $getUser) {
    $userId = $getUser->user_id;
    require_once("../../../../../wp-admin/includes/user.php");
    wp_delete_user($userId);
    $wpdb->get_results("DELETE FROM ".$wpdb->base_prefix."users WHERE ID=$userId");
    $wpdb->get_results("DELETE FROM ".$wpdb->base_prefix."usermeta WHERE user_id=$userId");
  }
}

/* Package Restore */
function packageRestore(){
  global $wpdb;
  if(isset($_FILES['plugins']) && !empty($_FILES['plugins'])){
    $no_plugins = count($_FILES["plugins"]['name']);
    $pluginPath = ABSPATH . 'wp-content/plugins/';
    for ($i = 0; $i < $no_plugins; $i++) {
        if ($_FILES["plugins"]["error"][$i] > 0) {
            echo "Error: " . $_FILES["plugins"]["error"][$i] . "<br>";
        } else {
            if (file_exists($pluginPath . $_FILES["plugins"]["name"][$i])) {
            } else {
                copy($_FILES["plugins"]["tmp_name"][$i], $pluginPath . $_FILES["plugins"]["name"][$i]);
                $zip = new ZipArchive;
                $res = $zip->open($pluginPath . $_FILES["plugins"]["name"][$i]);
                if ($res === TRUE) {
                  $zip->extractTo($pluginPath);
                  $zip->close();
                  unlink($pluginPath . $_FILES["plugins"]["name"][$i]);
                }
            }
        }
    }
  }

  if(isset($_FILES['themes']) && !empty($_FILES['themes'])){
    $no_themes = count($_FILES["themes"]['name']);
    $themesPath = ABSPATH . 'wp-content/themes/';
    for ($j = 0; $j < $no_themes; $j++) {
        if ($_FILES["themes"]["error"][$j] > 0) {
            echo "Error: " . $_FILES["themes"]["error"][$j] . "<br>";
        } else {
            if (file_exists($themesPath . $_FILES["themes"]["name"][$j])) {
            } else {
                copy($_FILES["themes"]["tmp_name"][$j], $themesPath . $_FILES["themes"]["name"][$j]);
                $zip = new ZipArchive;
                $res = $zip->open($themesPath . $_FILES["themes"]["name"][$j]);
                if ($res === TRUE) {
                  $zip->extractTo($themesPath);
                  $zip->close();
                  unlink($themesPath . $_FILES["themes"]["name"][$j]);
                }
            }
        }
    }
  }

  if(isset($_FILES['medias']) && !empty($_FILES['medias'])){
    $no_medias = count($_FILES["medias"]['name']);
    $mediaPath = ABSPATH . 'wp-content/uploads/';
    for ($k = 0; $k < $no_medias; $k++) {
        if ($_FILES["medias"]["error"][$k] > 0) {
            echo "Error: " . $_FILES["medias"]["error"][$k] . "<br>";
        } else {
            if (file_exists($mediaPath . $_FILES["medias"]["name"][$k])) {
            } else {
                $upload_dir = wp_upload_dir();
                if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                  $file = $upload_dir['path'] . '/' . $_FILES["medias"]["name"][$k];
                }
                else {
                  $file = $upload_dir['basedir'] . '/' . $_FILES["medias"]["name"][$k];
                }
                copy($_FILES["medias"]["tmp_name"][$k], $file);
                $wp_filetype = wp_check_filetype( $_FILES["medias"]["name"][$k], null );

                $attachment = array(
                  'post_mime_type' => $wp_filetype['type'],
                  'post_title' => sanitize_file_name( $_FILES["medias"]["name"][$k] ),
                  'post_content' => '',
                  'post_status' => 'inherit'
                );

                $attach_id = wp_insert_attachment( $attachment, $file );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
                wp_update_attachment_metadata( $attach_id, $attach_data );

                $zip = new ZipArchive;
                $res = $zip->open($mediaPath . $_FILES["medias"]["name"][$k]);
                if ($res === TRUE) {
                  $zip->extractTo($mediaPath);
                  $zip->close();
                  unlink($mediaPath . $_FILES["medias"]["name"][$k]);
                }
            }
        }
    }
  }
}
