<?php
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';
// check if media cleaner page is active
@$content_scan = get_option('content_scan_activate');
if($content_scan == "yes"){
?>
<style type="text/css">
	table.dataTable thead tr th {
		border: none;
	}
	table.dataTable {
		border: none;
	}
	.dataTables_wrapper.no-footer .dataTables_scrollBody {
		border-bottom: 1px solid #ddd;
		border-top: 1px solid #ddd;
	}
	.btn-blue, .multiselect.dropdown-toggle.btn.btn-default {
		background-color: #4DB9AB;
		color: #fff;
	}
	.btn-red {
		background-color: #E53373;
		color: #fff;
	}	
	/*small loader*/
	.loader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 60px;
		height: 60px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
		margin: 0 auto;
	}
	@-webkit-keyframes spin {
	 0% {
	 -webkit-transform: rotate(0deg);
	}
	 100% {
	 -webkit-transform: rotate(360deg);
	}
	}
	 @keyframes spin {
	 0% {
	 transform: rotate(0deg);
	}
	 100% {
	 transform: rotate(360deg);
	}
	}
	/* Tooltip container */
	.tooltipstext{
		display: none;
	}
	.tooltip_inline{display: inline-block;width: 100%;margin-top:15px;}
	.tooltip_inline_child{
		display: inline-block;
	}
	.tooltip_inline p { margin:0;}
	.tooltip_inline .tooltip_date {margin: 0 0 0 15px;}
	.tooltip_content{
		max-height: 180px; 
		overflow-y: scroll;
	}
	p.tooltip_title {
		font-size: 20px;
	}
	.tooltip_button a{
		color: #fff;
	}
</style>
<div class="alert-box"></div>
<!-- Rotating logo -->
<div class="loading" style="display: none">
	<div class="loading_box">Scanning for contents <span>Do not close this window</span>
		<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
	</div>
</div>
<!-- Rotating logo for actions-->
<div class="action_loader">
<div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span>LOADING</span> </div>
</div>
<!-- Confirm taking media backup Modal -->
<div id="mediaDeleter" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4><?php _e('Do you want to take backup of selected media before deleting them ?','wp_media_string'); ?></h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="DeleteFilteredImages('yes')">Yes</button>
        <button type="button" id="no" class="btn btn-red" onclick="DeleteFilteredImages('no')">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Edit media modal -->
<div id="editMedia" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <form type="form" method="post" id="updateMediaData">
        <div class="modal-body media_results"> </div>
        <div class="modal-footer">
          <div class="container">
            <input type="submit" class="btn btn-blue" value="Save">
            <button type="button" id="save_exit" class="btn btn-blue">Save and Exit</button>
            <button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="WPMC_media_scan">
  <!-- CMS usage data from w3techs.com / captured 7/6/16 -->
  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="sss" type="button">Scan Content</button>
      </li>
      <li> <span class="text">Filter:</span>
        <select name="contentTypeOpt[]" class="btn btn-blue" multiple id="contentTypeOpt">
        </select>
      </li>
      <li>
        <input type="button" name="daterange" value="Date Picker" id="datePicker" class="btn btn-blue" />
      </li>
      <li>
        <select name="categoryTypeOpt[]" multiple id="categoryTypeOpt" class="btn btn-blue">
        </select>
      </li>
      <li>
        <select name="postStatusOpt[]" multiple id="postStatusOpt" class="btn btn-blue">
          <option value="publish">Publish</option>
          <option value="pending">Pending</option>
          <option value="draft">Draft</option>
          <option value="private">Private</option>
          <option value="future">Future</option>
          <option value="trash">Trash</option>
        </select>
      </li>
      <li>
        <label class="gray"><span id="total_files"></span><span id="no_files">No</span> media files found</label>
        <!--label class="red">Original size <span id="media_size"></span><span id="no_size">- 0.0 MB</span> - Compressed size 0.0MB</label-->
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="scaned_content" class="table table-striped table-sm table_design" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th>Content</th>
          <th>Content Type</th>
          <th>Media</th>
          <th>Creation Date</th>
          <th>Status</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody id="content_scanned_result">
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="center"><strong class="red">No Content Found, Please scan content !</strong></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-blue" id="getfilteredimages" type="button">Select Filtered Content</button>
      </li>
      <li>
        <button class="btn btn-red" id="deletefilteredimages" type="button" data-toggle="modal" data-target="#mediaDeleter">Delete Filtered Content</button>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript">
	jQuery(window).load(function(){
		setTimeout(function() {
		    jQuery('.action_loader').fadeOut('fast');
		}, 3000);
	})
	jQuery(document).on("click",".editMediaModal",function() {
		jQuery("#editMedia .media_results").html('<div class="loader"></div>');
		jQuery('#editMedia').modal('show');
		var media = jQuery(this).attr("data-link");
		var source = jQuery(this).attr("data-source");
		var pagefrom = jQuery(this).attr("data-from");
		var pagetype = jQuery(this).attr("data-type");
		var pagesite = jQuery(this).attr("data-site");
		var page_url = "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/lib/content-scan/edit-media-result.php";
		jQuery.post("<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/get_attachment_meta.php", function (data) {
		    var w = window.open(page_url);
		    w.document.open();
		    w.document.write(data);
		    w.document.close();
		});
		// jQuery.ajax({
		// 	type: "POST",
		// 	url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/get_attachment_meta.php",
		// 	data: {
		// 		request_type: "get_attachment_meta",
		// 		media_src: media,
		// 		media_source: source,
		// 		page_from: pagefrom,
		// 		page_type: pagetype,
		// 		page_site: pagesite
		// 	},
		// 	cache: false,
		// 	success: function(results){
		// 		jQuery("#editMedia .media_results").html(results);
		// 	},
		// 	error: function (results) {
		// 		alert('Something Went Wrong, Please Try Again');
		// 	}
		// });
	});
	jQuery(document).on("click",".hover_me",function(e){
	    e.preventDefault();
	    jQuery(this).siblings(".tooltipstext").fadeIn(300,function(){jQuery(this).focus();});
	});

	jQuery(document).on("click",".close",function(){
	   jQuery(".tooltipstext").fadeOut(300);
	});
	jQuery(document).on("blur",".tooltipstext",function(){
	    jQuery(this).fadeOut(300);
	});

	jQuery(document).ready(function() {
	    var $chkboxes = jQuery('.chkbox');
	    var lastChecked = null;
		jQuery(document).on("click",".chkbox",function(e) {
	        if (!lastChecked) {
	            lastChecked = this;
	            return;
	        }
	        if (e.shiftKey) {
	            var start = jQuery('.chkbox').index(this);
	            var end = jQuery('.chkbox').index(lastChecked);
	            jQuery('.chkbox').slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
	        }
	        lastChecked = this;
	    });
	});
	
	jQuery("#sss").click(function(){
		jQuery(".loading").show();
		jQuery('#scaned_content').dataTable().fnDestroy();
		jQuery('#scaned_content').DataTable({
	        'ajax': {
				type: 'POST',
				'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/contentscan.php',
				data: {
					request_type: "scan_content",
				},
				"dataSrc": function (json) {
					
					jQuery("#no_size").hide();
					jQuery("#total_files").html(json.total_files);
					jQuery("#no_files").hide();
					jQuery(".loading").hide();
					return json.data;
				},
				complete: function(){
					if(jQuery("#datePicker").val() != 'Date Picker'){
						var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
						var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
						dependFilter(startDate,endDate);
					}else{
						dependFilter();
					}
	            },

		    },
		    "paging": false,
		    "info": false,
		    "scrollY": "400px",
        	"scrollCollapse": true,
        	"language": {
		        searchPlaceholder: "Search content",
		    }
	    });
	});

	jQuery('#getfilteredimages').toggle(function(){
        jQuery("input[name=check_list]").prop('checked', true);
        jQuery(this).text("Deselect Filtered Images");
    },function(){
        jQuery("input[name=check_list]").prop('checked', false);
        jQuery(this).text("Select Filtered Images");
    })

    // Script for opening edit media in new window start
    function openPopupPage(relativeUrl, media_name, media_src, page_from,page_type, page_site){
		var param = {
			'media_src': media_src,
			'page_from': page_from,
			'page_type': page_type,
			'page_site': page_site,
			'media_source': media_name
		};
		OpenWindowWithPost(relativeUrl, "width=1000, height=600, left=100, top=100, resizable=yes, scrollbars=yes", "NewFile", param);
	}
	function OpenWindowWithPost(url, windowoption, name, params){
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("action", url);
		form.setAttribute("target", name);
		for (var i in params){
			if (params.hasOwnProperty(i)){
				var input = document.createElement('input');
				input.type = 'hidden';
				input.name = i;
				input.value = params[i];
				form.appendChild(input);
			}
		}
		document.body.appendChild(form);
		//note I am using a post.htm page since I did not want to make double request to the page 
		//it might have some Page_Load call which might screw things up.
		window.open("post.htm", name, windowoption);
		form.submit();
		document.body.removeChild(form);
	}
	// Script for opening edit media in new window Ends

	function GetCheckboxSelected(){
		jQuery(".action_loader").show();
		var checkValues = jQuery('input[name=check_list]:checked').map(function()
        {
            return jQuery(this).val();
        }).get();
    	jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/add_to_media.php",
			data: {
				request_type: "add_to_media",
				media_src: checkValues
			},
			cache: false,
			success: function(res){
				jQuery(".action_loader").hide();
				var obj = jQuery.parseJSON(res);
				var i = 0;
				jQuery.each(obj, function(key, value ) {
					jQuery("input[type=checkbox][value='"+value+"']").prop("checked", false);
					i++;
				});
				jQuery( ".alert-box" ).addClass('success');
				jQuery( ".alert-box" ).html('<p>'+i+' media file(s) added to library</p>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
			},
			error: function (res) {
				jQuery(".action_loader").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<p>Something went wrong, Please try again</p>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
			}
		});
	}

	function DeleteFilteredImages($backup){
		jQuery('#mediaDeleter').modal('toggle');
		if($backup == 'yes'){
			var isbackup = 'yes';
		}else{
			var isbackup = 'no';
		}
		jQuery(".action_loader").show();
		var checkValues = jQuery('input[name=check_list]:checked').map(function(){
            return jQuery(this).val();
        }).get();
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/delete_content.php",
			data: {
				backup: isbackup,
				content_src: checkValues
			},
			cache: false,
			success: function(response){
				jQuery(".action_loader").hide();
				var obj = jQuery.parseJSON(response);
				if(obj.length>0){
					var i = 0;
					jQuery.each(obj, function(key, value ) {
						jQuery("input[type=checkbox][value='"+value+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800);
						i++;
					});
					jQuery( ".alert-box" ).removeClass('unsuccess');
					jQuery( ".alert-box" ).addClass('success');
					jQuery( ".alert-box" ).html('<p>'+i+' file(s) deleted</p>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				}else{
					jQuery(".action_loader").hide();
					jQuery( ".alert-box" ).removeClass('success');
					jQuery( ".alert-box" ).addClass('unsuccess');
					jQuery( ".alert-box" ).html('<p>Something went wrong, Please try again</p>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
				}
				jQuery("input[name=check_list]").prop('checked', false);
			},
			error: function (jqXHR, exception) {
				jQuery(".action_loader").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<p>Something went wrong, Please try again</p>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
			}
		});
	}

	jQuery(document).ready(function(){
		jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_cpt',
			},
			success: function(response) {
				jQuery('#contentTypeOpt').empty().append(response);
				jQuery('#contentTypeOpt').multiselect({
				    nonSelectedText: 'Content Type',
				    allSelectedText: 'Content Type',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});
			},
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });

	    jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_categories',
			},
			success: function(response) {
				jQuery('#categoryTypeOpt').empty().append(response);
				jQuery('#categoryTypeOpt').multiselect({
				    nonSelectedText: 'Category',
				    allSelectedText: 'Category',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});
			},
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });
	});

	jQuery('#linkedOpt').on('change',function(){
		var linked_status = jQuery(this).val().toString();

		if(linked_status == "no"){
			jQuery('#abcd tbody tr').each(function(){
				console.log('tr');
				console.log(jQuery('td:eq(4)', this));
			});

			console.log('if condition');
		}

		console.log(linked_status);

	});
/* media scan filter start */
jQuery(document).ready(function(){
	jQuery("#contentTypeOpt").on('change',function(){
		dependFilter();
	});
	jQuery("#postStatusOpt").on('change',function(){
		dependFilter();
	});
	jQuery("#categoryTypeOpt").on('change',function(){
		dependFilter();
	});
});
jQuery(document).on("click",".daterangepicker .applyBtn",function() {
	var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
	var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
	dependFilter(startDate,endDate);
});
jQuery(document).on("click",".daterangepicker .cancelBtn",function() {
	dependFilter();
});
function dependFilter(startDate,endDate){
	var filterValue, filterValueArr, matchArray, found, found1, found2, found3, contentTypeOptArr, postStatusOptArr, categoryTypeOptArr, startDateArr, endDateArr, fDate,lDate,cDate;
	var contentTypeOpt = jQuery("#contentTypeOpt").val();
	var postStatusOpt = jQuery("#postStatusOpt").val();
	var categoryTypeOpt = jQuery("#categoryTypeOpt").val();
	var startDate = startDate;
	var endDate = endDate;
	var mainFilter = [];
	if(contentTypeOpt != null){
		contentTypeOpt = jQuery.map(contentTypeOpt, function(n,i){return n.toLowerCase();});
		mainFilter['contentTypeOpt'] = contentTypeOpt;
	}
	if(postStatusOpt != null){
		postStatusOpt = jQuery.map(postStatusOpt, function(n,i){return n.toLowerCase();});
		mainFilter['postStatusOpt'] = postStatusOpt;
	}
	if(categoryTypeOpt != null){
		categoryTypeOpt = jQuery.map(categoryTypeOpt, function(n,i){return n.toLowerCase();});
		mainFilter['categoryTypeOpt'] = categoryTypeOpt;
	}
	if(startDate != null && endDate != null){
		mainFilter['startDate'] = startDate;
		mainFilter['endDate'] = endDate;
	}
	contentTypeOptArr = mainFilter.contentTypeOpt;
	postStatusOptArr = mainFilter.postStatusOpt;
	categoryTypeOptArr = mainFilter.categoryTypeOpt;
	startDateArr = mainFilter.startDate;
	endDateArr = mainFilter.endDate;
	if(contentTypeOptArr == null){
		contentTypeOptArr = [];
	}
	if(postStatusOptArr == null){
		postStatusOptArr = [];
	}
	if(categoryTypeOptArr == null){
		categoryTypeOptArr = [];
	}
	if(startDateArr == null && endDateArr == null){
		startDateArr = [];
		endDateArr = [];
	}
	jQuery("#content_scanned_result tr td").find("#att-filter").each(function(){
		var getDiv = jQuery(this).parent().parent();
		if(contentTypeOptArr.length != 0 || postStatusOptArr.length != 0 || categoryTypeOptArr.length != 0 || startDateArr.length != 0 || endDateArr.length != 0){
			filterValue = jQuery(this).val();
			filterValueArr = filterValue.split(',');
			filterValueArr = jQuery.map(filterValueArr, function(n,i){return n.toLowerCase();});
			if(contentTypeOptArr.length != 0){
				found = contentTypeOptArr.some(r=> filterValueArr.includes(r));
			}else{
				found = true;
			}
			if(postStatusOptArr.length != 0){
				found1 = postStatusOptArr.some(r=> filterValueArr.includes(r));
			}else{
				found1 = true;
			}
			if(categoryTypeOptArr.length != 0){
				found2 = categoryTypeOptArr.some(r=> filterValueArr.includes(r));
			}else{
				found2 = true;
			}
			if(startDateArr.length != 0 || endDateArr.length != 0){
				postDate = filterValueArr[1];
				fDate = Date.parse(startDateArr);
			    lDate = Date.parse(endDateArr);
			    cDate = Date.parse(postDate);
			    if((cDate <= lDate && cDate >= fDate)) {
			    	console.log('match');
			        found3 = true;
			    }else{
			    	console.log('un');
			    	found3 = false;
			    }
			}else{
				found3 = true;
			}
			if(found && found1 && found2 && found3){
				getDiv.show();
				getDiv.find('.sorting_1 input').attr('class', 'chkbox');
				getDiv.find('.sorting_1 input').attr('name', 'check_list');
			}else{
				getDiv.hide();
				getDiv.find('.sorting_1 input').attr('class', 'chkbox1');
				getDiv.find('.sorting_1 input').attr('name', 'check_list1');
			}
		}else{
			getDiv.show();
			getDiv.find('.sorting_1 input').attr('class', 'chkbox');
			getDiv.find('.sorting_1 input').attr('name', 'check_list');
		}
	});
}

/* media scan filter end */
</script>
<?php
}else{
	?>
	<div class="WPMC_media_scan">
		<div><h3><?php _e("Content scan page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_string"); ?></h3></div>
	</div>
	<?php
}
?>