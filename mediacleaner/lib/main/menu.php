<?php
add_action('admin_menu', 'WPMC_Menu');
 
function WPMC_Menu(){
/*	add_menu_page( 'WP Media Cleaner', 'Media Cleaner', 'manage_options', 'media-cleaner-media-scan', 'MediaCleanerMediaScan', '', 30);

    add_submenu_page('media-cleaner-media-scan', 'Settings', 'Settings', 'manage_options', 'media-cleaner-setting', 'MediaCleanerSetting' );
    add_submenu_page('media-cleaner-media-scan', 'Media Scanner', 'Media Scanner', 'manage_options', 'media-cleaner-media-scan', 'MediaCleanerMediaScan' );
    add_submenu_page('media-cleaner-media-scan', 'Content Scanner', 'Content Scanner', 'manage_options', 'media-cleaner-content-scan', 'MediaCleanerContentScan' );
    add_submenu_page('media-cleaner-media-scan', 'Image Optimize', 'Image Optimize', 'manage_options', 'media-cleaner-image-optimize', 'MediaCleanerImageOptimize' );
    add_submenu_page('media-cleaner-media-scan', 'Database Cleaner', 'Database Cleaner', 'manage_options', 'media-cleaner-database-cleaner', 'MediaCleanerDatabaseCleaner' );
    add_submenu_page('media-cleaner-media-scan', 'Backup', 'Backup', 'manage_options', 'media-cleaner-backup', 'MediaCleanerBackup' );*/

     add_menu_page( 'WP Media Cleaner', __('Media Cleaner','wp_media_cleaner') , 'manage_options', 'media-cleaner-media-scan', 'MediaCleanerMediaScan', '', 30);
    add_submenu_page('media-cleaner-media-scan', 'Settings', __('Setting','wp_media_cleaner') , 'manage_options', 'media-cleaner-setting', 'MediaCleanerSetting' );
    add_submenu_page('media-cleaner-media-scan', 'Media Scanner',  __('Media Scanner','wp_media_cleaner') , 'manage_options', 'media-cleaner-media-scan', 'MediaCleanerMediaScan' );
    add_submenu_page('media-cleaner-media-scan', 'Content Scanner', __('Content Scanner','wp_media_cleaner'), 'manage_options', 'media-cleaner-content-scan', 'MediaCleanerContentScan' );
    add_submenu_page('media-cleaner-media-scan', 'Image Optimize', __('Image Optimize','wp_media_cleaner'), 'manage_options', 'media-cleaner-image-optimize', 'MediaCleanerImageOptimize' );
    add_submenu_page('media-cleaner-media-scan', 'Database Cleaner', __('Database Cleaner','wp_media_cleaner') , 'manage_options', 'media-cleaner-database-cleaner', 'MediaCleanerDatabaseCleaner' );
    add_submenu_page('media-cleaner-media-scan', 'Backup', __('Backup','wp_media_cleaner') , 'manage_options', 'media-cleaner-backup', 'MediaCleanerBackup' );
}

function MediaCleanerSetting(){
    require_once WPMC_SETTINGS . DIRECTORY_SEPARATOR . 'settings.php';
}
function MediaCleanerMediaScan(){
    require_once WPMC_MEDIA_SCAN . DIRECTORY_SEPARATOR . 'media-scan.php';
}
function MediaCleanerContentScan(){
    require_once WPMC_CONTENT_SCAN . DIRECTORY_SEPARATOR . 'content-scan.php';
}
function MediaCleanerImageOptimize(){
    require_once WPMC_IMAGE_OPTIMIZE . DIRECTORY_SEPARATOR . 'image-optimize.php';
}
function MediaCleanerDatabaseCleaner(){
    require_once WPMC_DATABASE_CLEANER . DIRECTORY_SEPARATOR . 'database-cleaner.php';
}
function MediaCleanerBackup(){
    require_once WPMC_BACKUP . DIRECTORY_SEPARATOR . 'backup.php';
}