<?php
$current_dir = dirname(__FILE__);
$current_dir = str_replace("/wp-content/plugins/wp_media_cleaner/lib/media-scan", "", $current_dir);
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';

// check if media cleaner page is active
@$media_scan = get_option('media_scan_activate');
if($media_scan == "yes"){
?>
<style type="text/css">
	.exclude_bulk_media{
		display: none;	
	}
	/*Excluded image css*/
	.yes_excluded .img_name{
		/*background-color: rgba(229, 50, 115, 0.14) !important;*/
		color: #f24e85 !important;
	}
	.ui-timepicker-container.ui-timepicker-standard.ui-timepicker-no-scrollbar{
		z-index: 1100 !important;
	}
	.alert-box {
		display: none;
		position: fixed;
		width: 25%;
		height: 60px;
		z-index: 99999999999;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		padding: 15px;
		margin-bottom: 20px;
		border: 1px solid transparent;
		border-radius: 4px;
	}
	#myProgress {
	  width: 100%;
	  background-color: #ddd;
	}

	#myBar {
	  width: 1%;
	  height: 30px;
	  background-color: #4CAF50;
	}
	/*small loader*/
	.loader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 60px;
		height: 60px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
		margin: 0 auto;
	}
	@-webkit-keyframes spin {
		0% {
			-webkit-transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
		}
		}
		@keyframes spin {
		0% {
			transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
		}
	}
</style>
<div class="alert-box"></div>
<!-- filtered images resultant div -->
<div id="filtered_image_result" style="display: none">
</div>

<!-- backup all media rows div -->
<div id="backup_media_result" style="display: none">
</div>

<!-- Confirm taking media backup Modal -->
<div id="mediaDeleter" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Do you want to make a backup before deleting your files?','wp_media_string'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="DeleteFilteredImages('yes')">Yes</button>
        <button type="button" id="no" class="btn btn-red" onclick="DeleteFilteredImages('no')">No</button>
      </div>
    </div>
  </div>
</div>

<!-- Exclude bulk media Modal start -->
<div id="excludeMediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to perform this action?','wp_media_string'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" onclick="excludeMedia()">Yes</button>
        <button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Exclude bulk media Modal ends -->

<!-- Automatic Image Deleter Modal start -->
<div id="automaticImageDelete" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-12">
        		<h4><?php _e("Select Time Interval For Shedule Image Deletion","wp_media_string"); ?></h4>
        	</div>
			<div class="col-md-2">
				<input type="checkbox" name="delter_is_active" id="delter_is_active" value="yes">
			</div>
			<div class="col-md-5">
				<select class="form-control" id="shedule_interval">
					<option value="hourly"><?php _e("Hourly","wp_media_string"); ?></option>
					<option value="daily"><?php _e("Daily","wp_media_string"); ?></option>
					<option value="monthly"><?php _e("Monthly","wp_media_string"); ?></option>
					<option value="yearly"><?php _e("Yearly","wp_media_string"); ?></option>
				</select>
			</div>
			<div class="col-md-5">
				<input type="text" class="timepicker" id="shedule_time" placeholder="Time">
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="AutomaticImageDeleter()">Save</button>
        <button type="button" class="btn btn-red" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Automatic Image Deleter Modal ends -->

<div class="WPMC_media_scan">
	<!-- Rotating logo -->
	<div class="loading" style="display: none">
	<div class="loading_box">Scanning for media files <span>Do not close this window</span> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"> </div>
	</div>
	<!-- Rotating logo for actions-->
	<div class="action_loader">
	<div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span>LOADING</span> </div>
	</div>
	<!-- Only Rotating logo -->
	<div class="loading_rotating" style="display: none;"><div class="loading_box"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"> </div>
	</div>
  <!-- CMS usage data from w3techs.com / captured 7/6/16 -->
  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="sss" type="button"><?php _e("Scan server","wp_media_string"); ?></button>
      </li>
      <li>
        <button class="btn btn-blue" id="scan_library" type="button"><?php _e("Scan Media Library","wp_media_string"); ?></button>
      </li>
      <li> <span class="text">Filter:</span>
        <select name="fileTypeOpt[]" multiple id="fileTypeOpt" class="btn btn-blue" style="display: none;">          
        </select>
        <select name="fileTypeOptDemo[]" multiple id="fileTypeOptDemo" class="btn btn-blue">
        	<option><?php echo __("Click on Ready to Scan","wp_media_string"); ?></option>
        </select>
      </li>
      <li>
        <input type="button" name="daterange" id="datePicker" value="Date Picker" class="btn btn-blue" />
      </li>
      <li>
        <select name="linkedOpt[]" size="2" multiple id="linkedOpt" class="btn btn-blue">
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>
      </li>
      <li>
        <select name="postTypeOpt[]" class="btn btn-blue" multiple id="postTypeOpt">
        </select>
      </li>
      <li>
        <label class="gray"><span id="total_files"></span><span id="no_files">No</span> media files found</label>
        <label class="red">Media size <span id="media_size"></span><span id="no_size">- 0.0 MB</span></label>
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="abcd" class="table table-striped table-sm table_design" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th><?php echo __("Files","wp_media_string") ?></th>
          <th><?php echo __("Post type","wp_media_string") ?></th>
          <th><?php echo __("Upload Date","wp_media_string") ?></th>
          <th><?php echo __("Linked","wp_media_string") ?></th>
        </tr>
      </thead>
      <tbody id="media_scanned_result">
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td colspan="5" align="center"><strong class="red"><strong>Ready to scan</strong></strong></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-blue" id="getfilteredimages" type="button">Select Filtered Images</button>
      </li>
      <li>
        <button class="btn btn-red" id="deletefilteredimages" type="button" data-toggle="modal" data-target="#mediaDeleter">Delete Filtered Images</button>
      </li>
      <li>
      	<button class="btn btn-red" id="" type="button" data-toggle="modal" data-target="#automaticImageDelete">Automatic Image Deleter</button>
      </li>
      <li>
        <button class="btn btn-red" id="addtomedia" type="button" onclick="GetCheckboxSelected()"><?php _e("Add to Media Library","wp_media_string"); ?></button>
      </li>
      <li>
        <button class="btn btn-red exclude_bulk_media" type="button" data-toggle="modal" data-target="#excludeMediaModal"><?php _e("Exclude Media","wp_media_string"); ?></button>
        <button class="btn btn-red show_excluded_media" type="button"><?php _e("Show Excluded Media","wp_media_string"); ?></button>
      </li>
      <li>
        <button class="btn btn-blue" id="showfilteredimages" type="button">Show Filtered Images</button>
        <button class="btn btn-blue" id="backupfilteredimages" type="button" style="display: none">Back To Scanned Media</button>
      </li>
    </ul>
  </div>
</div>
<button type="button" id="demos">Click</button>
<button type="button" id="undo">UNDO THIS</button>
<script type="text/javascript">
	jQuery(window).load(function(){
		setTimeout(function() {
		    jQuery('.action_loader').fadeOut('fast');
		}, 3000);
	})
	jQuery(document).ready(function() {


		// var array1 = ['a', 'b', 'c', 'd', 'e'];
		// var array2 = ['a', 'b', 'c'];

		// array1 = array1.filter(function(item) {
		//   return !array2.includes(item); 
		// })
		// console.log(array1); // [ 'a', 'c', 'e' ]

		jQuery("#demos").click(function(){
			jQuery("#media_scanned_result input[name=check_list]:checkbox:not(:checked)").parent().parent().hide();
			jQuery("#media_scanned_result input[name=check_list1]:checked").parent().parent().show();
			// jQuery("#media_scanned_result input[name=check_list]:checkbox(:checked)").parent().parent().show();
			// var all_row = jQuery('#media_scanned_result input[name=check_list]').map(function(){
	  //           return jQuery(this).parent().parent().html();
	  //       }).get();
			// var checkValues = jQuery('#media_scanned_result input[name=check_list]:checked').map(function(){
	  //           return jQuery(this).parent().parent().html();
	  //       }).get();
	  //       var checkValues2 = jQuery('#media_scanned_result input[name=check_list1]:checked').map(function(){
	  //           return jQuery(this).parent().parent().html();
	  //       }).get();
	  //       var all_values = jQuery.merge( jQuery.merge( [], checkValues ), checkValues2 );
	        
	  //       array1 = all_row.filter(function(item) {
			//   return !all_values.includes(item); 
			// })
	  //       console.log(array1);
			// // jQuery("#media_scanned_result").html("");
			// jQuery.each( arr, function( i, val ) {
			// 	val.hide();
			// 	// jQuery("#media_scanned_result").append('<tr role="row" class="odd">'+val+'</tr>');
			// });
		})
		jQuery("#undo").on('click',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		// Dummy filter for showing before scanning
		jQuery('#fileTypeOptDemo').multiselect({
		    nonSelectedText: 'File Type',
		    allSelectedText: 'File Type',
		    onChange: function(option, checked) {
		    	
	        }
		});
		// Disable all action button when no selection
		jQuery("#deletefilteredimages").prop("disabled",true);
	    jQuery("#addtomedia").prop("disabled",true);
	    // jQuery(".exclude_bulk_media").prop("disabled",true);

	    // jQuery("#getfilteredimages").prop("disabled",true);

		jQuery(document).ready(function(){
		    jQuery('input.timepicker').timepicker({ timeFormat: 'HH:mm:ss' });
		});
	    var $chkboxes = jQuery('.chkbox');
	    var lastChecked = null;
		jQuery(document).on("click",".chkbox",function(e) {    	
			if (!lastChecked) {
			    lastChecked = this;          
			    return;
			}
			if (e.shiftKey) {        	
			    var start = jQuery('.chkbox').index(this);
			    var end = jQuery('.chkbox').index(lastChecked);
			    console.log(end);            
			    jQuery('.chkbox').slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
			}
			lastChecked = this;
		});
	});
	jQuery(document).on("click","input[name=check_list]",function() {
      // Disable action button if checkbox not selected
      var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
      if(atLeastOneIsChecked){
        jQuery("#deletefilteredimages").prop("disabled",false);
	    jQuery("#addtomedia").prop("disabled",false);
	    // jQuery(".exclude_bulk_media").prop("disabled",false);

	    // Check if selected row is excluded
	    var row_excluded = jQuery('input[name=check_list]:checked').map(function(){
			var excluded = jQuery(this).data("excluded");
			if(excluded == "Excluded"){
				return "excluded";
			}else{
				return "no_excluded";
			}
		}).get();
		var if_excluded = jQuery.inArray( "excluded", row_excluded );
		if(if_excluded != -1){
			jQuery(".exclude_bulk_media").html("Do Not Exclude Media");
			jQuery(".show_excluded_media").hide();
			jQuery(".exclude_bulk_media").show();
		}else{
			jQuery(".exclude_bulk_media").html("Exclude Media");
			jQuery(".show_excluded_media").hide();
			jQuery(".exclude_bulk_media").show();
		}
      }else{
      	jQuery(".show_excluded_media").show();
      	jQuery(".exclude_bulk_media").hide();
        jQuery("#deletefilteredimages").prop("disabled",true);
	    jQuery("#addtomedia").prop("disabled",true);
	    // jQuery(".exclude_bulk_media").prop("disabled",true);
      }
    });
	jQuery("#sss").click(function(){
		// Get all the available extension from database
		var homeurl = '<?php echo $current_dir; ?>';
		jQuery.ajax({
		    url: ajaxurl,
		    type: "POST",
	        data: {
				action: 'wpmc_get_image_extensions',
				home_url: homeurl
			},
			success: function(response) {
				// console.log(response);
				jQuery('#fileTypeOpt').empty().append(response);
				jQuery('#fileTypeOpt').multiselect({
				    nonSelectedText: 'File Type',
				    allSelectedText: 'File Type',
				    onChange: function(option, checked) {				    	
			        }
				});
	          	jQuery('#fileTypeOptDemo').siblings(".btn-group").hide();	          	
	        },
	        error: function (ErrorResponse) {
	            // console.log(ErrorResponse);
	        }
	    });
		// Pauase loader
		var filetypes = jQuery('select[name="fileTypeOpt[]"]').val();
		var linked_status = jQuery('select[name="linkedOpt[]"]').val();
		var daterange = jQuery('input[name="daterange"]').val();
		var posttypefilter = jQuery('select[name="postTypeOpt[]"]').val();
		var daterangearr = daterange.split('-');
		var start_date = daterangearr[0];
		var end_date = daterangearr[1];
		jQuery(".loading").show();
		jQuery('#abcd').dataTable().fnDestroy();
		jQuery('#abcd').DataTable({
	        'ajax': {
				type: 'POST',
				'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/method.php',
				data: {
					request_type: "scan_media",
					"filetypes": filetypes,
					"start_date": start_date,
					"end_date": end_date,
					"linked_status": linked_status,
					"posttypefilter": posttypefilter 
				},
				"dataSrc": function (json) {
					jQuery("#media_size").html(json.media_size);
					jQuery("#no_size").hide();
					jQuery("#total_files").html(json.total_files);
					jQuery("#no_files").hide();
					jQuery(".loading").hide();
					if(jQuery.isEmptyObject(json.data)){
						jQuery('.dataTables_empty').text('No media found');
					}
					// jQuery("#getfilteredimages").prop("disabled",false);
					return json.data;
				},
				complete: function(){
					if(jQuery("#datePicker").val() != 'Date Picker'){
						var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
						var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
						dependFilter(startDate,endDate);
					}else{
						dependFilter();
					}
					setTimeout(function() {
				        // First take backup of all scanned media files
						var backup_media = jQuery("#media_scanned_result").html();
						jQuery("#backup_media_result").empty().html(backup_media);
				    }, 2000);
	            },

		    },
		    "paging": false,
		    "info": false,
		    "scrollY": "400px",
        	"scrollCollapse": true,
        	"language": {
		        searchPlaceholder: "Search media",
		    }
	    });
	});
	jQuery("#scan_library").click(function(){
		// Get all the available extension from database
		var homeurl = '<?php echo $current_dir; ?>';
		jQuery.ajax({
		    url: ajaxurl,
		    type: "POST",
	        data: {
				action: 'wpmc_get_image_extensions',
				home_url: homeurl
			},
			success: function(response) {
				// console.log(response);
				jQuery('#fileTypeOpt').empty().append(response);
				jQuery('#fileTypeOpt').multiselect({
				    nonSelectedText: 'File Type',
				    allSelectedText: 'File Type',
				    onChange: function(option, checked) {				    	
			        }
				});
	          	jQuery('#fileTypeOptDemo').siblings(".btn-group").hide();	          	
	        },
	        error: function (ErrorResponse) {
	            // console.log(ErrorResponse);
	        }
	    });
		jQuery(".loading").show();
		jQuery('#abcd').dataTable().fnDestroy();
		jQuery('#abcd').DataTable({
	        'ajax': {
				type: 'POST',
				'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/scan_media_library.php',
				data: {
					request_type: "scan_media"
				},
				"dataSrc": function (json) {
					jQuery("#media_size").html(json.media_size);
					jQuery("#no_size").hide();
					jQuery("#total_files").html(json.total_files);
					jQuery("#no_files").hide();
					jQuery(".loading").hide();
					if(jQuery.isEmptyObject(json.data)){
						jQuery('.dataTables_empty').text('No media found');
					}
					return json.data;
				},
				complete: function(){
					if(jQuery("#datePicker").val() != 'Date Picker'){
						var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
						var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
						dependFilter(startDate,endDate);
					}else{
						dependFilter();
					}
					setTimeout(function() {
				        // First take backup of all scanned media files
						var backup_media = jQuery("#media_scanned_result").html();
						jQuery("#backup_media_result").empty().html(backup_media);
				    }, 2000);
	            },

		    },
		    "paging": false,
		    "info": false,
		    "scrollY": "400px",
        	"scrollCollapse": true,
        	"language": {
		        searchPlaceholder: "Search media",
		    }
	    });
	});
	jQuery('#getfilteredimages').toggle(function(){
        jQuery("input[name=check_list]").prop('checked', true);
        jQuery(this).text("Deselect Filtered Images");
    },function(){
        jQuery("input[name=check_list]").prop('checked', false);
        jQuery(this).text("Select Filtered Images");
    })
	// function selectAllCheck(){
	// 	jQuery("input[name=check_list]").prop('checked', true);
	// }

	// Automatic Image Deleter Script
	function AutomaticImageDeleter(){
		jQuery(".loading").show();
		var if_checked = jQuery("input[name=delter_is_active]:checked").val();
		var shedule_interval = jQuery("#shedule_interval").val();
		var shedule_time = jQuery("#shedule_time").val();
		if(if_checked == 'yes'){
			var the_cron_type = 'add_cron';
		}else{
			var the_cron_type = 'remove_cron';
		}
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/automatic_image_deleter.php",
			data: {
				time: shedule_time,
				interval: shedule_interval,
				hook: 'AutoDeleteImage',
				cron_type: the_cron_type
			},
			cache: false,
			success: function(response){
				jQuery("#automaticImageDelete").modal('toggle');
				jQuery(".loading").hide();
				jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				jQuery("input[name=check_list]").prop('checked', false);
			},
			error: function (jqXHR, exception) {
				jQuery("#automaticImageDelete").modal('toggle');
				jQuery(".loading").hide();
				jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
			}
		});
	}
	function GetCheckboxSelected(){
		jQuery(".action_loader").show();
		var checkValues = jQuery('input[name=check_list]:checked').map(function()
        {
            return jQuery(this).val();
        }).get();
    	jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/add_to_media.php",
			data: {
				request_type: "add_to_media",
				media_src: checkValues
			},
			cache: false,
			success: function(res){
				jQuery(".action_loader").hide();
				var obj = jQuery.parseJSON(res);
				var i = 0;
				jQuery.each(obj, function(key, value ) {
					jQuery("input[type=checkbox][value='"+value+"']").prop("checked", false);
					i++;
				});
				jQuery( ".alert-box" ).addClass('success');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) added to library</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				jQuery("input[name=check_list]").prop('checked', false);
			},
			error: function (res) {
				jQuery(".action_loader").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
			}
		});
	}
	function DeleteFilteredImages($backup){
		jQuery('.action_loader').show();
		jQuery('#mediaDeleter').modal('toggle');
		if($backup == 'yes'){
			var isbackup = 'yes';
		}else{
			var isbackup = 'no';
		}
		var checkValues = jQuery('input[name=check_list]:checked').map(function(){
            return jQuery(this).val();
        }).get();
        var website_prefix = jQuery('input[name=check_list]:checked').map(function(){
            return jQuery(this).data('prefix');
        }).get();
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/delete_media.php",
			data: {
				backup: isbackup,
				media_src: checkValues,
				site_prefix : website_prefix,
				request_type: 'bulk_delete'
			},
			cache: false,
			success: function(response){				
				jQuery(".action_loader").hide();
				var obj = jQuery.parseJSON(response);
				if (obj && obj.length > 0) {
					var i = 0;
					jQuery.each(obj, function(key, value ) {
						jQuery("input[type=checkbox][value='"+value+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800);
						i++;
					});
					jQuery( ".alert-box" ).addClass('success');
					jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) deleted</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				}else{
					jQuery( ".alert-box" ).addClass('success');
					jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Selected files are excluded.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				}
				jQuery("input[name=check_list]").prop('checked', false);
			},
			error: function (jqXHR, exception) {
				jQuery(".action_loader").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
				jQuery("input[name=check_list]").prop('checked', false);
			}
		});
	}
	function excludeMedia(){
		jQuery('#excludeMediaModal').modal('toggle');
		jQuery('.action_loader').show();
		var media_url = jQuery('input[name=check_list]:checked').map(function(){
			return jQuery(this).val();
		}).get();
    	jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
			// dataType: "json",
			data: {
				type: "excluded_media",
				action: "bulk_media_exclude",
				media_src: media_url

			},
			cache: false,
			success: function(response){				
				var obj = jQuery.parseJSON(response);
				jQuery.each(obj, function(key, value ) {
					console.log(value.actions);
					if(value.actions == "excluded"){
						jQuery("input[type=checkbox][value='"+value.url+"']").parent().parent().addClass("yes_excluded");
						jQuery("input[type=checkbox][value='"+value.url+"']").data("excluded","Excluded")
					}else{
						jQuery("input[type=checkbox][value='"+value.url+"']").parent().parent().removeClass("yes_excluded");
						jQuery("input[type=checkbox][value='"+value.url+"']").data("excluded","Exclude")
					}
				});
				jQuery('.action_loader').hide();
			    jQuery("input[name=check_list]").prop('checked', false);
			    jQuery(".show_excluded_media").show();
			    jQuery(".exclude_bulk_media").hide();
			},
			error: function (jqXHR, exception) {
	      		jQuery('.action_loader').hide();
		    	jQuery("input[name=check_list]").prop('checked', false);
				alert("Something went wrong. Please try again.");
				jQuery(".show_excluded_media").show();
			    jQuery(".exclude_bulk_media").hide();
			}
		});
	}
	jQuery( document ).ajaxSuccess(function(event, xhr, options) {			
		try {
		    var parse = jQuery.parseJSON(xhr.responseText);
		    // Add red backkground color to all excluded tr
			jQuery('[data-excluded="Excluded"]').parents("tr").addClass("yes_excluded");
		} catch(e) {
		}
	});
	jQuery(document).ready(function(){
		jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_cpt',
			},
			success: function(response) {
				jQuery('#postTypeOpt').empty().append(response);
				jQuery('#postTypeOpt').multiselect({
				    nonSelectedText: 'Post Type',
				    allSelectedText: 'Post Type',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});	          	
	        },
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });
	});
	/* media scan filter start */
	jQuery(document).ready(function(){	
		// Show only selected (checked) media	
		jQuery("#showfilteredimages").on('click',function(){
			jQuery("#backupfilteredimages").show();
			jQuery("#showfilteredimages").hide();			
			jQuery("#filtered_image_result").html("");
			var checkValues = jQuery('#media_scanned_result input[name=check_list]:checked').map(function(){
	            var vals = jQuery(this).parent().parent().html();
	            jQuery("#filtered_image_result").append('<tr role="row" class="odd">'+vals+'</tr>');
	        }).get();
	        var get_result = jQuery("#filtered_image_result").html();
	        jQuery("#media_scanned_result").html(get_result);
		});
		// Return to all media files
		jQuery("#backupfilteredimages").on('click',function(){
			jQuery("#showfilteredimages").show();
			jQuery(".show_excluded_media").show();
			jQuery("#backupfilteredimages").hide();
			var backup_media = jQuery("#backup_media_result").html();
			jQuery("#media_scanned_result").empty().html(backup_media);
		});
		// Show only excluded media
		jQuery(document).on("click",".show_excluded_media",function(e) {
			jQuery("#backupfilteredimages").show();
			jQuery(".show_excluded_media").hide();

			jQuery("#filtered_image_result").html("");
			var checkValues = jQuery('#media_scanned_result input[name=check_list]').map(function(){
				if_excluded = jQuery(this).data("excluded");
				if(if_excluded == "Excluded"){
		            var vals = jQuery(this).parent().parent().html();
		            jQuery("#filtered_image_result").append('<tr role="row" class="odd">'+vals+'</tr>');
		        }
	        }).get();
	        var get_result = jQuery("#filtered_image_result").html();
	        jQuery("#media_scanned_result").html(get_result);
		});

		jQuery("#fileTypeOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		jQuery("#linkedOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		jQuery("#postTypeOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
	});
	jQuery(document).on("click",".daterangepicker .applyBtn",function() {
		var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
		var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
		dependFilter(startDate,endDate);
	});
	jQuery(document).on("click",".daterangepicker .cancelBtn",function() {
		if(jQuery("#datePicker").val() != 'Date Picker'){
			var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
			var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
			dependFilter(startDate,endDate);
		}else{
			dependFilter();
		}
	});
	function dependFilter(startDate,endDate){
		var filterValue, filterValueArr, matchArray, found, found1, found2, found3, fileTypeOpt, linkedOptArr, postTypeOptArr, startDateArr, endDateArr, fDate,lDate,cDate;
		var fileTypeOpt = jQuery("#fileTypeOpt").val();
		var linkedOpt = jQuery("#linkedOpt").val();
		var postTypeOpt = jQuery("#postTypeOpt").val();
		var startDate = startDate;
		var endDate = endDate;
		var mainFilter = [];
		if(fileTypeOpt != null){
			fileTypeOpt = jQuery.map(fileTypeOpt, function(n,i){return n.toLowerCase();});
			mainFilter['fileTypeOpt'] = fileTypeOpt;
		}
		if(linkedOpt != null){
			linkedOpt = jQuery.map(linkedOpt, function(n,i){return n.toLowerCase();});
			mainFilter['linkedOpt'] = linkedOpt;
		}
		if(postTypeOpt != null){
			postTypeOpt = jQuery.map(postTypeOpt, function(n,i){return n.toLowerCase();});
			mainFilter['postTypeOpt'] = postTypeOpt;
		}
		if(startDate != null && endDate != null){
			mainFilter['startDate'] = startDate;
			mainFilter['endDate'] = endDate;
		}
		fileTypeOptArr = mainFilter.fileTypeOpt;
		linkedOptArr = mainFilter.linkedOpt;
		postTypeOptArr = mainFilter.postTypeOpt;
		startDateArr = mainFilter.startDate;
		endDateArr = mainFilter.endDate;
		if(fileTypeOptArr == null){
			fileTypeOptArr = [];
		}
		if(linkedOptArr == null){
			linkedOptArr = [];
		}
		if(postTypeOptArr == null){
			postTypeOptArr = [];
		}
		if(startDateArr == null && endDateArr == null){
			startDateArr = [];
			endDateArr = [];
		}
		jQuery("#media_scanned_result tr td").find("#att-filter").each(function(){
			var getDiv = jQuery(this).parent().parent();
			if(fileTypeOptArr.length != 0 || linkedOptArr.length != 0 || postTypeOptArr.length != 0 || startDateArr.length != 0 || endDateArr.length != 0){
				filterValue = jQuery(this).val();
				filterValueArr = filterValue.split(',');
				filterValueArr = jQuery.map(filterValueArr, function(n,i){return n.toLowerCase();});
				if(fileTypeOptArr.length != 0){
					found = fileTypeOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found = true;
				}
				if(linkedOptArr.length != 0){
					found1 = linkedOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found1 = true;
				}
				if(postTypeOptArr.length != 0){
					found2 = postTypeOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found2 = true;
				}
				if(startDateArr.length != 0 || endDateArr.length != 0){
					postDate = filterValueArr[1];
					fDate = Date.parse(startDateArr);
				    lDate = Date.parse(endDateArr);
				    cDate = Date.parse(postDate);
				    if((cDate <= lDate && cDate >= fDate)) {
				        found3 = true;
				    }else{
				    	found3 = false;
				    }
				}else{
					found3 = true;
				}
				if(found && found1 && found2 && found3){
					getDiv.show();
					getDiv.find('.sorting_1 input').attr('class', 'chkbox');
					getDiv.find('.sorting_1 input').attr('name', 'check_list');
				}else{
					getDiv.hide();
					getDiv.find('.sorting_1 input').attr('class', 'chkbox1');
					getDiv.find('.sorting_1 input').attr('name', 'check_list1');
				}
			}else{
				getDiv.show();
				getDiv.find('.sorting_1 input').attr('class', 'chkbox');
				getDiv.find('.sorting_1 input').attr('name', 'check_list');
			}
		});
	}
	/* media scan filter end */
</script>
<?php
}else{
	?>
	<div class="WPMC_media_scan">
		<div><h3><?php _e("Media scan page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_string"); ?></h3></div>
	</div>
	<?php
}
?>