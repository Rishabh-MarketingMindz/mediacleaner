<div class="deactivate_block">
  <div class="form-group">
    <label>
      <?php echo __('Media Scan' , 'wp_media_cleaner');?>
    </label>
    <div class="custom_switch">
      <?php
      $media_scan = get_option('media_scan_activate');
      if($media_scan == "yes"){
        $media_checked = "checked";
      }
      ?>
      <input type="checkbox" class="checkbox" id="media_scan" <?php echo  $media_checked; ?>>
      <span class="handle_btn">Off</span> </div>
  </div>
  <div class="form-group">
    <label>
       <?php echo __('Content Scan' , 'wp_media_cleaner');?>
    </label>
    <div class="custom_switch">
      <?php
      $content_scan = get_option('content_scan_activate');
      if($content_scan == "yes"){
        $content_checked = "checked";
      }
      ?>
      <input type="checkbox" class="checkbox" id="content_scan" <?php echo $content_checked; ?>>
      <span class="handle_btn">Off</span> </div>
  </div>
  <div class="form-group">
    <label>
       <?php echo __('Image Optimize' , 'wp_media_cleaner');?>
    </label>
    <div class="custom_switch">
      <?php
      $image_optimize = get_option('image_optimize_activate');
      if($image_optimize == "yes"){
        $optimizer_checked = "checked";
      }
      ?>
      <input type="checkbox" class="checkbox" id="image_optimize" <?php echo $optimizer_checked; ?>>
      <span class="handle_btn">Off</span> </div>
  </div>
  <div class="form-group">
    <label> 
      <?php echo __('Database Cleaner' , 'wp_media_cleaner');?>
    </label>
    <div class="custom_switch">
      <?php
      $database_cleaner = get_option('database_cleaner_activate');
      if($database_cleaner == "yes"){
        $dbcleaner_checked = "checked";
      }
      ?>
      <input type="checkbox" class="checkbox" id="database_cleaner" <?php echo $dbcleaner_checked; ?>>
      <span class="handle_btn">Off</span> </div>
  </div>
  <div class="form-group">
    <label>
        <?php echo __('Backup' , 'wp_media_cleaner');?>
    </label>
    <div class="custom_switch">
      <?php
      $database_backup = get_option('database_backup_activate');
      if($database_backup == "yes"){
        $dbbackup_checked = "checked";
      }
      ?>
      <input type="checkbox" class="checkbox" id="database_backup" <?php echo $dbbackup_checked; ?>>
      <span class="handle_btn">Off</span> </div>
  </div>
</div>
