<?php
$current_dir = dirname(__FILE__);
$current_dir = str_replace("/wp-content/plugins/wp_media_cleaner/lib/media-scan", "", $current_dir);
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';

// check if media scanner page is active
@$media_scan = get_option('media_scan_activate');
@$lisence_activation = get_option('WPMC_lisence_activation');
if($media_scan == "yes" && $lisence_activation == 1){
?>
<style type="text/css">
	/*CSS FOR PROGRESS LOADER BAR*/
	#myProgress {
	  width: 100%;
	  background-color: #fff;
	  border-radius: 5px;
	  overflow: hidden;
	  margin-left: 10px;
	  position: relative;
	}
	#myBar {		
		width: 0%;
		height: 30px;
		background-color: #f24e85;
	}
	.progress_center {
	    display: flex;
	    align-items: center;
	}
	#myProgress #progress_counter {
	    position: absolute;
	    top: 5px;
	    left: 50%;
	    transform: translateX(-50%);
	    color: #000;
	    z-index: 1;
	    margin: 0;
	}
	/*CSS FOR PROGRESS LOADER BAR ENDS */

	.exclude_bulk_media{
		display: none;	
	}
	/*Excluded image css*/
	.yes_excluded .img_name{
		/*background-color: rgba(229, 50, 115, 0.14) !important;*/
		color: #f24e85 !important;
	}
	.ui-timepicker-container.ui-timepicker-standard.ui-timepicker-no-scrollbar{
		z-index: 1100 !important;
	}
	.alert-box {
		display: none;
		position: fixed;
		width: 25%;
		height: 60px;
		z-index: 99999999999;
		left: 50%;
		top: 50%;
		transform: translate(-50%, -50%);
		padding: 15px;
		margin-bottom: 20px;
		border: 1px solid transparent;
		border-radius: 4px;
	}
	/*small loader*/
	.loader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 60px;
		height: 60px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
		margin: 0 auto;
	}
	@-webkit-keyframes spin {
		0% {
			-webkit-transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
		}
		}
		@keyframes spin {
		0% {
			transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
		}
	}
</style>
<div class="alert-box"></div>
<!-- Confirm taking media backup Modal -->
<div id="mediaDeleter" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Do you want to make a backup before deleting your files?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="DeleteFilteredImages('yes')"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" id="no" class="btn btn-red" onclick="DeleteFilteredImages('no')"><?php _e("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>

<!-- Exclude bulk media Modal start -->
<!--
<div id="excludeMediaModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php _e('Are you sure you want to perform this action?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" onclick="excludeMedia()"><?php _e("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
-->
<!-- Exclude bulk media Modal ends -->

<!-- Automatic Image Deleter Modal start -->
<div id="automaticImageDelete" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-12">
        		<h4><?php _e("Select Time Interval For Shedule Image Deletion","wp_media_cleaner"); ?></h4>
        	</div>
			<div class="col-md-2">
				<input type="checkbox" name="delter_is_active" id="delter_is_active" value="yes">
			</div>
			<div class="col-md-5">
				<select class="form-control" id="shedule_interval">
					<option value="hourly"><?php _e("Hourly","wp_media_cleaner"); ?></option>
					<option value="daily"><?php _e("Daily","wp_media_cleaner"); ?></option>
					<option value="monthly"><?php _e("Monthly","wp_media_cleaner"); ?></option>
					<option value="yearly"><?php _e("Yearly","wp_media_cleaner"); ?></option>
				</select>
			</div>
			<div class="col-md-5">
				<input type="text" class="timepicker" id="shedule_time" placeholder="Time">
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" id="yes" class="btn btn-blue" onclick="AutomaticImageDeleter()"><?php _e("Save","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php _e("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Automatic Image Deleter Modal ends -->

<div style="display: none;">
	<!-- All message for the actions (like deltetion, exclusion, optimization...etc) -->
	<span class="action_message_automatic_deleter_disable"><?php _e("Removing Automatic Deleter Cron","wp_media_cleaner"); ?></span>
	<span class="action_message_automatic_deleter"><?php _e("Setting Automatic Deleter","wp_media_cleaner"); ?></span>
	<span class="action_message_deletion"><?php _e("Deleting Files","wp_media_cleaner"); ?></span>
	<span class="action_message_adding_media"><?php _e("Adding Media Files","wp_media_cleaner"); ?></span>
	<span class="action_message_exclusion"><?php _e("Excluding Files","wp_media_cleaner"); ?></span>	
	<span class="action_message_remove_exclusion"><?php _e("Removing Exclusion","wp_media_cleaner"); ?></span>
	<span class="action_message_exclusion_both"><?php _e("Excluding Files/Removing Exclusion","wp_media_cleaner"); ?></span>
	<span class="action_message_additionto_medialibrary"><?php _e("Addind Files To Media Library","wp_media_cleaner"); ?></span>
</div>
<div class="WPMC_media_scan">
	<!-- Rotating logo -->
	<div class="loading" style="display: none">
		<div class="loading_box"><?php _e("Scanning for media files","wp_media_cleaner"); ?><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span> 
			<div class="progress_center">
				<img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
				<div id="myProgress">
					<span id="progress_counter"></span>
					<div id="myBar"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Rotating logo for actions-->
	<div class="action_loader">
	<div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span><?php _e("LOADING...","wp_media_cleaner"); ?></span> </div>
	</div>
	<!-- Only Rotating logo -->
	<div class="loading_rotating" style="display: none;">
		<div class="loading_box"><div class="action_message"></div><span><?php _e("Do not close this window","wp_media_cleaner"); ?></span><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
		</div>
	</div>
  <!-- CMS usage data from w3techs.com / captured 7/6/16 -->
  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="sss" type="button"><?php _e("Scan server","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-blue" id="scan_library" type="button"><?php _e("Scan Media Library","wp_media_cleaner"); ?></button>
      </li>      
      <li> <span class="text"><?php _e("Filter:","wp_media_cleaner"); ?></span>
        <select name="fileTypeOpt[]" multiple id="fileTypeOpt" class="btn btn-blue" style="display: none;">          
        </select>
        <select name="fileTypeOptDemo[]" multiple id="fileTypeOptDemo" class="btn btn-blue">
        	<option><?php echo __("Click on Ready to Scan","wp_media_cleaner"); ?></option>
        </select>
      </li>
      <li>
        <input type="button" name="daterange" id="datePicker" value="Date Picker" class="btn btn-blue" />
      </li>
      <li>
        <select name="linkedOpt[]" size="2" multiple id="linkedOpt" class="btn btn-blue">
          <option value="yes"><?php _e("Yes","wp_media_cleaner"); ?></option>
          <option value="no"><?php _e("No","wp_media_cleaner"); ?></option>
        </select>
      </li>
      <li>
        <select name="postTypeOpt[]" class="btn btn-blue" multiple id="postTypeOpt">
        </select>
      </li>
      <li>
        <label class="gray"><span id="total_files"></span><span id="no_files"><?php _e("No","wp_media_cleaner"); ?></span> <?php _e("media files found","wp_media_cleaner"); ?></label>
        <label class="red"><?php _e("Media size","wp_media_cleaner"); ?> <span id="media_size"></span><span id="no_size">- 0.0 MB</span></label>
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="abcd" class="table table-striped table-sm table_design" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th><input name="show_all_check" type="checkbox"></th>
          <th><?php echo __("Files","wp_media_cleaner"); ?></th>
          <th><?php echo __("Post Type","wp_media_cleaner") ;?></th>
          <th><?php echo __("Upload Date","wp_media_cleaner") ;?></th>
          <th><?php echo __("Linked","wp_media_cleaner"); ?></th>
        </tr>
      </thead>
      <tbody id="media_scanned_result">
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">&nbsp;</td>
			<td colspan="5" align="center"><strong class="red"><?php _e("Ready to scan !","wp_media_cleaner"); ?></strong></td>
			<td style="display: none;">&nbsp;</td>
			<td style="display: none;">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-blue getfilteredimages select" type="button"><?php echo __("Select Media Files","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-red" id="deletefilteredimages" type="button" data-toggle="modal" data-target="#mediaDeleter"><?php _e("Delete Filtered Media","wp_media_cleaner"); ?></button>
      </li>
      <li>
      	<button class="btn btn-red" id="" type="button" data-toggle="modal" data-target="#automaticImageDelete"><?php _e("Automatic Image Deleter","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-red" id="addtomedia" type="button" onclick="GetCheckboxSelected()"><?php _e("Add to Media Library","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-red exclude_bulk_media" type="button" onclick="excludeMedia()"><?php _e("Exclude Media","wp_media_cleaner"); ?></button>
        <button class="btn btn-red show_excluded_media" type="button"><?php _e("Show Excluded Media","wp_media_cleaner"); ?></button>
      </li>
      <li>
        <button class="btn btn-blue" id="showfilteredimages" type="button"><?php _e("Show Filtered Media","wp_media_cleaner"); ?></button>
        <button class="btn btn-blue" id="showAllFilteredImages" type="button"><?php _e("Show All Media","wp_media_cleaner"); ?></button>
      </li>
    </ul>
  </div>
</div>
<script type="text/javascript">
	jQuery(window).load(function(){
		setTimeout(function() {
		    jQuery('.action_loader').fadeOut('fast');
		}, 3000);
	})
	jQuery(document).ready(function() {
		jQuery("#showfilteredimages").click(function(){
			jQuery("#media_scanned_result input[name=check_list]:checkbox:not(:checked)").parent().parent().hide();
			jQuery("#media_scanned_result input[name=check_list1]:checked").parent().parent().show();
		})
		jQuery("#showAllFilteredImages").on('click',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		// Dummy filter for showing before scanning
		jQuery('#fileTypeOptDemo').multiselect({
		    nonSelectedText: '<?php _e('File Type', 'wp_media_cleaner'); ?>',
		    allSelectedText: '<?php _e('File Type', 'wp_media_cleaner'); ?>',
		    onChange: function(option, checked) {
		    	
	        }
		});
		// Disable all action button when no selection
		jQuery("#deletefilteredimages").prop("disabled",true);
	    jQuery("#addtomedia").prop("disabled",true);

		jQuery(document).ready(function(){
		    jQuery('input.timepicker').timepicker({ timeFormat: 'HH:mm:ss' });
		});
	    var $chkboxes = jQuery('.chkbox');
	    var lastChecked = null;
		jQuery(document).on("click",".chkbox",function(e) {    	
			if (!lastChecked) {
			    lastChecked = this;          
			    return;
			}
			if (e.shiftKey) {        	
			    var start = jQuery('.chkbox').index(this);
			    var end = jQuery('.chkbox').index(lastChecked);
			    console.log(end);            
			    jQuery('.chkbox').slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
			}
			lastChecked = this;
		});
	});
	jQuery(document).on("click","input[name=check_list]",function() {
      // Disable action button if checkbox not selected
      var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
      if(atLeastOneIsChecked){
        jQuery("#deletefilteredimages").prop("disabled",false);
	    jQuery("#addtomedia").prop("disabled",false);

	    // Check if selected row is excluded
	    var row_excluded = jQuery('input[name=check_list]:checked').map(function(){
			var has_class = jQuery(this).parent().parent().attr('class');
			if(has_class.includes("yes_excluded")){
				return "excluded";
			}else{
				return "no_excluded";
			}
		}).get();
		var if_excluded = jQuery.inArray( "excluded", row_excluded );
		if(if_excluded != -1){
			jQuery(".exclude_bulk_media").html("Remove Exclusion");
			jQuery(".show_excluded_media").hide();
			jQuery(".exclude_bulk_media").show();
		}else{
			jQuery(".exclude_bulk_media").html("Exclude Media");
			jQuery(".show_excluded_media").hide();
			jQuery(".exclude_bulk_media").show();
		}
      }else{
      	jQuery(".show_excluded_media").show();
      	jQuery(".exclude_bulk_media").hide();
        jQuery("#deletefilteredimages").prop("disabled",true);
	    jQuery("#addtomedia").prop("disabled",true);
      }
    });
	jQuery("#sss").click(function(){
		// Progress bar start
		var i = 0;
		var myvar = 33;
		var width = 0;
		jQuery("#myBar").css("background-color","#f24e85");
		jQuery("#progress_counter").text("0%");
		jQuery("#myBar").css("width","0%");
		jQuery(".loading").show();
		jQuery("#myBar").show();
		if (i == 0) {
		    i = 1;
		    time_var = window.setInterval(function(){
			  myvar = parseInt(myvar) +  1950;
			  var id = setInterval(frame, myvar);
			  jQuery("#progress_counter").empty().text(width+'%');
			  console.log(width);
			  if(width == 100){				  	
			  	clearInterval(id);
			  	clearInterval(time_var);
			  }
			}, 100);
		    var elem = document.getElementById("myBar");			    
		    // var id = setInterval(frame, myvar);
		    function frame() {			    	
				if (width >= 100) {
					// clearInterval(id);
					i = 0;
				} else {
					if (width <= 97 || width == 99) {
						width++;
						elem.style.width = width + "%";
					}
				}
		    }
		}
		// Progress bar ends

		// Deselect All Filter (Linked & Post type filter)
		jQuery("#linkedOpt").multiselect('deselectAll', false);
		jQuery("#linkedOpt").multiselect('refresh');
		jQuery("#postTypeOpt").multiselect('deselectAll', false);
		jQuery("#postTypeOpt").multiselect('refresh');
		// Reset date
		jQuery('input[name="daterange"]').val('Date Picker');
		jQuery('input[name="daterange"]').data('daterangepicker').setStartDate(moment());
		jQuery('input[name="daterange"]').data('daterangepicker').setEndDate(moment());

		// Get all the available extension from database
		var homeurl = '<?php echo $current_dir; ?>';
		jQuery.ajax({
		    url: ajaxurl,
		    type: "POST",
	        data: {
				action: 'wpmc_get_image_extensions',
				type: 'all_media',
				home_url: homeurl
			},
			success: function(response) {
				// console.log(response);
				jQuery('#fileTypeOpt').empty().append(response);
				jQuery('#fileTypeOpt').multiselect({
				    nonSelectedText: '<?php _e('File Type', 'wp_media_cleaner'); ?>',
				    allSelectedText: '<?php _e('File Type', 'wp_media_cleaner'); ?>',
				    onChange: function(option, checked) {				    	
			        }
				});
	          	jQuery('#fileTypeOptDemo').siblings(".btn-group").hide();	          	
	        },
	        error: function (ErrorResponse) {
	            // console.log(ErrorResponse);
	        }
	    });
		// Pauase loader
		var filetypes = jQuery('select[name="fileTypeOpt[]"]').val();
		var linked_status = jQuery('select[name="linkedOpt[]"]').val();
		var daterange = jQuery('input[name="daterange"]').val();
		var posttypefilter = jQuery('select[name="postTypeOpt[]"]').val();
		var daterangearr = daterange.split('-');
		var start_date = daterangearr[0];
		var end_date = daterangearr[1];		
		jQuery('#abcd').dataTable().fnDestroy();
		jQuery('#abcd').DataTable({
	        'ajax': {
				type: 'POST',
				'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/method.php',
				data: {
					request_type: "scan_media",
					"filetypes": filetypes,
					"start_date": start_date,
					"end_date": end_date,
					"linked_status": linked_status,
					"posttypefilter": posttypefilter 
				},
				"dataSrc": function (json) {
					// Deselect All Filter (File type filter)
		          	jQuery("#fileTypeOpt").multiselect('deselectAll', false);
			  		jQuery("#fileTypeOpt").multiselect('refresh');

					jQuery("#media_size").html(json.media_size);
					jQuery("#no_size").hide();
					jQuery("#total_files").html(json.total_files);
					jQuery("#no_files").hide();
					
					if(jQuery.isEmptyObject(json.data)){
						jQuery('.dataTables_empty').text('<?php _e('No media found', 'wp_media_cleaner'); ?>');
					}
					// jQuery("#getfilteredimages").prop("disabled",false);
					return json.data;
				},
				complete: function(){
					// For Progress bar loader
					width = 100;
					jQuery("#myBar").css("background-color","#4DB9AB");
					jQuery("#myBar").css("width","100%");
					setTimeout(function() {
					    jQuery('.loading').fadeOut('fast');
					}, 1000);
					// For Progress bar loader ends
					if(jQuery("#datePicker").val() != 'Date Picker'){
						var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
						var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
						dependFilter(startDate,endDate);
					}else{
						dependFilter();
					}
					setTimeout(function() {
				        // First take backup of all scanned media files
						var backup_media = jQuery("#media_scanned_result").html();
				    }, 2000);
	            },

		    },
		    "paging": false,
		    "info": false,
		    "scrollY": "400px",
        	"scrollCollapse": true,
        	"language": {
		        searchPlaceholder: "Search media",
		    }
	    });
	});
	jQuery("#scan_library").click(function(){
		// Progress bar start
		var counter = 0;
		var myvars = 33;
		var widths = 0;
		jQuery("#myBar").css("background-color","#f24e85");
		jQuery("#progress_counter").text("0%");
		jQuery("#myBar").css("width","0%");
		jQuery(".loading").show();
		jQuery("#myBar").show();
		if (counter == 0) {
		    counter = 1;
		    time_vars = window.setInterval(function(){
			  myvars = parseInt(myvars) +  300;
			  var ids = setInterval(frames, myvars);
			  jQuery("#progress_counter").empty().text(widths+'%');
			  if(widths == 100){				  	
			  	clearInterval(ids);
			  	clearInterval(time_vars);
			  }
			}, 100);
		    var elems = document.getElementById("myBar");			    
		    // var id = setInterval(frames, myvars);
		    function frames() {			    	
				if (widths >= 100) {
					// clearInterval(id);
					counter = 0;
				} else {
					if (widths <= 97 || widths == 99) {
						widths++;
						elems.style.width = widths + "%";
					}
				}
		    }
		}
		// Progress bar ends

		// Deselect All Filter (Linked & Post type filter)
		jQuery("#linkedOpt").multiselect('deselectAll', false);
		jQuery("#linkedOpt").multiselect('refresh');
		jQuery("#postTypeOpt").multiselect('deselectAll', false);
		jQuery("#postTypeOpt").multiselect('refresh');
		// Reset date
		jQuery('input[name="daterange"]').val('Date Picker');
		jQuery('input[name="daterange"]').data('daterangepicker').setStartDate(moment());
		jQuery('input[name="daterange"]').data('daterangepicker').setEndDate(moment());

		// Get all the available extension from database
		var homeurl = '<?php echo $current_dir; ?>';
		jQuery.ajax({
		    url: ajaxurl,
		    type: "POST",
	        data: {
				action: 'wpmc_get_image_extensions',
				type: 'only_media_library',
				home_url: homeurl
			},
			success: function(response) {
				// console.log(response);
				jQuery('#fileTypeOpt').empty().append(response);
				jQuery('#fileTypeOpt').multiselect({
				    nonSelectedText: 'File Type',
				    allSelectedText: 'File Type',
				    onChange: function(option, checked) {				    	
			        }
				});
	          	jQuery('#fileTypeOptDemo').siblings(".btn-group").hide();
	          	// Deselect All Filter (File type filter)
	          	jQuery("#fileTypeOpt").multiselect('deselectAll', false);
		  		jQuery("#fileTypeOpt").multiselect('refresh');
	        },
	        error: function (ErrorResponse) {
	            // console.log(ErrorResponse);
	        }
	    });
		jQuery(".loading").show();
		jQuery('#abcd').dataTable().fnDestroy();
		jQuery('#abcd').DataTable({
	        'ajax': {
				type: 'POST',
				'url': '<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/scan_media_library.php',
				data: {
					request_type: "scan_media"
				},
				"dataSrc": function (json) {
					jQuery("#media_size").html(json.media_size);
					jQuery("#no_size").hide();
					jQuery("#total_files").html(json.total_files);
					jQuery("#no_files").hide();
					if(jQuery.isEmptyObject(json.data)){
						jQuery('.dataTables_empty').text('No media found');
					}
					return json.data;
				},
				complete: function(){
					widths = 100;
					jQuery("#myBar").css("background-color","#4DB9AB");
					jQuery("#myBar").css("width","100%");
					setTimeout(function() {
					    jQuery('.loading').fadeOut('fast');
					}, 1000);

					if(jQuery("#datePicker").val() != 'Date Picker'){
						var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
						var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
						dependFilter(startDate,endDate);
					}else{
						dependFilter();
					}
					setTimeout(function() {
				        // First take backup of all scanned media files
						var backup_media = jQuery("#media_scanned_result").html();
				    }, 2000);
	            },

		    },
		    "paging": false,
		    "info": false,
		    "scrollY": "400px",
        	"scrollCollapse": true,
        	"language": {
		        searchPlaceholder: "Search media",
		    }
	    });
	});
	jQuery("input[name=show_all_check]").change(function(){ 
		if(jQuery(this).prop("checked") == true){
            jQuery("input[name=show_all_check]").prop('checked', true);
            jQuery("input[name=check_list1]").prop('checked', true);
            jQuery("input[name=check_list]").prop('checked', true);
            jQuery('.getfilteredimages').text("Deselect Media Files");
            jQuery('.getfilteredimages').removeClass("select").addClass("deselect");
            jQuery("#deletefilteredimages").prop("disabled",false);
	    	jQuery("#addtomedia").prop("disabled",false);
	    	var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
	    	if(atLeastOneIsChecked){
	    		// Check if selected row is excluded
			    var row_excluded = jQuery('input[name=check_list]:checked').map(function(){
					var has_class = jQuery(this).parent().parent().attr('class');
					if(has_class.includes("yes_excluded")){
						return "excluded";
					}else{
						return "no_excluded";
					}
				}).get();
				var if_excluded = jQuery.inArray( "excluded", row_excluded );
				if(if_excluded != -1){
					jQuery(".exclude_bulk_media").html("Remove Exclusion");
					jQuery(".show_excluded_media").hide();
					jQuery(".exclude_bulk_media").show();
				}else{
					jQuery(".exclude_bulk_media").html("Exclude Media");
					jQuery(".show_excluded_media").hide();
					jQuery(".exclude_bulk_media").show();
				}
	    	}else{
	    		jQuery(".show_excluded_media").show();
      			jQuery(".exclude_bulk_media").hide();
	    	}
        }else{
        	jQuery("input[name=show_all_check]").prop('checked', false);
        	jQuery("input[name=check_list1]").prop('checked', false);
        	jQuery("input[name=check_list]").prop('checked', false);
        	jQuery('.getfilteredimages').text("Select Media Files");
        	jQuery('.getfilteredimages').removeClass("deselect").addClass("select");
        	jQuery("#deletefilteredimages").prop("disabled",true);
	    	jQuery("#addtomedia").prop("disabled",true);

	    	jQuery(".show_excluded_media").show();
      		jQuery(".exclude_bulk_media").hide();
        }		
	});
	// Select Deselect filtered button
	jQuery(document).on("click",".getfilteredimages",function() {
		var text = jQuery(this).hasClass("select");
		if(text){
			jQuery(this).text("Deselect Media Files");
			jQuery(this).removeClass("select").addClass("deselect");
			jQuery("input[name=check_list]").prop('checked', true);
			jQuery("#deletefilteredimages").prop("disabled",false);
	    	jQuery("#addtomedia").prop("disabled",false);
	    	var atLeastOneIsChecked = jQuery('input[name=check_list]:checkbox:checked').length > 0;
	    	if(atLeastOneIsChecked){
	    		// Check if selected row is excluded
			    var row_excluded = jQuery('input[name=check_list]:checked').map(function(){
					var has_class = jQuery(this).parent().parent().attr('class');
					if(has_class.includes("yes_excluded")){
						return "excluded";
					}else{
						return "no_excluded";
					}
				}).get();
				var if_excluded = jQuery.inArray( "excluded", row_excluded );
				if(if_excluded != -1){
					jQuery(".exclude_bulk_media").html("Remove Exclusion");
					jQuery(".show_excluded_media").hide();
					jQuery(".exclude_bulk_media").show();
				}else{
					jQuery(".exclude_bulk_media").html("Exclude Media");
					jQuery(".show_excluded_media").hide();
					jQuery(".exclude_bulk_media").show();
				}
	    	}else{
	    		jQuery(".show_excluded_media").show();
      			jQuery(".exclude_bulk_media").hide();
	    	}
		}else{
			jQuery(this).text("Select Media Files");
			jQuery(this).removeClass("deselect").addClass("select");
			jQuery("input[name=check_list]").prop('checked', false);
			jQuery("input[name=show_all_check]").prop('checked', false);
			jQuery("#deletefilteredimages").prop("disabled",true);
	    	jQuery("#addtomedia").prop("disabled",true);
	    	jQuery(".show_excluded_media").show();
      		jQuery(".exclude_bulk_media").hide();
		}
	});

	// Automatic Image Deleter Script
	function AutomaticImageDeleter(){
		jQuery("#automaticImageDelete").modal('toggle');		
		var if_checked = jQuery("input[name=delter_is_active]:checked").val();
		var shedule_interval = jQuery("#shedule_interval").val();
		var shedule_time = jQuery("#shedule_time").val();
		if(if_checked == 'yes'){
			var the_cron_type = 'add_cron';
			var popup_message = jQuery(".action_message_automatic_deleter").html();
		}else{
			var the_cron_type = 'remove_cron';
			var popup_message = jQuery(".action_message_automatic_deleter_disable").html();
		}
		// showing loader for deletion start
		jQuery(".loading_rotating .action_message").empty().html(popup_message).parent().parent().show();
		// showing loader for deletion ends
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/automatic_image_deleter.php",
			data: {
				time: shedule_time,
				interval: shedule_interval,
				hook: 'AutoDeleteImage',
				cron_type: the_cron_type
			},
			cache: false,
			success: function(response){				
				jQuery(".loading_rotating").hide();
				jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				jQuery("input[name=check_list]").prop('checked', false);
				jQuery("input[name=show_all_check]").prop('checked', false);
				jQuery(".getfilteredimages").text("Select Media Files ");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			},
			error: function (jqXHR, exception) {
				jQuery(".loading_rotating").hide();
				jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+response+'</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				jQuery("input[name=check_list]").prop('checked', false);
				jQuery("input[name=show_all_check]").prop('checked', false);
				jQuery(".getfilteredimages").text("Select Media Files");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			}
		});
	}
	function GetCheckboxSelected(){
		// showing loader for deletion start
		var adding_message = jQuery(".action_message_adding_media").html();
		jQuery(".loading_rotating .action_message").empty().html(adding_message).parent().parent().show();
		// showing loader for deletion ends

		var checkValues = jQuery('input[name=check_list]:checked').map(function()
        {
            return jQuery(this).val();
        }).get();
    	jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/add_to_media.php",
			data: {
				request_type: "add_to_media",
				media_src: checkValues
			},
			cache: false,
			success: function(res){
				jQuery(".loading_rotating").hide();
				var obj = jQuery.parseJSON(res);
				var i = 0;
				jQuery.each(obj, function(key, value ) {
					jQuery("input[type=checkbox][value='"+value+"']").prop("checked", false);
					i++;
				});
				jQuery( ".alert-box" ).addClass('success');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) added to library</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				jQuery("input[name=check_list]").prop('checked', false);
				jQuery("input[name=show_all_check]").prop('checked', false);
				jQuery(".getfilteredimages").text("Select Media Files");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			},
			error: function (res) {
				jQuery(".loading_rotating").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
				jQuery("input[name=check_list]").prop('checked', false);
				jQuery("input[name=show_all_check]").prop('checked', false);
				jQuery(".getfilteredimages").text("Select Media Files");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			}
		});
	}
	function DeleteFilteredImages($backup){
		// showing loader for deletion start
		var deletion_message = jQuery(".action_message_deletion").html();
		jQuery(".loading_rotating .action_message").empty().html(deletion_message).parent().parent().show();
		// showing loader for deletion ends

		jQuery('#mediaDeleter').modal('toggle');
		if($backup == 'yes'){
			var isbackup = 'yes';
		}else{
			var isbackup = 'no';
		}
		var checkValues = jQuery('input[name=check_list]:checked').map(function(){
			return jQuery(this).val();
		}).get();
		var website_prefix = jQuery('input[name=check_list]:checked').map(function(){
			return jQuery(this).data('prefix');
		}).get();
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/delete_media.php",
			data: {
				backup: isbackup,
				media_src: checkValues,
				site_prefix : website_prefix,
				request_type: 'bulk_delete'
			},
			cache: false,
			success: function(response){				
				jQuery(".loading_rotating").hide();
				var obj = jQuery.parseJSON(response);
				if (obj && obj.length > 0) {
					var i = 0;
					jQuery.each(obj, function(key, value ) {
						jQuery("input[type=checkbox][value='"+value+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800);
						i++;
					});
					jQuery( ".alert-box" ).addClass('success');
					jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) deleted</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				}else{
					jQuery( ".alert-box" ).addClass('success');
					jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Selected files are excluded.</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
				}
				jQuery("input[name=check_list]").prop('checked', false);
				jQuery("input[name=show_all_check]").prop('checked', false);
				jQuery(".getfilteredimages").text("Select Media Files");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			},
			error: function (jqXHR, exception) {
				jQuery(".action_loader").hide();
				jQuery( ".alert-box" ).addClass('unsuccess');
				jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>Something went wrong, Please try again</span></div>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
				jQuery("input[name=check_list]").prop('checked', false);
				jQuery("input[name=show_all_check]").prop('checked', false);
				jQuery(".getfilteredimages").text("Select Media Files");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			}
		});
	}
	function excludeMedia(){
		// check if checked row has Excluded files
		var yes, no, action;
		var if_excluded = [];
		jQuery.each(jQuery("input[name='check_list']:checked"), function(){
            var data = jQuery(this).parent().parent().attr('class');
			if(data.includes("yes_excluded")){
				yes = "yes";
			}else{
				no = "no";
			}
			if(yes && no){
				if_excluded = "both";
			}else{
				if(yes){
					if_excluded = "yes";
				}else{
					if_excluded = "no";
				}
			}
        });
		jQuery('#excludeMediaModal').modal('toggle');
		// showing loader for deletion start
		if(if_excluded == "both"){
			action = ".action_message_exclusion_both";
		}
		if(if_excluded == "yes"){
			action = ".action_message_remove_exclusion";
		}
		if(if_excluded == "no"){
			action = ".action_message_exclusion";
		}
		var deletion_message = jQuery(action).html();
		jQuery(".loading_rotating .action_message").empty().html(deletion_message).parent().parent().show();
		// showing loader for deletion ends

		var media_url = jQuery('input[name=check_list]:checked').map(function(){
			return jQuery(this).val();
		}).get();
		jQuery("input[name=check_list]").prop('checked', false);
	    jQuery("input[name=show_all_check]").prop('checked', false);
	    jQuery(".getfilteredimages").text("Select Media Files");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/optimization_action.php",
			// dataType: "json",
			data: {
				type: "excluded_media",
				action: "bulk_media_exclude",
				media_src: media_url

			},
			cache: false,
			success: function(response){				
				var obj = jQuery.parseJSON(response);
				jQuery.each(obj, function(key, value ) {
					if(value.actions == "excluded"){
						jQuery("input[type=checkbox][value='"+value.url+"']").attr('name', 'check_list');
						jQuery("input[type=checkbox][value='"+value.url+"']").parent().parent().addClass("yes_excluded");
						jQuery("input[type=checkbox][value='"+value.url+"']").attr('data-excluded','Excluded');
					}else{
						jQuery("input[type=checkbox][value='"+value.url+"']").attr('name', 'check_list');
						jQuery("input[type=checkbox][value='"+value.url+"']").parent().parent().removeClass("yes_excluded");
						jQuery("input[type=checkbox][value='"+value.url+"']").attr('data-excluded','Exclude');
					}
				});
				jQuery('.loading_rotating').hide();
			    jQuery("input[name=check_list]").prop('checked', false);
			    jQuery("input[name=show_all_check]").prop('checked', false);
			    jQuery(".getfilteredimages").text("Select Media Files fdsdsdfdf");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
			    jQuery(".show_excluded_media").show();
			    jQuery(".exclude_bulk_media").hide();
			},
			error: function (jqXHR, exception) {
	      		jQuery('.loading_rotating').hide();
		    	jQuery("input[name=check_list]").prop('checked', false);
		    	jQuery("input[name=show_all_check]").prop('checked', false);
		    	jQuery(".getfilteredimages").text("Select Media Files dfdfdsfdf");
				jQuery(".getfilteredimages").removeClass("deselect").addClass("select");
				alert("Something went wrong. Please try again.");
				jQuery(".show_excluded_media").show();
			    jQuery(".exclude_bulk_media").hide();
			}
		});
	}
	jQuery( document ).ajaxSuccess(function(event, xhr, options) {			
		try {
		    var parse = jQuery.parseJSON(xhr.responseText);
		    // Add red backkground color to all excluded tr
			jQuery('[data-excluded="Excluded"]').parents("tr").addClass("yes_excluded");
		} catch(e) {
		}
	});
	jQuery(document).ready(function(){
		jQuery.ajax({
		    url: ajaxurl,
	        data: {
				action: 'wpmc_get_cpt',
			},
			success: function(response) {
				jQuery('#postTypeOpt').empty().append(response);
				jQuery('#postTypeOpt').multiselect({
				    nonSelectedText: ' <?php _e('Post Type', 'wp_media_cleaner'); ?>',
				    allSelectedText: '<?php _e('Post Type', 'wp_media_cleaner'); ?>',
				    /*includeSelectAllOption: true,*/
				    onChange: function(option, checked) {
				    	
			        }
				});	          	
	        },
	        error: function (ErrorResponse) {
	            console.log(ErrorResponse);
	        }
	    });
	});
	/* media scan filter start */
	jQuery(document).ready(function(){
		// Show only excluded media
		jQuery(document).on("click",".show_excluded_media",function(e) {
			jQuery("#media_scanned_result *input[name=check_list][data-excluded='Exclude']").map(function(){
				return jQuery(this).attr('name', 'check_list1');
			}).get();			
			jQuery("#media_scanned_result *input[name=check_list1][data-excluded='Exclude']").map(function(){
				return jQuery(this).attr('name', 'check_list1');
			}).get();
			jQuery("#media_scanned_result *input[name=check_list][data-excluded='Exclude']").parent().parent().hide();
			jQuery("#media_scanned_result *input[name=check_list1][data-excluded='Exclude']").parent().parent().hide();

			jQuery("#media_scanned_result *input[name=check_list][data-excluded='Excluded']").parent().parent().show();
			jQuery("#media_scanned_result *input[name=check_list1][data-excluded='Excluded']").parent().parent().show();
		});

		jQuery("#fileTypeOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		jQuery("#linkedOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
		jQuery("#postTypeOpt").on('change',function(){
			if(jQuery("#datePicker").val() != 'Date Picker'){
				var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
				var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
				dependFilter(startDate,endDate);
			}else{
				dependFilter();
			}
		});
	});
	jQuery(document).on("click",".daterangepicker .applyBtn",function() {
		var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
		var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
		dependFilter(startDate,endDate);
	});
	jQuery(document).on("click",".daterangepicker .cancelBtn",function() {
		if(jQuery("#datePicker").val() != 'Date Picker'){
			var startDate = jQuery('#datePicker').data('daterangepicker').startDate.format('YYYY-MM-DD');
			var endDate = jQuery('#datePicker').data('daterangepicker').endDate.format('YYYY-MM-DD');
			dependFilter(startDate,endDate);
		}else{
			dependFilter();
		}
	});
	function dependFilter(startDate,endDate){
		var filterValue, filterValueArr, matchArray, found, found1, found2, found3, fileTypeOpt, linkedOptArr, postTypeOptArr, startDateArr, endDateArr, fDate,lDate,cDate;
		var fileTypeOpt = jQuery("#fileTypeOpt").val();
		var linkedOpt = jQuery("#linkedOpt").val();
		var postTypeOpt = jQuery("#postTypeOpt").val();
		var startDate = startDate;
		var endDate = endDate;
		var mainFilter = [];
		if(fileTypeOpt != null){
			fileTypeOpt = jQuery.map(fileTypeOpt, function(n,i){return n.toLowerCase();});
			mainFilter['fileTypeOpt'] = fileTypeOpt;
		}
		if(linkedOpt != null){
			linkedOpt = jQuery.map(linkedOpt, function(n,i){return n.toLowerCase();});
			mainFilter['linkedOpt'] = linkedOpt;
		}
		if(postTypeOpt != null){
			postTypeOpt = jQuery.map(postTypeOpt, function(n,i){return n.toLowerCase();});
			mainFilter['postTypeOpt'] = postTypeOpt;
		}
		if(startDate != null && endDate != null){
			mainFilter['startDate'] = startDate;
			mainFilter['endDate'] = endDate;
		}
		fileTypeOptArr = mainFilter.fileTypeOpt;
		linkedOptArr = mainFilter.linkedOpt;
		postTypeOptArr = mainFilter.postTypeOpt;
		startDateArr = mainFilter.startDate;
		endDateArr = mainFilter.endDate;
		if(fileTypeOptArr == null){
			fileTypeOptArr = [];
		}
		if(linkedOptArr == null){
			linkedOptArr = [];
		}
		if(postTypeOptArr == null){
			postTypeOptArr = [];
		}
		if(startDateArr == null && endDateArr == null){
			startDateArr = [];
			endDateArr = [];
		}
		jQuery("#media_scanned_result tr td").find("#att-filter").each(function(){
			var getDiv = jQuery(this).parent().parent();
			if(fileTypeOptArr.length != 0 || linkedOptArr.length != 0 || postTypeOptArr.length != 0 || startDateArr.length != 0 || endDateArr.length != 0){
				filterValue = jQuery(this).val();
				filterValueArr = filterValue.split(',');
				filterValueArr = jQuery.map(filterValueArr, function(n,i){return n.toLowerCase();});
				if(fileTypeOptArr.length != 0){
					found = fileTypeOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found = true;
				}
				if(linkedOptArr.length != 0){
					found1 = linkedOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found1 = true;
				}
				if(postTypeOptArr.length != 0){
					found2 = postTypeOptArr.some(r=> filterValueArr.includes(r));
				}else{
					found2 = true;
				}
				if(startDateArr.length != 0 || endDateArr.length != 0){
					postDate = filterValueArr[1];
					fDate = Date.parse(startDateArr);
				    lDate = Date.parse(endDateArr);
				    cDate = Date.parse(postDate);
				    if((cDate <= lDate && cDate >= fDate)) {
				        found3 = true;
				    }else{
				    	found3 = false;
				    }
				}else{
					found3 = true;
				}
				if(found && found1 && found2 && found3){
					getDiv.show();
					getDiv.find('.sorting_1 input').attr('class', 'chkbox');
					getDiv.find('.sorting_1 input').attr('name', 'check_list');
				}else{
					getDiv.hide();
					getDiv.find('.sorting_1 input').attr('class', 'chkbox1');
					getDiv.find('.sorting_1 input').attr('name', 'check_list1');
				}
			}else{
				getDiv.show();
				getDiv.find('.sorting_1 input').attr('class', 'chkbox');
				getDiv.find('.sorting_1 input').attr('name', 'check_list');
			}
		});
	}
	/* media scan filter end */
</script>
<?php
}else{
	if($media_scan != "yes" && $lisence_activation == 1){
		?>
		<div class="WPMC_media_scan">
			<div class="disable_section">
				<h3><?php echo __("Media scanner page is disabled. If you wish to see this page, please enable it from setting page.","wp_media_cleaner"); ?></h3>
				<a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Go to settings","wp_media_cleaner"); ?></a>
			</div>
		</div>
		<?php
	}elseif($media_scan == "yes" && $lisence_activation == 0){
		$admin_url = get_admin_url();
		?>
		<div class="WPMC_media_scan">
			<div class="disable_section">
				<h3><?php echo __("Please activate your license to access media scanner page","wp_media_cleaner"); ?></h3>
				<a href="<?php echo $admin_url; ?>admin.php?page=media-cleaner-setting" class="btn btn-white"><?php echo __("Activate Now","wp_media_cleaner"); ?></a>
			</div>
		</div>
		<?php
	}
	
}
?>