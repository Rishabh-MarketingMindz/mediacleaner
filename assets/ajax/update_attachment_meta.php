<?php
error_reporting(0);
@ini_set('display_errors','Off');
@ini_set('error_reporting', E_ALL );
@define('WP_DEBUG', false);
@define('WP_DEBUG_DISPLAY', false);
require("../../../../../wp-load.php");
global $wpdb;
// get main site prefix
$main_prefix = $wpdb->get_blog_prefix(1);

$title = $_POST['title'];
$alternative = $_POST['alternative'];
$image_caption = $_POST['image_caption'];
$description = $_POST['description'];
$post_id = $_POST['post_id'];
$website_prefix = $_POST['website_prefix'];

$counter=0;
foreach ($title as $filename) {
  $arr[$counter]['filename'] = $filename;
  $counter++;
}

$counter2=0;
foreach ($alternative as $alt_text) {
  $arr[$counter2]['alt_text'] = $alt_text;
  $counter2++;
}

$counter3=0;
foreach ($image_caption as $caption) {
  $arr[$counter3]['caption'] = $caption;
  $counter3++;
}

$counter4=0;
foreach ($description as $img_description) {
  $arr[$counter4]['img_description'] = $img_description;
  $counter4++;
}

$counter5=0;
foreach ($post_id as $postid) {
  $arr[$counter5]['postid'] = $postid;
  $counter5++;
}

foreach ($arr as $media_value) {
  $postid = $media_value['postid'];
  $title = $media_value['filename'];
  $alt_text = $media_value['alt_text'];
  $caption = $media_value['caption'];
  $img_description = $media_value['img_description'];

  $my_post = array(
    'ID'           => $postid,
    'post_title'   => $title,
    'post_content' => $img_description,
    'post_excerpt' => $caption,
  );
  // Update the post into the database
  if($website_prefix == $main_prefix){
    $result = wp_update_post( $my_post );
  }else{
    $table_name = $website_prefix.'posts';
    $result = $wpdb->query($wpdb->prepare('UPDATE `'.$table_name.'` SET `post_content`= "'.$img_description.'", `post_excerpt`="'.$caption.'", `post_title`="'.$title.'" WHERE ID ="'.$postid.'"'));
    $result = 1;
  }
  $res='';
  if($alt_text){
    if($website_prefix == $main_prefix){
      $old_alt = get_post_meta($postid,'_wp_attachment_image_alt',true);
    }else{
      $table_name = $website_prefix.'postmeta';
      $alternative_text = $wpdb->get_results('SELECT meta_value from '.$table_name.' WHERE meta_key = "_wp_attachment_image_alt" AND post_id = "'.$postid.'"');
      $old_alt = $alternative_text[0]->meta_value;
    }   
    if($old_alt == $alt_text){
      $result2 = 'same';
    }else{
      if($website_prefix == $main_prefix){
        $result2 = update_post_meta( $postid, '_wp_attachment_image_alt', $alt_text);
      }else{
        $table_name = $website_prefix.'postmeta';
        if($old_alt){
          $result2 = $wpdb->query($wpdb->prepare('UPDATE '.$table_name.' SET meta_value = "'.$alt_text.'" WHERE post_id ="'.$postid.'" AND meta_key ="_wp_attachment_image_alt"'));
          $result2 = 1;
        }else{
          $result2 = $wpdb->query($wpdb->prepare('INSERT INTO '.$table_name.' (meta_key,meta_value,post_id)VALUES("_wp_attachment_image_alt", "'.$alt_text.'", "'.$postid.'")'));
          $result2 = 1;
        }
        
      }
    }
  }else{
    $result2 = 'empty';
  }

  if($result && $result2){
    $response['success'] = 1;
  }else{
    $response['error'] = 1;
  }
}
echo json_encode($response);