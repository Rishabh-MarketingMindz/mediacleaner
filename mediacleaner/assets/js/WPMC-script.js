jQuery(document).ready(function(){
	jQuery('#dtBasicExample').DataTable();
	jQuery('.dataTables_length').addClass('bs-select');

  jQuery(".WPMC_settings .setting_menu li a").click(function(){
    jQuery(".WPMC_settings .setting_section_block").hide();
    jQuery('.'+jQuery(this).attr('class')+'_block').show();
  });
  /* Credits:
	This bit of code: Exis | exisweb.net/responsive-tables-in-wordpress
	Original idea: Dudley Storey | codepen.io/dudleystorey/pen/Geprd */
  
	var headertext = [];
	var headers = document.querySelectorAll("thead");
	var tablebody = document.querySelectorAll("tbody");

	for (var i = 0; i < headers.length; i++) {
		headertext[i]=[];
		for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) {
		  var current = headrow;
		  headertext[i].push(current.textContent);
		  }
	} 

	for (var h = 0, tbody; tbody = tablebody[h]; h++) {
		for (var i = 0, row; row = tbody.rows[i]; i++) {
		  for (var j = 0, col; col = row.cells[j]; j++) {
		    col.setAttribute("data-th", headertext[h][j]);
		  } 
		}
	}
	jQuery(function() {
		jQuery('input[name="daterange"]').daterangepicker({
		opens: 'right',
		autoUpdateInput: false,
		}, function(start, end, label) {
		console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		});

	   	jQuery('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
	      jQuery(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	  	});

	  	jQuery('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
	      jQuery(this).val('Date Picker');
	  	});
	});

	
	// jQuery('#fileTypeOpt').multiselect({
	//     nonSelectedText: 'File Types',
	//     allSelectedText: 'File Types',
	//     onChange: function(option, checked) {
	    	
 //        }
	// });


	jQuery('#linkedOpt').multiselect({
	    nonSelectedText: 'Linked',
	    onChange: function(option, checked) {
	    	var opselected = jQuery(option).val();
	    	if(opselected == 'yes'){
	    		jQuery('#linkedOpt').multiselect('deselect', 'no');
	    	}else{
	    		jQuery('#linkedOpt').multiselect('deselect', 'yes');
	    	}
	    	console.log(option);
        }
	});
	jQuery('#postStatusOpt').multiselect({
	    nonSelectedText: 'Status',
	    allSelectedText: 'Status',
	    onChange: function(option, checked) {
	    	
        }
	});
	// For backup: multisite dropdown
	jQuery('#multisiteOpt').multiselect({
	    nonSelectedText: 'Multisite',
	    allSelectedText: 'Multisite',
	    onChange: function(option, checked) {
	    	
        }
	});
});