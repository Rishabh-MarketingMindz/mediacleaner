<div class="support_form_block">
  <div class="support_form_detail">
    <p>
      <?= __('Did you found a bug or do you have a request we can implement?') ?>
    <br />
      <?= __('Please let us know and we will be happy to help.') ?>
    </p>
  </div>
  <p>
    <?= __('You can let us know by filling in the form or email us at support@wp-media-cleaner.com') ?>
  </p>
  <form id="support-mail.php" method="post" class="support_form">
<!--   <form id="WPSupportForm" method="post" class="support_form"> -->
    <div class="form-group">
      <label for="name">Name</label>
      <input class="form-control" type="text" id="user_name" name="user_name">
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input class="form-control"  type="email" id="user_email" name="user_email">
    </div>
    <div class="form-group">
      <label for="subject">Subject</label>
      <input class="form-control"  type="text" id="user_subject" name="user_subject">
    </div>
    <div class="form-group">
      <label for="license">License</label>
      <input class="form-control"  type="text" id="user_license" name="user_license" required>
    </div>
    <div class="form-group">
      <label for="message">Message</label>
      <textarea class="form-control"  name="user_message" id="user_message"></textarea>
    </div>
    <div class="form-group">
    <label>&nbsp;</label>
      <input type="submit" name="support" value="Submit">
    </div>
  </form>
</div>
