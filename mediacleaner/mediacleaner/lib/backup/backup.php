<?php
require_once WPMC_MAIN . DIRECTORY_SEPARATOR . 'header.php';

global $wpdb;
$prefixTable = $wpdb->get_blog_prefix();
//$BackupTable = $prefixTable.'mc_backup';

// Bytes to formatSizeUnits
function formatSizeUnits($bytes)
{
  $label = array( 'B', 'KB', 'MB', 'GB');
  for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
  return( round( $bytes, 2 ) . " " . $label[$i] );
}
?>
<style type="text/css">
  /*CSS FOR PROGRESS LOADER BAR*/
  #myProgress {
    width: 100%;
    background-color: #fff;
    border-radius: 5px;
    overflow: hidden;
    margin-left: 10px;
    position: relative;
  }
  #myBar {    
    width: 0%;
    height: 30px;
    background-color: #f24e85;
  }
  .progress_center {
      display: flex;
      align-items: center;
  }
  #myProgress #progress_counter {
      position: absolute;
      top: 5px;
      left: 50%;
      transform: translateX(-50%);
      color: #000;
      z-index: 1;
      margin: 0;
  }
  /*CSS FOR PROGRESS LOADER BAR ENDS */
</style>

<div style="display: none;">
  <!-- All message for the actions (like create backup, restore, deletion...etc) -->
  <span class="action_message_create_backup"><?php echo __("Creating Backup","wp_media_cleaner"); ?></span>
  <span class="action_message_restoring_backup"><?php echo __("Restoring Backup","wp_media_cleaner"); ?></span>
  <span class="action_message_deletion"><?php echo __("Deleting Backup","wp_media_cleaner"); ?></span>
  <span class="action_message_importing_backup"><?php echo __("Importing Backup","wp_media_cleaner"); ?></span>
</div>

<div class="alert-box"></div>

<div id="fullBackupModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12">
            <span id="full_backup_popup"><?php echo __('Are you sure to create full backup ?' ,'wp_media_cleaner') ?></span>
            <span id="filtered_backup_popup"><?php echo  __('Are you sure to create selected filter backup ?','wp_media_cleaner') ?></span>
        </div>
      </div>
      <div class="modal-footer"> 
        <!--input type="submit" class="btn btn-blue" value="Save"-->
        <div class="text-center">
          <button type="submit" id="fullBackupUrl" class="btn btn-blue">
          <?php echo  __('Backup','wp_media_cleaner') ?>
          </button>
          <button type="button" class="btn btn-red" data-dismiss="modal">
          <?php echo  __('Cancel','wp_media_cleaner') ?>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="fullBackupUrlBox" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12"><?php echo  __('If you want replace URL so please enter old URL and New URL ?','wp_media_cleaner') ?></label>
        </div>
        <div class="row" style="margin: 20px 0px 10px 0px">
          <div class="col-sm-12">
            <div class="col-sm-6">
              <label><?php echo __('Old URL','wp_media_cleaner'); ?></label>
              <input type="text" name="old_URL" id="old_URL" placeholder="Please enter old URL" />
            </div>
            <div class="col-sm-6">
              <label><?php echo __('New URL','wp_media_cleaner'); ?></label>
              <input type="text" name="new_URL" id="new_URL" placeholder="Please enter new URL" />
            </div>
          </div>
        </div>
        <!-- When only one multisite is selected -->
        <div class="row only_single_site">
          <label class="col-sm-12">
            <?php echo __('Are you going to restore this backup on single site or multisite ?','wp_media_cleaner'); ?>
          </label>
          <div class="col-md-6"><input type="radio" name="choose_site" value="single" checked> <?php echo __('Single Site','wp_media_cleaner') ?></div>
          <div class="col-md-6"><input type="radio" name="choose_site" value="multi"> <?php echo __('Multi Site','wp_media_cleaner') ?></div>
        </div>
      </div>
      <div class="modal-footer"> 
        <div class="text-center">
          <button type="submit" id="fullBackupConfm" class="btn btn-blue"><?php echo __('Full Backup','wp_media_cleaner') ?></button>
          <button type="button" class="btn btn-red" data-dismiss="modal"><?php echo  __('Cancel','wp_media_cleaner') ?></button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Confirm restoring backup Modal -->
<div id="restorebackupModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php echo __('Are you sure you want to restore this backup? This action cannot UNDO. <br>So please take bakup before proceeding.','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" onclick="restoreBackup()"><?php echo __("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php echo __("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>

<!-- Confirm restoring backup Modal -->
<div id="removeBackupFileModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h4>
          <?php echo __('Are you sure you want to delete selected backup file?','wp_media_cleaner'); ?>
        </h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" id="RemoveBackupFile"><?php echo __("Yes","wp_media_cleaner"); ?></button>
        <button type="button" class="btn btn-red" data-dismiss="modal"><?php echo __("Cancel","wp_media_cleaner"); ?></button>
      </div>
    </div>
  </div>
</div>

<div id="openimportBackupModel" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-md-10"><label class="col-sm-12"><?php echo __('Please Select Backup File with .wpmc File Extention','wp_media_cleaner') ?></label></div>
        <div class="col-sm-2">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
      </div>
      <form method="post" action="<?= WPMC_AJAX . DIRECTORY_SEPARATOR . 'ajax_backup.php' ?>" id="updateMediaData" enctype="multipart/form-data">
      <div class="modal-body" style="position: relative;">
        <div class="row">          
          <div class="col-md-12" style="height: 50px;">
            <input type="hidden" name="action" value="importBackup" />
            <input type="file" name="importBackupFile[]" id="importBackupFile" value="Import Backup file" accept=".wpmc" multiple="multiple" required="required" style="position: absolute;top: 0;width: 95%;height: 60px;">
          </div>
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <button type="submit" id="importBackup" class="btn btn-blue">
        <?php echo __('Import','wp_media_cleaner') ?>
        </button>
        <button type="button" class="btn btn-red" data-dismiss="modal">
        <?php echo __('Cancel','wp_media_cleaner') ?>
        </button>
      </div>
    </div>
    </form>
  </div>
</div>
</div>

<div id="downloadBackupModel" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12 text-center">
            <p id="backupdescription"></p>
          </label>
          <div class="col-sm-12">
            
          </div>
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        <a href="javascript:void(0);" id="downloadBackup" class="btn btn-blue"><?php echo __('Download','wp_media_cleaner') ?></a>
      </div>
    </div>
    
  </div>
</div>
</div>

<div id="backupNotificationModel" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      
      <div class="modal-body">
        <div class="row">
          <label class="col-sm-12 text-center">
            <p id="backupnotificationdescription"></p>
          </label>
          <div class="col-sm-12">
            
          </div>
        </div>
    </div>
    <div class="modal-footer"> 
      <!--input type="submit" class="btn btn-blue" value="Save"-->
      <div class="text-center">
        
      </div>
    </div>
    
  </div>
</div>
</div>


<div class="WPMC_media_scan">
  <!-- All loaders start -->
    <!-- Rotating logo -->
    <div class="loading" style="display: none">
      <div class="loading_box"><div class="action_message_main"></div><span><?php echo __("Do not close this window","wp_media_cleaner"); ?></span> 
        <div class="progress_center">
          <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
          <div id="myProgress">
            <span id="progress_counter"></span>
            <div id="myBar"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Rotating logo for actions-->
    <div class="action_loader">
    <div class="loading_box"> <img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'wpmedicleaner-black-background.svg'; ?>"> <span><?php echo __("LOADING...","wp_media_cleaner"); ?></span> </div>
    </div>
    <!-- Only Rotating logo -->
    <div class="loading_rotating" style="display: none;">
      <div class="loading_box"><div class="action_message"></div><span><?php echo __("Do not close this window","wp_media_cleaner"); ?></span><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>">
      </div>
    </div>
  <!-- All loaders ends -->
  
  <div class="col-md-12">
    <ul class="filter_top">
      <li>
        <button class="btn btn-blue" id="fullBackup" type="button"><?php echo __('Create Full Backup','wp_media_cleaner') ?></button>
      </li>
      <li> 
        <span class="text"><?php echo __('Settings:','wp_media_cleaner')?></span>
        <select name="content[]" multiple id="content" class="btn btn-blue">
          <option value="Pages">Pages</option>
          <option value="Post">Post</option>
          <option value="Custom Post">Custom Posts</option>
        </select>
      </li>
      <!--li>
        <select name="plugins[]" multiple id="plugins" class="btn btn-blue">
          <option value="Yoast SEO">Yoast SEO</option>
          <option value="WP Cache">WP Cache</option>
          <option value="WPML">WPML</option>
        </select>
      </li-->
      <li>
        <?php if( is_multisite()) { 
          $blogids = $wpdb->get_results("SELECT * FROM $wpdb->blogs");
          $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
        ?>
          <select name="multisiteOpt[]" multiple id="multisiteOpt" class="btn btn-blue">
          <?php foreach ($blogids as $value) { ?>
          <option value="<?= $value->blog_id ?>" title="<?= $protocol.$value->domain.$value->path ?>"><?= 'Multisite '.$value->blog_id ?></option>
          <?php   }  ?>
          </select>
          <?php   } ?>
      </li>
      <li>
        <?php
          /*$lastbackup = $wpdb->get_row("SELECT created_at FROM $BackupTable ORDER BY id DESC LIMIT 1");
          if($lastbackup){
            $lastbackupDate = date_create($lastbackup->created_at);
            $lastbackupDate = date_format($lastbackupDate,"d-M-Y");
          }else{
            $lastbackupDate = __('Not Found');
          }*/
        ?>
     <!--    <label class="gray">Last Backup: <?= $lastbackupDate ?></label> -->
      </li>
    </ul>
  </div>
  <div class="col-md-12">
    <table id="backup-datatable" class="table table-striped table-sm table_design no-footer dataTable" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th><?php echo __('Backups' ,'wp_media_cleaner')?></th>
          <th><?php echo __('Backups Content' ,'wp_media_cleaner')?></th>
          <th><?php echo __('Creation Date' ,'wp_media_cleaner')?></th>
          <th><?php echo __('Size' ,'wp_media_cleaner')?></th>
        </tr>
      </thead>
      <tbody id="backup_data_result">
        <?php
          /*$backup_all_data = $wpdb->get_results("SELECT * FROM $BackupTable");
          foreach ($backup_all_data as $backup_data):*/
        ?>
        <!--tr>
          <td><input type="checkbox" name="backupck" value=""></td>
          <td><span class="img_pic"></span><span class="img_name"><span class="backupName"><?= $backup_data->title ?></span>

            <a href="javascript:void(0);" data-backup-name=""><span class="red">restore</span></a>

          </span></td>
          <td><?= $backup_data->content_title ?>
            <div class="bottom_btn"><span class="red"><?= $backup_data->content_type ?></span></div></td>
          <td><?= str_replace(" "," | ",$backup_data->created_at); ?></td>
          <td class="size-optimize"><span class="old-size"><?= formatSizeUnits($backup_data->size) ?></span></td>
        </tr-->
      <?php //endforeach;
        $dir = ABSPATH.'wp-content/plugins/wp_media_cleaner/backup';
             
        if (file_exists($dir) && is_dir($dir) ) {
            $scan_arr = scandir($dir);
            $files_arr = array_diff($scan_arr, array('.','..') );
            foreach ($files_arr as $file) {
             
              $file_path = $dir."/".$file;
              $file_ext = pathinfo($file_path, PATHINFO_EXTENSION);
              if ($file_ext=="wpmc") { ?>
                 <tr>
                    <td><input type="checkbox" name="backupck" value="<?= $file ?>"></td>
                    <td><span class="img_pic"></span><span class="img_name"><span class="backupName"><?= $file ?></span>
                      <a href="javascript:void(0);" class="restoreBackup" data-backup-name="<?= $file ?>"><span class="red"><?= __('Restore','wp_media_cleaner') ?></span></a>

                      <!--a download="<?= $file ?>" href="<?= ABSPATH.'wp-content/plugins/wp_media_cleaner/backup/'.$file ?>"><span class="red"><?= __('Download','wp_media_cleaner') ?></span></a-->
                    </td>
                    <td><?= $file ?>
                    <div class="bottom_btn"><span class="red"><?php echo __("Full Backup","wp_media_cleaner"); ?></span></div></td>
                    <td><?php if(file_exists($file_path)){echo str_replace(" "," | ",date ("F d Y H:i:s.", filemtime($file_path)));}?></td>
                    <td>
                      <?php $filesize=realFileSize($file_path);

                    /*  $filesize=round($filesize / 1024 / 1024, 1);echo $filesize.' MB';*/

                      echo formatSizeUnits($filesize); ?>
                    </td>

                    
                   
                  </tr>

              <?php 
                
              }
              
            }
        }
       ?>
      </tbody>
    </table>
  </div>  
  <div class="col-md-12">
    <ul class="filter_bottom">
      <li>
        <button class="btn btn-red" id="" type="button"><?php echo __('Backup Filtered Content','wp_media_cleaner');?></button>
      </li>
      <li>
        <button class="btn btn-red" type="button" data-toggle="modal" data-target="#restorebackupModal"><?php echo __('Restore Backup file','wp_media_cleaner');?></button>
      </li>
      <li>
        <button class="btn btn-red" id="importBackupModel" type="button"><?php echo __('Import Backup file','wp_media_cleaner');?></button>
      </li>
      <li>
        <button class="btn btn-red" type="button" data-toggle="modal" data-target="#removeBackupFileModal"><?php echo __('Remove Backup file','wp_media_cleaner');?></button>
      </li>
    </ul>
  </div>
</div>
<?php $site_Url = get_site_url().'/wp-admin'; ?>

<?php 

function realFileSize($path)
{
    if (!file_exists($path))
        return false;

    $size = filesize($path);
    
    if (!($file = fopen($path, 'rb')))
        return false;
    
    if ($size >= 0)
    {//Check if it really is a small file (< 2 GB)
        if (fseek($file, 0, SEEK_END) === 0)
        {//It really is a small file
            fclose($file);
            return $size;
        }
    }
    
    //Quickly jump the first 2 GB with fseek. After that fseek is not working on 32 bit php (it uses int internally)
    $size = PHP_INT_MAX - 1;
    if (fseek($file, PHP_INT_MAX - 1) !== 0)
    {
        fclose($file);
        return false;
    }
    
    $length = 1024 * 1024;
    while (!feof($file))
    {//Read the file until end
        $read = fread($file, $length);
        $size = bcadd($size, $length);
    }
    $size = bcsub($size, $length);
    $size = bcadd($size, strlen($read));
    
    fclose($file);
    return $size;
}

// Translated strings
$full_backup = __("Create full backup","wp_media_cleaner");
$filtered_backup = __("Create filtered backup","wp_media_cleaner");
?>
<script type="text/javascript">
  jQuery(window).load(function(){
    setTimeout(function() {
        jQuery('.action_loader').fadeOut('fast');
    }, 1000);
  });

  jQuery(document).ready(function() {
    // Change button text when select filter start
    var full_backup = "<?php echo $full_backup; ?>";
    var filtered_backup = "<?php echo $filtered_backup; ?>";

    jQuery("#content").change(function(){
      var is_sel = jQuery('#content option:selected').length;
      var is_select = jQuery('#multisiteOpt option:selected').length;
      if(is_sel>0 || is_select>0){
        jQuery("#fullBackup").text(filtered_backup);
      }else{
        jQuery("#fullBackup").text(full_backup);
      }
    });

    jQuery("#multisiteOpt").change(function(){
      var is_sel = jQuery('#content option:selected').length;
      var is_select = jQuery('#multisiteOpt option:selected').length;
      if(is_sel>0 || is_select>0){
        jQuery("#fullBackup").text(filtered_backup);
      }else{
        jQuery("#fullBackup").text(full_backup);
      }
    });
    // Change button text when select filter start

    jQuery('#backup-datatable').DataTable({
      "paging":   false,
          "info":     false
    });

    jQuery.ajax({
        url: ajaxurl,
          data: {
        action: 'wpmc_get_cpt',
      },
      success: function(response) {
        jQuery('#content').empty().append(response);
        jQuery('#content').multiselect({
            nonSelectedText: '<?php _e('Content', 'wp_media_cleaner'); ?>',
            allSelectedText: '<?php _e('Content', 'wp_media_cleaner'); ?>',
            /*includeSelectAllOption: true,*/
            onChange: function(option, checked) {
              
              }
        });
        /*jQuery('#postTypeOpt').multiselect('refresh');*/
              
      },
      error: function (ErrorResponse) {
          console.log(ErrorResponse);
      }
    });
  });

  jQuery("#fullBackup").click(function(){
    // Put modal text according to selection
    var is_sel = jQuery('#content option:selected').length;
    var is_select = jQuery('#multisiteOpt option:selected').length;
    if(is_sel>0 || is_select>0){
      jQuery("#full_backup_popup").hide();
      jQuery("#filtered_backup_popup").show();
    }else{
      jQuery("#full_backup_popup").show();
      jQuery("#filtered_backup_popup").hide();
    }
    jQuery('#fullBackupModal').modal('show');
  });

  jQuery("#fullBackupUrl").click(function(){    
    var is_select = jQuery('#multisiteOpt option:selected').length;
    if(is_select>1){
      jQuery(".only_single_site").hide();
    }
    if(is_select == 1){
      jQuery(".only_single_site").show();
    }
    if(is_select == 0){
      jQuery(".only_single_site").hide();
    }

    jQuery('#fullBackupModal').modal('hide');
    jQuery('#fullBackupUrlBox').modal('show');
  });

  jQuery("#fullBackupConfm").click(function(){
    jQuery('#fullBackupUrlBox').modal('hide');

    var is_select = jQuery('#multisiteOpt option:selected').length;
    if(is_select == 1){
      var value_check = jQuery("input[name='choose_site']:checked").val();
    }
    if(is_select == 0 || is_select > 1){
      var value_check = "";
    }

    // Progress bar start
      var i = 0;
      var myvar = 33;
      var width = 0;
      jQuery("#myBar").css("background-color","#f24e85");
      jQuery("#progress_counter").text("0%");
      jQuery("#myBar").css("width","0%");
      jQuery(".loading").show();
      jQuery("#myBar").show();
      if (i == 0) {
          i = 1;
          time_var = window.setInterval(function(){
          myvar = parseInt(myvar) +  1950;
          var id = setInterval(frame, myvar);
          jQuery("#progress_counter").empty().text(width+'%');
          console.log(width);
          if(width == 100){           
            clearInterval(id);
            clearInterval(time_var);
          }
        }, 100);
          var elem = document.getElementById("myBar");          
          // var id = setInterval(frame, myvar);
          function frame() {            
          if (width >= 100) {
            // clearInterval(id);
            i = 0;
          } else {
            if (width <= 97 || width == 99) {
              width++;
              elem.style.width = width + "%";
            }
          }
          }
      }
    // Progress bar ends
    
    // var multisite = JSON.stringify(jQuery('select[name="multisiteOpt[]"]').val());
    var multisite = jQuery('#multisiteOpt option:selected').map(function(){
      return jQuery(this).val();
    }).get();

    if(multisite != null){
      var actionValue = 'multisiteBackup';
      // var actionValue = 'fullBackup';
      var siteids = multisite;
    }else{
      var actionValue = 'fullBackup';
      var siteids = multisite;
    }
    var old_URL = jQuery('#old_URL').val();
    var new_URL = jQuery('#new_URL').val();
   
    var backup_popup_message = jQuery(".action_message_create_backup").html();
    // showing loader for deletion start
    jQuery(".loading .action_message_main").empty().html(backup_popup_message).parent().parent().show();
    // showing loader for deletion ends

    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/ajax_backup.php",
      data: {
        action: actionValue,
        siteids: siteids,
        old_URL: old_URL,
        new_URL: new_URL,
        one_multisite: value_check
      },
      cache: false,
      success: function(response){
        // For Progress bar loader
          width = 100;
          jQuery("#myBar").css("background-color","#4DB9AB");
          jQuery("#myBar").css("width","100%");
          setTimeout(function() {
              jQuery('.loading').fadeOut('fast');
          }, 1000);
        // For Progress bar loader ends

        var filedownloadurl= "http://localhost/mediacleaner/wp-content/plugins/wp_media_cleaner/backup/"+ response +"";
        jQuery('#downloadBackup').attr("href", filedownloadurl);
        jQuery('#downloadBackup').attr("download",response);
        jQuery('#backupdescription').text("Backup created successfully");
        jQuery('#downloadBackupModel').modal('show');
        // location.reload();
      },
      error: function (jqXHR, exception) {
        // For Progress bar loader
          width = 100;
          jQuery("#myBar").css("background-color","#4DB9AB");
          jQuery("#myBar").css("width","100%");
          setTimeout(function() {
              jQuery('.loading').fadeOut('fast');
          }, 1000);
        // For Progress bar loader ends

        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<p><?php echo __('Something went wrong, Please try again', 'wp_media_cleaner'); ?></p>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    }); 
  });

  function restoreBackup(){
    jQuery("#restorebackupModal").modal('toggle');
    var actionValue = 'restoreBackupFile';
    var removeFiles = [];
    jQuery.each(jQuery("#backup_data_result input[name='backupck']:checked"), function(){
        removeFiles.push(jQuery(this).parent().parent().find('.backupName').text());
    });
    var jsonString = JSON.stringify(removeFiles);
    if(removeFiles.length == 0){
      alert(' <?php echo __('Please select a backup to restore.', 'wp_media_cleaner'); ?>');
      return false;
    }

    if(removeFiles.length > 1){
      alert('<?php echo __('Please select single backup to restore.', 'wp_media_cleaner'); ?>');
      return false;
    }
    // Progress bar start
      var i = 0;
      var myvar = 33;
      var width = 0;
      jQuery("#myBar").css("background-color","#f24e85");
      jQuery("#progress_counter").text("0%");
      jQuery("#myBar").css("width","0%");
      
      var restore_popup_message = jQuery(".action_message_restoring_backup").html();
      // showing loader for deletion start
      jQuery(".loading .action_message_main").empty().html(restore_popup_message).parent().parent().show();
      // showing loader for deletion ends
      // jQuery(".loading").show();
      jQuery("#myBar").show();
      if (i == 0) {
          i = 1;
          time_var = window.setInterval(function(){
            myvar = parseInt(myvar) +  1950;
            var id = setInterval(frame, myvar);
            jQuery("#progress_counter").empty().text(width+'%');
            console.log(width);
            if(width == 100){           
              clearInterval(id);
              clearInterval(time_var);
            }
          }, 100);
          var elem = document.getElementById("myBar");          
          // var id = setInterval(frame, myvar);
          function frame() {            
            if (width >= 100) {
              // clearInterval(id);
              i = 0;
            } else {
              if (width <= 97 || width == 99) {
                width++;
                elem.style.width = width + "%";
              }
            }
          }
      }
    // Progress bar ends
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/ajax_backup.php",
      data: {
        action: actionValue,
        backupfile: jsonString
      },
      cache: false,
      success: function(response){
        var object = jQuery.parseJSON(response);
        jQuery(".loading").hide();
        if(object.status == "success"){
          jQuery( ".alert-box" ).removeClass('unsuccess').addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+object.message+'</span></div>').fadeIn( 300 ).delay( 1800 ).fadeOut( 400 );
        }else{
          jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+object.message+'</span></div>').fadeIn( 300 ).delay( 1800 ).fadeOut( 400 );
        }
      },
      error: function (jqXHR, exception) {
        jQuery( ".alert-box" ).removeClass('success').addClass('unsuccess');
        jQuery( ".alert-box" ).html('<p><?php echo __('Something went wrong, Please try again', 'wp_media_cleaner'); ?></p>').fadeIn( 300 ).delay( 1800 ).fadeOut( 500 );
      }
    });
  }

  jQuery(".restoreBackup").click(function(){
    var actionValue = 'restoreBackupFile';
    var removeFiles = [];
    removeFiles.push(jQuery(this).data('backup-name'));
    
    var jsonString = JSON.stringify(removeFiles);

    console.log(jsonString);
    if(removeFiles.length == 0){
      alert('<?php echo __('Please select a backup to restore.', 'wp_media_cleaner'); ?>');
      return false;
    }

    if(removeFiles.length > 1){
      alert('<?php echo __('Please select single backup to restore.', 'wp_media_cleaner'); ?>');
      return false;
    }
    
    if (confirm('<?php echo __('Are you sure to restore backup ?', 'wp_media_cleaner'); ?>')) {
      jQuery(".loading").show();
      jQuery.ajax({
        type: "POST",
        url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/ajax_backup.php",
        data: {
          action: actionValue,
          backupfile: jsonString
        },
        cache: false,
        success: function(response){
          jQuery(".loading").hide();
          console.log(response);
          jQuery('#backupnotificationdescription').text('Successfully Restored !');
          jQuery('#backupNotificationModel').modal('show');
          
        },
        error: function (jqXHR, exception) {
          jQuery( ".alert-box" ).addClass('unsuccess');
          jQuery( ".alert-box" ).html('<p><?php echo __('Something went wrong, Please try again', 'wp_media_cleaner'); ?></p>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
        }
      });
    }
  });

  jQuery(document).on("click","#importBackupModel",function() {
    jQuery('#openimportBackupModel').modal('show');
  });

  jQuery("#RemoveBackupFile").click(function(){
    jQuery("#removeBackupFileModal").modal('toggle');
    var delete_files_popup_message = jQuery(".action_message_deletion").html();
    // showing loader for deletion start
    jQuery(".loading_rotating .action_message").empty().html(delete_files_popup_message).parent().parent().show();
    // showing loader for deletion ends
    var actionValue = 'RemoveBackupFile';
    var removeFiles = [];
    jQuery.each(jQuery("#backup_data_result input[name='backupck']:checked"), function(){
        removeFiles.push(jQuery(this).parent().parent().find('.backupName').text());
    });
    var jsonString = JSON.stringify(removeFiles);
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url('_FILE_')?>wp_media_cleaner/assets/ajax/ajax_backup.php",
      data: {
        action: actionValue,
        files: jsonString
      },
      cache: false,
      success: function(response){
        jQuery(".loading_rotating").hide();
        var obj = jQuery.parseJSON(response);
        if (obj && obj.length > 0) {
          var i = 0;
          jQuery.each(obj, function(key, value ) {
            jQuery("input[type=checkbox][value='"+value+"']").parent().parent().css('background-color','rgba(255, 0, 0, 0.77)').fadeOut(800);
            i++;
          });
          jQuery( ".alert-box" ).addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span>'+i+' media file(s) deleted</span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        }else{
          jQuery( ".alert-box" ).addClass('success');
          jQuery( ".alert-box" ).html('<div class="alert_wrap"><img src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'loading_icon.png'; ?>"><span><?php echo __('Selected files are excluded.', 'wp_media_cleaner'); ?></span></div>').fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        }
        jQuery("input[name=check_list]").prop('checked', false);
      },
      error: function (jqXHR, exception) {
        jQuery( ".alert-box" ).addClass('unsuccess');
        jQuery( ".alert-box" ).html('<p><?php echo __('Something went wrong, Please try again', 'wp_media_cleaner'); ?></p>').fadeIn( 300 ).delay( 1600 ).fadeOut( 500 );
      }
    });
  });

</script>