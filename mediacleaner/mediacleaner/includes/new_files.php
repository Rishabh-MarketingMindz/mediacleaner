<?php
error_reporting(0);
require_once('../../../../wp-load.php');
require_once('../../../../wp-admin/includes/file.php');
// require 'simple_html_dom.php';
  $Domain = get_site_url();
  $getWebsiteLinks = getinboundLinks($Domain);
  function getinboundLinks($domain_name) {
      $res = array();
      $arr = array();
      $url = $domain_name;
      $url_without_www=str_replace('http://','',$url);
      $url_without_www=str_replace('www.','',$url_without_www);
      $url_without_www= str_replace(strstr($url_without_www,'/'),'',$url_without_www);
      $url_without_www=trim($url_without_www);
      $input = @file_get_contents($url) or die('Could not access file: $url');
      $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
      if(preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) {
          // Site URL
          $wp_site_urls = get_site_url();
          foreach($matches as $match) {
              if(strpos($match[2],site_url()) !== false){
                  if (filter_var($match[2], FILTER_VALIDATE_URL) && $match[2] !='mailto:' && strpos($match[2],$wp_site_urls) !== false) {
                      $res[] = $match[2];
                  }
              }
          }
      }
      $data = array();
      $i = 0;
      foreach ($res as $value) {
          $html = file_get_html($value);
          // For Images
          foreach($html->find('img') as $element) {
              if(strpos($element->src,site_url()) !== false){
                  $data[$i]['src'] = $element->src;

                  // Root directory path of WordPress website
                  $wp_root_pathh = get_home_path();
                  // Site URL
                  $wp_site_urll = get_site_url();

                  $dir_path =  str_replace($wp_site_urll.'/',$wp_root_pathh,$data[$i]['src']);
                  $unixtime = filemtime($dir_path);
                  $data[$i]['datetime'] = date("Y-m-d | h:i:s",$unixtime);
                  $str = file_get_contents($value);
                  if(strlen($str)>0){
                      $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
                      preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
                      $data[$i]['title'] = $title[1];
                      $i++;
                  }
              }
          }
      }
      return $data;
  }

  function get_all_directory_and_files($dir){
    global $return;
    $dh = new DirectoryIterator($dir);   
    // Dirctary object
      foreach ($dh as $item) {
         if (!$item->isDot()) {
            if ($item->isDir()) {
                get_all_directory_and_files("$dir/$item");
            } else {
              if($item->isFile() && preg_match("/(\.gif|\.png|\.jpe?g)$/", $item->getFilename())){
                $fullpath = $dir . "/" . $item->getFilename();
                $type= $item->getExtension();
                $return[] = array('path'=>$fullpath,'Type'=>$type);
              }
            }
         }
      }
    return $return;
  }

  // #Call function
  $directory_path = get_home_path();
  $marr = get_all_directory_and_files($directory_path);
  $j = 0;
  foreach ($marr as $get_items) {
    // Complete directory path of images
    $the_path = $get_items['path'];
    // Root directory path of WordPress website
    $wp_root_path = get_home_path();
    // Site URL
    $wp_site_url = get_site_url();

    $response[$j]['src'] =  str_replace($wp_root_path,$wp_site_url,$the_path);
    $unixtime = filemtime($the_path);
    $response[$j]['datetime'] = date("Y-m-d | h:i:s",$unixtime);
    $j++;
  }

// die();
$array1 = $getWebsiteLinks; // website media
$array2 = $response; // directory media (127)

// foreach ($array1 as $value) {
//   $src[] = $value['src'];
//   // $title = $value['title'];
// }

// $counter = 0;
// $counter2 = 0;
// foreach ($src as $websrc) {

//   if(!empty($newArray) && !empty($newArray2)){
//     $array2 = array();
//     $array2 = $newArray;
//     $newArray = array();
//     $newArray2 = array();
//     $counter = 0;
//     $counter2 = 0;
//   }
//   foreach ($array2 as $values) {
//     $mainsrc = $values['src'];
//     $maintitle = $values['datetime'];
//     if($mainsrc != $websrc){
//       $newArray[$counter]['src'] = $mainsrc;
//       $newArray[$counter]['datetime'] = $maintitle;
//       $counter++;
//     }else{
//       $newArray2[$counter2]['src'] = $mainsrc;
//       $newArray2[$counter2]['datetime'] = $maintitle;
//       $counter2++;
//     }
//   }
// }
// echo "<pre>";
// print_r($newArray2);
// echo '<br>-----------------------<br>';
// print_r($newArray);
// die();
$newWebsiteMedia = array();
$counter = 0;
foreach ($array1 as $websrc) {
  $file_src = $websrc['src'];
  foreach ($array2 as $key => $values) {
    $mainsrc = $values['src'];
    $maintitle = $values['datetime'];
    if($mainsrc == $file_src){
      $array2[$key]['title'] = $websrc['title'];
      $array2[$key]['linked'] = 'Yes';
    }else{
      $array2[$key]['title'] = 'From Directory';
      $array2[$key]['linked'] = 'No';
    }
    $counter++;
  }
}
echo "<pre>";
print_r($array2);