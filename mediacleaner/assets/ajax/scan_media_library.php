<?php
error_reporting(0);
@ini_set('display_errors', 0);
require("../../../../../wp-load.php");
require_once('../../../../../wp-admin/includes/file.php');
// require '../../includes/simple_html_dom.php';

function getAllSitePrefix(){
	global $wpdb;
	$prefixes = array();
	$is_multisite = get_sites();
	if($is_multisite){
		$abcd = count($is_multisite);
		$count = 0;
		for ($i=2; $i <= $abcd; $i++) { 
			$prefixes[$count]['prefix'] = $wpdb->get_blog_prefix($i);
			$prefixes[$count]['multisite_id'] = $i;
			$count++;
		}
	}else{
		$prefixes = 
		array(
			0 => array("prefix" => "wp_")
		);
	}
	return $prefixes;
}
function getproductdetails($pid){
	global $wpdb;
	$pde = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE ID = '".$pid."'");
	return $pde[0];
}
function getMultisiteProductDetails($pid,$sitePrefix){
	global $wpdb;
	$pde = $wpdb->get_results("SELECT * FROM ".$sitePrefix."posts WHERE ID = '".$pid."'");
	return $pde[0];
}
function formatSizeUnits($bytes){
    // Calculate size in KB/MB/GB
	if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}
function getattachmentproducts(){
	$mediaexistdata = array();
	global $wpdb;
	$attachmentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where post_type='attachment'");
	if(!empty($attachmentdata)){
		$attachmentarray = array();		
		foreach ($attachmentdata as $key => $attachvalue) {
			$media_id = $attachvalue->ID;
			$media_url = $attachvalue->guid;
			$mediauploaddate = $attachvalue->post_date;
			$media_name = substr($media_url, strrpos($media_url, '/') + 1);
			$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
						
			$attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'upload_date' =>$mediauploaddate,'media_url'=>$media_url,'source_from'=>'database');
		}
	}
	if(!empty($attachmentarray)){
		foreach ($attachmentarray as $value) {
			$media_id = $value['media_id']; 
			$medianame = $value['media_name']; 
			$mediadate = $value['upload_date'];
			$media_url = $value['media_url'];
			$media_type = substr($media_url, strrpos($media_url, '.') + 1);
			$source_from = $value['source_from'];

			$productids = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_thumbnail_id' AND  meta_value ='".$media_id."'");
			$postId = $productids[0]->post_id;
			$getAllMeta = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE post_id = '$postId' AND meta_key = '_variation_description'");
			if($getAllMeta){
				// get variation product details
				$variant_attribute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE post_id = '$postId' AND meta_key LIKE '%attribute_%'");
				$variant_attributes = '';
				foreach ($variant_attribute as $variant_value) {
					$variant_attributes .= $variant_value->meta_value.' | ';
				}
				$attribute = rtrim($variant_attributes,'| ');
				// $attribute_color = get_post_meta($postId, 'attribute_color', true);
				$sku_variant = get_post_meta($postId, '_sku', true);
				if(!empty($productids)){
					$productidsarray = array_column($productids, 'post_id');
					foreach ($productidsarray as $proid) {
						$productdetails = getproductdetails($proid);
						$post_categories = wp_get_post_terms($proid,'product_cat');
						if($post_categories){
							$post_cat = $post_categories[0]->name;
						}
						if(!empty($productdetails)){
							// $linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							$mediaexistdata[] = array(
								'media_id' => $media_id,
								'medianame' => $medianame,
								'src' => $media_url,
								'media_type' => $media_type,
								'title'=> $productdetails->post_title,
								'post_type' => $productdetails->post_type,
								'page_builder_name' => 'Variation Product',
								'post_category' => $post_cat,
								'variant_attribute' => $attribute,
								'variant_sku' => $sku_variant,
								'datetime' => $mediadate,
								'linked' => 'Yes',
								'source_from' => 'database',
								'website_prefix' => $wpdb->prefix
							);
						}
					}
				}
			}else{
				// if(!empty($productids)){
				// 	$productidsarray = array_column($productids, 'post_id');				
				// 	foreach ($productidsarray as $proid) {
				// 		$productdetails = getproductdetails($proid);
				// 		$post_categories = wp_get_post_terms($proid,'product_cat');
				// 		$post_cat = array();
				// 		$post_cats = "";
				// 		if(!empty($post_categories)){
				// 			foreach ($post_categories as $cat_value) {
				// 				$post_cat[] = $cat_value->name;
				// 			}
				// 			$post_cats = implode(",", $post_cat);
				// 		}
				// 		if(!empty($productdetails)){
				// 			$mediaexistdata[] = array(
				// 				'media_id' => $media_id,
				// 				'medianame' => $medianame,
				// 				'src' => $media_url,
				// 				'media_type' => $media_type,
				// 				'title'=> $productdetails->post_title,
				// 				'post_type' => $productdetails->post_type,
				// 				'page_builder_name' => 'Featured Image',
				// 				'post_category' => $post_cats,
				// 				'variant_attribute' => '',
				// 				'variant_sku' => '',
				// 				'datetime' => $mediadate,
				// 				'linked' =>'Yes',
				// 				'source_from' => 'database',
				// 				'website_prefix' => $wpdb->prefix
				// 			);
				// 		}
				// 	}
				// }
			}

			$pgde = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_product_image_gallery'  AND FIND_IN_SET(".$media_id.", meta_value)");
			if(!empty($pgde)){
				$productgids = array_column($pgde, 'post_id');
				foreach ($productgids as $valueg) {
					$productdetailsg = getproductdetails($valueg);
					$post_categories = wp_get_post_terms($valueg,'product_cat');
					if($post_categories){
						$post_cat = $post_categories[0]->name;
					}
					if(!empty($productdetailsg)){
						$mediaexistdata[] = array(
							'media_id' => $media_id,
							'medianame' => $medianame,
							'src' => $media_url,
							'media_type' => $media_type,
							'title'=> $productdetailsg->post_title,
							'post_type' => $productdetailsg->post_type,
							'page_builder_name' => 'Gallery Image',
							'post_category' => $post_cat,
							'variant_attribute' => '',
							'variant_sku' => '',
							'datetime' => $mediadate,
							'linked' =>'Yes',
							'source_from' => 'database',
								'website_prefix' => $wpdb->prefix
						);
					}
				}
			}
		}
	}
	return $mediaexistdata;
}
// Get multisite product's Media
function getMultisiteAttachmentProducts(){
	$mediaexistdata = array();
	global $wpdb;
	$prefixes = getAllSitePrefix();
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;
		$attachmentdata = $wpdb->get_results("SELECT * from ".$prefixValue."posts where post_type='attachment'");
		if(!empty($attachmentdata)){
			$attachmentarray = array();		
			foreach ($attachmentdata as $key => $attachvalue) {
				$media_id = $attachvalue->ID;
				$media_url = $attachvalue->guid;
				$mediauploaddate = $attachvalue->post_date;
				$media_name = substr($media_url, strrpos($media_url, '/') + 1);
				$mediabaseurl = substr($media_url, 0, strrpos( $media_url, '/'));
							
				$attachmentarray[] = array('media_id'=>$media_id, 'media_name' => $media_name, 'upload_date' =>$mediauploaddate,'media_url'=>$media_url,'source_from'=>'database');
			}
		}
		if(!empty($attachmentarray)){
			foreach ($attachmentarray as $value) {
				$media_id = $value['media_id']; 
				$medianame = $value['media_name']; 
				$mediadate = $value['upload_date'];
				$media_url = $value['media_url'];
				$media_type = substr($media_url, strrpos($media_url, '.') + 1);
				$source_from = $value['source_from'];

				$productids = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key = '_thumbnail_id' AND  meta_value ='".$media_id."'");
				$postId = $productids[0]->post_id;
				$getAllMeta = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE post_id = '$postId' AND meta_key = '_variation_description'");				
				if($getAllMeta){
					// get variation product details
					$variant_attribute = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE post_id = '$postId' AND meta_key LIKE '%attribute_%'");
					$variant_attributes = '';
					foreach ($variant_attribute as $variant_value) {
						$variant_attributes .= $variant_value->meta_value.' | ';
					}
					$attribute = rtrim($variant_attributes,'| ');
					// $attribute_color = get_post_meta($postId, 'attribute_color', true);
					// $sku_variant = get_post_meta($postId, '_sku', true);
					$getting_sku_inmeta = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_sku' AND post_id = $postId" );
					$sku_variant = $getting_sku_inmeta[0]->meta_value;

					if(!empty($productids)){
						$productidsarray = array_column($productids, 'post_id');
						foreach ($productidsarray as $proid) {
							$productdetails = getMultisiteProductDetails($proid,$prefixValue);
							// $post_categories = wp_get_post_terms($proid,'product_cat');
							$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$proid."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
							$post_categories = $wpdb->get_results($queryterms, OBJECT);
							$post_cat = array();
							$post_cats = "";
							if(!empty($post_categories)){
								foreach ($post_categories as $cat_value) {
									$post_cat[] = $cat_value->name;
								}
								$post_cats = implode(",", $post_cat);
							}
							if(!empty($productdetails)){
								$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
								$mediaexistdata[] = array(
									'media_id' => $media_id,
									'medianame' => $medianame,
									'src' => $media_url,
									'media_type' => $media_type,
									'title'=> $productdetails->post_title,
									'post_type' => $productdetails->post_type,
									'page_builder_name' => 'Variation Product',
									'post_category' => $post_cats,
									'variant_attribute' => $attribute,
									'variant_sku' => $sku_variant,
									'datetime' => $mediadate,
									'linked' =>$linkedd,
									'source_from' => 'database',
									'website_prefix' => $prefixValue
								);
							}
						}
					}
				}else{
					// if(!empty($productids)){
					// 	$productidsarray = array_column($productids, 'post_id');										
					// 	foreach ($productidsarray as $proid) {
					// 		$productdetails = getMultisiteProductDetails($proid,$prefixValue);
					// 		$post_categories = wp_get_post_terms($proid,'product_cat');
					// 		if($post_categories){
					// 			$post_cat = $post_categories[0]->name;
					// 		}
					// 		if(!empty($productdetails)){
					// 			$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					// 			$mediaexistdata[] = array(
					// 				'media_id' => $media_id,
					// 				'medianame' => $medianame,
					// 				'src' => $media_url,
					// 				'media_type' => $media_type,
					// 				'title'=> $productdetails->post_title,
					// 				'post_type' => $productdetails->post_type,
					// 				'page_builder_name' => 'Featured Image',
					// 				'post_category' => $post_cat,
					// 				'variant_attribute' => '',
					// 				'variant_sku' => '',
					// 				'datetime' => $mediadate,
					// 				'linked' =>$linkedd,
					// 				'source_from' => 'database',
					// 				'website_prefix' => $prefixValue
					// 			);
					// 		}
					// 	}
					// }
				}

				$pgde = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key = '_product_image_gallery'  AND FIND_IN_SET(".$media_id.", meta_value)");
				if(!empty($pgde)){
					$productgids = array_column($pgde, 'post_id');
					foreach ($productgids as $valueg) {
						$productdetailsg = getMultisiteProductDetails($valueg,$prefixValue);
						$post_categories = wp_get_post_terms($valueg,'product_cat');
						if($post_categories){
							$post_cat = $post_categories[0]->name;
						}
						if(!empty($productdetailsg)){
							$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
							$mediaexistdata[] = array(
								'media_id' => $media_id,
								'medianame' => $medianame,
								'src' => $media_url,
								'media_type' => $media_type,
								'title'=> $productdetailsg['post_title'],
								'post_type' => $productdetailsg['post_type'],
								'page_builder_name' => 'Gallery Image',
								'post_category' => $post_cat,
								'variant_attribute' => '',
								'variant_sku' => '',
								'datetime' => $mediadate,
								'linked' =>$linkedd,
								'source_from' => 'database',
								'website_prefix' => $prefixValue
							);
						}
					}
				}
			}
		}
	}
	return $mediaexistdata;
}
// Get Media URL by id
function getImageUrlbyId($imgId){
	global $wpdb;
	$imgIdget = wp_get_attachment_url($imgId);
	if(!empty($imgIdget)){
		return $imgIdget;
	}else{
		return false;
	}
}
// Get Multisite Media URL by id
function getMultiSiteImageUrlbyId($imgId,$prefixValue){
	global $wpdb;
	$imgIdget = $wpdb->get_results("SELECT guid from ".$prefixValue."posts WHERE ID = $imgId" );
	$imgIdget = $imgIdget[0]->guid;
	if(!empty($imgIdget)){
		return $imgIdget;
	}else{
		return false;
	}
}
// Get media from page builder Coded start by Hemant
function getPageBuilderContentMedia(){
	global $wpdb;
	$pbContent = $wpdb->get_results("SELECT * from ".$wpdb->prefix."posts WHERE post_type !='revision' && (post_status = 'publish' OR post_status = 'draft')");
	$uniqueArr = array();
	$inc = 0;
	$uniqueArrs = array();
	$uniqueArrs2 = array();
	$uniqueArrs3 = array();
	$uniqueArred = array();
	// Total types of extensions
	$totalExtensionsAvailable = array('gif','jpg','jpeg','png','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
	foreach ($pbContent as $getvalues) {
		$mediadate = $getvalues->post_date;
		$getFiles = array();
		if(!empty($getvalues)){
			$elem_builder = get_post_meta($getvalues->ID, '_elementor_edit_mode', true);
			$vc_builder = get_post_meta($getvalues->ID, '_wpb_vc_js_status', true);
			$beaver_builder = get_post_meta($getvalues->ID, '_fl_builder_enabled', true);			
			$brizy_builder = !empty(get_post_meta($getvalues->ID, 'brizy', true));
			$oxygen_builder = !empty(get_post_meta($getvalues->ID, 'ct_builder_shortcodes', true));
			$siteorigin_builder = get_post_meta($getvalues->ID, 'panels_data', true);
			$siteorigin_ck_builder = '';

			if(!empty($siteorigin_builder)){
				$siteorigin_ck_builder = "SiteOrigin";
			}
			$featuredImageArr = get_post_meta($getvalues->ID, '_thumbnail_id');
			@$featuredImagesId = $featuredImageArr[0];
			if($featuredImagesId){
				$featuredImageId = getImageUrlbyId($featuredImagesId);
			}else{
				$featuredImageId = '';
			}
			$productgalleryArr = get_post_meta($getvalues->ID, '_product_image_gallery');
			@$productgalleryImg = $productgalleryArr[0];

			$the_title = $getvalues->post_title;
			$post_type = $getvalues->post_type;
			$the_content = $getvalues->post_content;

			if($elem_builder == 'builder'){
				$PageId = $getvalues->ID;
				$elementor_sql = get_post_meta($PageId, '_elementor_data');
				$elementor_content = $elementor_sql[0];
				
				// Get files url from URL & ID tag start
				$group4 = array();
				$group5 = array();
				$getsrcfiles = array();
				preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
				preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
				// Get files url from URL & ID tag end

				// Get all categories
				$post_categories = wp_get_post_terms($PageId,'category');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}

				$imgArry = array();
				foreach ($group4[1] as $ElementorimageUrl) {
					$ElementorimageUrl = stripslashes($ElementorimageUrl);
					$if_file_exist =  str_replace(get_site_url().'/',get_home_path(),$ElementorimageUrl);
					if(file_exists($if_file_exist)){
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $ElementorimageUrl;
						$new_array['medianame'] = basename($ElementorimageUrl);
						$new_array['page_builder_name'] = "Elementor";
						// Getting filetime from dir URL
						$elementor_unixtime = filemtime($if_file_exist);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				// Get files url from href tag start
				$gethreffiles = array();
				preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
				$hreffiles = array();
				if(!empty($gethreffiles[1])){
					foreach ($gethreffiles[1] as $hrefValues) {
						$hrefValues = stripslashes($hrefValues);
						$elementor2_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
						if(file_exists($elementor2_url_dir_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $hrefValues;
							$new_array['medianame'] = basename($hrefValues);
							$new_array['page_builder_name'] = "Elementor";
							// Getting filetime from dir URL
							$elementor2_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
							$elementor2_unixtime = filemtime($elementor2_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$elementor2_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				// Get files url from href tag end

				// Get files src from href tag start
				$srcfiles = array();
				preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
				if(!empty($getsrcfiles[1])){
					foreach ($getsrcfiles[1] as $srcValues) {
						$srcValues = stripslashes($srcValues);
						$elementor3_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$srcValues);
						if(file_exists($elementor3_url_dir_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $srcValues;
							$new_array['medianame'] = basename($srcValues);
							$new_array['page_builder_name'] = "Elementor";
							// Getting filetime from dir URL
							$elementor3_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$srcValues);
							$elementor3_unixtime = filemtime($elementor3_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$elementor3_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				// Get files src from href tag start

				$contentimgid = implode(',', $imgArry);
				if($group5[1][0] != ""){
					$group5arr = explode(',', $group5[1][0]);
					foreach ($group5arr as $imggelid) {
						$getidurlimge  = getImageUrlbyId($imggelid);
						$getidurlimge = stripslashes($getidurlimge);
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$getidurlimge);
						if(file_exists($elementor4_url_dir_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $getidurlimge;
							$new_array['medianame'] = basename($getidurlimge);
							$new_array['page_builder_name'] = "Elementor";
							// Getting filetime from dir URL
							$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$getidurlimge);
							$elementor4_unixtime = filemtime($elementor4_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
				}

				// If have featured image
				if(!empty($featuredImageId)){
					$new_array['media_id'] = $featuredImagesId;
					$new_array['title'] = $the_title;
					$new_array['src'] = $featuredImageId;
					$new_array['medianame'] = basename($featuredImageId);
					$new_array['page_builder_name'] = "Featured Image | Elementor";
					// Getting filetime from dir URL
					$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
					$elementor4_unixtime = filemtime($elementor4_url_dir_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $wpdb->prefix;
					array_push($uniqueArr,$new_array);
				}
			}
			if($vc_builder == 'true'){
				$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
				preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
				preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
				preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
				/* Raw HTML */
				preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
				$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
				if(!empty($RowHtmls)){
					foreach ($RowHtmls as $RowHtml) {
						$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
						$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
						
						preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
						$getFiles[] = str_replace(['?_=1'], [''], $group16[1]);
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				/* Raw HTML */
				$PageId = $getvalues->ID;
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@background-image: url([^"]+)@', $the_content, $group10);
				$bgimgUrlArr = str_replace(['(',')'], ['',''], $group10[1]);
				$bgimgUrl = array();
				foreach ($bgimgUrlArr as $BGvalue) {
					$bgimgUrl[] = substr($BGvalue, 0, strrpos($BGvalue, '?'));
				}
				
				// Merging Arrays Start
				$VCmediaContentPre = array();
				if(!empty($group9[1]) && !empty($bgimgUrl)){
					$VCmediaContentPre = array_merge($group9[1],$bgimgUrl);
				}elseif(!empty($group9[1]) && empty($bgimgUrl)){
					$VCmediaContentPre = $group9[1];
				}elseif(empty($group9[1]) && !empty($bgimgUrl)){
					$VCmediaContentPre = $bgimgUrl;
				}

				$VCmediaContent = array();
				if(!empty($VCmediaContentPre) && !empty($getFiles)){
					$VCmediaContent = array_merge($VCmediaContentPre,$getFiles);
				}elseif(!empty($VCmediaContentPre) && empty($getFiles)){
					$VCmediaContent = $VCmediaContentPre;
				}elseif(empty($VCmediaContentPre) && !empty($getFiles)){
					$VCmediaContent = $getFiles;
				}
				// Merging Arrays Ends
				
				$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
				$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
				$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
				$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

				// Get all categories
				$post_categories = wp_get_post_terms($PageId,'category');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}

				if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
					$VCimageids = '';
					$comma = '';
					if($VCsingleimageids != ""){
						$VCsingleimageidsArr = array();
						$VCsingleimageidsArr = explode(",", $VCsingleimageids);
						foreach ($VCsingleimageidsArr as $VCsingleimageidsArrValue) {
							$VCimages = getImageUrlbyId($VCsingleimageidsArrValue);
							$vc_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimages);
							if(file_exists($vc_url_dir_path)){
								$new_array['media_id'] = $idvalue;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimages;
								$new_array['medianame'] = basename($VCimages);
								$new_array['page_builder_name'] = "Visual Composer";
								// Getting filetime from dir URL
								$vc_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimages);
								$vc_unixtime = filemtime($vc_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$vc_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					if(!empty($VCmediaContent)){
						$incs = 0;
						foreach ($VCmediaContent as $VCmediaContentUrl) {
							// Getting filetime from dir URL
							$vc2_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCmediaContentUrl);
							if(file_exists($vc2_url_dir_path)){
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCmediaContentUrl;
								$new_array['medianame'] = basename($VCmediaContentUrl);
								$new_array['page_builder_name'] = "Visual Composer";
								$vc2_unixtime = filemtime($vc2_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$vc2_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								$incs++;
								array_push($uniqueArr,$new_array);
							}
						}
					}

					if($VCgalleryimageids != ""){
						if($VCimageids != ''){ $comma = ','; }						
						$theGalIds = explode(",", $VCgalleryimageids);
						$inc = 0;
						foreach ($theGalIds as $idvalue) {							
							$VCimage = getImageUrlbyId($idvalue);
							// Getting filetime from dir URL
							$vc3_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage);
							if(file_exists($vc3_url_dir_path)){
								$new_array['media_id'] = $idvalue;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage;
								$new_array['medianame'] = basename($VCimage);
								$new_array['page_builder_name'] = "Visual Composer";
								$vc3_unixtime = filemtime($vc3_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$vc3_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								$inc++;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					
					if($VCcarouselimageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$theGalIds2 = explode(",", $VCcarouselimageids);
						$inc2 = 0;
						foreach ($theGalIds2 as $idvalue2) {
							$VCimage2 = getImageUrlbyId($idvalue2);
							// Getting filetime from dir URL
							$vc4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage2);
							if(file_exists($vc4_url_dir_path)){
								$new_array['media_id'] = $idvalue2;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage2;
								$new_array['medianame'] = basename($VCimage2);
								$new_array['page_builder_name'] = "Visual Composer";
								$vc4_unixtime = filemtime($vc4_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$vc4_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								$inc2++;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					
					if($VChoverboximageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$VChoverboximagesIdArr2 = array();
						$VChoverboximagesIdArr2 = explode(",", $VChoverboximageids);
						foreach ($VChoverboximagesIdArr2 as $VChoverboximagesIdArrValue2) {
							$VCimage3 = getImageUrlbyId($VChoverboximagesIdArrValue2);
							// Getting filetime from dir URL
							$vc5_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage3);
							if(file_exists($vc5_url_dir_path)){
								$new_array['media_id'] = $VChoverboximageids;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage3;
								$new_array['medianame'] = basename($VCimage3);
								$new_array['page_builder_name'] = "Visual Composer";
								$vc5_unixtime = filemtime($vc5_url_dir_path);
								$new_array['datetime'] = date("Y-m-d h:i:s",$vc5_unixtime);
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = 'Yes';
								$new_array['website_prefix'] = $wpdb->prefix;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image | Visual Composer";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
			}
			if($oxygen_builder){
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
				$PageId = $getvalues->ID;
				$the_content = get_post_meta($PageId, 'ct_builder_shortcodes', true);

				preg_match_all('@"url":"([^"]+)"@', $the_content, $WordPressWidgetImages);
				preg_match_all('@"src":"([^"]+)"@', $the_content, $contentimgurl);
				preg_match_all('@"background-image":"([^"]+)"@', $the_content, $contentimgurl1);				
				preg_match_all('@"image_ids":"([^"]+)"@', $the_content, $gelleryimgids);
				preg_match_all('@"code-php":"([^"]+)"@', $the_content, $phpcodes_encode);
				$OxyCodeArr = array();
				foreach ($phpcodes_encode[1] as $phpcode_encode) {
					$phpcodes_decode = base64_decode($phpcode_encode);
					preg_match_all('@(?:src[^>]+>)(.*?)@', $phpcodes_decode, $phpcodesImgUrl);
					$phpcode_encode_url = str_replace(['src=',"'",'"',"/>",">"," type=application/pdf"], ['','','','','',''], $phpcodesImgUrl[0]);
					$OxyCodeArr[] = explode(" ", $phpcode_encode_url[0]);
				}

				$OXImageurls = array();
				foreach ($OxyCodeArr as $OxyCodevalue) {
					$OXImageurls[] = $OxyCodevalue[0];
				}

				$widget_images = array();
				foreach ($WordPressWidgetImages[1] as $WordPressWidgetValue) {
					$widget_images[] = base64_decode($WordPressWidgetValue);
				}

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_gallery);
				// for gallery image
				$urls = '';
				if(!empty($oxy_gallery[1])){					
					foreach ($oxy_gallery[1] as $oxy_gallery_id) {
						$urls .= $oxy_gallery_id.',';
					}
				}
				$imageID = rtrim($urls,",");
				$imageIDs = explode(",", $imageID);
				foreach ($imageIDs as $EachId) {
					$getFiles[] = getImageUrlbyId($EachId);
				}
				
				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);

				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				// Merging arrays
				$OXI1mageurlsArr = array();
				if(!empty($OXImageurls) && !empty($contentimgurl[1])){
					$OXI1mageurlsArr = array_merge($OXImageurls,$contentimgurl[1]);
				}elseif(!empty($OXImageurls) && empty($contentimgurl[1])){
					$OXI1mageurlsArr = $OXImageurls;
				}elseif(empty($OXImageurls) && !empty($contentimgurl[1])){
					$OXI1mageurlsArr = $contentimgurl[1];
				}
				$OXI2mageurlsArr = array();
				if(!empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
					$OXI2mageurlsArr = array_merge($OXI1mageurlsArr,$contentimgurl1[1]);
				}elseif(!empty($OXI1mageurlsArr) && empty($contentimgurl1[1])){
					$OXI2mageurlsArr = $OXI1mageurlsArr;
				}elseif(empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
					$OXI2mageurlsArr = $contentimgurl1[1];
				}

				$OXImageurlsArr = array();
				if(!empty($OXI2mageurlsArr) && !empty($getFiles)){
					$OXImageurlsArr = array_merge($OXI2mageurlsArr,$getFiles);
				}elseif(!empty($OXI2mageurlsArr) && empty($getFiles)){
					$OXImageurlsArr = $OXI2mageurlsArr;
				}elseif(empty($OXI2mageurlsArr) && !empty($getFiles)){
					$OXImageurlsArr = $getFiles;
				}

				$OXImageurlsArrFinal = array();
				if(!empty($OXImageurlsArr) && !empty($widget_images)){
					$OXImageurlsArrFinal = array_merge($OXImageurlsArr,$widget_images);
				}elseif(!empty($OXImageurlsArr) && empty($widget_images)){
					$OXImageurlsArrFinal = $OXImageurlsArr;
				}elseif(empty($OXImageurlsArr) && !empty($widget_images)){
					$OXImageurlsArrFinal = $widget_images;
				}

				$OXimgidsArr = array();
				if(!empty($OXImageurlsArrFinal)){
					foreach ($OXImageurlsArrFinal as $OXImageurl) {
						array_push($OXimgidsArr, $OXImageurl);
					}
				}

				if(!empty($gelleryimgids[1])){
					foreach ($gelleryimgids[1] as $gelleryimgid) {
						$gelleryimgURlbyid = array(getImageUrlbyId($gelleryimgid));
						array_push($OXimgidsArr,$gelleryimgURlbyid);
					}
				}
				$OXimgidsArrUni = array();
				$OXimgidsArrUni = array_unique($OXimgidsArr, SORT_REGULAR);

				// Get all categories
				$post_categories = wp_get_post_terms($PageId,'category');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				foreach ($OXimgidsArrUni as $oxi_url) {
					// Get the created date of this media
			        $dir_path =  str_replace(get_site_url().'/',get_home_path(),$oxi_url);

			        if(file_exists($dir_path)){
			        	$oxy_unixtime = filemtime($dir_path);
			        	$new_array['media_id'] = "";
						$new_array['title'] = $the_title;
						$new_array['src'] = $oxi_url;
						$new_array['medianame'] = basename($oxi_url);
						$new_array['page_builder_name'] = "Oxygen Builder";
						$new_array['datetime'] = date("Y-m-d h:i:s",$oxy_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
			        }
				}
				// If have featured image
				if(!empty($featuredImageId)){
					$new_array['media_id'] = $featuredImagesId;
					$new_array['title'] = $the_title;
					$new_array['src'] = $featuredImageId;
					$new_array['medianame'] = basename($featuredImageId);
					$new_array['page_builder_name'] = "Featured Image | Oxygen Builder";
					// Getting filetime from dir URL
					$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
					$elementor4_unixtime = filemtime($elementor4_url_dir_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $wpdb->prefix;
					array_push($uniqueArr,$new_array);
				}
			}
			if($beaver_builder == 1){
				$getFiles = array();
				$BBmergeArray = array();
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				$PageId = $getvalues->ID;
				preg_match_all('@src="([^"]+)"@', $the_content, $group6);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group7);

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
				
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}
				$beaver_src_array = $group6[1];
				$beaver_mp4_array = $group7[1];
				// Merging arrays
				$BBmergePreArray = array();
				if(!empty($getFiles) && !empty($beaver_src_array)){
					$BBmergePreArray = array_merge(array_unique($beaver_src_array),array_unique($getFiles));
				}elseif(!empty($getFiles) && empty($beaver_src_array)){
					$BBmergePreArray = array_unique($getFiles);
				}elseif(empty($getFiles) && !empty($beaver_src_array)){
					$BBmergePreArray = array_unique($beaver_src_array);
				}

				$BBmergeArray = array();
				if(!empty($BBmergePreArray) && !empty($beaver_mp4_array)){
					$BBmergeArray = array_merge(array_unique($beaver_mp4_array),array_unique($BBmergePreArray));
				}elseif(!empty($BBmergePreArray) && empty($beaver_mp4_array)){
					$BBmergeArray = array_unique($BBmergePreArray);
				}elseif(empty($BBmergePreArray) && !empty($beaver_mp4_array)){
					$BBmergeArray = array_unique($beaver_mp4_array);
				}
				// Get all categories
				$post_categories = wp_get_post_terms($PageId,'category');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				// Beaver Builder Final Array
				foreach ($BBmergeArray as $beaver_urls) {
					$beaver_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$beaver_urls);
					if(file_exists($beaver_url_dir_path)){
						$new_array['media_id'] = "";
						$new_array['title'] = $the_title;
						$new_array['src'] = $beaver_urls;
						$new_array['medianame'] = basename($beaver_urls);
						$new_array['page_builder_name'] = "Beaver Builder";
						$beaver_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$beaver_urls);
						$beaver_unixtime = filemtime($beaver_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$beaver_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				// If have featured image
				if(!empty($featuredImageId)){
					$new_array['media_id'] = $featuredImagesId;
					$new_array['title'] = $the_title;
					$new_array['src'] = $featuredImageId;
					$new_array['medianame'] = basename($featuredImageId);
					$new_array['page_builder_name'] = "Featured Image | Beaver Builder";
					// Getting filetime from dir URL
					$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
					$elementor4_unixtime = filemtime($elementor4_url_dir_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $wpdb->prefix;
					array_push($uniqueArr,$new_array);
				}
			}
			if($brizy_builder){
				$PageId = $getvalues->ID;
				$the_content = get_post_meta($PageId, 'brizy', true);
				$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
				$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
				preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
				preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);

				// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
				preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
				foreach ($case1_gethreffiles[1] as $case1_href_value){
					$case1_href_value = strtok($case1_href_value, " ");
		            $case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$case1_href_value);
					if(file_exists($case1_dir_path)){
						$hrefcase1[] = $case1_href_value;
					}
				}
				// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
				preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
				foreach ($case2_gethreffiles[1] as $case2_href_value) {
					$case2_href_value = strtok($case2_href_value, " ");
		            $case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$case2_href_value);
					if(file_exists($case2_dir_path)){
						$hrefcase2[] = $case2_href_value;
					}
				}				
				// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
				$srcfiles = array();
				preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
				foreach ($case3_gethreffiles[1] as $case3_href_value){
					$case3_href_value = strtok($case3_href_value, " ");
		            $case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$case3_href_value);
					if(file_exists($case3_dir_path)){
						$hrefcase3[] = $case3_href_value;
					}
				}				
				// CASE 4 (data-href) : Get url when pattern is <a data-href=
				preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
				foreach ($case4_gethreffiles[1] as $case4_href_value){					
					$case4_href_value = strtok($case4_href_value, " ");
					$case4_data_href = utf8_decode(urldecode($case4_href_value));
					preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
					$hrefcase4 = array();
					foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
						$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
						$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$theLinkVal);
						if(file_exists($case4_dir_path)){
							$hrefcase4[] = $theLinkVal;
						}
					}					
				}				
				// $the_content = htmlspecialchars($the_content);

				// CASE 5 (video) : Get url when pattern is "video":"http://some-url.mp4"
				preg_match_all('/"video"\h*:.*?\"(.*?)\",(?![^"\n]")/', $brizy_content_str, $videoUrl);
				foreach ($videoUrl[1] as $videoUrlValue) {
					$theVideoVal = str_replace("\",", "", $videoUrlValue);
					$theVideoVals = str_replace(get_site_url().'/',get_home_path(),$theVideoVal);
					if(file_exists($theVideoVals)){
						$hrefcase5[] = $theVideoVal;
					}
				}				

				// CASE 6 (image) : Get url when getting images
				// Merging Arrays Start
				$imagesNeme = array();
				if(!empty($group12[1]) && !empty($group14[1])){
					$imagesNeme = array_merge($group12[1],$group14[1]);
				}elseif(!empty($group12[1]) && empty($group14[1])){
					$imagesNeme = $group12[1];
				}elseif(empty($group12[1]) && !empty($group14[1])){
					$imagesNeme = $group14[1];
				}
				// Merging Arrays Ends
				$hrefcase6 = array();
				if(!empty($imagesNeme)){
					foreach ($imagesNeme as $imageNeme) {
						// Get Media id by Name only for brizy
						$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$wpdb->prefix."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
						if(!empty($imgIdsquery)){
							$imagesIds = getImageUrlbyId($imgIdsquery[0]->post_id);
							$hrefcase6[] = $imagesIds;
						}
					}
				}
				// Merging Arrays Start
				$href_arrayPre1 = array();
				if(!empty($hrefcase1) && !empty($hrefcase2)){
					$href_arrayPre1 = array_merge($hrefcase1,$hrefcase2);
				}elseif(!empty($hrefcase1) && empty($hrefcase2)){
					$href_arrayPre1 = $hrefcase1;
				}elseif(empty($hrefcase1) && !empty($hrefcase2)){
					$href_arrayPre1 = $hrefcase2;
				}

				$href_arrayPre2 = array();
				if(!empty($href_arrayPre1) && !empty($hrefcase3)){
					$href_arrayPre2 = array_merge($href_arrayPre1,$hrefcase3);
				}elseif(!empty($href_arrayPre1) && empty($hrefcase3)){
					$href_arrayPre2 = $href_arrayPre1;
				}elseif(empty($href_arrayPre1) && !empty($hrefcase3)){
					$href_arrayPre2 = $hrefcase3;
				}

				$href_arrayPre3 = array();
				if(!empty($href_arrayPre2) && !empty($hrefcase4)){
					$href_arrayPre3 = array_merge($href_arrayPre2,$hrefcase4);
				}elseif(!empty($href_arrayPre2) && empty($hrefcase4)){
					$href_arrayPre3 = $href_arrayPre2;
				}elseif(empty($href_arrayPre2) && !empty($hrefcase4)){
					$href_arrayPre3 = $hrefcase4;
				}

				$href_arrayPre4 = array();
				if(!empty($href_arrayPre3) && !empty($hrefcase5)){
					$href_arrayPre4 = array_merge($href_arrayPre3,$hrefcase5);
				}elseif(!empty($href_arrayPre3) && empty($hrefcase5)){
					$href_arrayPre4 = $href_arrayPre3;
				}elseif(empty($href_arrayPre3) && !empty($hrefcase5)){
					$href_arrayPre4 = $hrefcase5;
				}

				$href_array = array();
				if(!empty($href_arrayPre4) && !empty($hrefcase6)){
					$href_array = array_merge($href_arrayPre4,$hrefcase6);
				}elseif(!empty($href_arrayPre4) && empty($hrefcase6)){
					$href_array = $href_arrayPre4;
				}elseif(empty($href_arrayPre4) && !empty($hrefcase6)){
					$href_array = $hrefcase6;
				}
				// Merging Arrays Ends

				if(!empty($href_array)){
					// Get all categories
					$post_categories = wp_get_post_terms($PageId,'category');
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					foreach ($href_array as $brizy_value) {
						$hrefValues = stripslashes($brizy_value);
						$brizy_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
						if(file_exists($brizy_url_dir_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $hrefValues;
							$new_array['medianame'] = basename($hrefValues);
							$new_array['page_builder_name'] = "Brizy";
							$brizy_unixtime = filemtime($brizy_url_dir_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$brizy_unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = 'Yes';
							$new_array['website_prefix'] = $wpdb->prefix;
							array_push($uniqueArr,$new_array);
						}
					}
					// If have featured image
					if(!empty($featuredImageId)){
						$new_array['media_id'] = $featuredImagesId;
						$new_array['title'] = $the_title;
						$new_array['src'] = $featuredImageId;
						$new_array['medianame'] = basename($featuredImageId);
						$new_array['page_builder_name'] = "Featured Image | Brizy";
						// Getting filetime from dir URL
						$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
						$elementor4_unixtime = filemtime($elementor4_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
			}
			if($siteorigin_ck_builder == "SiteOrigin"){
				$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
				$PageId = $getvalues->ID;
				$the_content = get_post_meta($PageId, 'panels_data', true);		
				$siteorigenArray = array();		
				foreach ($the_content['widgets'] as $valueid) {
					// $siteorigenArray = array();
					// $getContentUrl = array();
					if(array_key_exists("attachment_id", $valueid)){
						$sitegetimgurlattachment_id = getImageUrlbyId($valueid['attachment_id']);
						array_push($siteorigenArray, $sitegetimgurlattachment_id);
					}
					if(array_key_exists("ids", $valueid)){
						foreach ($valueid['ids'] as $galleryimg) {
							$sitegetimgurlids = getImageUrlbyId($galleryimg);
							array_push($siteorigenArray, $sitegetimgurlids);
						}
					}
					if(array_key_exists("features", $valueid)){
						foreach ($valueid['features'] as $features_img) {
							$sitegetimgurlfeatures = getImageUrlbyId($features_img['icon_image']);
							array_push($siteorigenArray, $sitegetimgurlfeatures);
						}
					}
					if(array_key_exists("image", $valueid)){
						$sitegetimgurlimage = getImageUrlbyId($valueid['image']);
						array_push($siteorigenArray, $sitegetimgurlimage);
					}
					if(array_key_exists("frames", $valueid)){
						foreach ($valueid['frames'] as $frames_img) {
							$sitegetimgurlframes = getImageUrlbyId($frames_img['background_image']);
							array_push($siteorigenArray, $sitegetimgurlframes);
						}
					}
					if(array_key_exists("content", $valueid)){
						$custom_html = $valueid['content'];
						preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
						foreach ($contentimg[1] as $imgurl){
							array_push($siteorigenArray, $imgurl);
						}
					}
					if(array_key_exists("content", $valueid)){
						preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
						foreach ($contentimgs[1] as $imgurls){
							array_push($siteorigenArray, $imgurls);
						}
					}			
					if(array_key_exists("text", $valueid)){
						$siteEditor = $valueid['text'];
						preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
						preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
						$groupsfilehref = $groupsfile[1];
						$groupsfilesrc = $srcgroupsfile[1];
						if(!empty($groupsfilehref)){
							foreach ($groupsfilehref as $filesvalue) {								
								$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvalue;
							    }
							}
						}
						if(!empty($groupsfilesrc)){
							foreach ($groupsfilesrc as $filesvaluesrc) {
								$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvaluesrc;
							    }
							}
						}
						preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
						preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
						preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
						preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
						preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
						preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
						preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
						preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
						preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
						preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
						preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
						preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
						preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
						preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
						preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
						preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
						preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
						preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
						preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
						// for vc video
						if(!empty($vc_video_url[1])){
							foreach ($vc_video_url[1] as $vc_video) {
								$getContentUrl[] = $vc_video;
							}
						}
						// for src
						if(!empty($group9[1])){
							foreach ($group9[1] as $group9s) {
								$getContentUrl[] = $group9s;
							}
						}
						// for mp3
						if(!empty($group11[1])){
							foreach ($group11[1] as $group11s) {
								$getContentUrl[] = $group11s;
							}
						}
						// for mp4
						if(!empty($group12[1])){
							foreach ($group12[1] as $group12s) {
								$getContentUrl[] = $group12s;
							}
						}
						// for pdf
						if(!empty($group13[1])){
							foreach ($group13[1] as $group13s) {
								$getContentUrl[] = $group13s;
							}
						}
						// for docx
						if(!empty($group14[1])){
							foreach ($group14[1] as $group14s) {
								$getContentUrl[] = $group14s;
							}
						}
						// for doc
						if(!empty($group15[1])){
							foreach ($group15[1] as $group15s) {
								$getContentUrl[] = $group15s;
							}
						}
						// for ppt
						if(!empty($group16[1])){
							foreach ($group16[1] as $group16s) {
								$getContentUrl[] = $group16s;
							}
						}
						// for xls
						if(!empty($group17[1])){
							foreach ($group17[1] as $group17s) {
								$getContentUrl[] = $group17s;
							}
						}
						// for pps
						if(!empty($group18[1])){
							foreach ($group18[1] as $group18s) {
								$getContentUrl[] = $group18s;
							}
						}
						// for ppsx
						if(!empty($group19[1])){
							foreach ($group19[1] as $group19s) {
								$getContentUrl[] = $group19s;
							}
						}
						// for xlsx
						if(!empty($group20[1])){
							foreach ($group20[1] as $group20s) {
								$getContentUrl[] = $group20s;
							}
						}
						// for odt
						if(!empty($group21[1])){
							foreach ($group21[1] as $group21s) {
								$getContentUrl[] = $group21s;
							}
						}
						// for ogg
						if(!empty($group22[1])){
							foreach ($group22[1] as $group22s) {
								$getContentUrl[] = $group22s;
							}
						}
						// for m4a
						if(!empty($group23[1])){
							foreach ($group23[1] as $group23s) {
								$getContentUrl[] = $group23s;
							}
						}
						// for wav
						if(!empty($group24[1])){
							foreach ($group24[1] as $group24s) {
								$getContentUrl[] = $group24s;
							}
						}
						// for mp4
						if(!empty($group25[1])){
							foreach ($group25[1] as $group25s) {
								$getContentUrl[] = $group25s;
							}
						}
						// for mov
						if(!empty($group26[1])){
							foreach ($group26[1] as $group26s) {
								$getContentUrl[] = $group26s;
							}
						}
						// for avi
						if(!empty($group27[1])){
							foreach ($group27[1] as $group27s) {
								$getContentUrl[] = $group27s;
							}
						}
						// for 3gp
						if(!empty($group28[1])){
							foreach ($group28[1] as $group28s) {
								$getContentUrl[] = $group28s;
							}
						}
						// for pptx
						if(!empty($group29[1])){
							foreach ($group29[1] as $group29s) {
								$getContentUrl[] = $group29s;
							}
						}
					}
					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					if(!empty($group10)){
						$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
						if(!empty($bgimgUrl1)){
							foreach ($bgimgUrl1 as $filesvaluebackground) {								
								$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvaluebackground;
							    }
							}
						}
					}
					$VCmediaContent = array_unique($getContentUrl);
					$siteorigenArray = array_unique($siteorigenArray);
					$siteOriginUrl = array();
					if(!empty($VCmediaContent) && !empty($siteorigenArray)){
						$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
					}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
						$siteOriginUrl = $VCmediaContent;
					}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){
						$siteOriginUrl = $siteorigenArray;
					}
					$final_array = array();
					foreach ($siteOriginUrl as $thevalues) {
						array_push($final_array, $thevalues);
					}
				}
				// Get all categories
				$post_categories = wp_get_post_terms($PageId,'category');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				foreach ($final_array as $siteOriginUrlValue) {
					$SiteOriginimageUrl = stripslashes($siteOriginUrlValue);
					$siteorigin_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$SiteOriginimageUrl);
					if(file_exists($siteorigin_url_dir_path)){
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $SiteOriginimageUrl;
						$new_array['medianame'] = basename($SiteOriginimageUrl);
						$new_array['page_builder_name'] = "Site Origin";

						$unixtime = filemtime($siteorigin_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = 'Yes';
						$new_array['website_prefix'] = $wpdb->prefix;
						array_push($uniqueArr,$new_array);
					}
				}
				// If have featured image
				if(!empty($featuredImageId)){
					$new_array['media_id'] = $featuredImagesId;
					$new_array['title'] = $the_title;
					$new_array['src'] = $featuredImageId;
					$new_array['medianame'] = basename($featuredImageId);
					$new_array['page_builder_name'] = "Featured Image | Site Origin";
					// Getting filetime from dir URL
					$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
					$elementor4_unixtime = filemtime($elementor4_url_dir_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $wpdb->prefix;
					array_push($uniqueArr,$new_array);
				}
			}
			if($getvalues->post_type == 'product'){
				$PageId = $getvalues->ID;
				// Get thumbnail ID (For featured image)
				$productids = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key = '_thumbnail_id' AND  post_id ='".$PageId."'");
				$media_id = $productids[0]->meta_value;				
				$post_categories = wp_get_post_terms($PageId,'product_cat');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}

				$getidurlimge  = getImageUrlbyId($media_id);
				$product_feature_image = stripslashes($getidurlimge);
				$product_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$product_feature_image);
				if(file_exists($product_url_dir_path)){
					$new_array['media_id'] = $media_id;
					$new_array['title'] = $the_title;
					$new_array['src'] = $product_feature_image;
					$new_array['medianame'] = basename($product_feature_image);
					$new_array['page_builder_name'] = "Featured Image";
					$unixtime = filemtime($product_url_dir_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $wpdb->prefix;
					array_push($uniqueArr,$new_array);
				}
			}
			if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $getvalues->post_type != 'product_variation' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder){
				$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');				
				$the_content = $getvalues->post_content;			
				$PageId = $getvalues->ID;

				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for wmv
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}
				
				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

				if(!empty($gutenbergImgUrls) && !empty($getFiles)){
					$totalContentMedia = array_unique(array_merge($getFiles,$gutenbergImgUrls));
				}else{
					if(!empty($getFiles)){
						$totalContentMedia = array_unique($getFiles);
					}elseif(!empty($gutenbergImgUrls)){
						$totalContentMedia = array_unique($gutenbergImgUrls);
					}else{
						$totalContentMedia = array();
					}
				}

				// Get all categories
				$post_categories = wp_get_post_terms($PageId,'category');
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				foreach ($totalContentMedia as $gutenbergImgUrl) {
					$gutenberg_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$gutenbergImgUrl);
					if(file_exists($gutenberg_url_dir_path)){
						$guten_unixtime = filemtime($gutenberg_url_dir_path);
						$mediadatetime = date("Y-m-d h:i:s",$guten_unixtime);
						$uniqueArr[] = array(
							'media_id' => $PageId,
							'medianame' => basename($gutenbergImgUrl),
							'src' => $gutenbergImgUrl,
							'media_type' => "",
							'title'=> $the_title,
							'post_type' => $post_type,
							'page_builder_name' => 'Simple/Gutenberg Content Media',
							'post_category' => $post_cats,
							'variant_attribute' => '',
							'variant_sku' => '',
							'datetime' => $mediadatetime,
							'linked' =>'Yes',
							'source_from' => 'database',
							'website_prefix' => $wpdb->prefix
						);
					}
				}
				// If have featured image
				if(!empty($featuredImageId)){
					$new_array['media_id'] = $featuredImagesId;
					$new_array['title'] = $the_title;
					$new_array['src'] = $featuredImageId;
					$new_array['medianame'] = basename($featuredImageId);
					$new_array['page_builder_name'] = "Featured Image";
					// Getting filetime from dir URL
					$elementor4_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$featuredImageId);
					$elementor4_unixtime = filemtime($elementor4_url_dir_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$elementor4_unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $wpdb->prefix;
					array_push($uniqueArr,$new_array);
				}
			}
		}
	}
	return $uniqueArr;
}
$PageBuilderMedia = getPageBuilderContentMedia();
// Get media from page builder ends Coded by Hemant

// Get multsite media from page builder Coded start by Hemant
function getMultiSitePageBuilderContentMedia(){
	global $wpdb;
	$uniqueArr = array();
	$new_array = array();
	$prefixes = getAllSitePrefix();
	// Total types of extensions
	$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp');
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		foreach (get_sites() as $all_sites) {
			if($all_sites->blog_id == $multisiteId){
				$multisite_url = $all_sites->path;
			}
			if($all_sites->blog_id == 1){
				$mainsite_url = $all_sites->path;
			}
        }
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;
		$pbContent = $wpdb->get_results("SELECT * from ".$prefixValue."posts WHERE post_type !='revision' && (post_status = 'publish' OR post_status = 'draft')");
		$inc = 0;
		foreach ($pbContent as $getvalues) {
			$getFiles = array();
			$the_title = $getvalues->post_title;
			$post_type = $getvalues->post_type;
			$mediadate = $getvalues->post_date;
			$oxygen_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id=$getvalues->ID" );
			$oxygen_builder = $oxygen_builder[0]->meta_value;

			$elem_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_edit_mode' AND post_id=$getvalues->ID" );
			$elem_builder = $elem_builder[0]->meta_value;

			$vc_builder = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_wpb_vc_js_status' AND post_id=$getvalues->ID" );
			$vc_builder = $vc_builder[0]->meta_value;

			$siteorigin_builders = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id=$getvalues->ID" );
			$siteorigin_builder = $siteorigin_builders[0]->meta_value;

			$siteorigin_ck_builder = '';
			if(!empty($siteorigin_builder)){
				$siteorigin_ck_builder = "SiteOrigin";
			}

			$beaver_builders = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_fl_builder_enabled' AND post_id=$getvalues->ID" );
			$beaver_builder = $beaver_builders[0]->meta_value;

			$featuredImageArrs = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_thumbnail_id' AND post_id=$getvalues->ID" );
			$featuredImageArr = $featuredImageArrs[0]->meta_value;

			@$featuredImagesId = $featuredImageArr[0];
			if($featuredImagesId){
				$featuredImageId = getMultiSiteImageUrlbyId($featuredImagesId,$prefixValue);
			}else{
				$featuredImageId = '';
			}
			
			$productgalleryArrs = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_product_image_gallery' AND post_id=$getvalues->ID" );
			@$productgalleryImg = $productgalleryArrs[0];

			if($elem_builder == 'builder'){
				$PageId = $getvalues->ID;
				$elementor_sql = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='_elementor_data' AND post_id=$PageId" );
				$elementor_content = $elementor_sql[0]->meta_value;
				
				// Get files url from URL & ID tag start
				$group4 = array();
				$group5 = array();
				$getsrcfiles = array();
				preg_match_all('@"url":"([^"]+)"@', $elementor_content, $group4);			
				preg_match_all('@"ids":"([^"]+)"@', $elementor_content, $group5);
				// Get files url from URL & ID tag end

				$imgArry = array();
				$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
				// Get all category of the post
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				foreach ($group4[1] as $ElementorimageUrl) {
					$ElementorimageUrl = stripslashes($ElementorimageUrl);
					// Getting filetime from dir URL
					$elementor_url_dir_path4 =  str_replace(get_site_url().'/',get_home_path(),$ElementorimageUrl);
					$img_home_path4 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path4);
					if(file_exists($img_home_path4)){
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $ElementorimageUrl;
						$new_array['medianame'] = basename($ElementorimageUrl);
						$new_array['page_builder_name'] = "Elementor";
						$unixtime4 = filemtime($img_home_path4);
						$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime4);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = $linkedd;
						$new_array['website_prefix'] = $prefixValue;
						array_push($uniqueArr,$new_array);
					}
				}
				// Get files url from href tag start
				$gethreffiles = array();
				preg_match_all('/href\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_sql[0], $gethreffiles);
				$hreffiles = array();
				if(!empty($gethreffiles[1])){
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					foreach ($gethreffiles[1] as $hrefValues) {
						$hrefValues = stripslashes($hrefValues);
						// Getting filetime from dir URL
						$elementor_url_dir_path3 =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
						$img_home_path3 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path3);
						if(file_exists($img_home_path3)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $hrefValues;
							$new_array['medianame'] = basename($hrefValues);
							$new_array['page_builder_name'] = "Elementor href";
							$unixtime3 = filemtime($img_home_path3);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime3);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				// Get files url from href tag end

				// Get files src from href tag start
				$srcfiles = array();
				preg_match_all('/src\h*=.*?\"(.*?)\"(?![^"\n]")/', $elementor_content, $getsrcfiles);
				if(!empty($getsrcfiles[1])){
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					foreach ($getsrcfiles[1] as $srcValues) {
						$srcValues = stripslashes($srcValues);
						// Getting filetime from dir URL
						$elementor_url_dir_path2 =  str_replace(get_site_url().'/',get_home_path(),$srcValues);
						$img_home_path2 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path2);
						if(file_exists($img_home_path2)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $srcValues;
							$new_array['medianame'] = basename($srcValues);
							$new_array['page_builder_name'] = "Elementor href";
							$unixtime2 = filemtime($img_home_path2);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime2);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				// Get files src from href tag start
				$contentimgid = implode(',', $imgArry);
				if($group5[1][0] != ""){
					$group5arr = explode(',', $group5[1][0]);
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					foreach ($group5arr as $imggelid) {						
						$getidurlimge  = getMultiSiteImageUrlbyId($imggelid,$prefixValue);
						$getidurlimge = stripslashes($getidurlimge);
						// Getting filetime from dir URL
						$elementor_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$getidurlimge);
						$img_home_path = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path);
						if(file_exists($img_home_path)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $getidurlimge;
							$new_array['medianame'] = basename($getidurlimge);
							$new_array['page_builder_name'] = "Elementor href";

							$unixtime = filemtime($img_home_path);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;
							array_push($uniqueArr,$new_array);
						}
					}
				}
				return $uniqueArr;
			}
			if($vc_builder == 'true'){
				$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				preg_match_all('@vc_single_image image="([^"]+)"@', $the_content, $group1);
				preg_match_all('@vc_gallery interval="([^"]+)" images="([^"]+)"@', $the_content, $group2);			
				preg_match_all('@vc_images_carousel images="([^"]+)"@', $the_content, $group3);
				preg_match_all('@vc_hoverbox image="([^"]+)"@', $the_content, $group4);
				/* Raw HTML */
				preg_match_all('@vc_raw_html([^"]+)/vc_raw_html@', $the_content, $group15);
				$RowHtmls = str_replace([']','['], ['',''], $group15[1]);
				if(!empty($RowHtmls)){
					foreach ($RowHtmls as $RowHtml) {
						$RowContent = rawurldecode( base64_decode( wp_strip_all_tags( $RowHtml ) ) );
						$RowContent = wpb_js_remove_wpautop( apply_filters( 'vc_raw_html_module_content', $RowContent ) );
						
						preg_match_all('@src="([^"]+)"@', $RowContent, $group16);
						$getFiles[] = str_replace(['?_=1'], [''], $group16[1]);
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $RowContent, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				/* Raw HTML */
				$PageId = $getvalues->ID;

				// Get all category of the post
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}

				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@background-image: url([^"]+)@', $the_content, $group10);
				$bgimgUrlArr = str_replace(['(',')'], ['',''], $group10[1]);
				$bgimgUrl = array();
				foreach ($bgimgUrlArr as $BGvalue) {
					$bgimgUrl[] = substr($BGvalue, 0, strrpos($BGvalue, '?'));
				}

				// Merging Arrays Start
				$VCmediaContentPre = array();
				if(!empty($group9[1]) && !empty($bgimgUrl)){
					$VCmediaContentPre = array_merge($group9[1],$bgimgUrl);
				}elseif(!empty($group9[1]) && empty($bgimgUrl)){
					$VCmediaContentPre = $group9[1];
				}elseif(empty($group9[1]) && !empty($bgimgUrl)){
					$VCmediaContentPre = $bgimgUrl;
				}

				$VCmediaContent = array();
				if(!empty($VCmediaContentPre) && !empty($getFiles)){
					$VCmediaContent = array_merge($VCmediaContentPre,$getFiles);
				}elseif(!empty($VCmediaContentPre) && empty($getFiles)){
					$VCmediaContent = $VCmediaContentPre;
				}elseif(empty($VCmediaContentPre) && !empty($getFiles)){
					$VCmediaContent = $getFiles;
				}
				// Merging Arrays Ends
				
				$VCsingleimageids = implode(',',array_unique(explode(',', implode(',', $group1[1]))));
				$VCgalleryimageids = implode(',',array_unique(explode(',', implode(',', $group2[2]))));
				$VCcarouselimageids = implode(',',array_unique(explode(',', implode(',', $group3[1]))));
				$VChoverboximageids = implode(',',array_unique(explode(',', implode(',', $group4[1]))));

				if($VCsingleimageids != "" || $VCgalleryimageids != "" || $VCcarouselimageids != "" || $VChoverboximageids != "" || !empty($VCmediaContent)){
					$VCimageids = '';
					$comma = '';
					if($VCsingleimageids != ""){
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						$VChoverboximagesIdArr = array();
						$VChoverboximagesIdArr = explode(",", $VCsingleimageids);
						foreach ($VChoverboximagesIdArr as $VChoverboximagesIdArrValue) {
							$VCimages = getMultiSiteImageUrlbyId($VChoverboximagesIdArrValue,$prefixValue);
							// Get the created date of this media
							$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimages);
			                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
			                if(file_exists($multisite_url_dir)){
			                	$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
								$new_array['media_id'] = $idvalue;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimages;
								$new_array['medianame'] = basename($VCimages);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = $date_created;
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								array_push($uniqueArr,$new_array);
			                }
						}
					}
					if(!empty($VCmediaContent)){
						$incs = 0;
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($VCmediaContent as $VCmediaContentUrl) {
							// Get the created date of this media
			                $dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCmediaContentUrl);
			                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
			                if(file_exists($multisite_url_dir)){
								$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
								$new_array['media_id'] = '';
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCmediaContentUrl;
								$new_array['medianame'] = basename($VCmediaContentUrl);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = $date_created;
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								$incs++;
								array_push($uniqueArr,$new_array);
							}
						}
					}

					if($VCgalleryimageids != ""){
						if($VCimageids != ''){ $comma = ','; }						
						$theGalIds = explode(",", $VCgalleryimageids);
						$inc = 0;
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($theGalIds as $idvalue) {							
							$VCimage = getMultiSiteImageUrlbyId($idvalue,$prefixValue);
							// Get the created date of this media
							$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage);
			                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
			                if(file_exists($multisite_url_dir)){
								$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
								$new_array['media_id'] = $idvalue;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage;
								$new_array['medianame'] = basename($VCimage);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = $date_created;
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								$inc++;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					
					if($VCcarouselimageids != ""){
						if($VCimageids != ''){ $comma = ','; }
						$theGalIds2 = explode(",", $VCcarouselimageids);
						$inc2 = 0;
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						foreach ($theGalIds2 as $idvalue2) {
							$VCimage2 = getMultiSiteImageUrlbyId($idvalue2,$prefixValue);
							// Get the created date of this media
							$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage2);
			                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
			                if(file_exists($multisite_url_dir)){
								$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
								$new_array['media_id'] = $idvalue2;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage2;
								$new_array['medianame'] = basename($VCimage2);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = $date_created;
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								$inc2++;
								array_push($uniqueArr,$new_array);
							}
						}
					}
					
					if($VChoverboximageids != ""){
						$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
						if($VCimageids != ''){ $comma = ','; }
						$VChoverboximagesIdArr2 = array();
						$VChoverboximagesIdArr2 = explode(",", $VChoverboximageids);
						foreach ($VChoverboximagesIdArr2 as $VChoverboximagesIdArrValue2) {
							$VCimage3 = getMultiSiteImageUrlbyId($VChoverboximagesIdArrValue2,$prefixValue);
							// Get the created date of this media
							$dir_path =  str_replace(get_site_url().'/',get_home_path(),$VCimage3);
			                $multisite_url_dir = str_replace($multisite_url, $mainsite_url, $dir_path);
			                if(file_exists($multisite_url_dir)){
								$date_created = date("Y-m-d h:i:s",filemtime($multisite_url_dir));
								$new_array['media_id'] = $VChoverboximageids;
								$new_array['title'] = $the_title;
								$new_array['src'] = $VCimage3;
								$new_array['medianame'] = basename($VCimage3);
								$new_array['page_builder_name'] = "Visual Composer";
								$new_array['datetime'] = $date_created;
								$new_array['post_type'] = $post_type;
								$new_array['post_category'] = $post_cats;
								$new_array['variant_attribute'] = '';
								$new_array['variant_sku'] = '';
								$new_array['source_from'] = 'database';
								$new_array['linked'] = $linkedd;
								$new_array['website_prefix'] = $prefixValue;
								array_push($uniqueArr,$new_array);
							}
						}
					}
				}
			}
			if($oxygen_builder){
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');
				$PageId = $getvalues->ID;

				$the_content_meta = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='ct_builder_shortcodes' AND post_id = $PageId" );
				$the_content = $the_content_meta[0]->meta_value;

				preg_match_all('@"url":"([^"]+)"@', $the_content, $WordPressWidgetImages);
				preg_match_all('@"src":"([^"]+)"@', $the_content, $contentimgurl);
				preg_match_all('@"background-image":"([^"]+)"@', $the_content, $contentimgurl1);
				preg_match_all('@"image_ids":"([^"]+)"@', $the_content, $gelleryimgids);
				preg_match_all('@"code-php":"([^"]+)"@', $the_content, $phpcodes_encode);

				$OxyCodeArr = array();
				foreach ($phpcodes_encode[1] as $phpcode_encode) {
					$phpcodes_decode = base64_decode($phpcode_encode);
					preg_match_all('@(?:src[^>]+>)(.*?)@', $phpcodes_decode, $phpcodesImgUrl);
					$phpcode_encode_url = str_replace(['src=',"'",'"',"/>",">"," type=application/pdf"], ['','','','','',''], $phpcodesImgUrl[0]);
					$OxyCodeArr[] = explode(" ", $phpcode_encode_url[0]);
				}
				$OXImageurls = array();
				foreach ($OxyCodeArr as $OxyCodevalue) {
					$OXImageurls[] = $OxyCodevalue[0];
				}
				$widget_images = array();
				foreach ($WordPressWidgetImages[1] as $WordPressWidgetValue) {
					$widget_images[] = base64_decode($WordPressWidgetValue);
				}
				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				preg_match_all('/"image_ids"\h*:.*?\"(.*?)\"(?![^"\n]")/', $the_content, $oxy_gallery);
				// for gallery image
				$urls = '';
				if(!empty($oxy_gallery[1])){					
					foreach ($oxy_gallery[1] as $oxy_gallery_id) {
						$urls .= $oxy_gallery_id.',';
					}
				}
				$imageID = rtrim($urls,",");
				$imageIDs = explode(",", $imageID);
				foreach ($imageIDs as $EachId) {
					$getFiles[] = getMultiSiteImageUrlbyId($EachId,$prefixValue);
				}
				
				preg_match_all('/link\h*=.*?\"(.*?)\"(?![^"\n]")/', $the_content, $vc_video_url);
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);

				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}

				// Merging arrays
				$OXI1mageurlsArr = array();
				if(!empty($OXImageurls) && !empty($contentimgurl[1])){
					$OXI1mageurlsArr = array_merge($OXImageurls,$contentimgurl[1]);
				}elseif(!empty($OXImageurls) && empty($contentimgurl[1])){
					$OXI1mageurlsArr = $OXImageurls;
				}elseif(empty($OXImageurls) && !empty($contentimgurl[1])){
					$OXI1mageurlsArr = $contentimgurl[1];
				}
				$OXI2mageurlsArr = array();
				if(!empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
					$OXI2mageurlsArr = array_merge($OXI1mageurlsArr,$contentimgurl1[1]);
				}elseif(!empty($OXI1mageurlsArr) && empty($contentimgurl1[1])){
					$OXI2mageurlsArr = $OXI1mageurlsArr;
				}elseif(empty($OXI1mageurlsArr) && !empty($contentimgurl1[1])){
					$OXI2mageurlsArr = $contentimgurl1[1];
				}
				$OXImageurlsArr = array();
				if(!empty($OXI2mageurlsArr) && !empty($getFiles)){
					$OXImageurlsArr = array_merge($OXI2mageurlsArr,$getFiles);
				}elseif(!empty($OXI2mageurlsArr) && empty($getFiles)){
					$OXImageurlsArr = $OXI2mageurlsArr;
				}elseif(empty($OXI2mageurlsArr) && !empty($getFiles)){
					$OXImageurlsArr = $getFiles;
				}
				
				$OXImageurlsArrFinal = array();
				if(!empty($OXImageurlsArr) && !empty($widget_images)){
					$OXImageurlsArrFinal = array_merge($OXImageurlsArr,$widget_images);
				}elseif(!empty($OXImageurlsArr) && empty($widget_images)){
					$OXImageurlsArrFinal = $OXImageurlsArr;
				}elseif(empty($OXImageurlsArr) && !empty($widget_images)){
					$OXImageurlsArrFinal = $widget_images;
				}

				$OXimgidsArr = array();
				if(!empty($OXImageurlsArrFinal)){
					foreach ($OXImageurlsArrFinal as $OXImageurl) {
						array_push($OXimgidsArr, $OXImageurl);
					}
				}
				
				if(!empty($gelleryimgids[1])){
					foreach ($gelleryimgids[1] as $gelleryimgid) {
						$gelleryimgURlbyid = array(getMultiSiteImageUrlbyId($gelleryimgid,$prefixValue));
						array_push($OXimgidsArr,$gelleryimgURlbyid);
					}
				}
				$OXimgidsArrUni = array();
				$OXimgidsArrUni = array_unique($OXimgidsArr, SORT_REGULAR);
				// Get all categories
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
				foreach ($OXimgidsArrUni as $oxi_url) {
					// Get the created date of this media
			        $dir_path =  str_replace(get_site_url().'/',get_home_path(),$oxi_url);
			        $img_home_path = str_replace($multisite_url, $mainsite_url, $dir_path);
			        if(file_exists($img_home_path)){
			        	$unixtime = filemtime($img_home_path);
			        	$new_array['media_id'] = "";
						$new_array['title'] = $the_title;
						$new_array['src'] = $oxi_url;
						$new_array['medianame'] = basename($oxi_url);
						$new_array['page_builder_name'] = "Oxygen Builder";
						$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = $linkedd;
						$new_array['website_prefix'] = $prefixValue;
						array_push($uniqueArr,$new_array);
			        }
				}				
			}
			if($beaver_builder == 1){
				$getFiles = array();
				$BBmergeArray = array();
				$totalExtensionsAvailable = array('jpg','png','gif','svg','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;
				$PageId = $getvalues->ID;
				preg_match_all('@src="([^"]+)"@', $the_content, $group6);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group7);

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);
				
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfiles);
				$groupsfiles = $groupsfiles[1];
				if(!empty($groupsfiles)){
					foreach ($groupsfiles as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}
				// for vc video
				if(!empty($vc_video_url[1])){
					foreach ($vc_video_url[1] as $vc_video) {
						$getFiles[] = $vc_video;
					}
				}

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for mov
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}
				$beaver_src_array = $group6[1];
				$beaver_mp4_array = $group7[1];
				// Merging arrays
				$BBmergePreArray = array();
				if(!empty($getFiles) && !empty($beaver_src_array)){
					$BBmergePreArray = array_merge(array_unique($beaver_src_array),array_unique($getFiles));
				}elseif(!empty($getFiles) && empty($beaver_src_array)){
					$BBmergePreArray = array_unique($getFiles);
				}elseif(empty($getFiles) && !empty($beaver_src_array)){
					$BBmergePreArray = array_unique($beaver_src_array);
				}

				$BBmergeArray = array();
				if(!empty($BBmergePreArray) && !empty($beaver_mp4_array)){
					$BBmergeArray = array_merge(array_unique($beaver_mp4_array),array_unique($BBmergePreArray));
				}elseif(!empty($BBmergePreArray) && empty($beaver_mp4_array)){
					$BBmergeArray = array_unique($BBmergePreArray);
				}elseif(empty($BBmergePreArray) && !empty($beaver_mp4_array)){
					$BBmergeArray = array_unique($beaver_mp4_array);
				}
				// Get all categories
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
				// Beaver Builder Final Array
				foreach ($BBmergeArray as $beaver_urls) {
					$beaver_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$beaver_urls);
					if(file_exists($beaver_url_dir_path)){
						$new_array['media_id'] = "";
						$new_array['title'] = $the_title;
						$new_array['src'] = $beaver_urls;
						$new_array['medianame'] = basename($beaver_urls);
						$new_array['page_builder_name'] = "Beaver Builder";
						$beaver_unixtime = filemtime($beaver_url_dir_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$beaver_unixtime);
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = $linkedd;
						$new_array['website_prefix'] = $prefixValue;
						array_push($uniqueArr,$new_array);
					}
				}
			}
			if($brizy_builder){
				$PageId = $getvalues->ID;
				$the_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='brizy' AND post_id=$PageId" );
				$the_content = unserialize($the_content[0]->meta_value);
				$brizy_decode = base64_decode($the_content['brizy-post']['editor_data']);
				$brizy_content_str = str_replace(['\"'], [''], $brizy_decode);
				preg_match_all('@bgImageSrc":"([^"]+)"@', $brizy_content_str, $group14);
				preg_match_all('@imageSrc":"([^"]+)"@', $brizy_content_str, $group12);
				
				// HREF CASE 1: Get url when pattern is <a href=someurl.docx>
				preg_match_all('~href=(.*?)>~',$brizy_content_str,$case1_gethreffiles);
				foreach ($case1_gethreffiles[1] as $case1_href_value){
					$case1_href_value = strtok($case1_href_value, " ");
		            $case1_dir_path = str_replace(get_site_url().'/',get_home_path(),$case1_href_value);
					if(file_exists($case1_dir_path)){
						$hrefcase1[] = $case1_href_value;
					}
				}
				// HREF CASE 2: Get url when pattern is <a href='someurl.docx'>
				preg_match_all('~href=\'(.*?)\'~',$brizy_content_str,$case2_gethreffiles);
				foreach ($case2_gethreffiles[1] as $case2_href_value) {
					$case2_href_value = strtok($case2_href_value, " ");
		            $case2_dir_path = str_replace(get_site_url().'/',get_home_path(),$case2_href_value);
					if(file_exists($case2_dir_path)){
						$hrefcase2[] = $case2_href_value;
					}
				}				
				// CASE 3 (EMBED) : Get url when pattern is <embed src=someurl.docx type=application/pdf>
				$srcfiles = array();
				preg_match_all('~src=(.*?)>~', $brizy_content_str, $case3_gethreffiles);
				foreach ($case3_gethreffiles[1] as $case3_href_value){
					$case3_href_value = strtok($case3_href_value, " ");
		            $case3_dir_path = str_replace(get_site_url().'/',get_home_path(),$case3_href_value);
					if(file_exists($case3_dir_path)){
						$hrefcase3[] = $case3_href_value;
					}
				}				
				// CASE 4 (data-href) : Get url when pattern is <a data-href=
				preg_match_all('~data-href=(.*?)>~', $brizy_content_str, $case4_gethreffiles);				
				foreach ($case4_gethreffiles[1] as $case4_href_value){					
					$case4_href_value = strtok($case4_href_value, " ");
					$case4_data_href = utf8_decode(urldecode($case4_href_value));
					preg_match_all('/"external"\h*:.*?\"(.*?)\"(?![^"\n]")/', $case4_data_href, $case4_data_href_arr);
					$hrefcase4 = array();
					foreach ($case4_data_href_arr[1] as $case4_data_href_val) {
						$theLinkVal = str_replace("\",", "", $case4_data_href_val);						
						$case4_dir_path = str_replace(get_site_url().'/',get_home_path(),$theLinkVal);
						if(file_exists($case4_dir_path)){
							$hrefcase4[] = $theLinkVal;
						}
					}					
				}				
				// $the_content = htmlspecialchars($the_content);

				// CASE 5 (video) : Get url when pattern is "video":"http://some-url.mp4"
				preg_match_all('/"video"\h*:.*?\"(.*?)\",(?![^"\n]")/', $brizy_content_str, $videoUrl);
				foreach ($videoUrl[1] as $videoUrlValue) {
					$theVideoVal = str_replace("\",", "", $videoUrlValue);
					$theVideoVals = str_replace(get_site_url().'/',get_home_path(),$theVideoVal);
					if(file_exists($theVideoVals)){
						$hrefcase5[] = $theVideoVal;
					}
				}				

				// CASE 6 (image) : Get url when getting images
				// Merging Arrays Start
				$imagesNeme = array();
				if(!empty($group12[1]) && !empty($group14[1])){
					$imagesNeme = array_merge($group12[1],$group14[1]);
				}elseif(!empty($group12[1]) && empty($group14[1])){
					$imagesNeme = $group12[1];
				}elseif(empty($group12[1]) && !empty($group14[1])){
					$imagesNeme = $group14[1];
				}
				// Merging Arrays Ends
				$hrefcase6 = array();
				if(!empty($imagesNeme)){
					foreach ($imagesNeme as $imageNeme) {
						// Get Media id by Name only for brizy
						$imgIdsquery = $wpdb->get_results("SELECT post_id FROM ".$prefixValue."postmeta WHERE meta_key='brizy_attachment_uid' AND meta_value='$imageNeme'");
						if(!empty($imgIdsquery)){
							$imagesIds = getMultiSiteImageUrlbyId($imgIdsquery[0]->post_id,$prefixValue);
							$hrefcase6[] = $imagesIds;
						}
					}
				}
				
				// Mergining arrays start
				$brizyArr1 = array();
				if(!empty($hrefcase1) && !empty($hrefcase2)){
					$brizyArr1 = array_merge($hrefcase1,$hrefcase2);
				}elseif(!empty($hrefcase1) && empty($hrefcase2)){
					$brizyArr1 = $hrefcase1;
				}elseif(empty($hrefcase1) && !empty($hrefcase2)){
					$brizyArr1 = $hrefcase2;
				}

				$brizyArr2 = array();
				if(!empty($brizyArr1) && !empty($hrefcase3)){
					$brizyArr2 = array_merge($brizyArr1,$hrefcase3);
				}elseif(!empty($brizyArr1) && empty($hrefcase3)){
					$brizyArr2 = $brizyArr1;
				}elseif(empty($brizyArr1) && !empty($hrefcase3)){
					$brizyArr2 = $hrefcase3;
				}

				$brizyArr3 = array();
				if(!empty($brizyArr2) && !empty($hrefcase4)){
					$brizyArr3 = array_merge($brizyArr2,$hrefcase4);
				}elseif(!empty($brizyArr2) && empty($hrefcase4)){
					$brizyArr3 = $brizyArr2;
				}elseif(empty($brizyArr2) && !empty($hrefcase4)){
					$brizyArr3 = $hrefcase4;
				}

				$brizyArr4 = array();
				if(!empty($brizyArr3) && !empty($hrefcase5)){
					$brizyArr4 = array_merge($brizyArr3,$hrefcase5);
				}elseif(!empty($brizyArr3) && empty($hrefcase5)){
					$brizyArr4 = $brizyArr3;
				}elseif(empty($brizyArr3) && !empty($hrefcase5)){
					$brizyArr4 = $hrefcase5;
				}
				
				$href_array = array();
				if(!empty($brizyArr4) && !empty($hrefcase6)){
					$href_array = array_merge($brizyArr4,$hrefcase6);
				}elseif(!empty($brizyArr4) && empty($hrefcase6)){
					$href_array = $brizyArr4;
				}elseif(empty($brizyArr4) && !empty($hrefcase6)){
					$href_array = $hrefcase6;
				}
				// Mergining arrays ends
				if(!empty($href_array)){
					// Get all category of the post
					$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
					$post_categories = $wpdb->get_results($queryterms, OBJECT);
					$post_cat = array();
					$post_cats = "";
					if(!empty($post_categories)){
						foreach ($post_categories as $cat_value) {
							$post_cat[] = $cat_value->name;
						}
						$post_cats = implode(",", $post_cat);
					}
					$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
					foreach ($href_array as $brizy_value) {
						$hrefValues = stripslashes($brizy_value);
						// Getting filetime from dir URL
						$elementor_url_dir_path5 =  str_replace(get_site_url().'/',get_home_path(),$hrefValues);
						$img_home_path5 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path5);
						if(file_exists($img_home_path5)){
							$new_array['media_id'] = '';
							$new_array['title'] = $the_title;
							$new_array['src'] = $hrefValues;
							$new_array['medianame'] = basename($hrefValues);
							$new_array['page_builder_name'] = "Brizy";
							$unixtime5 = filemtime($img_home_path5);
							$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime5);
							$new_array['post_type'] = $post_type;
							$new_array['post_category'] = $post_cats;
							$new_array['variant_attribute'] = '';
							$new_array['variant_sku'] = '';
							$new_array['source_from'] = 'database';
							$new_array['linked'] = $linkedd;
							$new_array['website_prefix'] = $prefixValue;						
							array_push($uniqueArr,$new_array);
						}
					}
				}
			}
			if($siteorigin_ck_builder == "SiteOrigin"){
				$totalExtensionsAvailable = array('jpg','jpeg','png','gif','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$PageId = $getvalues->ID;
				$siteorigin_content = $wpdb->get_results("SELECT meta_value from ".$prefixValue."postmeta WHERE meta_key='panels_data' AND post_id = $PageId" );
				$the_content = unserialize($siteorigin_content[0]->meta_value);

				$siteorigenArray = array();
				
				foreach ($the_content['widgets'] as $valueid) {
					if(array_key_exists("attachment_id", $valueid)){
						$sitegetimgurlattachment_id = getMultiSiteImageUrlbyId($valueid['attachment_id'],$prefixValue);
						array_push($siteorigenArray, $sitegetimgurlattachment_id);
					}
					if(array_key_exists("ids", $valueid)){
						foreach ($valueid['ids'] as $galleryimg) {
							$sitegetimgurlids = getMultiSiteImageUrlbyId($galleryimg,$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlids);
						}
					}
					if(array_key_exists("features", $valueid)){
						foreach ($valueid['features'] as $features_img) {
							$sitegetimgurlfeatures = getMultiSiteImageUrlbyId($features_img['icon_image'],$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlfeatures);
						}
					}
					if(array_key_exists("image", $valueid)){
						$sitegetimgurlimage = getMultiSiteImageUrlbyId($valueid['image'],$prefixValue);
						array_push($siteorigenArray, $sitegetimgurlimage);
					}
					if(array_key_exists("frames", $valueid)){
						foreach ($valueid['frames'] as $frames_img) {
							$sitegetimgurlframes = getMultiSiteImageUrlbyId($frames_img['background_image'],$prefixValue);
							array_push($siteorigenArray, $sitegetimgurlframes);
						}
					}
					if(array_key_exists("content", $valueid)){
						$custom_html = $valueid['content'];
						preg_match_all('@src="([^"]+)"@', $custom_html, $contentimg);
						foreach ($contentimg[1] as $imgurl){
							array_push($siteorigenArray, $imgurl);
						}
					}
					if(array_key_exists("content", $valueid)){
						preg_match_all('@href="([^"]+)"@', $valueid['content'], $contentimgs);
						foreach ($contentimgs[1] as $imgurls){
							array_push($siteorigenArray, $imgurls);
						}
					}					
					if(array_key_exists("text", $valueid)){						
						$siteEditor = $valueid['text'];
						preg_match_all('@href="([^"]+)"@', $siteEditor, $groupsfile);
						preg_match_all('@src="([^"]+)"@', $siteEditor, $srcgroupsfile);
						$groupsfilehref = $groupsfile[1];
						$groupsfilesrc = $srcgroupsfile[1];
						if(!empty($groupsfilehref)){
							foreach ($groupsfilehref as $filesvalue) {								
								$fileExtension = strtolower(pathinfo($filesvalue, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvalue;
							    }
							}
						}
						if(!empty($groupsfilesrc)){
							foreach ($groupsfilesrc as $filesvaluesrc) {
								$fileExtension = strtolower(pathinfo($filesvaluesrc, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvaluesrc;
							    }
							}
						}
						preg_match_all('@src="([^"]+)"@', $siteEditor, $group9);
						preg_match_all('@mp3="([^"]+)"@', $siteEditor, $group11);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group12);
						preg_match_all('@pdf="([^"]+)"@', $siteEditor, $group13);
						preg_match_all('@docx="([^"]+)"@', $siteEditor, $group14);
						preg_match_all('@doc="([^"]+)"@', $siteEditor, $group15);
						preg_match_all('@ppt="([^"]+)"@', $siteEditor, $group16);
						preg_match_all('@xls="([^"]+)"@', $siteEditor, $group17);
						preg_match_all('@pps="([^"]+)"@', $siteEditor, $group18);
						preg_match_all('@ppsx="([^"]+)"@', $siteEditor, $group19);
						preg_match_all('@xlsx="([^"]+)"@', $siteEditor, $group20);
						preg_match_all('@odt="([^"]+)"@', $siteEditor, $group21);
						preg_match_all('@ogg="([^"]+)"@', $siteEditor, $group22);
						preg_match_all('@m4a="([^"]+)"@', $siteEditor, $group23);
						preg_match_all('@wav="([^"]+)"@', $siteEditor, $group24);
						preg_match_all('@mp4="([^"]+)"@', $siteEditor, $group25);
						preg_match_all('@mov="([^"]+)"@', $siteEditor, $group26);
						preg_match_all('@wmv="([^"]+)"@', $siteEditor, $group27);
						preg_match_all('@avi="([^"]+)"@', $siteEditor, $group28);
						preg_match_all('@3gp="([^"]+)"@', $siteEditor, $group29);
						preg_match_all('@pptx="([^"]+)"@', $siteEditor, $group30);
						// for vc video
						if(!empty($vc_video_url[1])){
							foreach ($vc_video_url[1] as $vc_video) {
								$getContentUrl[] = $vc_video;
							}
						}
						// for src
						if(!empty($group9[1])){
							foreach ($group9[1] as $group9s) {
								$getContentUrl[] = $group9s;
							}
						}
						// for mp3
						if(!empty($group11[1])){
							foreach ($group11[1] as $group11s) {
								$getContentUrl[] = $group11s;
							}
						}
						// for mp4
						if(!empty($group12[1])){
							foreach ($group12[1] as $group12s) {
								$getContentUrl[] = $group12s;
							}
						}
						// for pdf
						if(!empty($group13[1])){
							foreach ($group13[1] as $group13s) {
								$getContentUrl[] = $group13s;
							}
						}
						// for docx
						if(!empty($group14[1])){
							foreach ($group14[1] as $group14s) {
								$getContentUrl[] = $group14s;
							}
						}
						// for doc
						if(!empty($group15[1])){
							foreach ($group15[1] as $group15s) {
								$getContentUrl[] = $group15s;
							}
						}
						// for ppt
						if(!empty($group16[1])){
							foreach ($group16[1] as $group16s) {
								$getContentUrl[] = $group16s;
							}
						}
						// for xls
						if(!empty($group17[1])){
							foreach ($group17[1] as $group17s) {
								$getContentUrl[] = $group17s;
							}
						}
						// for pps
						if(!empty($group18[1])){
							foreach ($group18[1] as $group18s) {
								$getContentUrl[] = $group18s;
							}
						}
						// for ppsx
						if(!empty($group19[1])){
							foreach ($group19[1] as $group19s) {
								$getContentUrl[] = $group19s;
							}
						}
						// for xlsx
						if(!empty($group20[1])){
							foreach ($group20[1] as $group20s) {
								$getContentUrl[] = $group20s;
							}
						}
						// for odt
						if(!empty($group21[1])){
							foreach ($group21[1] as $group21s) {
								$getContentUrl[] = $group21s;
							}
						}
						// for ogg
						if(!empty($group22[1])){
							foreach ($group22[1] as $group22s) {
								$getContentUrl[] = $group22s;
							}
						}
						// for m4a
						if(!empty($group23[1])){
							foreach ($group23[1] as $group23s) {
								$getContentUrl[] = $group23s;
							}
						}
						// for wav
						if(!empty($group24[1])){
							foreach ($group24[1] as $group24s) {
								$getContentUrl[] = $group24s;
							}
						}
						// for mp4
						if(!empty($group25[1])){
							foreach ($group25[1] as $group25s) {
								$getContentUrl[] = $group25s;
							}
						}
						// for mov
						if(!empty($group26[1])){
							foreach ($group26[1] as $group26s) {
								$getContentUrl[] = $group26s;
							}
						}
						// for avi
						if(!empty($group27[1])){
							foreach ($group27[1] as $group27s) {
								$getContentUrl[] = $group27s;
							}
						}
						// for 3gp
						if(!empty($group28[1])){
							foreach ($group28[1] as $group28s) {
								$getContentUrl[] = $group28s;
							}
						}
						// for pptx
						if(!empty($group29[1])){
							foreach ($group29[1] as $group29s) {
								$getContentUrl[] = $group29s;
							}
						}
					}
					preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
					if(!empty($group10)){
						$bgimgUrl1 = str_replace(['(',')'], ['',''], $group10[1]);
						if(!empty($bgimgUrl1)){
							foreach ($bgimgUrl1 as $filesvaluebackground) {								
								$fileExtension = strtolower(pathinfo($filesvaluebackground, PATHINFO_EXTENSION));
								if(in_array($fileExtension, $totalExtensionsAvailable)){
							        $getContentUrl[] = $filesvaluebackground;
							    }
							}
						}
					}
					$VCmediaContent = array_unique($getContentUrl);
					$siteorigenArray = array_unique($siteorigenArray);					
					$siteOriginUrl = array();
					if(!empty($VCmediaContent) && !empty($siteorigenArray)){
						$siteOriginUrl = array_merge($VCmediaContent,$siteorigenArray);
					}elseif(!empty($VCmediaContent) && empty($siteorigenArray)){
						$siteOriginUrl = $VCmediaContent;
					}elseif(empty($VCmediaContent) && !empty($siteorigenArray)){						
						$siteOriginUrl = $siteorigenArray;
					}
					$final_array = array();
					foreach ($siteOriginUrl as $thevalues) {
						array_push($final_array, $thevalues);
					}
				}
				$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
				// Get all category of the post
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				foreach ($final_array as $siteOriginUrlValue) {
					$SiteOriginimageUrl = stripslashes($siteOriginUrlValue);
					$siteorigin_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$SiteOriginimageUrl);
					$img_home_path = str_replace($multisite_url, $mainsite_url, $siteorigin_url_dir_path);
					if(file_exists($img_home_path)){
						$new_array['media_id'] = '';
						$new_array['title'] = $the_title;
						$new_array['src'] = $SiteOriginimageUrl;
						$new_array['medianame'] = basename($SiteOriginimageUrl);
						$new_array['page_builder_name'] = "Site Origin";
						$unixtime = filemtime($img_home_path);
						$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);					
						$new_array['post_type'] = $post_type;
						$new_array['post_category'] = $post_cats;
						$new_array['variant_attribute'] = '';
						$new_array['variant_sku'] = '';
						$new_array['source_from'] = 'database';
						$new_array['linked'] = $linkedd;
						$new_array['website_prefix'] = $prefixValue;
						array_push($uniqueArr,$new_array);
					}
				}
			}
			if($getvalues->post_type == 'product'){
				$PageId = $getvalues->ID;
				// Get thumbnail ID (For featured image)
				$productids = $wpdb->get_results("SELECT * FROM ".$prefixValue."postmeta WHERE meta_key = '_thumbnail_id' AND  post_id ='".$PageId."'");
				$media_id = $productids[0]->meta_value;

				// Get all category of the post
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}

				$getidurlimge  = getMultiSiteImageUrlbyId($media_id,$prefixValue);
				$product_feature_image = stripslashes($getidurlimge);
				$product_url_dir_path =  str_replace(get_site_url().'/',get_home_path(),$product_feature_image);
				$prod_img_home_path = str_replace($multisite_url, $mainsite_url, $product_url_dir_path);
				if(file_exists($prod_img_home_path)){
					$new_array['media_id'] = $media_id;
					$new_array['title'] = $the_title;
					$new_array['src'] = $product_feature_image;
					$new_array['medianame'] = basename($product_feature_image);
					$new_array['page_builder_name'] = "Featured Image";
					$unixtime = filemtime($prod_img_home_path);
					$new_array['datetime'] = date("Y-m-d h:i:s",$unixtime);
					$new_array['post_type'] = $post_type;
					$new_array['post_category'] = $post_cats;
					$new_array['variant_attribute'] = '';
					$new_array['variant_sku'] = '';
					$new_array['source_from'] = 'database';
					$new_array['linked'] = 'Yes';
					$new_array['website_prefix'] = $prefixValue;
					array_push($uniqueArr,$new_array);
				}
			}
			if($elem_builder != 'builder' && $vc_builder != 'true' && $getvalues->post_type != 'product' && $getvalues->post_type != 'product_variation' && $beaver_builder != 1 && $siteorigin_ck_builder != "SiteOrigin" && !$brizy_builder && !$oxygen_builder){
				$totalExtensionsAvailable = array('gif','jpg','jpeg','png','pdf','docx','doc','ppt','xls','pps','ppsx','xlsx','odt','mp3','ogg','m4a','wav','mp4','mov','wmv','avi','3gp','pptx');

				$the_content = $getvalues->post_content;			
				$PageId = $getvalues->ID;
				preg_match_all('@href="([^"]+)"@', $the_content, $groupsfile);
				$groupsfile = $groupsfile[1];
				if(!empty($groupsfile)){
					foreach ($groupsfile as $filesvalue) {
						$fileExtension = pathinfo($filesvalue, PATHINFO_EXTENSION);
						if(in_array($fileExtension, $totalExtensionsAvailable)){
					        $getFiles[] = $filesvalue;
					    }
					}
				}

				preg_match_all('@src="([^"]+)"@', $the_content, $group9);
				preg_match_all('@mp3="([^"]+)"@', $the_content, $group11);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group12);
				preg_match_all('@pdf="([^"]+)"@', $the_content, $group13);
				preg_match_all('@docx="([^"]+)"@', $the_content, $group14);
				preg_match_all('@doc="([^"]+)"@', $the_content, $group15);
				preg_match_all('@ppt="([^"]+)"@', $the_content, $group16);
				preg_match_all('@xls="([^"]+)"@', $the_content, $group17);
				preg_match_all('@pps="([^"]+)"@', $the_content, $group18);
				preg_match_all('@ppsx="([^"]+)"@', $the_content, $group19);
				preg_match_all('@xlsx="([^"]+)"@', $the_content, $group20);
				preg_match_all('@odt="([^"]+)"@', $the_content, $group21);
				preg_match_all('@ogg="([^"]+)"@', $the_content, $group22);
				preg_match_all('@m4a="([^"]+)"@', $the_content, $group23);
				preg_match_all('@wav="([^"]+)"@', $the_content, $group24);
				preg_match_all('@mp4="([^"]+)"@', $the_content, $group25);
				preg_match_all('@mov="([^"]+)"@', $the_content, $group26);
				preg_match_all('@wmv="([^"]+)"@', $the_content, $group27);
				preg_match_all('@avi="([^"]+)"@', $the_content, $group28);
				preg_match_all('@3gp="([^"]+)"@', $the_content, $group29);
				preg_match_all('@pptx="([^"]+)"@', $the_content, $group30);

				// for src
				if(!empty($group9[1])){
					foreach ($group9[1] as $group9s) {
						$getFiles[] = $group9s;
					}
				}
				// for mp3
				if(!empty($group11[1])){
					foreach ($group11[1] as $group11s) {
						$getFiles[] = $group11s;
					}
				}
				// for mp4
				if(!empty($group12[1])){
					foreach ($group12[1] as $group12s) {
						$getFiles[] = $group12s;
					}
				}

				// for pdf
				if(!empty($group13[1])){
					foreach ($group13[1] as $group13s) {
						$getFiles[] = $group13s;
					}
				}

				// for docx
				if(!empty($group14[1])){
					foreach ($group14[1] as $group14s) {
						$getFiles[] = $group14s;
					}
				}

				// for doc
				if(!empty($group15[1])){
					foreach ($group15[1] as $group15s) {
						$getFiles[] = $group15s;
					}
				}

				// for ppt
				if(!empty($group16[1])){
					foreach ($group16[1] as $group16s) {
						$getFiles[] = $group16s;
					}
				}

				// for xls
				if(!empty($group17[1])){
					foreach ($group17[1] as $group17s) {
						$getFiles[] = $group17s;
					}
				}

				// for pps
				if(!empty($group18[1])){
					foreach ($group18[1] as $group18s) {
						$getFiles[] = $group18s;
					}
				}

				// for ppsx
				if(!empty($group19[1])){
					foreach ($group19[1] as $group19s) {
						$getFiles[] = $group19s;
					}
				}

				// for xlsx
				if(!empty($group20[1])){
					foreach ($group20[1] as $group20s) {
						$getFiles[] = $group20s;
					}
				}

				// for odt
				if(!empty($group21[1])){
					foreach ($group21[1] as $group21s) {
						$getFiles[] = $group21s;
					}
				}

				// for ogg
				if(!empty($group22[1])){
					foreach ($group22[1] as $group22s) {
						$getFiles[] = $group22s;
					}
				}

				// for m4a
				if(!empty($group23[1])){
					foreach ($group23[1] as $group23s) {
						$getFiles[] = $group23s;
					}
				}

				// for wav
				if(!empty($group24[1])){
					foreach ($group24[1] as $group24s) {
						$getFiles[] = $group24s;
					}
				}

				// for mp4
				if(!empty($group25[1])){
					foreach ($group25[1] as $group25s) {
						$getFiles[] = $group25s;
					}
				}

				// for wmv
				if(!empty($group26[1])){
					foreach ($group26[1] as $group26s) {
						$getFiles[] = $group26s;
					}
				}

				// for avi
				if(!empty($group27[1])){
					foreach ($group27[1] as $group27s) {
						$getFiles[] = $group27s;
					}
				}

				// for 3gp
				if(!empty($group28[1])){
					foreach ($group28[1] as $group28s) {
						$getFiles[] = $group28s;
					}
				}

				// for pptx
				if(!empty($group29[1])){
					foreach ($group29[1] as $group29s) {
						$getFiles[] = $group29s;
					}
				}
				
				preg_match_all('@background-image:url([^"]+)@', $the_content, $group10);
				$gutenbergImgUrls = str_replace(['(',')'], ['',''], $group10[1]);

				if(!empty($gutenbergImgUrls) && !empty($getFiles)){
					$totalContentMedia = array_unique(array_merge($getFiles,$gutenbergImgUrls));
				}else{
					if(!empty($getFiles)){
						$totalContentMedia = array_unique($getFiles);
					}elseif(!empty($gutenbergImgUrls)){
						$totalContentMedia = array_unique($gutenbergImgUrls);
					}else{
						$totalContentMedia = array();
					}
				}

				// Get all category of the post
				$queryterms = "SELECT * FROM ".$prefixValue."terms terms, ".$prefixValue."term_taxonomy term_taxonomy, ".$prefixValue."term_relationships term_relationships WHERE (terms.term_id = term_taxonomy.term_id AND term_taxonomy.term_taxonomy_id = term_relationships.term_taxonomy_id) AND term_relationships.object_id='".$PageId."' AND terms.slug !='variable' AND terms.slug !='simple' AND terms.slug !='grouped' AND terms.slug !='external'";
				$post_categories = $wpdb->get_results($queryterms, OBJECT);
				$post_cat = array();
				$post_cats = "";
				if(!empty($post_categories)){
					foreach ($post_categories as $cat_value) {
						$post_cat[] = $cat_value->name;
					}
					$post_cats = implode(",", $post_cat);
				}
				$linkedd = 'Yes <br><span class="p_detail">'.$site_name.'</span>';
				foreach ($totalContentMedia as $gutenbergImgUrl) {
					// Getting filetime from dir URL
					$elementor_url_dir_path6 =  str_replace(get_site_url().'/',get_home_path(),$gutenbergImgUrl);
					$img_home_path6 = str_replace($multisite_url, $mainsite_url, $elementor_url_dir_path6);
					if(file_exists($img_home_path6)){
						$unixtime6 = filemtime($img_home_path6);
						$mediadatetime = date("Y-m-d h:i:s",$unixtime6);
						$uniqueArr[] = array(
							'media_id' => $PageId,
							'medianame' => basename($gutenbergImgUrl),
							'src' => $gutenbergImgUrl,
							'media_type' => "",
							'title'=> $the_title,
							'post_type' => $post_type,
							'page_builder_name' => 'Simple/Gutenberg Content Media',
							'post_category' => $post_cats,
							'variant_attribute' => '',
							'variant_sku' => '',
							'datetime' => $mediadatetime,
							'linked' => $linkedd,
							'source_from' => 'database',
							'website_prefix' => $prefixValue
						);
					}					
				}
			}
		}
	}
	$uniqueArr = array_unique($uniqueArr, SORT_REGULAR);
	return $uniqueArr;
}
if(is_multisite()){
	$multisitePageBuilderMedia = getMultiSitePageBuilderContentMedia();
}else{
	$multisitePageBuilderMedia = array();
}
// Get multsite media from page builder ends Coded by Hemant

// Merge page builder content for single and multisite
if(!empty($PageBuilderMedia) && !empty($multisitePageBuilderMedia)){
	$getImgs = array_merge($PageBuilderMedia,$multisitePageBuilderMedia);
}elseif(empty($PageBuilderMedia) && !empty($multisitePageBuilderMedia)){
	$getImgs = $multisitePageBuilderMedia;
}elseif(!empty($PageBuilderMedia) && empty($multisitePageBuilderMedia)){
	$getImgs = $PageBuilderMedia;
}
//GET ATTACHMENT DETAILS FROM DATABASE
function getattachmentdata(){
	global $wpdb;
	$mediaexistdata=  array();
	$pagebuildername = '';
	$mimetype = "image/jpeg";
	if(is_multisite()){
		$prefixes = getAllSitePrefix();
		foreach ($prefixes as $mutliarray) {
			$prefixValue[] = $mutliarray['prefix'];
		}
	}
	$attachmentdata = $wpdb->get_results("select * from ".$wpdb->prefix."posts where post_type='attachment'");

	if(!empty($attachmentdata)){
		$attachmentarray = array();
		foreach ($attachmentdata as $key => $value) {
			$media_id = $value->ID;
			$media_url = $value->guid;
			$mediauploaddate = $value->post_date;
			$mime_type = $value->post_mime_type;
			$title = $value->post_title;
			$post_type = $value->post_type;

			$mediaexistdata[] = array(
				'media_id' => $media_id,
				'medianame' => basename($media_url),
				'src' => $media_url,
				'media_type' => $mime_type,
				'title'=> $title,
				'post_type' => $post_type,
				'page_builder_name' => "",
				'post_category' => "",
				'variant_attribute' => '',
				'variant_sku' => '',
				'datetime' => $mediauploaddate,
				'linked' =>'No',
				'source_from' => 'From Media Library',
				'website_prefix' => $wpdb->prefix
			);
		}
	}
	return $mediaexistdata;
}
//GET MULTISITE ATTACHMENT DETAILS FROM DATABASE
function getattAchmentDataMultiSite(){
	global $wpdb;
	$mediaexistdata=  array();
	$pagebuildername = '';
	$mimetype = "image/jpeg";
	$prefixes = getAllSitePrefix();
	foreach ($prefixes as $mutliarray) {
		$prefixValue = $mutliarray['prefix'];
		$multisiteId = $mutliarray['multisite_id'];
		// Get multisite title
		$current_blog_details = get_blog_details( array( 'blog_id' => $multisiteId ) );
		$site_name = $current_blog_details->blogname;
		$attachmentdata = $wpdb->get_results("SELECT * from ".$prefixValue."posts where post_type='attachment'");		
		if(!empty($attachmentdata)){
			$attachmentarray = array();
			foreach ($attachmentdata as $key => $value) {
				$media_id = $value->ID;
				$media_url = $value->guid;
				$mediauploaddate = $value->post_date;
				$mime_type = $value->post_mime_type;
				$title = $value->post_title;
				$post_type = $value->post_type;
				
				$mediaexistdata[] = array(
					'media_id' => $media_id,
					'medianame' => basename($media_url),
					'src' => $media_url,
					'media_type' => $mime_type,
					'title'=> $title,
					'post_type' => $post_type,
					'page_builder_name' => "",
					'post_category' => "",
					'variant_attribute' => '',
					'variant_sku' => '',
					'datetime' => $mediauploaddate,
					'linked' =>'No',
					'source_from' => 'From Media Library',
					'website_prefix' => $prefixValue
				);
			}			
		}
	}
	return $mediaexistdata;
}
// Vikram code for database media
$atdata1 = getattachmentdata();
if(is_multisite()){
	$atdata2 = getattAchmentDataMultiSite();
}else{
	$atdata2 = array();
}
if(!empty($atdata1) && !empty($atdata2)){
	$atdata = array_merge($atdata1,$atdata2);
}elseif(!empty($atdata1) && empty($atdata2)){
	$atdata = $atdata1;
}elseif(empty($atdata1) && !empty($atdata2)){
	$atdata = $atdata2;
}else{
	$atdata = array();
}

if ( class_exists( 'WooCommerce' ) ) {
  $newdata = getattachmentproducts();
} else {
  $newdata = array();
}
if ( class_exists( 'WooCommerce' ) ) {
	if(is_multisite()){
		$newdata2 = getMultisiteAttachmentProducts();
	}else{
		$newdata2 = array();
	}
}
if($newdata && $newdata2){
	$prodArrss = array_merge($newdata,$newdata2);
}else{
	if($newdata){
		$prodArrss = $newdata;
	}elseif($newdata2){
		$prodArrss = $newdata2;
	}else{
		$prodArrss = array();
	}
}

if($prodArrss && $getImgs){
	$result2 = array_merge($prodArrss,$getImgs);
}else{
	if($prodArrss){
		$result2 = $prodArrss;
	}elseif($getImgs){
		$result2 = $getImgs;
	}else{
		$result2 = array();
	}
}

if($atdata && $result2){
	$lresult = array_merge($result2,$atdata);
}else{
	if($atdata){
		$lresult = $atdata;
	}elseif($result2){
		$lresult = $result2;
	}else{
		$lresult = array();
	}
}

$get_result = array_unique($lresult, SORT_REGULAR);
// echo "<pre>"; print_r($get_result);
// die();
$new_array = array();
foreach ($get_result as $key => $value) {	
	$media_url = $value['src'];
	$table_name = $value['website_prefix']."posts";
	$only_original_image = "SELECT * FROM $table_name WHERE guid = '$media_url'";
	$if_yes = $wpdb->get_results($only_original_image, ARRAY_A);
	if(!empty($if_yes)){
		if(array_search($value['medianame'], array_column($new_array, 'medianame')) !== false) {
	        if($value['title'] != 'From Media Library' && $value['title'] != 'From Directory'){
	            $one_array = array('media_id' => $value['media_id'], 'medianame' => $value['medianame'], 'src' => $value['src'], 'media_type' => $value['media_type'], 'title' => $value['title'], 'post_type' => $value['post_type'], 'post_category' => $value['post_category'], 'variant_attribute' => $value['variant_attribute'], 'variant_sku' => $value['variant_sku'], 'page_builder_name' => $value['page_builder_name'], 'datetime' => $value['datetime'], 'linked' => $value['linked'], 'source_from' => $value['source_from'], 'website_prefix' => $value['website_prefix']);
	            array_push($new_array,$one_array);
	        }
	    } else {
	        $one_array = array('media_id' => $value['media_id'], 'medianame' => $value['medianame'], 'src' => $value['src'], 'media_type' => $value['media_type'], 'title' => $value['title'], 'post_type' => $value['post_type'], 'post_category' => $value['post_category'], 'variant_attribute' => $value['variant_attribute'], 'variant_sku' => $value['variant_sku'], 'page_builder_name' => $value['page_builder_name'], 'datetime' => $value['datetime'], 'linked' => $value['linked'], 'source_from' => $value['source_from'], 'website_prefix' => $value['website_prefix']);
	        array_push($new_array,$one_array);
	    }
	}
}
$table_name = $wpdb->prefix."mediascan_data"; // Table name
$del = $wpdb->query("TRUNCATE TABLE $table_name");

// Remove duplicacy for directory and database images
$new_arrays = array();
foreach ($new_array as $Newvalue) {
    if(array_search($Newvalue['src'], array_column($new_arrays, 'src')) !== false) {
    	if($Newvalue['source_from'] != 'directory'){
    		$new_arrays[] = $Newvalue;
    	}
    }else{
    	$new_arrays[] = $Newvalue;
    }
}
$successdata = array();
// echo "<pre>"; print_r($new_arrays); die();
foreach ($new_arrays as $finalResponse) {
	$src = $finalResponse['src'];
    $media_name = basename($finalResponse['src']);
    $media_type = $finalResponse['media_type'];
    $datetime = $finalResponse['datetime'];
    $title = $finalResponse['title'];
    $posttypeformedia = $finalResponse['post_type'];
    if(is_array($finalResponse['post_category'])){
    	$post_category = '';
    	foreach ($finalResponse['post_category'] as $cat_value) {
    		$post_category .= $cat_value.',';
    	}
    	$post_category = rtrim($post_category,',');
    }else{
    	$post_category = $finalResponse['post_category'];
    }
    $variant_attribute = $finalResponse['variant_attribute'];
    $variant_sku = $finalResponse['variant_sku'];
    $page_builder_name = $finalResponse['page_builder_name'];
    $linked = $finalResponse['linked'];
    $source_from = $finalResponse['source_from'];
    $website_prefix = $finalResponse['website_prefix'];
	
    $successdata[] = $wpdb->insert( $wpdb->prefix."mediascan_data", array("media_url" => $src,  "media_type" => $media_type, "medianame" => $media_name, "post_title" => $title,  "post_type"=> $posttypeformedia, "post_category"=> $post_category, "variant_attribute"=> $variant_attribute, "variant_sku"=> $variant_sku, "page_builder_name"=> $page_builder_name, "upload_date" => $datetime, "linked_status" => $linked,"source_from" => $source_from, "website_prefix" => $website_prefix));
}
if(in_array(0, $successdata)){
    $getFinalResults[0][] = "";
	$getFinalResults[0][] = "";
	$getFinalResults[0][] = __("Something went wrong. Please try again.","wp_media_cleaner");
	$getFinalResults[0][] = "";
	$getFinalResults[0][] = "";
    $media_size = 0;
}else{
	$querystr = "SELECT * FROM $table_name ORDER BY upload_date DESC";
	$pageposts = $wpdb->get_results($querystr, ARRAY_A);
	$count = 0;
	$bytes = '';
	$seprator = " ";
	$getFinalResults = array();
	foreach ($pageposts as $finalResult) {
		$website_prefix = $finalResult['website_prefix'];
		$medialinked = $finalResult['linked_status'];
		// Get data from optimzation backup table for getting Image opt value
		$image_url = $finalResult['media_url'];
		$final_backup_table_name = $wpdb->prefix."optimizer_backup";
		$url_exist = $wpdb->get_results("SELECT * FROM $final_backup_table_name WHERE Image_url ='$image_url' AND type = 'exclude'");
		$if_have = count($url_exist);
		if($if_have>0){
			$exclude_action = "Excluded";
		}else{
			$exclude_action = "Exclude";
		}
		// Get image size
		$numb = filesize($img_home_path);
		$is_numeric = is_numeric($numb);
		if($is_numeric){
			$bytes += $numb;
		}
		$linked_filename = "<a class='img_name' target='_blank' href='".$finalResult['media_url']."'>".$finalResult['medianame']."</a>";
		$select_row = '<input type="checkbox" name="check_list" data-excluded="'.$exclude_action.'" data-prefix="'.$website_prefix.'" value="'.$finalResult['media_url'].'" class="chkbox" id="id_chk'.$count.'">';
		if(!empty($finalResult['post_title'])){
			$seprator = " | ";
			if($finalResult['post_type'] == "product_variation"){
				$fposttitle = substr($finalResult['post_title'], 0, strrpos($finalResult['post_title'], '-'));
			}else{
				$fposttitle = $finalResult['post_title'];
			}
		}else{
			$fposttitle = "Not Linked";
		}
		// If have category
		$category_string = '';
		if($finalResult['post_category']){
			$products_cats = explode(",",$finalResult['post_category']);			
			$count_the_cat = count($products_cats);			
			if($count_the_cat>1){
				$more_cats = $count_the_cat-1;
				$more_cat = __(" and ","wp_media_cleaner");
				$more_cat .= "(".$more_cats.")";
				if($more_cats>1){
					$more_cat .= __(" more categories","wp_media_cleaner");
				}else{
					$more_cat .= __(" more category","wp_media_cleaner");
				}
				if($products_cats[0]){
					$category_string = $products_cats[0].$more_cat;
				}
			}else{
				if($products_cats[0]){
					$category_string = $products_cats[0];
				}
			}
		}
		if($finalResult['post_type'] == "product_variation"){
			$posttypename = 'Variation Product';
			if($finalResult['variant_attribute'] || $finalResult['variant_sku']){
				if(!empty($finalResult['variant_attribute'])){
					$side_details = $finalResult['variant_attribute'];
				}
				if($finalResult['variant_sku']){
					$side_details .= ' | SKU: '.$finalResult['variant_sku'];
				}
			}else{
				$side_details = $finalResult['page_builder_name'];
			}
		}else{
			$posttypename = ucwords($finalResult['post_type']);
			$side_details = $finalResult['page_builder_name'];
			if(!empty($posttypename)){
				$posttypename = ucwords($finalResult['post_type']);
				if($posttypename == "Attachment"){
					$posttypename = "thumbnail";
				}else{
					$posttypename = $finalResult['post_type'];
				}
			}
		}
		$if_attachment = $wpdb->get_results("SELECT * from ".$website_prefix."posts where post_type='attachment' AND guid = '$image_url'");
		$image_type = '';
		if(!empty($if_attachment)){
			if(empty($finalResult['page_builder_name'])){
				$image_type = __("Media Library | ","wp_media_cleaner");
			}else{
				$image_type = '';
			}
		}else{
			if(empty($finalResult['page_builder_name'])){
				$image_type = __("thumbnail | ","wp_media_cleaner");
			}else{
				$image_type = '';
			}
		}
		if($posttypename && $side_details && $category_string){
			if($medialinked == "No"){
				$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
			}else{
				$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$posttypename." | ".$side_details." | ".$category_string."</span>";
			}
		}else{
			if(empty($posttypename) && $side_details && $category_string){
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$side_details." | ".$category_string."</span>";
				}
			}elseif($posttypename && empty($side_details) && $category_string){
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$posttypename." | ".$category_string."</span>";
				}
			}elseif($posttypename && $side_details && empty($category_string)){
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$posttypename." | ".$side_details."</span>";
				}
			}elseif(empty($posttypename) && empty($side_details) && $category_string){
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$category_string."</span>";	
				}
			}elseif(empty($posttypename) && $side_details && empty($category_string)){
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$side_details."</span>";
				}
			}elseif($posttypename && empty($side_details) && empty($category_string)){				
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$posttypename."</span>";
				}
			}else{
				if($medialinked == "No"){
					$relatedpageinfo = "<span class='p_detail empty_dash'>-</span><br><span class='p_detail'>".str_replace(" | ","",$image_type)."</span>";	
				}else{
					$relatedpageinfo = $fposttitle."<br><span class='p_detail'>".$image_type.$posttypename." | ".$side_details." | ".$category_string."</span>";
				}
			}
		}
		$postDate = explode(' ', $finalResult['upload_date']);		
		$postType = $finalResult['post_type'];

		$extension = pathinfo(parse_url($finalResult['media_url'], PHP_URL_PATH), PATHINFO_EXTENSION);
		$select_row .= "<input type='hidden' id='att-filter' value='$extension,$postDate[0],$medialinked,$postType' />";
		$getFinalResults[$count][] = $select_row;
		$getFinalResults[$count][] = $linked_filename;
		$getFinalResults[$count][] = $relatedpageinfo;
		$getFinalResults[$count][] = str_replace(" "," | ",$finalResult['upload_date']);
		$getFinalResults[$count][] = $medialinked;
		$count++;
	}
}
$media_size = formatSizeUnits($bytes);
$getFinalResult['data'] = $getFinalResults;
$getFinalResult['media_size'] = $media_size;
$getFinalResult['total_files'] = count($getFinalResults);
$ars = json_encode($getFinalResult);
echo $ars;
exit;